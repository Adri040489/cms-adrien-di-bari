# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.6] - 2021-09-27
### Added
- Prevent customers who migrated from Easy to Profile from using the Easy module

### Fixed
- Integrate the country Italy as a europe destination

## [2.0.5] - 2020-02-05

### Fixed
- Fix error when get Delivengo user during module initialization

## [2.0.3] - 2020-10-05
### Added
- Add webhook test from MyDelivengo account

### Fixed
- Fix database

## [2.0.1] - 2020-07-20
### Fixed
- Fix variables escape in templates

## [2.0.0] - 2020-07-01
### Added
- First stable version
