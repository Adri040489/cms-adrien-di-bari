<?php
/**
 * 2020 Delivengo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0).
 * It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
 *
 * @author    Delivengo
 * @copyright 2020 Delivengo
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *
 */

use Delivengo\ObjectModel\DelivengoAddress;
use Delivengo\ObjectModel\DelivengoLabel;
use Delivengo\ObjectModel\DelivengoOrder;
use Delivengo\Settings;
use DelivengoSDK\DelivengoClient;
use DelivengoSDK\Entity\AdresseExpeditrice;
use DelivengoSDK\Entity\Utilisateur;
use DelivengoSDK\Entity\EnvoiPli;
use DelivengoSDK\Entity\Extra;
use DelivengoSDK\Entity\Paiement;
use DelivengoSDK\Entity\Module;
use DelivengoSDK\Entity\Createur;
use DelivengoSDK\Entity\AdresseNationale;
use DelivengoSDK\Entity\AdresseInternationale;
use DelivengoSDK\Entity\Article;
use DelivengoSDK\Entity\DocumentsDouaniers;
use DelivengoSDK\Request\CreateEnvoiWithDocumentsRequest;
use DelivengoSDK\Request\GetUtilisateurRequest;
use DelivengoSDK\Request\ListExpediteursRequest;
use DelivengoSDK\Request\ListMandatsRequest;
use DelivengoSDK\Request\CreateEnvoiRequest;
use DelivengoSDK\Request\CreateEnvoiSimulationRequest;
use DelivengoSDK\Request\GetEnvoiWithDocumentsRequest;

/**
 * Class AdminDelivengoAjaxController
 */
class AdminDelivengoAjaxController extends ModuleAdminController
{
    /** @var Delivengo $module */
    public $module;

    /**
     * @throws JsonMapper_Exception
     * @throws SmartyException
     */
    public function displayAjaxWhatsNew()
    {
        $settings = new Settings();
        if ($settings->displayWhatsNew) {
            $settings->updateSettings(['displayWhatsNew' => false]);
        }
        $html = $this->context->smarty->fetch($this->module->getLocalPath().'views/templates/admin/whatsnew/whatsnew.tpl');

        die(json_encode([
            'result_html' => $html,
            'errors' => [],
        ]));
    }

    /**
     * @throws JsonMapper_Exception
     * @throws SmartyException
     */
    public function displayAjaxLogin()
    {
        $settings = new Settings();
        $apiKey = $settings->apiKey;
        $this->context->smarty->assign([
            'delivengo_api_key' => $apiKey,
        ]);
        $html = $this->context->smarty->fetch($this->module->getLocalPath().'views/templates/admin/_partials/modal-account-api.tpl');

        die(json_encode([
            'result_html' => $html,
            'errors' => [],
        ]));
    }

    /**
     * @throws JsonMapper_Exception
     * @throws SmartyException
     */
    public function displayAjaxValidateApiKey()
    {
        $settings = new Settings();
        $apiKey = Tools::getValue('apiKey') ? Tools::getValue('apiKey') : $settings->apiKey;
        $request = new GetUtilisateurRequest();
        $client = new DelivengoClient($apiKey);
        try {
            $response = $client->send('get', $request, ['id' => 0]);
        } catch (\Exception $e) {
            //@formatter:off
            die(json_encode([
                'result_html' => false,
                'errors' => [[$this->module->l('Please verify your API key and try again.', 'AdminDelivengoAjaxController')]],
            ]));
            //@formatter:on
        }
        /** @var Utilisateur $user */
        $user = $response->getData();

        if ($user->getId()) {
            $marque = $user->getMarque()->getNom();
	    if ($marque == "MyDelivengo easy"){	
                $mandateRequest = new ListMandatsRequest();
                try {
                    $response = $client->send('list', $mandateRequest);
                } catch (\Exception $e) {
                    //@formatter:off
                    die(json_encode([
                        'result_html' => false,
                        'errors' => [[$this->module->l('Cannot retrieve your mandates at the moment. Please try again later', 'AdminDelivengoAjaxController')]],
                    ]));
                   //@formatter:on
                }
                /** @var \DelivengoSDK\Entity\Mandat[] $mandates */
                $mandates = $response->getData();

                $this->context->smarty->assign([
                    'delivengo_api_key' => $apiKey,
                    'delivengo_user' => json_encode($user),
                    'delivengo_mandates' => json_decode(json_encode($mandates), true),
                ]);
                $html = $this->context->smarty->fetch($this->module->getLocalPath().'views/templates/admin/_partials/modal-account-mandate.tpl');
                die(json_encode([
                    'result_html' => $html,
                    'errors' => [],
                ]));
            } else {
		die(json_encode([
	            'result_html' => false,
		    'errors' => [[$this->module->l('Informations submitted are not related to a Delivengo EASY account. This addon is only eligible to Delivengo Easy contract.', 'AdminDelivengoAjaxController')]],
		]));
	    }
        } else {
            //@formatter:off
            die(json_encode([
                'result_html' => false,
                'errors' => [[$this->module->l('Cannot identify the user account, please try again later.', 'AdminDelivengoAjaxController')]],
            ]));
            //@formatter:on
        }
    }

    /**
     * @throws SmartyException
     */
    public function displayAjaxResetModal()
    {
        $html = $this->context->smarty->fetch($this->module->getLocalPath().'views/templates/admin/_partials/modal-loading.tpl');

        die(json_encode([
            'result_html' => $html,
            'errors' => [],
        ]));
    }

    /**
     * @throws JsonMapper_Exception
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function ajaxProcessDownloadDelivengoAddresses()
    {
        $settings = new Settings();
        if (($settings->synchronizeAddress == 0)) {
            $apiKey = Tools::getValue('apiKey');
            $addressListRequest = new ListExpediteursRequest();
            $client = new DelivengoClient($apiKey);
            try {
                $response = $client->send('list', $addressListRequest);
            } catch (\Exception $e) {
                $this->module->logger->error($e->getMessage());
                die();
            }

            /** @var AdresseExpeditrice[] $addresses */
            $addresses = $response->getData();
            foreach ($addresses as $address) {
                $newAddress = new DelivengoAddress();
                $newAddress->address_id = $address->getId();
                $newAddress->user_id = $address->getIdUtilisateur();
                $newAddress->social_reason = $address->getRaisonSociale();
                $newAddress->full_name = $address->getNomComplet();
                $newAddress->address1 = $address->getVoie();
                $newAddress->address2 = $address->getComplementVoie();
                $newAddress->postbox = $address->getBoitePostale();
                $newAddress->zipcode = $address->getCodePostal();
                $newAddress->city = $address->getCommune();
                $newAddress->deleted = 0;
                try {
                    $totalExistingAddresses = count(DelivengoAddress::getAddressList());
                    $newAddress->add();
                    if (!$totalExistingAddresses) {
                        $settings->updateSettings(['defaultAddress' => $newAddress->id]);
                    }
                } catch (\Exception $e) {
                    $this->module->logger->error(sprintf('Cannot add address Delivengo', $address->getId()));
                }
            }
            $settings->updateSettings(['synchronizeAddress' => 1]);
            die();
        }
    }

    /**
     * @throws JsonMapper_Exception
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function ajaxProcessSetDefaultDelivengoAddress()
    {
        $settings = new Settings();
        $idDelivengoAddress = Tools::getValue('id_address');
        $delivengoAddress = new DelivengoAddress((int) $idDelivengoAddress);
        $errorMessage = false;
        if (Validate::isLoadedObject($delivengoAddress)) {
            try {
                $settings->updateSettings(['defaultAddress' => $idDelivengoAddress]);
            } catch (\Exception $e) {
                $errorMessage = $this->module->l('Could not update default address.', 'AdminDelivengoAjaxController');
            }
        }
        //@formatter:off
        die(json_encode([
            'error' => $errorMessage,
            'message' => $this->module->l('The default address has been successfully updated.', 'AdminDelivengoAjaxController'),
        ]));
        //@formatter:on
    }

    /**
     * @throws JsonMapper_Exception
     */
    public function ajaxProcessSaveExportationMode()
    {
        $settings = new Settings();
        $exportationModeValue = Tools::getValue('value');
        $settings->updateSettings(['exportationMode' => (int) $exportationModeValue]);
    }

    /**
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function ajaxProcessDeleteLabel()
    {
        $idDelivengoLabel = Tools::getValue('id_delivengo_label');
        $this->module->logger->info('Deleting label #'.(int) $idDelivengoLabel);
        $delivengoLabel = new  DelivengoLabel((int) $idDelivengoLabel);
        $delivengoOrder = new  DelivengoOrder((int) $delivengoLabel->id_delivengo_order);
        $idOrder = (int) $delivengoOrder->id_order;
        if (!Validate::isLoadedObject($delivengoLabel)) {
            $this->module->logger->error('Label not valid.');
            die(json_encode([
                'errors' => true,
                'message' => [[$this->module->l('Label not valid.', 'AdminDelivengoAjaxController')]],
            ]));
        }
        $delivengoLabel->deleted = 1;
        if ($delivengoLabel->save()) {
            $this->module->logger->info('Label deleted successfully.');
        } else {
            $this->module->logger->error('Could not delete label.');
        }
        $url = $this->context->link->getAdminLink('AdminOrders', true, [], ['vieworder' => 1, 'id_order' => $idOrder]);
        die(json_encode([
            'errors' => false,
            'message' => [$this->module->l('Label successfully deleted.', 'AdminDelivengoAjaxController')],
            'redirect_url' => $url.'#delivengo-module',
        ]));
    }

    /**
     * @throws JsonMapper_Exception
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function ajaxProcessCreateLabel()
    {
        $form = Tools::getValue('delivengo_label_form');
        if (!$form['shipping_address_id']) {
            die(json_encode([
                'errors' => true,
                'message' => [[$this->module->l('You must select a sender address.', 'AdminDelivengoAjaxController')]],
            ]));
        }
        $settings = new Settings();
        $idOrder = $form['idOrder'];
        $order = new Order((int) $idOrder);
        $delivengoDeliveryAddress = new Address((int) $order->id_address_delivery);
        if(Country::getIsoById((int) $delivengoDeliveryAddress->id_country) == "GB"){
            $apiKey = $settings->apiKey;
            $request = new GetUtilisateurRequest();
            $client = new DelivengoClient($apiKey);
            $response = $client->send('get', $request, ['id' => 0]);
            $user = $response->getData();
            if ($user->getId()) {
                $eori = $user->getExtra()->eori;
                $user = json_decode(json_encode($user), true);
                $settings->updateAccount($user);
                if($eori == ""){
                    die(json_encode([
                        'errors' => true,
                        'message' => [[$this->module->l('If you want to send goods to United Kingdom, you must provide a valid EORI UK Code, to set into mydelivengo.laposte.fr (My account > Customs documents)', 'AdminDelivengoAjaxController')]],
                    ]));
                }
            }
        }
        //set extra object
        $extra = new Extra();
        $paiement = new Paiement();
        $module = new Module();
        $createur = new Createur();
        $createur->setNom('Delivengo & PrestaShop partner');
        $paiement->setRum($settings->mandate->getRum());
        $module->setNom('PrestaShop Module');
        $module->setVersion($this->module->version);
        $module->setCreateur($createur);
        $extra->setPaiement($paiement);
        $extra->setModule($module);
        //set national address object
        $idSenderAddress = (int) $form['shipping_address_id'];
        $delivengoSenderAddress = new DelivengoAddress((int) $idSenderAddress);
        $expediteur = new AdresseNationale();
        $expediteur->setRaisonSociale($delivengoSenderAddress->social_reason);
        $expediteur->setNom($delivengoSenderAddress->full_name);
        $expediteur->setComplementVoie($delivengoSenderAddress->address2);
        $expediteur->setVoie($delivengoSenderAddress->address1);
        $expediteur->setBoitePostale($delivengoSenderAddress->postbox);
        $expediteur->setCodePostalCommune($delivengoSenderAddress->zipcode." ".$delivengoSenderAddress->city);
        //set international address object
        $destinataire = new AdresseInternationale();
        $destinataire->setRaisonSociale($delivengoDeliveryAddress->company);
        $destinataire->setNom($delivengoDeliveryAddress->firstname." ".$delivengoDeliveryAddress->lastname);
        $destinataire->setComplementVoie($delivengoDeliveryAddress->address2);
        $destinataire->setVoie($delivengoDeliveryAddress->address1);
        $destinataire->setBoitePostale('');
        $destinataire->setCodePostalCommune($delivengoDeliveryAddress->postcode." ".$delivengoDeliveryAddress->city);
        $destinataire->setCodePays(Country::getIsoById((int) $delivengoDeliveryAddress->id_country));
        $envoipli = new EnvoiPli();
        $products = $form['orderDetail'];
        $articles = [];
        $totalWeight = 0;
        foreach ($products as $product) {
            if (isset($product['selected'])) {
                if (isset($form['customs']) && !$product['hscode']) {
                    //@formatter:off
                    die(json_encode([
                        'errors' => true,
                        'message' => [[$this->module->l('HS codes are required for shipments out of EU.', 'AdminDelivengoAjaxController')]],
                    ]));
                    //@formatter:on
                }
                $article = new Article();
                $subtotalWeight = $product['weight'] * $product['quantity'];
                $article->setDescriptionDetaillee($product['description']);
                $article->setQuantite((int) $product['quantity']);
                $article->setPoids((int) $product['weight'] / 1000);
                $article->setValeur((float) $product['price']);
                $article->setPaysOrigine(
                    Country::getNameById($this->context->language->id, (int) $product['id_country'])
                );
                $article->setNumTarifaire($product['hscode']);
                $articles[] = $article;
                $totalWeight += (float) $subtotalWeight;
            }
        }
        if (count($articles) > Delivengo::DELIVENGO_MAX_ARTICLES) {
            //@formatter:off
            die(json_encode([
                'errors' => true,
                'message' => [[$this->module->l('You cannot select more than 4 articles.', 'AdminDelivengoAjaxController')]],
            ]));
            //@formatter:on
        }
        if (isset($form['customs'])) {
            $documentsDouaniers = new DocumentsDouaniers();
            $documentsDouaniers->setEnvoiNature([(int) $form['shipmentNature']]);
            $documentsDouaniers->setEnvoiCommercial(1);
            $documentsDouaniers->setObservation($form['observation']);
            $documentsDouaniers->setNumLicence($form['licenseNumber']);
            $documentsDouaniers->setNumCertificat($form['certificateNumber']);
            $documentsDouaniers->setNumFacture('IN'.$order->invoice_number);
            $documentsDouaniers->setFraisPort((float) $order->total_shipping_tax_excl);
            $documentsDouaniers->setArticles($articles);
            $envoipli->setDocumentsDouaniers($documentsDouaniers);
        }
        $envoipli->setExpediteur($expediteur);
        $envoipli->setDestinataire($destinataire);
        $envoipli->setOptions(8);
        $envoipli->setPoids($totalWeight);
        //set envoi object
        if (Tools::getValue('quotation')) {
            $createEnvoiRequest = new CreateEnvoiSimulationRequest();
        } else {
            $createEnvoiRequest = new CreateEnvoiWithDocumentsRequest();
        }
        $createEnvoiRequest->setExtra($extra);
        $createEnvoiRequest->setIdSupport(50);
        $createEnvoiRequest->setIdUtilisateur($settings->account->getId());
        $createEnvoiRequest->setDescriptif($order->reference);
        $createEnvoiRequest->setPlis([$envoipli]);
        if (Tools::getValue('quotation')) {
            $this->getLabelQuotation($createEnvoiRequest);
        } else {
            $this->generateLabel($createEnvoiRequest, $idOrder);
        }
    }

    /**
     * @param CreateEnvoiWithDocumentsRequest $envoiRequest
     * @param int                             $idOrder
     * @throws JsonMapper_Exception
     */
    protected function generateLabel($envoiRequest, $idOrder)
    {
        $settings = new Settings();
        $client = new DelivengoClient($settings->apiKey);
        try {
            /** @var \DelivengoSDK\Response\EnvoiWithDocumentsResponse $response */
            $response = $client->send('create', $envoiRequest);
            if ($response->error) {
                $errors = $response->error->getDescription();
                die(json_encode([
                    'errors' => true,
                    'message' => [$errors],
                ]));
            } else {
                $this->module->logger->info('Generate label for order'.$idOrder);
                $envoiResponse = $response->getData();
                $idEnvoi = $envoiResponse->getId();
                $pli = $envoiResponse->getPlis()[0];
                $label = new DelivengoLabel();
                $idDelivengoOrder = DelivengoOrder::getIdDelivengoOrderByOrderId((int) $idOrder);
                $label->id_delivengo_order = $idDelivengoOrder;
                $label->id_envoi = (int) $idEnvoi;
                $label->deleted = 0;
                $this->module->logger->info(json_encode($label));
                if ($label->save()) {
                    $order = new Order((int) $idOrder);
                    $orderCarrier = \Delivengo\Utils\Tools::getOrderCarrierByIdOrder($order->id);
                    if (Validate::isLoadedObject($orderCarrier) && !$orderCarrier->tracking_number) {
                        $orderCarrier->tracking_number = pSQL($pli->getNumero());
                        $orderCarrier->save();
                    }
                    if (Configuration::get('DELIVENGO_OS_SHIPPING_IN_PROGRESS')) {
                        $idNewState = Configuration::get('DELIVENGO_OS_SHIPPING_IN_PROGRESS');
                        $shippingInProgressOS = new OrderState((int) $idNewState);
                        if (Validate::isLoadedObject($shippingInProgressOS) &&
                            !$order->getHistory($this->context->language->id, $idNewState)
                        ) {
                            $history = new OrderHistory();
                            $history->id_order = (int) $idOrder;
                            $history->changeIdOrderState((int) $idNewState, (int) $idOrder);
                            try {
                                $history->add();
                            } catch (Exception $e) {
                                $this->module->logger->error(sprintf('Cannot change status of order #%d ', $idOrder));
                            }

                        } else {
                            $this->module->logger->error('Shipping in Progress order state is not valid');
                        }
                    }
                    $carrier = new Carrier($order->id_carrier);
                    $customer = new Customer((int) $order->id_customer);
                    $templateVars = [
                        '{order_name}' => $order->getUniqReference(),
                        '{firstname}' => $customer->firstname,
                        '{lastname}' => $customer->lastname,
                        '{followup}' => str_replace('@', $orderCarrier->tracking_number, $carrier->url),
                    ];
                    Mail::Send(
                        (int) $order->id_lang,
                        'delivengo_shipment',
                        $this->module->followupMailSubjects[Language::getIsoById($order->id_lang)],
                        $templateVars,
                        $customer->email,
                        $customer->firstname.' '.$customer->lastname,
                        null,
                        null,
                        null,
                        null,
                        dirname(__FILE__).'/../../mails/',
                        false,
                        (int) $order->id_shop
                    );
                    die(json_encode([
                        'errors' => false,
                        'message' => [$this->module->l('Label successfully created.', 'AdminDelivengoAjaxController')],
                        'redirect_url' => $this->context->link->getAdminLink('AdminOrders',
                                true,
                                [],
                                ['vieworder' => 1, 'id_order' => $order->id]).'#delivengo-module',
                    ]));
                } else {
                    die(json_encode([
                        'errors' => true,
                        'message' => ['An error occurred during label creation'],
                    ]));
                }
            }
        } catch (\Exception $e) {
            $this->module->logger->error($e->getMessage());
            die(json_encode([
                'errors' => true,
                'message' => [$e->getMessage()],
            ]));
        }
    }

    /**
     * @param CreateEnvoiSimulationRequest $envoi
     * @throws JsonMapper_Exception
     */
    protected function getLabelQuotation($envoi)
    {
        $this->module->logger->info(json_encode($envoi));
        $settings = new Settings();
        $request = new GetUtilisateurRequest();
        $client = new DelivengoClient($settings->apiKey);
        try {
            $response = $client->send('get', $request, ['id' => 0]);
        } catch (\Exception $e) {
            //@formatter:off
            die(json_encode([
	        'errors' => true,
                'message' => [[$this->module->l('Please verify your API key and try again.', 'AdminDelivengoAjaxController')]],
            ]));
            //@formatter:on
        }
        /** @var Utilisateur $user */
        $user = $response->getData();
        if ($user->getId()) {
            $marque = $user->getMarque()->getNom();
	    if ($marque == "MyDelivengo easy"){
                try {
                    /** @var \DelivengoSDK\Response\EnvoiResponse $response */
                    $response = $client->send('create', $envoi);
                    if ($response->error) {
                        $errors = \Delivengo\Utils\Tools::arrayFlatten($response->error->getDetails());
                        die(json_encode([
                            'errors' => true,
                            'message' => $errors,
                        ]));
                    } else {
                        $envoiSimulation = $response->getData();
                        $pliSimultation = $envoiSimulation->getPlis();
                        $this->context->smarty->assign([
                            'produit' => json_decode(json_encode($pliSimultation[0]->getProduit()), true),
                        ]);
                        $html = $this->context->smarty->fetch($this->module->getLocalPath().'views/templates/hook/admin/'.$this->module->adminTheme.'/_estimation_result.tpl');
                        die(json_encode([
                            'errors' => false,
                            'result_html' => $html,
                        ]));
                    }
                } catch (\Exception $e) {
                    $this->module->logger->error($e->getMessage());
                    die(json_encode([
                        'errors' => true,
                        'message' => [$e->getMessage()],
                    ]));
                }
            } else {
	        die(json_encode([
		    'errors' => true,
		    'message' => [[$this->module->l('Informations submitted are not related to a Delivengo EASY account. This addon is only eligible to Delivengo Easy contract.', 'AdminDelivengoAjaxController')]],
		]));		 
	    }
        } else {
            //@formatter:off
            die(json_encode([
		'errors' => true,
                'message' => [[$this->module->l('Cannot identify the user account, please try again later.', 'AdminDelivengoAjaxController')]],
           
            ]));
            //@formatter:on
        }
    }

    /**
     * @throws JsonMapper_Exception
     */
    public function processDownloadLabel()
    {
        $settings = new Settings();
        $client = new DelivengoClient($settings->apiKey);
        $envoiRequest = new GetEnvoiWithDocumentsRequest();
        $idEnvoi = Tools::getValue('id_envoi');

        try {
            if ($settings->labelType == 'PDF_A4') {
                $position = (int) Tools::getValue('label_position');
                /** @var \DelivengoSDK\Response\EnvoiWithDocumentsResponse $response */
                $response = $client->send(
                    'get',
                    $envoiRequest,
                    ['id' => (int) $idEnvoi, 'support' => 4, 'position' => $position]
                );
            } else {
                /** @var \DelivengoSDK\Response\EnvoiWithDocumentsResponse $response */
                $response = $client->send('get', $envoiRequest, ['id' => (int) $idEnvoi, 'support' => 32]);
            }
            $envoiWithDocuments = $response->getData();
            $documentSupports = base64_decode($envoiWithDocuments->getDocumentsSupports());
            $file = 'label-'.$idEnvoi.'.pdf';
            file_put_contents($file, $documentSupports);
            if (file_exists($file)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename($file).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: '.filesize($file));
                readfile($file);
                unlink($file);
                exit;
            }
        } catch (\Exception $e) {
            $this->module->logger->error($e->getMessage());
        }
    }
}
