<?php
/**
 * 2020 Delivengo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0).
 * It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
 *
 * @author    Delivengo
 * @copyright 2020 Delivengo
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *
 */

use Delivengo\ObjectModel\DelivengoAddress;
use DelivengoSDK\DelivengoClient;
use DelivengoSDK\Request\GetUtilisateurRequest;
use Delivengo\Settings;

/**
 * Class AdminDelivengoConfigurationController
 */
class AdminDelivengoConfigurationController extends ModuleAdminController
{
    const DELIVENGO_ADDONS_ID = 48967;

    /** @var Delivengo $module */
    public $module;

    /** @var array $labelTypes */
    public $labelTypes = [
        'PDF_A4' => 'PDF A4',
        'lABEL_10x15cm' => 'LABEL 10x15cm',
    ];

    /**
     * AdminDelivengoConfigurationController constructor.
     * @throws PrestaShopException
     * @throws SmartyException
     */
    public function __construct()
    {
        $this->bootstrap = true;
        parent::__construct();
        //@formatter:off
        $this->modals[] = [
            'modal_id' => 'delivengo-modal-whatsnew',
            'modal_class' => 'modal-lg',
            'modal_title' => $this->module->l('Latest version - What\'s new?', 'AdminDelivengoConfigurationController'),
            'modal_content' => $this->context->smarty->fetch($this->module->getLocalPath().'views/templates/admin/_partials/modal-loading.tpl'),
        ];
        $this->modals[] = [
            'modal_id' => 'delivengo-modal-signin',
            'modal_class' => 'modal-xl',
            'modal_title' => $this->module->l('Connect your Delivengo account', 'AdminDelivengoConfigurationController'),
            'modal_content' => $this->context->smarty->fetch($this->module->getLocalPath().'views/templates/admin/_partials/modal-loading.tpl'),
        ];
        $this->modals[] = [
            'modal_id' => 'delivengo-modal-mandates',
            'modal_class' => 'modal-xl',
            'modal_title' => $this->module->l('Update your mandate', 'AdminDelivengoConfigurationController'),
            'modal_content' => $this->context->smarty->fetch($this->module->getLocalPath().'views/templates/admin/_partials/modal-loading.tpl'),
        ];
        //@formatter:on
    }

    /**
     * @throws JsonMapper_Exception
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     * @throws SmartyException
     */
    public function initContent()
    {
        $prestUI = \Delivengo\Utils\Tools::getPrestUI($this->context, $this->module->getLocalPath());
        $activeTab = Tools::getValue('delivengo_tab') ? Tools::getValue('delivengo_tab') : 'intro';
        $settings = new Settings();
        //update Brexit EORI
        $apiKey = $settings->apiKey;
        if($apiKey){ 
            $request = new GetUtilisateurRequest();
            $client = new DelivengoClient($apiKey);
            try {
                $response = $client->send('get', $request, ['id' => 0]);
                $user = $response->getData();
                if ($user->getId()) {
                    $marque = $user->getMarque()->getNom();
		    if ($marque == "MyDelivengo easy") {
                        $user = json_decode(json_encode($user), true);
                        $settings->updateAccount($user);
                    } else{
			$this->errors[] = $this->module->l('Informations submitted are not related to a Delivengo EASY account. This addon is only eligible to Delivengo Easy contract.', 'AdminDelivengoConfigurationController');
	            }
                }
            } catch (\Exception $e) {
                $this->errors[] = $this->module->l('Please verify your API key.', 'AdminDelivengoConfigurationController');

            }
        }
        $this->assignBoConfigFields();
        $this->assignBoAddressBook();

        $this->context->smarty->assign([
            'module_version' => $this->module->version,
            'delivengo_addons_id' => self::DELIVENGO_ADDONS_ID,
            'active_tab' => $activeTab,
            'data' => json_decode(json_encode($settings), true),
            'webhook_url' => \Delivengo\Utils\Tools::getModuleLinkWithoutLang($this->context->link, 'webhook', []),
            'webhook_hash' => \Delivengo\Utils\Tools::getWebhookHash($this->module),
        ]);
        Media::addJsDef([
            'tokenDelivengoAjax' => Tools::getAdminTokenLite('AdminDelivengoAjax'),
            'displayWhatsNew' => $settings->displayWhatsNew,
        ]);
        $this->content = $this->createTemplate('configuration-layout.tpl')->fetch().$prestUI;
        parent::initContent();
    }

    /**
     * @throws Exception
     */
    public function postProcess()
    {
        if (Tools::isSubmit('newDelivengoAddress')) {
            $this->context->smarty->assign('newDelivengoAddress', 1);
        }
        if (Tools::isSubmit('deleteDelivengoAddress')) {
            $this->postProcessDeleteAddress();
        }

        return parent::postProcess();
    }

    /**
     * @throws JsonMapper_Exception
     * @throws PrestaShopException
     */
    public function processSaveAccount()
    {
        $apiKey = Tools::getValue('delivengo_api_key');
        $mandate = Tools::getValue('delivengo_mandate');
        $user = json_decode(Tools::getValue('delivengo_user'), true);
        $settings = new Settings();

        if ($mandate && $apiKey && !empty($user)) {
            $settings->updateAccount($user);
            $settings->updateMandate($mandate);
            $settings->updateSettings(['apiKey' => $apiKey]);
        }
        Tools::redirectAdmin(Context::getContext()->link->getAdminLink('AdminDelivengoConfiguration').'&delivengo_tab=account');
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function renderDelivengoAddressForm()
    {
        $idDelivengoAddress = (int) Tools::getValue('id_delivengo_address');
        $address = new DelivengoAddress((int) $idDelivengoAddress);
        $fieldsValue = [
            'id_delivengo_address' => $address->id_delivengo_address,
            'social_reason' => Tools::getValue('social_reason', $address->social_reason),
            'full_name' => Tools::getValue('full_name', $address->full_name),
            'address1' => Tools::getValue('address1', $address->address1),
            'address2' => Tools::getValue('address2', $address->address2),
            'postbox' => Tools::getValue('postbox', $address->postbox),
            'zipcode' => Tools::getValue('zipcode', $address->zipcode),
            'city' => Tools::getValue('city', $address->city),
            'action' => 'saveAddress',
        ];
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->module = $this->module;
        $helper->token = Tools::getAdminTokenLite('AdminDelivengoConfigurationController');
        $helper->default_form_language = $this->context->language->id;
        $helper->submit_action = 'submitDelivengoAddressForm';
        $helper->tpl_vars = [
            'fields_value' => $fieldsValue,
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        ];

        return $helper->generateForm([$this->getAddressForm()]);
    }

    /**
     * @return array
     * @throws PrestaShopException
     */
    public function getAddressForm()
    {
        $backUrl = $this->context->link->getAdminLink(
            'AdminDelivengoConfiguration',
            true,
            [],
            ['delivengo_tab' => 'address-book']
        );

        return [
            'form' => [
                'legend' => [
                    'title' => $this->module->l('Add a new address', 'AdminDelivengoConfigurationController'),
                    'icon' => 'icon-envelope',
                ],
                'input' => [
                    [
                        'type' => 'hidden',
                        'name' => 'id_delivengo_address',
                    ],
                    [
                        'type' => 'text',
                        'col' => 3,
                        'label' => $this->module->l('Social reason', 'AdminDelivengoConfigurationController'),
                        'name' => 'social_reason',
                        'required' => true,
                    ],
                    [
                        'type' => 'text',
                        'col' => 3,
                        'label' => $this->module->l('Full name', 'AdminDelivengoConfigurationController'),
                        'name' => 'full_name',
                        'maxlength' => 35,
                        'required' => false,
                    ],
                    [
                        'type' => 'text',
                        'col' => 3,
                        'label' => $this->module->l('Address', 'AdminDelivengoConfigurationController'),
                        'name' => 'address1',
                        'maxchar' => 35,
                        'maxlength' => 35,
                        'required' => true,
                    ],
                    [
                        'type' => 'text',
                        'col' => 3,
                        'label' => $this->module->l('Additional address', 'AdminDelivengoConfigurationController'),
                        'name' => 'address2',
                        'maxchar' => 35,
                        'maxlength' => 35,
                        'required' => false,
                    ],
                    [
                        'type' => 'text',
                        'col' => 3,
                        'label' => $this->module->l('Zip code', 'AdminDelivengoConfigurationController'),
                        'required' => true,
                        'name' => 'zipcode',
                    ],
                    [
                        'type' => 'text',
                        'col' => 3,
                        'label' => $this->module->l('City', 'AdminDelivengoConfigurationController'),
                        'required' => true,
                        'name' => 'city',
                    ],
                    [
                        'type' => 'text',
                        'col' => 3,
                        'label' => $this->module->l('Post box', 'AdminDelivengoConfigurationController'),
                        'name' => 'postbox',
                        'required' => false,
                    ],
                    [
                        'type' => 'hidden',
                        'name' => 'action',
                    ],
                ],
                'buttons' => [
                    [
                        'href' => $backUrl,
                        'id' => 'viewAddress',
                        'icon' => 'process-icon-back icon-back',
                        'title' => $this->module->l('Back', 'AdminDelivengoConfigurationController'),
                        'class' => 'pull-right',
                    ],
                ],
                'submit' => [
                    'title' => $this->module->l('Save', 'AdminDelivengoConfigurationController'),
                ],
            ],
        ];
    }

    /**
     * @return bool
     * @throws JsonMapper_Exception
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function processSaveAddress()
    {
        $idDelivengoAddress = (int) Tools::getValue('id_delivengo_address');
        $delivengoAddress = new DelivengoAddress($idDelivengoAddress);
        $settings = new Settings();
        $fields = DelivengoAddress::$definition['fields'];
        $country = new Country((int) Country::getByIso('FR'));
        $zipCode = Tools::getValue('zipcode');
        if (!Validate::isPostCode($zipCode) || !$country->checkZipCode($zipCode)) {
            $this->errors[] = $this->module->l('Please fill a valid zipcode.', 'AdminDelivengoConfigurationController');

            return false;
        }
        foreach (array_keys($fields) as $field) {
            $validate = $delivengoAddress->validateField($field, Tools::getValue($field), null, [], true);
            //make zipcode and full name required in form address
            if ($field == 'zipcode') {
                if (Tools::isEmpty(Tools::getValue($field))) {
                    //@formatter:off
                    $validate = sprintf($this->module->l('The %s field is required.', 'AdminDelivengoConfigurationController'), $field);
                    //@formatter:on
                }
            }
            if (true !== $validate) {
                $this->context->smarty->assign('newDelivengoAddress', 1);
                $this->errors[] = $validate;
            }
        }
        if ($this->errors) {
            return false;
        }

        $delivengoAddress->social_reason = Tools::getValue('social_reason');
        $delivengoAddress->full_name = Tools::getValue('full_name');
        $delivengoAddress->address1 = Tools::getValue('address1');
        $delivengoAddress->address2 = Tools::getValue('address2');
        $delivengoAddress->postbox = Tools::getValue('postbox');
        $delivengoAddress->zipcode = pSQL($zipCode);
        $delivengoAddress->city = Tools::getValue('city');
        if ($delivengoAddress->save()) {
            $addresses = $delivengoAddress::getAddressList();
            // set default address
            if (is_array($addresses) && 1 == count($addresses)) {
                $settings->updateSettings(['defaultAddress' => $delivengoAddress->id]);
            }
            Tools::redirectAdmin(Context::getContext()->link->getAdminLink('AdminDelivengoConfiguration').'&delivengo_tab=address-book&conf=1002');
        } else {
            $this->context->smarty->assign('addDelivengoNewAddress', 1);
            //@formatter:off
            $this->errors[] = ($this->module->l('An error occurred during address creation', 'AdminDelivengoConfigurationController'));
            //@formatter:on
        }

        return true;
    }

    /**
     * @throws JsonMapper_Exception
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function postProcessDeleteAddress()
    {
        $settings = new Settings();
        $idDelivengoAddress = (int) Tools::getValue('id_delivengo_address');
        $delivengoAddress = new DelivengoAddress($idDelivengoAddress);
        if (Validate::isLoadedObject($delivengoAddress)) {
            if ($delivengoAddress->delete()) {
                if ((int) $settings->defaultAddress == $idDelivengoAddress) {
                    $newDefaultAddress = $delivengoAddress::getFirstAddress();
                    if (false === $newDefaultAddress) {
                        $settings->updateSettings(['defaultAddress' => '']);
                    } else {
                        $settings->updateSettings(['defaultAddress' => $newDefaultAddress->id_delivengo_address]);
                    }
                }
                Tools::redirectAdmin(Context::getContext()->link->getAdminLink('AdminDelivengoConfiguration').'&delivengo_tab=address-book&conf=1000');
            } else {
                //@formatter:off
                $this->errors[] = $this->module->l('Address cannot be deleted.', 'AdminDelivengoConfigurationController');
                //@formatter:on
            }
        } else {
            //@formatter:off
            $this->errors[] = $this->module->l('The address you try to delete is not valid.', 'AdminDelivengoConfigurationController');
            //@formatter:on
        }
    }

    /**
     * @throws JsonMapper_Exception
     * @throws PrestaShopException
     */
    public function processSaveConfigForm()
    {
        $settings = new Settings();
        $formData = Tools::getValue('delivengoSettings');
        $formData['exportationMode'] = Tools::getValue('exportationMode');
        $settings->updateSettings($formData);
        Tools::redirectAdmin(Context::getContext()->link->getAdminLink('AdminDelivengoConfiguration').'&delivengo_tab=bo-settings&conf=1001');
    }

    /**
     *
     */
    public function assignBoConfigFields()
    {
        $countries = Country::getCountries((int) Context::getContext()->cookie->id_lang);
        $this->context->smarty->assign([
            'label_types' => $this->labelTypes,
            'countries' => $countries,
        ]);
    }

    /**
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function assignBoAddressBook()
    {
        $delivengoAddress = new DelivengoAddress();
        $addresses = $delivengoAddress->getAddressList();
        $addressForm = $this->renderDelivengoAddressForm();
        $this->context->smarty->assign([
            'newDelivengoAddressForm' => $addressForm,
            'delivengo_addresses' => $addresses,
        ]);
    }

    /**
     * @throws SmartyException
     */
    public function display()
    {
        $this->_conf += [
            1000 => $this->module->l('Address deleted successfully.', 'AdminDelivengoConfigurationController'),
            1001 => $this->module->l('Config params saved successfully.', 'AdminDelivengoConfigurationController'),
            1002 => $this->module->l('Address saved successfully.', 'AdminDelivengoConfigurationController'),
        ];
        parent::display();
    }
}
