<?php
/**
 * 2020 Delivengo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0).
 * It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
 *
 * @author    Delivengo
 * @copyright 2020 Delivengo
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *
 */

use Delivengo\Settings;
use DelivengoSDK\DelivengoClient;
use DelivengoSDK\Request\GetEnvoiRequest;

/**
 * Class DelivengoWebhookModuleFrontController
 */
class DelivengoTrackingModuleFrontController extends ModuleFrontController
{
    /** @var Delivengo $module */
    public $module;

    /**
     * @return bool
     */
    public function checkAccess()
    {
        return parent::checkAccess();
    }

    /**
     * @throws PrestaShopException
     */
    public function initContent()
    {
        parent::initContent();
        $this->setTemplate('module:delivengo/views/templates/front/tracking.tpl');
    }

    /**
     * @throws JsonMapper_Exception
     */
    public function postProcess()
    {
        if (Tools::getValue('tracking_number')) {
            $trackingNumber = Tools::getValue('tracking_number');
            $delivengoEnvoi = $this->getEnvoiByTrackingNumber($trackingNumber);
            if ($delivengoEnvoi) {
                $idEnvoi = $delivengoEnvoi[0]['id_envoi'];
                $settings = new Settings();
                $apiKey = $settings->apiKey;
                $client = new DelivengoClient($apiKey);
                $envoiRequest = new GetEnvoiRequest();
                try {
                    /** @var \DelivengoSDK\Response\EnvoiResponse $response */
                    $response = $client->send('get', $envoiRequest, ['id' => (int) $idEnvoi]);
                } catch (\Exception $e) {
                    $this->module->logger->error($e->getMessage());
                    $this->context->smarty->assign([
                        'errors' => true,
                    ]);

                    return true;
                }
                if (!$response->error) {
                    $pli = $response->getData()->getPlis();
                    if ($pli[0]->getTracking()) {
                        $tracking = $pli[0]->getTracking()->getUrl();
                        Tools::redirect($tracking);
                    } else {
                        //@formatter:off
                        $this->context->smarty->assign([
                            'errors' => false,
                        ]);
                        //@formatter:on
                    }
                } else {
                    //@formatter:off
                    $this->context->smarty->assign([
                        'errors' => true,
                    ]);
                    //@formatter:on
                }
            } else {
                $this->context->smarty->assign([
                    'errors' => true,
                ]);
            }
        } else {
            $this->context->smarty->assign([
                'errors' => true,
            ]);
        }

        return true;
    }

    /**
     * @param string $trackingNumber
     * @return array|false|mysqli_result|PDOStatement|resource|null
     * @throws PrestaShopDatabaseException
     */
    public function getEnvoiByTrackingNumber($trackingNumber)
    {
        $dbQuery = new DbQuery();
        $dbQuery->select('dl.id_envoi')
                ->from('delivengo_label', 'dl')
                ->where('dl.tracking_number = "'.pSQL($trackingNumber).'"')
                ->where('dl.deleted != 1');
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbQuery);

        return $result;
    }
}
