<?php
/**
 * 2020 Delivengo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0).
 * It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
 *
 * @author    Delivengo
 * @copyright 2020 Delivengo
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *
 */

/**
 * Class DelivengoWebhookModuleFrontController
 */
class DelivengoWebhookModuleFrontController extends ModuleFrontController
{
    const WEBHOOK_TRACKING_NUMBER_TEST = 'XX0000000000FR';

    /** @var Delivengo $module */
    public $module;

    /**
     * @return bool
     */
    public function checkAccess()
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            header("HTTP/1.1 401 Unauthorized");
            exit;
        }
        // Check du hash
        $hash = \Delivengo\Utils\Tools::getWebhookHash($this->module);
        $paramHash = false;
        foreach ($_SERVER as $name => $value) {
            if (Tools::substr($name, 0, 5) == 'HTTP_') {
                $name = str_replace(' ',
                    '-',
                    ucwords(Tools::strtolower(str_replace('_', ' ', Tools::substr($name, 5)))));
                if ($name == 'Prestashop-Hash') {
                    $paramHash = true;
                    if ($hash !== $value) {
                        header("HTTP/1.1 401 Unauthorized");
                        exit;
                    }
                }
            }
        }
        if ($paramHash === false) {
            header("HTTP/1.1 401 Unauthorized");
            exit;
        }
        if (parent::checkAccess() == false) {
            header("HTTP/1.1 401 Unauthorized");
            exit;
        }

        return true;
    }

    /**
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function display()
    {
        $json = Tools::file_get_contents('php://input');
        $data = json_decode($json, true);
        $isDelivered = false;
        foreach ($data['numbers'] as $row) {
            $trackingNumber = $row['number'];
            foreach ($row['status'] as $status) {
                if ($status['status_code'] == 61) {
                    $isDelivered = true;
                }
            }
            //update state
            $idOrder = $this->getOrderIdByTrackingNumber($trackingNumber);
            if ($idOrder) {
                $order = new Order((int) $idOrder);
                $idStatusShipped = Configuration::get('PS_OS_SHIPPING');
                $idStatusDelivered = Configuration::get('PS_OS_DELIVERED');
                $history = new OrderHistory();
                $history->id_order = (int) $idOrder;
                if ($isDelivered) {
                    if (!$order->getHistory($this->context->language->id, $idStatusDelivered)) {
                        $history->changeIdOrderState((int) $idStatusDelivered, (int) $idOrder);
                        $history->add();
                        header('HTTP/1.1 200 OK');
                        exit;
                    }
                } else {
                    if (!$order->getHistory($this->context->language->id, $idStatusShipped)) {
                        $history->changeIdOrderState((int) $idStatusShipped, (int) $idOrder);
                        $history->add();
                        header('HTTP/1.1 200 OK');
                        exit;
                    }
                }
            } else {
                if ($trackingNumber == self::WEBHOOK_TRACKING_NUMBER_TEST) {
                    header('HTTP/1.1 200 OK');
                    exit;
                }
                header("HTTP/1.1 400 Unauthorized");
                exit;
            }
        }
    }

    /**
     * @param string $trackingNumber
     * @return int
     */
    public function getOrderIdByTrackingNumber($trackingNumber)
    {
        $dbQuery = new DbQuery();
        $dbQuery->select('do.id_order')
                ->from('delivengo_order', 'do')
                ->leftJoin('delivengo_label', 'dl', 'dl.id_delivengo_order = do.id_delivengo_order')
                ->where('dl.tracking_number = "'.pSQL($trackingNumber).'"')
                ->where('dl.deleted != 1');

        return (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($dbQuery);
    }
}
