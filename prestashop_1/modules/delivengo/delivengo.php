<?php
/**
 * 2020 Delivengo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0).
 * It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
 *
 * @author    Delivengo
 * @copyright 2020 Delivengo
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *
 */

use Delivengo\ObjectModel\DelivengoCustomCategory;
use Delivengo\ObjectModel\DelivengoCustomProduct;
use Delivengo\ObjectModel\DelivengoLabel;
use Delivengo\ObjectModel\DelivengoOrder;
use Delivengo\Settings;
use Delivengo\Utils\Installer;
use DelivengoSDK\DelivengoClient;
use DelivengoSDK\Request\GetEnvoiRequest;
use Doctrine\ORM\QueryBuilder;
use PrestaShop\PrestaShop\Core\Grid\Column\ColumnCollection;
use PrestaShop\PrestaShop\Core\Grid\Column\Type\BooleanColumn;
use PrestaShop\PrestaShop\Core\Grid\Definition\GridDefinitionInterface;
use PrestaShop\PrestaShop\Core\Grid\Filter\Filter;
use PrestaShop\PrestaShop\Core\Grid\Filter\FilterCollectionInterface;
use PrestaShop\PrestaShop\Core\Grid\Search\SearchCriteriaInterface;
use PrestaShopBundle\Form\Admin\Type\YesAndNoChoiceType;

/**
 * Class Delivengo
 */
class Delivengo extends CarrierModule
{
    const LOGS_DIR = 'logs';

    const DELIVENGO_MAX_ARTICLES = 4;

    /** @var array $hooks */
    private $hooks = [
        'actionAdminControllerSetMedia',
        'newOrder',
        'displayAdminOrder',
        'displayAdminOrderMainBottom',
        'actionAdminOrdersListingFieldsModifier',
        'displayAdminProductsExtra',
        'actionProductUpdate',
        'actionAdminCategoriesControllerSaveAfter',
        'actionCategoryFormBuilderModifier',
        'actionAfterCreateCategoryFormHandler',
        'actionAfterUpdateCategoryFormHandler',
        'actionAdminCategoriesFormModifier',
        'actionOrderGridQueryBuilderModifier',
        'actionOrderGridDefinitionModifier',
    ];

    /** @var \Monolog\Logger $logger */
    public $logger;

    /** @var array $carriersKeys */
    public $carriersKeys = ['DELIVENGO_CARRIER_ID'];

    /**
     * @var array $controllers
     */
    public $controllers = ['webhook', 'tracking'];

    /**
     * @var int $order_limit_rows
     */
    public $order_limit_rows = 4;

    /** @var string $adminTheme */
    public $adminTheme;

    /** @var array $followupMailSubjects */
    public $followupMailSubjects = [
        'fr' => 'En préparation d’expédition',
        'en' => 'In preparation for shipment',
    ];

    /**
     * Delivengo constructor.
     * @throws JsonMapper_Exception
     */
    public function __construct()
    {
        require_once(dirname(__FILE__).'/vendor/autoload.php');

        $this->name = 'delivengo';
        $this->tab = 'shipping_logistics';
        $this->version = '2.0.6';
        $this->module_key = '7761dcc87afa4e0c217e4733d6b43edc';
        $this->author = 'Delivengo';
        $this->need_instance = 0;
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Delivengo');
        $this->description = $this->l('Delivengo module for PrestaShop');
        $this->ps_versions_compliancy = ['min' => '1.7.1', 'max' => _PS_VERSION_];
        $this->adminTheme = version_compare(_PS_VERSION_, '1.7.7', '>=') ? 'new-theme' : 'legacy';
        \Delivengo\Utils\Tools::initLogger($this);
    }

    /**
     * @return bool
     */
    public function install()
    {
        try {
            //@formatter:off
            \Delivengo\Utils\Tools::initLogger($this, 'install', true);
            if (!parent::install()) {
                return false;
            }
            $this->logger->info('Start Delivengo install');
            $installer = new Installer($this);
            $installer->checkTechnicalRequirements();
            $this->logger->info('Adding menus');
            $installer->addMenu(['en' => 'Logs Delivengo', 'fr' => 'Delivengo Logs'], 'AdminDelivengoLogs', '', false);
            $installer->addMenu(['en' => 'Ajax Delivengo', 'fr' => 'Delivengo Ajax'], 'AdminDelivengoAjax', '', false);
            $installer->addMenu(['en' => 'Configuration Delivengo', 'fr' => 'Delivengo Configuration'], 'AdminDelivengoConfiguration', '', false);
            $installer->registerHooks($this->hooks);
            $url = str_replace(
                'tracking_number_value',
                '@',
                \Delivengo\Utils\Tools::getModuleLinkWithoutLang($this->context->link, 'tracking', ['tracking_number' => 'tracking_number_value'])
            );
            $installer->createCarriers([
                [
                    'configuration' => 'DELIVENGO_CARRIER_ID',
                    'name' => 'Delivengo easy',
                    'delays' => ['fr' => 'Livraison à domicile', 'en' => 'Home delivery'],
                    'url' => $url,
                    'active' => true,
                    'shipping_handling' => false,
                    'range_behavior' => 0,
                    'is_module' => true,
                    'is_free' => false,
                    'shipping_external' => true,
                    'need_range' => true,
                    'external_module_name' => $this->name,
                    'shipping_method' => Carrier::SHIPPING_METHOD_WEIGHT,
                    'max_weight' => \Delivengo\Utils\Tools::getWeightFromKg(2),
                    'max_width' => 90.00,
                    'max_height' => 90.00,
                    'max_depth' => 90.00,
                ],
            ]);
            $installer->createTables();
            $installer->createOrderStatuses([[
                'configuration' => 'DELIVENGO_OS_SHIPPING_IN_PROGRESS',
                'color' => '#ffef2a',
                'names' => ['en' => 'In preparation for shipment', 'fr' => 'En préparation d’expédition']
            ]]);
            $installer->initConfiguration();
            $this->logger->info('Module installed successfully!');
            //@formatter:on
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            //@formatter:off
            $this->_errors[] = $this->l('Delivengo module could not be installed. Please check the logs inside the delivengo "logs" directory.');
            //@formatter:on

            return false;
        }

        return true;
    }

    /**
     * @throws PrestaShopException
     */
    public function getContent()
    {
        Tools::redirectAdmin(Context::getContext()->link->getAdminLink('AdminDelivengoConfiguration'));
    }

    /**
     * @param Cart  $cart
     * @param float $shippingCost
     * @param array $products
     * @return float
     */
    public function getPackageShippingCost($cart, $shippingCost, $products)
    {
        return $shippingCost;
    }

    /**
     * @param array $params
     * @param float $shippingCost
     * @return float
     */
    public function getOrderShippingCost($params, $shippingCost)
    {
        return $this->getPackageShippingCost($params['cart'], $shippingCost, null);
    }

    /**
     * @param array $params
     * @return bool
     */
    public function getOrderShippingCostExternal($params)
    {
        return false;
    }

    /**
     * @throws PrestaShopException
     */
    public function hookActionAdminControllerSetMedia()
    {
        if (in_array(Tools::getValue('controller'), ['AdminDelivengoConfiguration'])) {
            Media::addJsDef(['baseAdminDir' => __PS_BASE_URI__.basename(_PS_ADMIN_DIR_).'/']);
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
            $this->context->controller->addJS($this->_path.'views/js/back.js');
        }
        Media::addJsDef([
            'tokenDelivengoAjax' => Tools::getAdminTokenLite('AdminDelivengoAjax'),
            'delivengoAjaxUrl' => $this->context->link->getAdminLink('AdminDelivengoAjax', true, [], ['ajax' => 1]),
        ]);
        if (Tools::getValue('controller') == 'AdminOrders') {
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
            $this->context->controller->addJS($this->_path.'views/js/adminorder.js');
        }
    }

    /**
     * @param array $params
     * @return bool
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function hookNewOrder($params)
    {
        $this->logger->info('Hook newOrder called.');
        $order = new Order((int) $params['order']->id);
        if (Validate::isLoadedObject($order) && $order->current_state != Configuration::get('PS_OS_ERROR')) {
            $carrier = new Carrier((int) $order->id_carrier);
            $idCarrierReference = $carrier->id_reference;
            if ($carrier->external_module_name != $this->name) {
                $this->logger->info(
                    sprintf('Order #%d - Not a Delivengo Carrier.', $order->id),
                    ['id' => (int) $idCarrierReference]
                );

                return true;
            }
            $delivengoOrder = new DelivengoOrder();
            $delivengoOrder->id_order = (int) $order->id;
            try {
                $delivengoOrder->save();
            } catch (Exception $e) {
                $this->logger->error(sprintf('Order #%d - Cannot save order. '.$e->getMessage(), $order->id));

                return true;
            }
            $this->logger->info(
                sprintf('Order #%d - Delivengo Order created', $order->id),
                ['obj' => $delivengoOrder]
            );
        } else {
            $this->logger->error('Not a valid order.');
        }

        return true;
    }

    /**
     * @param array $params
     * @return string
     * @throws JsonMapper_Exception
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     * @throws SmartyException
     */
    public function displayAdminOrder($params)
    {
        $idOrder = $params['id_order'];
        $delivengoOrder = DelivengoOrder::getDelivengoOrderByOrderId($idOrder);
        if (!Validate::isLoadedObject($delivengoOrder)) {
            return '';
        }
        $settings = new Settings();
        /** @var \Delivengo\ObjectModel\DelivengoAddress[] $delivengoLabels */
        $delivengoLabels = $delivengoOrder->getLabels();
        if (isset($delivengoLabels[0]) && $delivengoLabels[0] instanceof DelivengoLabel && Validate::isLoadedObject($delivengoLabels[0])) {
            /** @var DelivengoLabel $delivengoLabel */
            $delivengoLabel = $delivengoLabels[0];
            $client = new DelivengoClient($settings->apiKey);
            try {
                /** @var \DelivengoSDK\Response\EnvoiResponse $response */
                $response = $client->send('get', new GetEnvoiRequest(), ['id' => $delivengoLabel->id_envoi]);
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
                $this->context->controller->warnings[] = $this->l('Cannot retrieve Delivengo easy informations.');

                return '';
            }
            if ($response->error) {
                return '';
            }
            /** @var \DelivengoSDK\Entity\Envoi $envoi */
            $envoi = $response->getData();
            $pli = $envoi->getPlis()[0];
            if ($pli->getNumero() && !$delivengoLabel->tracking_number) {
                try {
                    $delivengoLabel->tracking_number = pSQL($pli->getNumero());
                    $orderCarrier = \Delivengo\Utils\Tools::getOrderCarrierByIdOrder($idOrder);
                    if (Validate::isLoadedObject($orderCarrier) && !$orderCarrier->tracking_number) {
                        $orderCarrier->tracking_number = pSQL($pli->getNumero());
                        $orderCarrier->save();
                    }
                    $delivengoLabel->save();
                } catch (\Exception $e) {
                    $this->logger->error($e->getMessage());
                }
            }
            $this->context->smarty->assign([
                'envoi' => json_decode(json_encode($envoi), true),
                'pli' => json_decode(json_encode($pli), true),
                'settings' => json_decode(json_encode($settings), true),
                'id_delivengo_label' => $delivengoLabel->id_delivengo_label,
            ]);

            return $this->context->smarty->fetch($this->getLocalPath().'views/templates/hook/admin/'.$this->adminTheme.'/displayAdminOrderMainBottom_view.tpl');
        } else {
            $this->context->smarty->assign([
                'articles' => $this->getDelivengoOrderProducts($idOrder),
                'countries' => Country::getCountries($this->context->employee->id_lang),
                'settings' => json_decode(json_encode($settings), true),
                'params' => [
                    'id_order' => $idOrder,
                    'max_articles' => self::DELIVENGO_MAX_ARTICLES,
                    'need_customs_documents' => $this->needCustomsDocuments($idOrder),

                ],
            ]);

            return $this->context->smarty->fetch($this->getLocalPath().'views/templates/hook/admin/'.$this->adminTheme.'/displayAdminOrderMainBottom_form.tpl');
        }
    }

    /**
     * @param array $params
     * @return string|void
     * @throws JsonMapper_Exception
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     * @throws SmartyException
     */
    public function hookAdminOrder($params)
    {
        if (version_compare(_PS_VERSION_, '1.7.7', '>=')) {
            return;
        }
        $prestUI = \Delivengo\Utils\Tools::getPrestUI($this->context, $this->local_path);

        return $this->displayAdminOrder($params).$prestUI;
    }

    /**
     * @param array $params
     * @return string
     * @throws JsonMapper_Exception
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     * @throws SmartyException
     */
    public function hookDisplayAdminOrderMainBottom($params)
    {
        if (version_compare(_PS_VERSION_, '1.7.7', '<')) {
            return '';
        }

        return $this->displayAdminOrder($params);
    }

    /**
     * @param array $params
     */
    public function hookActionAdminOrdersListingFieldsModifier($params)
    {
        if (isset($params['fields'])) {
            $column = [
                'is_delivengo' => [
                    'title' => $this->l('Delivengo shipping ?'),
                    'align' => 'text-center',
                    'type' => 'bool',
                    'class' => 'fixed-width-xs',
                    'orderby' => false,
                    'havingFilter' => true,
                    'callback_object' => $this,
                    'callback' => 'printDelivengo',
                ],
            ];
            $position = array_search('cname', array_keys($params['fields']));
            \Delivengo\Utils\Tools::arraySpliceAssoc($params['fields'], $position ? ($position + 1) : 4, 0, $column);
        }

        if (isset($params['select'])) {
            $params['select'] = trim($params['select'], ', ').',
                !ISNULL(deo.id_delivengo_order) AS `is_delivengo`';
        }

        if (isset($params['join'])) {
            $params['join'] .= '
                LEFT JOIN `'._DB_PREFIX_.'delivengo_order` deo ON (a.`id_order` = deo.`id_order`)';
        }
    }

    /**
     * @param int   $isDelivengo
     * @param array $tr
     * @return string
     */
    public function printDelivengo($isDelivengo, $tr)
    {
        $iconPath = $this->getPathUri().'views/img/icons/icon_order_state.gif';

        return $isDelivengo ? '<img src="'.$iconPath.'" />' : '';
    }

    /**
     * @param array $params
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function hookDisplayAdminProductsExtra($params)
    {
        $idProduct = _PS_VERSION_ < '1.7' ? (int) Tools::getValue('id_product') : (int) $params['id_product'];
        $countries = Country::getCountries((int) Context::getContext()->cookie->id_lang);
        $productCustomDetails = DelivengoCustomProduct::getByIdProduct((int) $idProduct);
        $this->context->smarty->assign([
            'countries' => $countries,
            'ps_version' => _PS_VERSION_,
            'product_details' => $productCustomDetails,
        ]);
        $tpl = Tools::version_compare(_PS_VERSION_, '1.7', '<') ? '_16' : '';

        return $this->display(__FILE__, 'views/templates/hook/admin/displayAdminCustomProduct'.$tpl.'.tpl');
    }

    /**
     * @param array $params
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function hookActionProductUpdate($params)
    {
        $idProduct = $params['id_product'];
        $hsCode = Tools::getValue('hs_code');
        $shortDescription = Tools::getValue('short_desc');
        $countryOrigin = Tools::getValue('country_origin');

        $customProduct = DelivengoCustomProduct::getByIdProduct((int) $idProduct);
        $customProduct->id_product = (int) $idProduct;
        $customProduct->short_desc = $shortDescription;
        $customProduct->id_country_origin = (int) $countryOrigin;
        $customProduct->hs_code = $hsCode;
        $customProduct->save();
    }

    /**
     * @param array $params
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function hookActionAdminCategoriesFormModifier($params)
    {
        $countries = Country::getCountries((int) Context::getContext()->cookie->id_lang);
        array_unshift($countries, ['id_country' => '0', 'name' => $this->l('-- Please select a country --')]);
        $params['fields']['delivengo'] = [
            'form' => [
                'legend' => [
                    'title' => $this->l('Customs data Delivengo'),
                    'icon' => 'icon-tags',
                ],
                'input' => [
                    [
                        'type' => 'text',
                        'label' => $this->l('Short description'),
                        'name' => 'delivengo_short_desc',
                        'size' => 64,
                    ],
                    [
                        'type' => 'select',
                        'label' => $this->l('Origin country'),
                        'name' => 'delivengo_country_origin',
                        'options' => [
                            'query' => $countries,
                            'id' => 'id_country',
                            'name' => 'name',
                        ],
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('HS Code'),
                        'name' => 'delivengo_hs_code',
                    ],
                ],
                'submit' => [
                    'title' => $this->l('Save'),
                ],
            ],
        ];
        $idCategory = (int) Tools::getValue('id_category');
        $categoryCustomDetails = DelivengoCustomCategory::getByIdCategory($idCategory);
        $params['fields_value']['delivengo_short_desc'] = $categoryCustomDetails->short_desc;
        $params['fields_value']['delivengo_country_origin'] = $categoryCustomDetails->id_country_origin;
        $params['fields_value']['delivengo_hs_code'] = $categoryCustomDetails->hs_code;
    }

    /**
     * @param array $params
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function hookActionAdminCategoriesControllerSaveAfter($params)
    {
        $idCategory = (int) Tools::getValue('id_category');
        $shortDescription = Tools::getValue('delivengo_short_desc');
        $countryOrigin = Tools::getValue('delivengo_country_origin');
        $hsCode = Tools::getValue('delivengo_hs_code');

        $customCategory = DelivengoCustomCategory::getByIdCategory((int) $idCategory);
        $customCategory->id_category = (int) $idCategory;
        $customCategory->short_desc = $shortDescription;
        $customCategory->id_country_origin = (int) $countryOrigin;
        $customCategory->hs_code = $hsCode;
        $customCategory->save();
    }

    /**
     * @param array $params
     * @throws \PrestaShop\PrestaShop\Core\Module\Exception\ModuleErrorException
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function hookActionAfterUpdateCategoryFormHandler($params)
    {
        $this->updateCategoryCustomData($params);
    }

    /**
     * @param array $params
     * @throws \PrestaShop\PrestaShop\Core\Module\Exception\ModuleErrorException
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function hookActionAfterCreateCategoryFormHandler($params)
    {
        $this->updateCategoryCustomData($params);
    }

    /**
     * @param array $params
     * @throws \PrestaShop\PrestaShop\Core\Module\Exception\ModuleErrorException
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    private function updateCategoryCustomData($params)
    {
        $customCategory = new DelivengoCustomCategory((int) $params['form_data']['delivengo_custom_category_id']);
        $customCategory->id_category = $params['id'];
        $customCategory->short_desc = $params['form_data']['delivengo_short_desc'];
        $customCategory->id_country_origin = (int) $params['form_data']['delivengo_country_origin'];
        $customCategory->hs_code = $params['form_data']['delivengo_hs_code'];
        try {
            $customCategory->save();
        } catch (Exception $e) {
            throw new \PrestaShop\PrestaShop\Core\Module\Exception\ModuleErrorException($e->getMessage());
        }
    }

    /**
     * @param array $params
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function hookActionCategoryFormBuilderModifier($params)
    {
        /** @var \Symfony\Component\Form\FormBuilderInterface $formBuilder */
        $formBuilder = $params['form_builder'];
        $customCategory = DelivengoCustomCategory::getByIdCategory($params['id']);
        $countries = Country::getCountries($this->context->language->id);
        $countryChoices = [];
        array_walk(
            $countries,
            function (&$country) use (&$countryChoices) {
                $countryChoices[$country['name']] = $country['id_country'];
            }
        );
        $formBuilder->add(
            'delivengo_short_desc',
            \Symfony\Component\Form\Extension\Core\Type\TextType::class,
            ['label' => $this->l('Short description'), 'required' => false]
        );
        $formBuilder->add(
            'delivengo_country_origin',
            \Symfony\Component\Form\Extension\Core\Type\ChoiceType::class,
            ['label' => $this->l('Origin country'), 'choices' => $countryChoices, 'required' => false]
        );
        $formBuilder->add(
            'delivengo_hs_code',
            \Symfony\Component\Form\Extension\Core\Type\TextType::class,
            ['label' => $this->l('HS Code'), 'required' => false]
        );
        $formBuilder->add(
            'delivengo_custom_category_id',
            \Symfony\Component\Form\Extension\Core\Type\HiddenType::class,
            ['data' => $customCategory->id]
        );

        $params['data']['delivengo_short_desc'] = $customCategory->short_desc;
        $params['data']['delivengo_country_origin'] = $customCategory->id_country_origin;
        $params['data']['delivengo_hs_code'] = $customCategory->hs_code;

        $formBuilder->setData($params['data']);
    }

    /**
     * @param array $params
     */
    public function hookActionOrderGridDefinitionModifier(array $params)
    {
        /** @var GridDefinitionInterface $definition */
        $definition = $params['definition'];

        /** @var ColumnCollection */
        $columns = $definition->getColumns();
        $columnIsDelivengo = new BooleanColumn('is_delivengo');
        $columnIsDelivengo->setOptions([
            'field' => 'is_delivengo',
            'true_name' => $this->l('Yes'),
            'false_name' => $this->l('No'),
            'clickable' => false,
        ]);
        $columnIsDelivengo->setName($this->l('Delivengo order'));
        $columns->addAfter('country_name', $columnIsDelivengo);

        /** @var FilterCollectionInterface $filters */
        $filters = $definition->getFilters();
        $filterIsDelivengo = new Filter('is_delivengo', YesAndNoChoiceType::class);
        $filterIsDelivengo->setAssociatedColumn('is_delivengo');
        $filters->add($filterIsDelivengo);
    }

    /**
     * @param array $params
     */
    public function hookActionOrderGridQueryBuilderModifier(array $params)
    {
        /** @var QueryBuilder $searchQueryBuilder */
        $searchQueryBuilder = $params['search_query_builder'];
        $searchQueryBuilder->addSelect('!ISNULL(deo.id_delivengo_order) AS `is_delivengo`')
                           ->leftJoin('o', _DB_PREFIX_.'delivengo_order', 'deo', 'deo.id_order = o.id_order');

        /** @var SearchCriteriaInterface $searchCriteria */
        $searchCriteria = $params['search_criteria'];

        $strictComparisonFilters = [
            'is_delivengo' => '!ISNULL(deo.id_delivengo_order)',
        ];

        $filters = $searchCriteria->getFilters();
        foreach ($filters as $filterName => $filterValue) {
            if (isset($strictComparisonFilters[$filterName])) {
                $alias = $strictComparisonFilters[$filterName];
                $searchQueryBuilder->andWhere("$alias = :$filterName");
                $searchQueryBuilder->setParameter($filterName, $filterValue);
                continue;
            }
        }
    }

    /**
     * @param int $idOrder
     * @return array
     * @throws Exception
     */
    public function getDelivengoOrderProducts($idOrder)
    {
        $settings = new Settings();
        $order = new Order((int) $idOrder);
        $products = $order->getProducts();
        $articles = [];
        foreach ($products as $product) {
            $originCountry = (int) Country::getByIso('FR');
            $hsCode = $settings->defaultHsCode;
            $productCustomDetails = DelivengoCustomProduct::getByIdProduct((int) $product['product_id']);
            $categoryCustomDetails = DelivengoCustomCategory::getByIdCategory((int) $product['id_category_default']);
            $shortDescription = $product['product_name'];
            if ($productCustomDetails->short_desc) {
                $shortDescription = $productCustomDetails->short_desc;
            } elseif ($categoryCustomDetails->short_desc) {
                $shortDescription = $categoryCustomDetails->short_desc;
            }
            if ($productCustomDetails->hs_code) {
                $hsCode = $productCustomDetails->hs_code;
            } elseif ($categoryCustomDetails->hs_code) {
                $hsCode = $categoryCustomDetails->hs_code;
            }
            if ($productCustomDetails->id_country_origin) {
                $originCountry = (int) $productCustomDetails->id_country_origin;
            } elseif ($categoryCustomDetails->id_country_origin) {
                $originCountry = (int) $categoryCustomDetails->id_country_origin;
            }
            $articles[] = [
                'id' => $product['product_id'],
                'description' => $shortDescription,
                'quantity' => $product['product_quantity'],
                'weight' => \Delivengo\Utils\Tools::getWeightInKg($product['product_weight']) * 1000,
                'price' => number_format($product['product_price'], 2, '.', ''),
                'hsCode' => $hsCode,
                'originCountry' => $originCountry,
            ];
        }

        return $articles;
    }

    /**
     * @param int $idOrder
     * @return bool
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function needCustomsDocuments($idOrder)
    {
        $order = new Order((int) $idOrder);
        if (Validate::isLoadedObject($order)) {
            $address = new Address((int) $order->id_address_delivery);
            $isoCode = Country::getIsoById((int) $address->id_country);
            $isoEUCountries = \Delivengo\Utils\Tools::$isoEUCountries;
            if (date('Y') >= '2021') {
                if (($key = array_search('GB', $isoEUCountries)) !== false) {
                    unset($isoEUCountries[$key]);
                }
            }
            if (!in_array($isoCode, $isoEUCountries)) {
                return true;
            }
        }

        return false;
    }
}
