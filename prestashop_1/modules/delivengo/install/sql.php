<?php
/**
 * 2020 Delivengo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0).
 * It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
 *
 * @author    Delivengo
 * @copyright 2020 Delivengo
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *
 */

$tables = [];
$tables[_DB_PREFIX_.'delivengo_order'] = '
CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'delivengo_order` (
	`id_delivengo_order` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_order` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`id_delivengo_order`),
	UNIQUE INDEX `id_order_unique` (`id_order`),
	INDEX `id_order` (`id_order`)
)
COLLATE=\'utf8mb4_general_ci\'
ENGINE=InnoDB
;';

$tables[_DB_PREFIX_.'delivengo_label'] = '
CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'delivengo_label` (
	`id_delivengo_label` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_delivengo_order` INT(10) UNSIGNED NOT NULL,
	`id_envoi` INT(10) UNSIGNED NOT NULL,
	`tracking_number` VARCHAR(35) NULL,
	`deleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT \'0\',
	PRIMARY KEY (`id_delivengo_label`),
	INDEX `id_delivengo_order` (`id_delivengo_order`)
)
COLLATE=\'utf8mb4_general_ci\'
ENGINE=InnoDB
;';

$tables[_DB_PREFIX_.'delivengo_address'] = '
CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'delivengo_address` (
        `id_delivengo_address` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        `address_id` INT(10) NULL DEFAULT \'0\',
        `user_id` INT(10) NULL DEFAULT \'0\',
        `social_reason` VARCHAR(35) NOT NULL ,
        `full_name` VARCHAR(35) NULL ,
        `address1` VARCHAR(35) NOT NULL ,
        `address2` VARCHAR(35) NULL ,
        `postbox` VARCHAR(35) NULL  ,
        `zipcode` VARCHAR(12) NULL DEFAULT \'0\',
        `city` VARCHAR(35) NOT NULL,
        `deleted` TINYINT(4) NOT NULL DEFAULT \'0\',
        PRIMARY KEY (`id_delivengo_address`)
)
COLLATE=\'utf8mb4_general_ci\'
ENGINE=InnoDB
;';

$tables[_DB_PREFIX_.'delivengo_custom_category'] = '
CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'delivengo_custom_category` (
            `id_delivengo_custom_category` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `id_category` INT(10) NULL DEFAULT 0,
            `short_desc` VARCHAR(64) NULL DEFAULT NULL,
            `id_country_origin` INT(10) NULL DEFAULT 0,
            `hs_code` VARCHAR(50) NULL DEFAULT NULL,
            PRIMARY KEY (`id_delivengo_custom_category`)
)
COLLATE=\'utf8mb4_general_ci\'
ENGINE=InnoDB
;';

$tables[_DB_PREFIX_.'delivengo_custom_product'] = '
CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'delivengo_custom_product` (
            `id_delivengo_custom_product` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `id_product` INT(10) NULL DEFAULT 0,
            `short_desc` VARCHAR(64) NULL DEFAULT NULL,
            `id_country_origin` INT(10) NULL DEFAULT 0,
            `hs_code` VARCHAR(50) NULL DEFAULT NULL,
            PRIMARY KEY (`id_delivengo_custom_product`)
)
COLLATE=\'utf8mb4_general_ci\'
ENGINE=InnoDB
;';
