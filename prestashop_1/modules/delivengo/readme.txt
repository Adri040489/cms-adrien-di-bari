## DELIVENGO EASY - OFFICIAL MODULE


Available for version 1.6.1 to v.1.7.X!
 

To install, please insure to run at least a version 1.6.1 on your back office (written in top of screen besides the prestashop logo or in Menu / Advanced parameters / informations)
 

Insure that your hosting is based on a php5.3 version & provide the following library: Curl

 
### To install, 
Upload your file via the Menu / Modules => Add a new module & Upload the zip version (follow PrestaShop recommendations)

Once zip file uploaded onto your hostings, click on Install, Configure => you will need a DELIVENGO account to go further. 

If you don't have any account yet, module propose to subscribe online via tab "introduction > button "subscribe". (You will be redericted to a new website : mydelivengo.fr to subscribe)

 

/!\ IMPORTANT : Module install create automatically the necessary 1 carrier "MANDATORY" to use your DELIVENGO API onto your shop. 
This carrier will be created to enjoy all Delivengo services & products (international shipping, label edition... shipment tracking).
Please do not remove it.

This carrier will inherit your PrestaShop standard configuration (Zones & countries, Delivery ranges, Carrier preferences...). 
You should customize this carrier : range of prices, weight, but you should limit your shipment up to 2 kilos and dimensions of 90cm.


However to fully enjoy all the services provided by this Delivengo easy - Official module, please make sure you have:
- Created a delivengo easy account
- Added a direct debit mandate to your account
- Retrieved your API key from the "My Account" part of the MyDelivengo easy web application

Then you could ship the world!