<?php
/**
 * 2020 Delivengo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0).
 * It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
 *
 * @author    Delivengo
 * @copyright 2020 Delivengo
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *
 */

namespace Delivengo;

use JsonMapper;
use JsonSerializable;

/**
 * Class Account
 * @package Delivengo
 */
class Account implements JsonSerializable
{
    /** @var \DelivengoSDK\Entity\Utilisateur $details */
    public $details;

    /** @var \DelivengoSDK\Entity\Mandat $mandate */
    public $mandate;

    /**
     * Account constructor.
     * @throws \JsonMapper_Exception
     */
    public function __construct()
    {
        $json = \Configuration::get('DELIVENGO_ACCOUNT');
        $mapper = new JsonMapper();
        $mapper->bEnforceMapType = false;
        $mapper->bStrictNullTypes = false;
        $mapper->map(json_decode($json, true), $this);
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'details' => $this->details,
            'mandate' => $this->mandate,
        ];
    }
}
