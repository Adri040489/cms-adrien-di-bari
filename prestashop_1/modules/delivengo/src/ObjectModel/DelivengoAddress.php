<?php
/**
 * 2020 Delivengo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0).
 * It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
 *
 * @author    Delivengo
 * @copyright 2020 Delivengo
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *
 */

namespace Delivengo\ObjectModel;

use Db;
use DbQuery;

/**
 * Class DelivengoAddress
 * @package Delivengo\ObjectModel
 */
class DelivengoAddress extends \ObjectModel
{
    /** @var int $id_delivengo_address */
    public $id_delivengo_address;

    /** @var int $address_id */
    public $address_id;

    /** @var int $user_id */
    public $user_id;

    /** @var string $social_reason */
    public $social_reason;

    /** @var string $full_name */
    public $full_name;

    /** @var string $address1 */
    public $address1;

    /** @var string $address2 */
    public $address2;

    /** @var string $postbox */
    public $postbox;

    /** @var string $zipcode */
    public $zipcode;

    /** @var string $city */
    public $city;

    /** @var int $deleted */
    public $deleted;

    /** @var array */
    public static $definition = [
        'table' => 'delivengo_address',
        'primary' => 'id_delivengo_address',
        'fields' => [
            'address_id' => ['type' => self::TYPE_INT, 'required' => false],
            'user_id' => ['type' => self::TYPE_INT, 'required' => false],
            'social_reason' => ['type' => self::TYPE_STRING, 'required' => true, 'size' => 35],
            'full_name' => ['type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => false, 'size' => 35],
            'address1' => ['type' => self::TYPE_STRING, 'validate' => 'isAddress', 'required' => true, 'size' => 35],
            'address2' => ['type' => self::TYPE_STRING, 'validate' => 'isAddress', 'required' => false, 'size' => 35],
            'postbox' => ['type' => self::TYPE_STRING, 'required' => false, 'size' => 35],
            'zipcode' => ['type' => self::TYPE_STRING, 'validate' => 'isPostCode', 'required' => false, 'size' => 12],
            'city' => ['type' => self::TYPE_STRING, 'required' => true, 'size' => 35],
            'deleted' => ['type' => self::TYPE_BOOL, 'validate' => 'isBool'],
        ],
    ];

    /**
     * @return array
     * @throws \PrestaShopDatabaseException
     */
    public static function getAddressList()
    {
        $dbQuery = new DbQuery();
        $dbQuery->select('da.*');
        $dbQuery->from(self::$definition['table'], 'da');
        $dbQuery->where('da.deleted=0');
        $results = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbQuery);

        return $results ? : [];
    }

    /**
     * @return bool|DelivengoAddress
     * @throws \PrestaShopDatabaseException
     * @throws \PrestaShopException
     */
    public static function getFirstAddress()
    {
        $dbQuery = new DbQuery();
        $dbQuery->select('da.id_delivengo_address');
        $dbQuery->from(self::$definition['table'], 'da');
        $dbQuery->where('da.deleted=0');
        $dbQuery->orderBy('da.id_delivengo_address ASC');
        $id = (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($dbQuery);
        if (false === $id) {
            return false;
        } else {
            return new self($id);
        }
    }

    /**
     * @return bool
     * @throws \PrestaShopDatabaseException
     * @throws \PrestaShopException
     */
    public function delete()
    {
        $this->deleted = true;

        return $this->update();
    }
}
