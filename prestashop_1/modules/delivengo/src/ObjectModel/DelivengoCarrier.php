<?php
/**
 * 2020 Delivengo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0).
 * It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
 *
 * @author    Delivengo
 * @copyright 2020 Delivengo
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *
 */

namespace Delivengo\ObjectModel;

use Db;

/**
 * Class DelivengoCarrier
 * @package Delivengo\ObjectModel
 */
class DelivengoCarrier extends \Carrier
{
    /**
     * @param array $groups
     * @param bool  $delete
     * @return bool
     * @throws \PrestaShopDatabaseException
     */
    public function setGroups($groups, $delete = true)
    {
        if ($delete) {
            Db::getInstance()->delete('carrier_group', 'id_carrier = '.(int) $this->id);
        }
        if (!is_array($groups) || !count($groups)) {
            return true;
        }
        $return = true;
        foreach ($groups as $group) {
            Db::getInstance()->insert(
                'carrier_group',
                ['id_carrier' => (int) $this->id, 'id_group' => (int) $group['id_group']]
            );
        }

        return $return;
    }
}
