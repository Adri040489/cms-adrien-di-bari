<?php
/**
 * 2020 Delivengo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0).
 * It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
 *
 * @author    Delivengo
 * @copyright 2020 Delivengo
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *
 */

namespace Delivengo\ObjectModel;

use Db;
use DbQuery;

/**
 * Class DelivengoCustomCategory
 * @package Delivengo\ObjectModel
 */
class DelivengoCustomCategory extends \ObjectModel
{
    /** @var int $id_category */
    public $id_category;

    /** @var string $short_desc */
    public $short_desc;

    /** @var int $id_country_origin */
    public $id_country_origin;

    /** @var string $hs_code */
    public $hs_code;

    /** @var array $definition */
    public static $definition = [
        'table' => 'delivengo_custom_category',
        'primary' => 'id_delivengo_custom_category',
        'fields' => [
            'id_category' => ['type' => self::TYPE_INT, 'required' => false],
            'short_desc' => ['type' => self::TYPE_STRING, 'required' => false, 'size' => 64],
            'id_country_origin' => ['type' => self::TYPE_INT, 'required' => false],
            'hs_code' => ['type' => self::TYPE_STRING, 'required' => false, 'size' => 8],
        ],
    ];

    /**
     * @param int $idCategory
     * @return DelivengoCustomCategory
     * @throws \PrestaShopDatabaseException
     * @throws \PrestaShopException
     */
    public static function getByIdCategory($idCategory)
    {
        $dbQuery = new DbQuery();
        $dbQuery->select('id_delivengo_custom_category');
        $dbQuery->from(self::$definition['table']);
        $dbQuery->where('id_category='.(int) $idCategory);
        $id = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($dbQuery);

        return new self((int) $id);

    }
}
