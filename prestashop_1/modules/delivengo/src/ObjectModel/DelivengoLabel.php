<?php
/**
 * 2020 Delivengo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0).
 * It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
 *
 * @author    Delivengo
 * @copyright 2020 Delivengo
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *
 */

namespace Delivengo\ObjectModel;

use Db;
use DbQuery;

/**
 * Class DelivengoLabel
 * @package Delivengo\ObjectModel
 */
class DelivengoLabel extends \ObjectModel
{
    /** @var int $id_delivengo_label */
    public $id_delivengo_label;

    /** @var int $id_delivengo_order */
    public $id_delivengo_order;

    /** @var int $id_envoi */
    public $id_envoi;

    /** @var string $tracking_number */
    public $tracking_number;

    /** @var bool $deleted */
    public $deleted;

    /** @var array $definition */
    public static $definition = [
        'table' => 'delivengo_label',
        'primary' => 'id_delivengo_label',
        'fields' => [
            'id_delivengo_order' => ['type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId', 'required' => true],
            'id_envoi' => ['type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId', 'required' => true],
            'tracking_number' => ['type' => self::TYPE_STRING, 'required' => false, 'size' => 32],
            'deleted' => ['type' => self::TYPE_BOOL, 'validate' => 'isBool'],
        ],
    ];

    /**
     * @param int $idOrder
     * @return array|false|\mysqli_result|\PDOStatement|resource|null
     * @throws \PrestaShopDatabaseException
     */
    public static function getLabelList($idOrder)
    {
        $dbQuery = new DbQuery();
        $dbQuery->select('dl.*');
        $dbQuery->from(self::$definition['table'], 'dl');
        $dbQuery->where('dl.id_delivengo_order = '.(int) $idOrder);
        $dbQuery->where('dl.deleted != 1');

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbQuery);
    }
}
