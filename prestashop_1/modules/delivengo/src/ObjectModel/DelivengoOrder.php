<?php
/**
 * 2020 Delivengo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0).
 * It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
 *
 * @author    Delivengo
 * @copyright 2020 Delivengo
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *
 */

namespace Delivengo\ObjectModel;

use Db;
use DbQuery;

/**
 * Class DelivengoOrder
 * @package Delivengo\ObjectModel
 */
class DelivengoOrder extends \ObjectModel
{
    /** @var int $id_delivengo_order */
    public $id_delivengo_order;

    /** @var int $id_order */
    public $id_order;

    /** @var array $definition */
    public static $definition = [
        'table' => 'delivengo_order',
        'primary' => 'id_delivengo_order',
        'fields' => [
            'id_order' => ['type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId', 'required' => true],
        ],
    ];

    /**
     * @param int $idOrder
     * @return int
     */
    public static function getIdDelivengoOrderByOrderId($idOrder)
    {
        $dbQuery = new DbQuery();
        $dbQuery->select('id_delivengo_order')
                ->from('delivengo_order')
                ->where('id_order = '.(int) $idOrder);

        return (int) Db::getInstance(_PS_USE_SQL_SLAVE_)
                       ->getValue($dbQuery);
    }

    /**
     * @param int $idOrder
     * @return DelivengoOrder
     * @throws \PrestaShopDatabaseException
     * @throws \PrestaShopException
     */
    public static function getDelivengoOrderByOrderId($idOrder)
    {
        $dbQuery = new DbQuery();
        $dbQuery->select('id_delivengo_order')
                ->from('delivengo_order')
                ->where('id_order = '.(int) $idOrder);

        $id = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($dbQuery);

        return new self((int) $id);
    }

    /**
     * @return array
     * @throws \PrestaShopDatabaseException
     * @throws \PrestaShopException
     */
    public function getLabels()
    {
        $dbQuery = new DbQuery();
        $dbQuery->select('id_delivengo_label')
                ->from('delivengo_label')
                ->where('id_delivengo_order = '.(int) $this->id)
                ->where('deleted = 0');

        $ids = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbQuery);
        $labels = [];
        foreach ($ids as $id) {
            $labels[] = new DelivengoLabel((int) $id['id_delivengo_label']);
        }

        return $labels;
    }
}
