<?php
/**
 * 2020 Delivengo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0).
 * It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
 *
 * @author    Delivengo
 * @copyright 2020 Delivengo
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *
 */

namespace Delivengo;

use Configuration;
use Delivengo\ObjectModel\DelivengoAddress;
use DelivengoSDK\Entity\Mandat;
use DelivengoSDK\Entity\Utilisateur;
use JsonMapper;
use JsonSerializable;

/**
 * Class Settings
 * @package Delivengo
 */
class Settings implements JsonSerializable
{
    /** @var Utilisateur $account */
    public $account;

    /** @var Mandat $mandate */
    public $mandate;

    /** @var DelivengoAddress[] $shippingAddresses */
    public $shippingAddresses;

    /** @var string $apiKey */
    public $apiKey;

    /** @var bool $logsEnabled */
    public $logsEnabled;

    /** @var bool $displayWhatsNew */
    public $displayWhatsNew;

    /** @var JsonMapper $mapper */
    private $mapper;

    /** @var bool $synchronizeAddress */
    public $synchronizeAddress;

    /** @var int $defaultAddress */
    public $defaultAddress;

    /** @var bool $exportationMode */
    public $exportationMode;

    /** @var string $labelType */
    public $labelType;

    /** @var string $defaultHsCode */
    public $defaultHsCode;

    /** @var string $defaultDescription */
    public $defaultDescription;

    /** @var int $defaultOriginCountry */
    public $defaultOriginCountry = 8;

    /** @var bool $useInvoice */
    public $useInvoice;

    /** @var array $customsDocuments */
    public $customsDocuments = [
        'commercial_shipment' => 1,
        'shipmentNature' => 'merchandise_sale',
    ];

    /**
     * Settings constructor.
     * @throws \JsonMapper_Exception
     */
    public function __construct()
    {
        $this->mapper = new JsonMapper();
        $this->mapper->bEnforceMapType = false;
        $this->mapper->bStrictNullTypes = false;
        $this->account = $this->mapper->map(
            json_decode(Configuration::get('DELIVENGO_ACCOUNT') ? : '[]', true), new Utilisateur()
        );
        $this->mandate = $this->mapper->map(
            json_decode(Configuration::get('DELIVENGO_MANDATE') ? : '[]', true), new Mandat()
        );
        try {
            $addressIds = DelivengoAddress::getAddressList();
            foreach ($addressIds as $addressId) {
                $this->shippingAddresses[] = new DelivengoAddress((int) $addressId['id_delivengo_address']);
            }
        } catch (\Exception $e) {
            $this->shippingAddresses = [];
        }
        $this->mapper->map(json_decode(Configuration::get('DELIVENGO_SETTINGS') ? : '[]', true), $this);
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'account' => $this->account,
            'mandate' => $this->mandate,
            'shippingAddresses' => $this->shippingAddresses,
            'configuration' => [
                'apiKey' => $this->apiKey,
                'logsEnabled' => $this->logsEnabled,
                'synchronizeAddress' => $this->synchronizeAddress,
                'defaultAddress' => $this->defaultAddress,
                'exportationMode' => $this->exportationMode,
                'labelType' => $this->labelType,
                'defaultHsCode' => $this->defaultHsCode,
                'defaultDescription' => $this->defaultDescription,
                'defaultOriginCountry' => $this->defaultOriginCountry,
                'useInvoice' => $this->useInvoice,
                'customsDocuments' => $this->customsDocuments,
                'displayWhatsNew' => $this->displayWhatsNew,
            ],
            'path' => [
                'module' => __PS_BASE_URI__.'modules/delivengo/',
                'img' => __PS_BASE_URI__.'modules/delivengo/views/img/',
            ],
        ];
    }

    /**
     * @param array $account
     * @throws \JsonMapper_Exception
     */
    public function updateAccount($account)
    {
        $this->account = $this->mapper->map($account, new Utilisateur());
        Configuration::updateValue('DELIVENGO_ACCOUNT', json_encode($this->account));
    }

    /**
     * @param array $mandate
     * @throws \JsonMapper_Exception
     */
    public function updateMandate($mandate)
    {
        $this->mandate = $this->mapper->map($mandate, new Mandat());
        Configuration::updateValue('DELIVENGO_MANDATE', json_encode($this->mandate));
    }

    /**
     * @param array $settings
     * @throws \JsonMapper_Exception
     */
    public function updateSettings($settings)
    {
        $this->mapper->map($settings, $this);
        $settingsArray = json_decode(json_encode($this), true);
        Configuration::updateValue('DELIVENGO_SETTINGS', json_encode($settingsArray['configuration']));
    }
}
