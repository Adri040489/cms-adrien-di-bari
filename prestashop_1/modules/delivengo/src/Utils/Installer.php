<?php
/**
 * 2020 Delivengo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0).
 * It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
 *
 * @author    Delivengo
 * @copyright 2020 Delivengo
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *
 */

namespace Delivengo\Utils;

use Carrier;
use Configuration;
use Country;
use Db;
use Delivengo;
use Delivengo\ObjectModel\DelivengoCarrier;
use Group;
use Language;
use Tab;
use Validate;

/**
 * Class Installer
 * @package Delivengo\Utils
 */
class Installer
{
    /** @var Delivengo $module */
    private $module;

    /**
     * Installer constructor.
     * @param Delivengo $module
     */
    public function __construct($module)
    {
        $this->module = $module;
    }

    /**
     * @throws \Exception
     */
    public function checkTechnicalRequirements()
    {
        //@formatter:off
        if (extension_loaded('curl') == false) {
            throw new \Exception(
                $this->module->l('You need to enable the cURL extension to use this module.', 'Installer')
            );
        }
        //@formatter:on
        $this->module->logger->info('Configuration meets technical requirements');
    }

    /**
     * @param array  $tabNames
     * @param string $className
     * @param string $parentClassName
     * @param string $icon
     * @param bool   $visible
     * @return bool
     * @throws \PrestaShopDatabaseException
     * @throws \PrestaShopException
     */
    public function addMenu($tabNames, $className, $parentClassName, $icon = '', $visible = true)
    {
        $tab = new Tab();
        $tab->class_name = $className;
        $tab->module = $this->module->name;
        $tab->icon = $icon;
        $tab->id_parent = (int) Tab::getIdFromClassName($parentClassName);
        $tab->active = $visible;
        $tab->name = [];
        foreach (Language::getLanguages() as $lang) {
            $isoCode = $lang['iso_code'];
            $tabName = $tabNames[$isoCode] = isset($tabNames[$isoCode]) ? $tabNames[$isoCode] : $tabNames['en'];
            $tab->name[$lang['id_lang']] = $tabName;
        }

        return $tab->add();
    }

    /**
     * @param array $hooks
     */
    public function registerHooks($hooks)
    {
        foreach ($hooks as $hook) {
            $this->module->logger->info(sprintf('Register module on %s', $hook));
            $this->module->registerHook($hook);
        }
    }

    /**
     * @param array $carriersDetails
     * @throws \Exception
     */
    public function createCarriers($carriersDetails)
    {
        foreach ($carriersDetails as $carrierDetails) {
            $idCarrier = Configuration::getGlobalValue($carrierDetails['configuration']);
            $oldCarrier = Carrier::getCarrierByReference((int) $idCarrier);
            if ($oldCarrier !== false &&
                Validate::isLoadedObject($oldCarrier) &&
                $oldCarrier->external_module_name == $this->module->name
            ) {
                $this->module->logger->info('Carrier already exists');
                continue;
            }
            $this->module->logger->info('Creating carrier.');
            $languages = Language::getLanguages(false);
            $newCarrier = $this->createCarrier($carrierDetails, $languages);
            $newCarrier->setGroups(Group::getGroups(Configuration::get('PS_LANG_DEFAULT')));
        }
        $carriersToKeep = Configuration::getMultiple($this->module->carriersKeys);
        //@formatter:off
        Db::getInstance()->update(
            'carrier',
            ['deleted' => 1],
            'external_module_name = "'.pSQL($this->module->name).'" AND id_reference NOT IN('.implode(',', array_map('intval', $carriersToKeep)).')'
        );
        //@formatter:on
    }

    /**
     * @param array $carrierArray
     * @param array $languages
     * @return DelivengoCarrier
     * @throws \Exception
     */
    public function createCarrier($carrierArray, $languages)
    {
        $carrier = new DelivengoCarrier();
        $carrier->hydrate($carrierArray);
        foreach ($languages as $language) {
            $carrier->delay[(int) $language['id_lang']] =
                isset($carrierArray['delays'][$language['iso_code']]) ? $carrierArray['delays'][$language['iso_code']] :
                    $carrierArray['delays']['en'];
        }

        if (!$carrier->save()) {
            throw new \Exception('Cannot create carriers.');
        }
        $logoPath = _PS_MODULE_DIR_.$this->module->name.'/views/img/icons/icon_carrier.png';
        Tools::copy($logoPath, _PS_SHIP_IMG_DIR_.(int) $carrier->id.'.jpg');
        Configuration::updateGlobalValue($carrierArray['configuration'], $carrier->id);

        return $carrier;
    }

    /**
     * @param array $orderStatuses
     * @throws \PrestaShopDatabaseException
     * @throws \PrestaShopException
     */
    public function createOrderStatuses($orderStatuses)
    {
        foreach ($orderStatuses as $orderStatus) {
            $shippingInProgressState = new \OrderState(Configuration::getGlobalValue($orderStatus['configuration']));
            if (!Validate::isLoadedObject($shippingInProgressState)) {
                $this->module->logger->info(sprintf('Creating order status %s', $orderStatus['configuration']));
                $shippingInProgressState->hydrate($orderStatus);
                $shippingInProgressState->name = [];
                $shippingInProgressState->module_name = pSQL($this->module->name);
                $languages = Language::getLanguages(false);
                foreach ($languages as $language) {
                    $name = isset($orderStatus['names'][$language['iso_code']]) ?
                        $orderStatus['names'][$language['iso_code']] :
                        $orderStatus['names']['en'];
                    $shippingInProgressState->name[(int) $language['id_lang']] = pSQL($name);
                }
                if ($shippingInProgressState->save()) {
                    $source = _PS_MODULE_DIR_.$this->module->name.'/views/img/icons/icon_order_state.gif';
                    $destination = _PS_ROOT_DIR_.'/img/os/'.(int) $shippingInProgressState->id.'.gif';
                    Tools::copy($source, $destination);
                    Configuration::updateGlobalValue($orderStatus['configuration'], (int) $shippingInProgressState->id);
                }
            } else {
                $this->module->logger->info(sprintf('Order status %s already exists.', $orderStatus['configuration']));
            }
        }
    }

    /**
     * @throws \Exception
     */
    public function createTables()
    {
        if (@include $this->module->getLocalPath().'/install/sql.php') {
            foreach ($tables as $tableName => $createCode) {
                $this->module->logger->info(sprintf('Creating table %s if not exists', $tableName));
                Db::getInstance()->execute($createCode);
            }
        } else {
            throw new \Exception('Cannot found sql.php file.');
        }

        // In case of module reset
        try {
            $columnExists = Db::getInstance()
                              ->executeS('SHOW COLUMNS FROM `'._DB_PREFIX_.'delivengo_label` LIKE "tracking_number"');
        } catch (\Exception $e) {
            throw new \Exception('Cannot retrieve column.');
        }
        if (empty($columnExists)) {
            $result = Db::getInstance()
                        ->execute('ALTER TABLE `'._DB_PREFIX_.'delivengo_label` ADD COLUMN `tracking_number` VARCHAR(35) NULL AFTER `id_envoi`;');
            if (!$result) {
                throw new \Exception('Cannot add tracking_number column.');
            }
        }
    }

    /**
     * @throws \JsonMapper_Exception
     */
    public function initConfiguration()
    {
        $this->module->logger->info('Setting default configuration');
        $settings = new Delivengo\Settings();
        $settings->updateSettings(['labelType' => 'PDF_A4', 'defaultOriginCountry' => Country::getByIso('FR')]);
        $settings->updateAccount([]);
        $settings->updateMandate([]);
    }
}
