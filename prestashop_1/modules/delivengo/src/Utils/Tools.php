<?php
/**
 * 2020 Delivengo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0).
 * It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
 *
 * @author    Delivengo
 * @copyright 2020 Delivengo
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *
 */

namespace Delivengo\Utils;

use Configuration;
use Context;
use Country;
use Db;
use DbQuery;
use Delivengo\ObjectModel\DelivengoCustomCategory;
use Delivengo\ObjectModel\DelivengoCustomProduct;
use Delivengo\Settings;
use Dispatcher;
use Link;
use Monolog\Handler\NullHandler;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use OrderCarrier;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class Tools
 * @package Delivengo\Utils
 */
class Tools
{
    //@formatter:off
    /** @var array $isoEUCountries */
    public static $isoEUCountries = ['IT', 'DE', 'AT', 'BE', 'CY', 'HR', 'DK', 'ES', 'EE', 'FI', 'GR', 'HU', 'IE', 'LV', 'LT', 'MT', 'NL', 'PL', 'PT', 'CZ', 'RO', 'GB', 'SK', 'SI', 'SE'];
    //@formatter:on

    /**
     * @param \Delivengo $module
     * @param string     $name
     * @param bool       $force
     * @throws \JsonMapper_Exception
     */
    public static function initLogger($module, $name = 'module', $force = false)
    {
        $module->logger = new Logger($name);
        $settings = new Settings();
        if ($force || $settings->logsEnabled) {
            $fileHandler = new RotatingFileHandler(
                $module->getLocalPath().sprintf('%s/%s.log', $module::LOGS_DIR, self::hash(_PS_MODULE_DIR_)),
                3
            );
            $fileHandler->setFilenameFormat('{date}_{filename}', 'Ym');
            $module->logger->pushHandler($fileHandler);
        } else {
            $module->logger->pushHandler(new NullHandler());
        }
    }

    /**
     * @param Context $context
     * @param string  $localPath
     * @return string
     * @throws \SmartyException
     */
    public static function getPrestUI($context, $localPath)
    {
        $context->controller->addJS($localPath.'views/js/riot+compiler.min.js');
        $context->smarty->assign('ps_version', _PS_VERSION_);
        $prestUI = $context->smarty->fetch($localPath.'views/templates/admin/prestui/ps-alert.tpl');
        $prestUI .= $context->smarty->fetch($localPath.'views/templates/admin/prestui/ps-form.tpl');
        $prestUI .= $context->smarty->fetch($localPath.'views/templates/admin/prestui/ps-panel.tpl');
        $prestUI .= $context->smarty->fetch($localPath.'views/templates/admin/prestui/ps-table.tpl');
        $prestUI .= $context->smarty->fetch($localPath.'views/templates/admin/prestui/ps-tabs.tpl');
        $prestUI .= $context->smarty->fetch($localPath.'views/templates/admin/prestui/ps-tags.tpl');

        return $prestUI;
    }

    /**
     * @param string $value
     * @return string
     */
    public static function hash($value)
    {
        return md5(_COOKIE_IV_.$value);
    }

    /**
     * @param \Delivengo $module
     * @return string
     */
    public static function getWebhookHash($module)
    {
        return self::hash($module->getLocalPath());
    }

    /**
     * @param string $source
     * @param string $destination
     */
    public static function copy($source, $destination)
    {
        $filesystem = new Filesystem();
        $filesystem->copy($source, $destination);
    }

    /**
     * @param float $weight
     * @return float|int
     */
    public static function getWeightInKg($weight)
    {
        $conversion = [
            'g' => 0.001,
            'gr' => 0.001,
            'kg' => 1,
            'kgs' => 1,
            'lb' => 0.453592,
            'lbs' => 0.453592,
            'oz' => 0.0283495,
        ];
        $weightUnit = \Tools::strtolower(Configuration::get('PS_WEIGHT_UNIT'));

        return isset($conversion[$weightUnit]) ? $weight * $conversion[$weightUnit] : $weight;
    }

    /**
     * @param float $weight
     * @return float|int
     */
    public static function getWeightFromKg($weight)
    {
        $conversion = [
            'g' => 0.001,
            'gr' => 0.001,
            'kg' => 1,
            'kgs' => 1,
            'lb' => 0.453592,
            'lbs' => 0.453592,
            'oz' => 0.0283495,
        ];
        $weightUnit = \Tools::strtolower(Configuration::get('PS_WEIGHT_UNIT'));

        return isset($conversion[$weightUnit]) ? $weight / $conversion[$weightUnit] : $weight;
    }

    /**
     * @param int    $idProduct
     * @param string $defaultDescription
     * @return array
     * @throws \PrestaShopDatabaseException
     * @throws \PrestaShopException
     */
    public static function getProductCustomDetails($idProduct, $defaultDescription = '')
    {
        $productCustomDetails = DelivengoCustomProduct::getByIdProduct((int) $idProduct);
        $categoryCustomDetails = DelivengoCustomCategory::getByIdCategory((int) $idProduct);
        $description = $productCustomDetails->short_desc ? : $categoryCustomDetails->short_desc;
        $hsCode = $productCustomDetails->hs_code ? : $categoryCustomDetails->hs_code;
        $idCountry = $productCustomDetails->id_country_origin ? : $categoryCustomDetails->id_country_origin;

        return [
            'description' => $description ? : $defaultDescription,
            'hs_code' => $hsCode,
            'iso_country' => $idCountry ? Country::getIsoById($idCountry) : 'FR',
        ];
    }

    /**
     * @param int $idOrder
     * @return OrderCarrier
     * @throws \PrestaShopDatabaseException
     * @throws \PrestaShopException
     */
    public static function getOrderCarrierByIdOrder($idOrder)
    {
        $dbQuery = new DbQuery();
        $dbQuery->select('id_order_carrier')
                ->from('order_carrier')
                ->where('id_order = '.(int) $idOrder);
        $id = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($dbQuery);

        return new OrderCarrier((int) $id);
    }

    /**
     * @param Link   $link
     * @param string $controller
     * @param array  $params
     * @param int    $idShop
     * @return string
     * @throws \PrestaShopException
     */
    public static function getModuleLinkWithoutLang($link, $controller, $params, $idShop = null)
    {
        $module = 'delivengo';
        $url = $link->getBaseLink($idShop);

        $params['module'] = $module;
        $params['controller'] = $controller ? $controller : 'default';

        if (Dispatcher::getInstance()->hasRoute('module-'.$module.'-'.$controller, null, $idShop)) {
            unset($params['module']);
            unset($params['controller']);
            $uriPath = Dispatcher::getInstance()->createUrl('module-'.$module.'-'.$controller, null, $params, false, '', $idShop);

            return $link->getBaseLink($idShop).ltrim($uriPath, '/');
        } else {
            return $url.Dispatcher::getInstance()->createUrl('module', null, $params, false, '', $idShop);
        }
    }

    /**
     * @param       $input
     * @param       $offset
     * @param       $length
     * @param array $replacement
     */
    public static function arraySpliceAssoc(&$input, $offset, $length, $replacement = [])
    {
        $replacement = (array) $replacement;
        $indices = array_flip(array_keys($input));
        if (isset($input[$offset]) && is_string($offset)) {
            $offset = $indices[$offset];
        }
        if (isset($input[$length]) && is_string($length)) {
            $length = $indices[$length] - $offset;
        }

        $input = array_slice($input, 0, $offset, true)
                 + $replacement
                 + array_slice($input, $offset + $length, null, true);
    }

    /**
     * @param array $array
     * @param array $newArray
     * @return array
     */
    public static function arrayFlatten($array, $newArray = [])
    {
        foreach ($array as $key => $child) {
            if (is_array($child)) {
                $newArray = self::arrayFlatten($child, $newArray);
            } else {
                $newArray[] = $child;
            }
        }

        return $newArray;
    }
}
