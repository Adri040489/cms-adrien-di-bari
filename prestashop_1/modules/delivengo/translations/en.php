<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{delivengo}prestashop>delivengo_dd1842d74e123ab5b97993493cdaff9a'] = 'Delivengo easy official - Your light international shipment';
$_MODULE['<{delivengo}prestashop>delivengo_6fd554f281b365ab6cd82a43949fed03'] = 'Easily send your small goods internationally! Install the Delivengo easy module on your PrestaShop store and send your low value and low volume goods at the best price.';
$_MODULE['<{delivengo}prestashop>delivengo_8f71902b74d5ca0c341d998730f91b2f'] = 'Delivengo easy shipping ?';
$_MODULE['<{delivengo}prestashop>delivengo_0030f2fb9d2cd95018dac125d4df92a1'] = 'Customs data Delivengo easy';
$_MODULE['<{delivengo}prestashop>delivengo_034d9e68d0583407d4dec39bcd0d131b'] = 'Delivengo easy order';
$_MODULE['<{delivengo}prestashop>whatsnew_b49e22be0cb3fd3dd7d257b6d8974112'] = 'Delivengo easy v1.0.0';
$_MODULE['<{delivengo}prestashop>whatsnew_5a2ce39e6edefc0cbe67c563035c14b3'] = 'Connect your Delivengo easy account in the \"My Delivengo easy account\" tab using your API key';
$_MODULE['<{delivengo}prestashop>whatsnew_29d54566b81c3f02097af9f0f8d292c8'] = 'If you have shipping address on MyDelivengo easy side, please choose or create a new one in the \"Address book\" tab';
$_MODULE['<{delivengo}prestashop>modal-account-api_e82c54dc191ef24ce57976971a9789ac'] = 'Delivengo easy API key';
$_MODULE['<{delivengo}prestashop>modal-account-mandate_54ef9b44fd61f7401a2e4bdc816ed3fb'] = 'Please add at least one mandate in your Delivengo easy account';
$_MODULE['<{delivengo}prestashop>tab-account_e0d427068174445be4be86900064ad9e'] = 'Connect your Delivengo easy account to take full advantage of this module.';
$_MODULE['<{delivengo}prestashop>tab-account_a96d402c5241ba57efc6edd029c7625c'] = 'Connect Delivengo easy account';
$_MODULE['<{delivengo}prestashop>configuration-layout_6c7f6f4b7349f5de1eb45024daab09f7'] = 'Delivengo easy v';
$_MODULE['<{delivengo}prestashop>configuration-layout_0a926e0482af50691c7946146ac4cb26'] = 'My Delivengo easy account';
$_MODULE['<{delivengo}prestashop>displayadminordermainbottom_view_79949c0ac50a7144a06c3d2d91a05ead'] = 'Delivengo easy Shipping';
$_MODULE['<{delivengo}prestashop>displayadminordermainbottom_view_7a4ee3d56a27bdff92b755788b90551b'] = 'Tracking link will be available soon. Please refresh this page or come later.';
$_MODULE['<{delivengo}prestashop>displayadminordermainbottom_view_d5c57c0fbf61a1e4b72108dd788448be'] = '(Please choose where to place the label on the A4 paper)';
$_MODULE['<{delivengo}prestashop>displayadminordermainbottom_view_e3c71ec64c1852fd2fb03b45a320263f'] = '(Please not that your label has already been invoiced)';
$_MODULE['<{delivengo}prestashop>displayadminordermainbottom_form_79949c0ac50a7144a06c3d2d91a05ead'] = 'Delivengo easy Shipping';
$_MODULE['<{delivengo}prestashop>displayadminordermainbottom_form_89da8560dbf2a7f1b853f600ee8d4870'] = 'You have not created a Delivengo easy shipment yet';
$_MODULE['<{delivengo}prestashop>displayadminordermainbottom_view_05d71c9944ab38626779f8dee08c8602'] = 'Product';
$_MODULE['<{delivengo}prestashop>tracking_85c059aa3d56208bb51858005d11981c'] = 'Delivengo easy Tracking';
$_MODULE['<{delivengo}prestashop>admindelivengoconfigurationcontroller_cb0eddc73f4cad08901c8378bc15d129'] = 'Connect your Delivengo easy account';
