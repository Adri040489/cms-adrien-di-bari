<?php
/**
 * 2020 Delivengo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0).
 * It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
 *
 * @author    Delivengo
 * @copyright 2020 Delivengo
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * @param Delivengo $module
 * @return bool
 * @throws JsonMapper_Exception
 */
function upgrade_module_2_0_3($module)
{
    $settings = new \Delivengo\Settings();
    $logsEnabled = $settings->logsEnabled;
    $settings->updateSettings(['logsEnabled' => true]);

    try {
        $columnExists = Db::getInstance()
                          ->executeS('SHOW COLUMNS FROM `'._DB_PREFIX_.'delivengo_label` LIKE "tracking_number"');
    } catch (\Exception $e) {
        $module->logger->error($e->getMessage());

        return false;
    }
    if (empty($columnExists)) {
        $result = Db::getInstance()
                    ->execute('ALTER TABLE `'._DB_PREFIX_.'delivengo_label` ADD COLUMN `tracking_number` VARCHAR(35) NULL AFTER `id_envoi`;');
        if (!$result) {
            $module->logger->error('Cannot add tracking_number column.');

            return false;
        }
    }

    $settings->updateSettings(['logsEnabled' => $logsEnabled]);

    return true;
}
