<?php

namespace DelivengoSDK\Entity;

/**
 * Class Extra
 * @package DelivengoSDK\Entity
 */
class ExtraUser extends Entity
{
    /** @var string $eori */
    public $eori;

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'eori' => $this->getEori(),
        );
    }

    /**
     * @return string $eori
     */
    public function getEori()
    {
        return $this->eori;
    }

    /**
     * @param string $eori
     */
    public function setEori($eori)
    {
        $this->eori = $eori;
    }

 
}
