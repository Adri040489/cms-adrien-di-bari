<?php

namespace DelivengoSDK\Entity;

/**
 * Class Marque
 * @package DelivengoSDK\Entity
 */
 
class Marque extends Entity
{
	
    /** @var int $id */
    protected $id;

    /** @var array $nom */
    protected $nom;

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->getId(),
            'nom' => $this->getNom(),
        );
    }

  /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom[0];
    }

    /**
     * @param array $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }	
}
