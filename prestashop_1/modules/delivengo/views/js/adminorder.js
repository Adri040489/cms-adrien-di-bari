/**
 * 2020 Delivengo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0).
 * It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
 *
 * @author    Delivengo
 * @copyright 2020 Delivengo
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *
 */

$(document).ready(function () {
    var labelForm = $('.js-delivengo-label-form');
    var noLabel = $('.js-delivengo-no-label');
    var delivengoLoading = $('.js-delivengo-loading');
    var estimationResult = $('.js-delivengo-estimation');
    var delivengoErrors = $('.js-delivengo-error');
    var btnEstimation = $('.js-delivengo-estimation-submit');


    $('.js-delivengo-show-label-form').on('click', function (e) {
        noLabel.hide(200);
        labelForm.show(400);
    });

    btnEstimation.click(function (e) {
        e.preventDefault();

        estimationResult.hide();
        delivengoErrors.hide();
        delivengoLoading.show();
        btnEstimation.attr('disabled', 'disabled');
        var data = {
            quotation: 1,
            action: 'createLabel',
        };
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: delivengoAjaxUrl + '&' + $.param(data),
            data: $('.js-delivengo-label-form form').serialize(),
            success: function (data) {
                if (data.errors === true) {
                    delivengoErrors.html('');
                    for (msg of data.message) {
                        delivengoErrors.append('<p>' + msg + '</p>');
                    }
                    delivengoErrors.show();
                } else {
                    estimationResult.html(data.result_html);
                    estimationResult.show();
                }
            }
        }).always(function () {
            btnEstimation.removeAttr('disabled');
            delivengoLoading.hide();
        });

    });

    estimationResult.on('click', '.js-delivengo-label-submit', function (e) {
        e.preventDefault();

        var btnCreateLabel = $(this);

        delivengoErrors.hide();
        delivengoLoading.show();
        btnEstimation.attr('disabled', 'disabled');
        btnCreateLabel.attr('disabled', 'disabled');

        var data = {
            action: 'createLabel',
        };
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: delivengoAjaxUrl + '&' + $.param(data),
            data: $('.js-delivengo-label-form form').serialize(),
            success: function (data) {
                if (data.errors === true) {
                    delivengoErrors.html('');
                    for (msg of data.message) {
                        delivengoErrors.append('<p>' + msg + '</p>');
                    }
                    delivengoErrors.show();
                    btnEstimation.removeAttr('disabled');
                    btnCreateLabel.removeAttr('disabled');
                    delivengoLoading.hide();
                } else {
                    showSuccessMessage(data.message);
                    window.location.href = data.redirect_url;
                    window.location.reload();
                }
            }
        });
    });

    $('.js-delivengo-actions').on('click', '.js-delivengo-delete-label', function (e) {
        e.preventDefault();

        var idDelivengoLabel = parseInt($(this).attr('data-id-delivengo-label'));
        var data = {
            action: 'deleteLabel',
            id_delivengo_label: idDelivengoLabel,
        };

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: delivengoAjaxUrl,
            data: data,
            success: function (data) {
                if (data.errors === false) {
                    showSuccessMessage(data.message);
                    window.location.href = data.redirect_url;
                    window.location.reload();
                } else {
                    showErrorMessage(data.message);
                }
            }
        });
    });
});
