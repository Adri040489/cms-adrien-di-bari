/**
 * 2020 Delivengo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0).
 * It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
 *
 * @author    Delivengo
 * @copyright 2020 Delivengo
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *
 */

function resetModal(modal) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: 'index.php',
        data: {
            controller: 'AdminDelivengoAjax',
            ajax: 1,
            token: tokenDelivengoAjax,
            action: 'resetModal'
        }
    }).fail(function (jqXHR, textStatus) {
    }).done(function (data) {
        modal.find('.modal-body').html(data.result_html);
    }).always(function (data) {
    });
}

function validateApiKey(apiKey, modal, showStep) {
    return $.ajax({
        type: 'POST',
        dataType: 'json',
        url: 'index.php',
        data: {
            controller: 'AdminDelivengoAjax',
            ajax: 1,
            token: tokenDelivengoAjax,
            action: 'validateApiKey',
            apiKey: apiKey
        }
    }).done(function (data) {
        if (data.errors.length) {
            for (var i in data.errors) {
                showErrorMessage(data.errors[i]);
            }
        } else {
            modal.find('.modal-body').html(data.result_html);
            if (!showStep) {
                modal.find('.delivengo-modal-steps').hide();
            } else {
                modal.find('.step-api').toggleClass('inactive');
            }
        }
    });
}

function toggleCustomsDocuments() {
    if ($('#exportationMode_on').prop('checked')) {
        $('.delivengo-customs-documents').show(400);
    } else {
        $('.delivengo-customs-documents').hide(200);
    }
}

function processSaveExportationMode(val) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: 'index.php',
        data: {
            controller: 'AdminDelivengoAjax',
            ajax: 1,
            token: tokenDelivengoAjax,
            action: 'saveExportationMode',
            value: val
        }
    }).done(function (data) {
    });
}

function countDown($source, $target) {
    var max = $source.attr("data-maxchar");
    $target.html(max - $source.val().length);

    $source.keyup(function () {
        $target.html(max - $source.val().length);
    });
}

$(document).ready(function () {
    var modalSignin = $('#delivengo-modal-signin');
    var modalMandates = $('#delivengo-modal-mandates');
    var modalWhatsNew = $('#delivengo-modal-whatsnew');
    var body = $('body');
    toggleCustomsDocuments();

    modalWhatsNew.on('shown.bs.modal', function (e) {
        var modal = $(this);

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'index.php',
            data: {
                controller: 'AdminDelivengoAjax',
                ajax: 1,
                token: tokenDelivengoAjax,
                action: 'whatsNew'
            }
        }).fail(function (jqXHR, textStatus) {
        }).done(function (data) {
            modal.find('.modal-body').html(data.result_html);
        }).always(function (data) {
        });
    });

    modalWhatsNew.on('hide.bs.modal', function (e) {
        var modal = $(this);

        resetModal(modal);
    });

    modalSignin.on('hide.bs.modal', function (e) {
        var modal = $(this);

        resetModal(modal);
    });

    modalSignin.on('shown.bs.modal', function (e) {
        var modal = $(this);

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'index.php',
            data: {
                controller: 'AdminDelivengoAjax',
                ajax: 1,
                token: tokenDelivengoAjax,
                action: 'login'
            }
        }).fail(function (jqXHR, textStatus) {
        }).done(function (data) {
            modal.find('.modal-body').html(data.result_html);
            modal.find('.step-mandate').toggleClass('inactive');
        }).always(function (data) {
        });
    });

    body.on('submit', '#delivengo-api-config-form', function (e) {
        e.preventDefault();

        var $form = $(this),
            submitBtn = $form.find('button'),
            apiKey = $form.find('input[name="delivengo_api_key"]').val();

        submitBtn.toggleClass('disabled');
        submitBtn.find('i').toggleClass('icon-check icon-spin icon-spinner');

        var posting = validateApiKey(apiKey, modalSignin, true);

        posting.fail(function (jqXHR, textStatus) {
        });
        posting.always(function (data) {
            submitBtn.toggleClass('disabled');
            submitBtn.find('i').toggleClass('icon-check icon-spin icon-spinner');
        });
    });

    body.on('submit', '.delivengo-mandate-config-form', function (e) {
        e.preventDefault();

        var $form = $(this),
            submitBtn = $form.find('button'),
            submitBtns = $('.mandates-list').find('button'),
            apiKey = $form.find('input[name="delivengo_api_key"]').val();

        submitBtns.toggleClass('disabled');
        submitBtn.find('i').toggleClass('icon-spin icon-spinner');

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'index.php',
            data: {
                controller: 'AdminDelivengoAjax',
                ajax: 1,
                token: tokenDelivengoAjax,
                action: 'downloadDelivengoAddresses',
                apiKey: apiKey
            }
        }).always(function (data) {
            body.unbind();
            $form.submit();
        });
    });

    modalMandates.on('shown.bs.modal', function (e) {
        var modal = $(this);

        var posting = validateApiKey('', modal, false);

        posting.fail(function (jqXHR, textStatus) {
        });
        posting.always(function (data) {
        });
    });

    modalMandates.on('hide.bs.modal', function (e) {
        var modal = $(this);

        resetModal(modal);
    });

    $('input[name="delivengo-default-address"]').change(function () {
        var addressid = $(this).attr('data-delivengo-address-id');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'index.php',
            data: {
                controller: 'AdminDelivengoAjax',
                ajax: 1,
                token: tokenDelivengoAjax,
                action: 'SetDefaultDelivengoAddress',
                id_address: addressid
            }
        }).done(function (data) {
            if (data.error) {
                showErrorMessage(data.error);
            } else {
                $(this).attr('checked', true);
                showSuccessMessage(data.message);
            }
        });
    });

    $('input[type=radio][name=exportationMode]').change(function () {
        var delivengoExportationMode = $('input[name=exportationMode]:checked').val();
        toggleCustomsDocuments();
        processSaveExportationMode(delivengoExportationMode);
    });

    if (displayWhatsNew) {
        modalWhatsNew.modal('show');
    }
    $("input[name='eori']").attr('disabled', 'disabled');
});
