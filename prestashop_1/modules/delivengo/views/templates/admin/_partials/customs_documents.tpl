{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<ps-radios label="{l s='Shipment nature' mod='delivengo'}">
  <ps-radio name="delivengoSettings[customsDocuments][shipmentNature]"
            value="gift"
            {if $data.configuration.customsDocuments.shipmentNature == "gift"}checked="true"{/if}>{l s='Gift' mod='delivengo'}</ps-radio>
  <ps-radio name="delivengoSettings[customsDocuments][shipmentNature]"
            value="commercial_sample"
            {if $data.configuration.customsDocuments.shipmentNature == "commercial_sample"}checked="true"{/if}> {l s='Samples' mod='delivengo'} </ps-radio>
  <ps-radio name="delivengoSettings[customsDocuments][shipmentNature]"
            value="merchandise_return"
            {if $data.configuration.customsDocuments.shipmentNature == "merchandise_return"}checked="true"{/if}> {l s='Merchandise return' mod='delivengo'} </ps-radio>
  <ps-radio name="delivengoSettings[customsDocuments][shipmentNature]"
            value="merchandise_sale"
            {if $data.configuration.customsDocuments.shipmentNature == "merchandise_sale"}checked="true"{/if}> {l s='Merchandise sale' mod='delivengo'} </ps-radio>
  <ps-radio name="delivengoSettings[customsDocuments][shipmentNature]"
            value="other"
            {if $data.configuration.customsDocuments.shipmentNature == "other"}checked="true"{/if}> {l s='Other' mod='delivengo'} </ps-radio>

</ps-radios>