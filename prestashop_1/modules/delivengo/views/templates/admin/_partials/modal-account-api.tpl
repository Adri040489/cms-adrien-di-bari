{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

{include file="./modal-account-steps.tpl"}

<form class="form-horizontal"
      action="#"
      name="delivengo_api_config_form"
      id="delivengo-api-config-form"
      method="post"
      enctype="multipart/form-data">
  <div class="row">
    <div class="form-group">
      <label class="control-label col-lg-4" for="delivengo-api-key">
        <span>{l s='Delivengo API key' mod='delivengo'}</span>
      </label>
      <div class="col-lg-8">
        <div class="input-group fixed-width-xxl">
          <span class="input-group-addon">
            <i class="icon-key"></i>
          </span>
          <input type="password" id="delivengo-api-key" name="delivengo_api_key"></div>
      </div>
      <div class="col-lg-9 col-lg-offset-3"></div>
    </div>
    <button type="submit" class="btn btn-primary col-lg-offset-4">
      <i class="icon icon-check"></i>
      {l s='Check' mod='delivengo'}
    </button>
  </div>
</form>
