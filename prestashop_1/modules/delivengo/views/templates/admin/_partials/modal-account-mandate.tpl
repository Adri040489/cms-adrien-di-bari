{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

{include file="./modal-account-steps.tpl"}

<div class="mandates-container">
  <div class="mandates-list">
    {foreach $delivengo_mandates as $mandate}
      <form class="form-horizontal delivengo-mandate-config-form"
            action="#"
            name="delivengo_account_config_form"
            method="post"
            enctype="multipart/form-data">
        <div class="delivengo-mandate">
          <p class="rum">{$mandate.rum|escape:'htmlall':'utf-8'}</p>
          <p class="iban"><span><b>{l s='IBAN:' mod='delivengo'}</b></span> {$mandate.iban|escape:'htmlall':'utf-8'}</p>
          <p class="bic"><span><b>{l s='BIC:' mod='delivengo'}</b></span> {$mandate.bic|escape:'htmlall':'utf-8'}</p>
          <input type="hidden" name="action" value="saveAccount"/>
          <input type="hidden" name="delivengo_mandate[rum]" value="{$mandate.rum|escape:'htmlall':'utf-8'}"/>
          <input type="hidden" name="delivengo_mandate[iban]" value="{$mandate.iban|escape:'htmlall':'utf-8'}"/>
          <input type="hidden" name="delivengo_mandate[bic]" value="{$mandate.bic|escape:'htmlall':'utf-8'}"/>
          <input type="hidden" name="delivengo_api_key" value="{$delivengo_api_key|escape:'htmlall':'utf-8'}"/>
          <input type="hidden" name="delivengo_user" value="{$delivengo_user|escape:'htmlall':'utf-8'}"/>
          <button class="btn btn-primary">
            <i class=""></i>
            {l s='Select this mandate' mod='delivengo'}
          </button>
        </div>
      </form>
      {foreachelse}
      <div class="alert alert-danger">
        <p>
          {l s='Please add at least one mandate in your Delivengo account' mod='delivengo'}
          (https://easy.mydelivengo.laposte.fr/mon-compte#paymentsMeans)
        </p>
        <p>{l s='Then refresh the page and try again.' mod='delivengo'}</p>
      </div>
    {/foreach}
  </div>
</div>
