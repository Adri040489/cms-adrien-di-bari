{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<div class="delivengo-modal-steps">
  <div class="delivengo-modal-step step-api">
    <span class="modal-step-number">1</span>
    <span class="modal-step-text">{l s='Enter your API key' mod='delivengo'}</span>
  </div>
  <div class="delivengo-modal-step step-mandate">
    <span class="modal-step-number">2</span>
    <span class="modal-step-text">{l s='Select your mandate' mod='delivengo'}</span>
  </div>
</div>
