{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<div class="modal-body">
  <div class="delivengo-modal-loading">
    <p>{l s='Loading...' mod='delivengo'}</p>
    <img src="/modules/delivengo/views/img/icons/icon_carrier.png" />
  </div>
</div>
