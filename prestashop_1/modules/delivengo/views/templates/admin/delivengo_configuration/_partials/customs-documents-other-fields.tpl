{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<h2 class="col-lg-offset-3">{l s='Default values for products' mod='delivengo'}</h2>
<ps-input-text name="delivengoSettings[defaultHsCode]"
               label="{l s='Default HS Code' mod='delivengo'}"
               size="10"
               hint="{l s='Information can be set either here for the global catalog, or in catalog category edition or product by product' mod='delivengo'}"
               help="{l s='Informations prefilled by default for each label edition to save time.Could be changed while editing.' mod='delivengo'}"
               value="{$data.configuration.defaultHsCode|escape:'htmlall':'utf-8'}"
               fixed-width="xxl">
</ps-input-text>
<ps-select label="{l s='Country of origin' mod='delivengo'}"
           name="delivengoSettings[defaultOriginCountry]"
           fixed-width="xxl">
  {foreach $countries as $id => $country}
    <option value="{$country['id_country']|intval}"
            {if $id == $data.configuration.defaultOriginCountry}selected{/if}>
      {$country['name']|escape:'htmlall':'utf-8'}
    </option>
  {/foreach}
</ps-select>
<h2 class="col-lg-offset-3">{l s='Brexit - Code EORI UK' mod='delivengo'}</h2>
<ps-input-text name="eori"
               label="{l s='Code EORI UK' mod='delivengo'}"
               hint="{l s='Mandatory EORI number to provide when generating labels. To request your EORI UK number, apply on the UK government website : https://www.gov.uk/eori' mod='delivengo'}"
               help="{l s='EORI UK Code should be registered in ' mod='delivengo'}
                        <a target='_blank' href='https://mydelivengo.laposte.fr/'>{l s='MyDelivengo.fr' mod='delivengo'}</a> : {l s='(My Account > Customs Documents) and it will get synchronized into your PrestaShop Back office when editing your first label to UK.' mod='delivengo'}"
               value="{$data.account.extra['eori']|escape:'htmlall':'utf-8'}"
               fixed-width="xxl"
               >  
</ps-input-text>
