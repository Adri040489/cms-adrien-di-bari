{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<div class="row delivengo-header">
  <div class="col-lg-10 col-lg-offset-1">
    <div class="delivengo-information">
      <i class="icon icon-info-circle"></i>
      {l s='Delivengo v' mod='delivengo'}{$module_version|escape:'htmlall':'utf-8'} -
      <a data-toggle="modal" data-target="#delivengo-modal-whatsnew" href="#">{l s='What\'s New?' mod='delivengo'}</a>
    </div>
    <div class="bootstrap panel">
      <div class="row">
        <div class="col-xs-12 delivengo-header-container">
          <div class="delivengo-logo">
            <img src="{$data.path.img|escape:'html':'utf-8'}logo-easy.png">
          </div>
          <div class="delivengo-contact">
            <i class="icon icon-question-circle icon-big"></i>
            <div>
              <p>
                <b>{l s='Do you have a question?' mod='delivengo'}</b><br>
                {l s='Contact us using' mod='delivengo'}
                <a href="https://addons.prestashop.com/contact-form.php?id_product={$delivengo_addons_id|intval}"
                   target="_blank">
                  {l s='this link' mod='delivengo'}
                </a>
              </p>
              <a href="{$data.path.module|escape:'html':'utf-8'}readme_fr.pdf" target="_blank" class="btn btn-primary">
                <i class="icon-arrow-circle-down"></i> {l s='Download documentation' mod='delivengo'}
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-10 col-lg-offset-1">
    <div id="bo-delivengo">
      <div class="form-wrapper">
        <ul class="nav nav-tabs">
          <li class="{if $active_tab == 'intro'}active{/if}">
            <a href="#intro" data-toggle="tab">{l s='Introduction' mod='delivengo'}</a>
          </li>
          <li class="{if $active_tab == 'account'}active{/if}">
            <a href="#account" data-toggle="tab">
              <i class="icon icon-user"></i> {l s='My Delivengo account' mod='delivengo'}
            </a>
          </li>
          <li class="{if $active_tab == 'address-book'}active{/if}">
            <a href="#address-book" data-toggle="tab">
              <i class="icon icon-book"></i> {l s='Address book' mod='delivengo'}
            </a>
          </li>
          <li class="{if $active_tab == 'bo-settings'}active{/if}">
            <a href="#bo-settings" data-toggle="tab">
              <i class="icon icon-cogs"></i> {l s='Back-Office settings' mod='delivengo'}
            </a>
          </li>
        </ul>

        <div class="tab-content panel">
          <div id="intro" class="tab-pane {if $active_tab == 'intro'}active{/if}">
            {include file="./tab-intro.tpl"}
          </div>
          <div id="account" class="tab-pane {if $active_tab == 'account'}active{/if}">
            {include file="./tab-account.tpl"}
          </div>
          <div id="address-book" class="tab-pane {if $active_tab == 'address-book'}active{/if}">
            {if isset($newDelivengoAddress)}
              {$newDelivengoAddressForm}
            {else}
              {include file="./tab-address-book.tpl"}
            {/if}
          </div>
          <div id="bo-settings" class="tab-pane {if $active_tab == 'bo-settings'}active{/if}">
            {include file="./tab-bo-settings.tpl"}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
