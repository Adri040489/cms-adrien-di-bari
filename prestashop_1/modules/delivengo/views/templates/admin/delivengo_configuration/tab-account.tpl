{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

{if (!$data.account.id)}
  <div class="alert alert-info">
    <p>{l s='Connect your Delivengo account to take full advantage of this module.' mod='delivengo'}</p>
    <p>{l s='Login to' mod='delivengo'} <a href="https://mydelivengo.laposte.fr/" target="_blank">https://mydelivengo.laposte.fr/</a> {l s='then go the "Mon compte" menu.' mod='delivengo'}
    </p>
    <p>{l s='On this page:' mod='delivengo'}</p>
    <ul>
      <li>{l s='find your API key in the "Mon compte" tab' mod='delivengo'}</li>
      <li>{l s='if you haven\'t, please add at least one mandate in the "Modes de paiement" tab (last tab)' mod='delivengo'}</li>
    </ul>
  </div>
{/if}

<div class="delivengo-account-panel">
  <div class="panel">
    {if ($data.account.id)}
      <p>{l s='Welcome' mod='delivengo'} {$data.account.prenom|upper|escape:'htmlall':'utf-8'} {$data.account.nom|escape:'htmlall':'utf-8'|truncate:2:'.'|upper|escape:'htmlall':'utf-8'}</p>
      <i class="icon icon-user"></i>
      <a class="btn btn-primary" data-toggle="modal" data-target="#delivengo-modal-signin">
        <i class="icon icon-refresh"></i>
        {l s='Update your API key' mod='delivengo'}
      </a>
      <div>
        <p>{l s='Your selected mandate' mod='delivengo'}</p>
        <div class="delivengo-mandate">
          <p class="rum">{$data.mandate.rum|escape:'htmlall':'utf-8'}</p>
          <p class="iban">
            <span><b>{l s='IBAN:' mod='delivengo'}</b></span> {$data.mandate.iban|escape:'htmlall':'utf-8'}
          </p>
          <p class="bic"><span><b>{l s='BIC:' mod='delivengo'}</b></span> {$data.mandate.bic|escape:'htmlall':'utf-8'}
          </p>
          <a class="btn btn-primary" data-toggle="modal" data-target="#delivengo-modal-mandates">
            <i class="icon"></i>
            {l s='Select another mandate' mod='delivengo'}
          </a>
        </div>
      </div>
    {else}
      <p>{l s='Welcome,' mod='delivengo'}</p>
      <i class="icon icon-user"></i>
      <a class="btn btn-primary" data-toggle="modal" data-target="#delivengo-modal-signin">
        <i class="icon"></i>
        {l s='Connect Delivengo account' mod='delivengo'}
      </a>
    {/if}
  </div>
</div>
