{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<div class="form-group">
  <div class="col-lg-12">
    <a class="btn btn-xl btn-primary"
       href="{$link->getAdminLink('AdminDelivengoConfiguration')|escape:'html':'utf-8'}&newDelivengoAddress&delivengo_tab=address-book"
       id="add-new-address">
      <i class="icon-plus-sign"></i> {l s='Add a new address' mod='delivengo'}</a>
  </div>
  <div class="clearfix"></div>
  {foreach $delivengo_addresses as $addr}
    <div id="module_form" class="defaultForm form-horizontal col-lg-4">
      <div class="panel" id="fieldset_0">
        <div class="panel-heading">
          <i class="icon-envelope"></i>&nbsp;{$addr.social_reason|escape:'html':'utf-8'}
          -{$addr.city|escape:'html':'utf-8'}
          <input type="radio"
                 name="delivengo-default-address"
                 data-delivengo-address-id="{$addr.id_delivengo_address|intval}"
                 class="pull-right"
                 {if $data.configuration.defaultAddress == $addr.id_delivengo_address}checked{/if}/>
        </div>
        <div class="form-wrapper form-wrapper-view">
          <span class="pull-right default">{l s='by default' mod='delivengo'} </span>
          <p>
            {$addr.social_reason|escape:'html':'utf-8'}<br>
            {if $addr.full_name}{$addr.full_name|escape:'html':'utf-8'}<br>{/if}
            {$addr.address1|escape:'html':'utf-8'}<br>
            {if $addr.address2}{$addr.address2|escape:'html':'utf-8'}<br>{/if}
            {if $addr.postbox}{$addr.postbox|escape:'html':'utf-8'}<br>{/if}
            {if $addr.zipcode}{$addr.zipcode|escape:'html':'utf-8'}<br>{/if}
            {$addr.city|escape:'html':'utf-8'}<br>
          </p>

        </div>
        <div class="panel-footer">
          <a type="button"
             href="{$link->getAdminLink('AdminDelivengoConfiguration')|escape:'html':'utf-8'}&newDelivengoAddress&id_delivengo_address={$addr.id_delivengo_address|intval}&delivengo_tab=address-book"
             class="btn btn-default pull-right">
            <i class="process-icon-edit"></i> {l s='Edit address' mod='delivengo'}
          </a>
          <a type="button"
             href="{$link->getAdminLink('AdminDelivengoConfiguration')|escape:'html':'utf-8'}&deleteDelivengoAddress&id_delivengo_address={$addr.id_delivengo_address|intval}&delivengo_tab=address-book"
             class="btn btn-danger pull-right"
             onclick="return confirm('{l s='Are you sure to delete this address?' mod='delivengo'}');">
            <i class="process-icon-trash icon-trash"></i> {l s='Delete address' mod='delivengo'}
          </a>
        </div>
      </div>
    </div>
    {foreachelse}
    <div id="delivengo-no-addresses">
      <div class="alert alert-info">
        {l s='Please create your first address.' mod='delivengo'}
      </div>
    </div>
  {/foreach}
  <div class="clearfix"></div>
</div>
