{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<form class="form-horizontal"
      action="#"
      name="delivengo_config_form"
      id="delivengo-config-form"
      method="post"
      enctype="multipart/form-data">
  <ps-panel icon="icon-cogs" header="{l s='Configuration' mod='delivengo'}">
    <div class="row">
      <ps-alert-hint>
        <p>
          {l s='Log files are stored in the "logs" directory of the module and are available for a 3-month period each.' mod='delivengo'}
        </p>
        <p>
          {l s='Click' mod='delivengo'}
          <a target="_blank"
             href="{$link->getAdminLink('AdminDelivengoLogs', true, [], ['action' => 'downloadLogFile'])}">{l s='here' mod='delivengo'}</a>
          {l s='to download the last file.' mod='delivengo'}</p>
      </ps-alert-hint>
      <ps-switch label="{l s='Enable log files' mod='delivengo'}"
                 name="delivengoSettings[logsEnabled]"
                 yes="{l s='Yes' mod='delivengo'}"
                 no="{l s='No' mod='delivengo'}"
                 active="{$data.configuration.logsEnabled|escape:'htmlall':'utf-8'}">
      </ps-switch>
      <ps-select label="{l s='Label type' mod='delivengo'}"
                 name="delivengoSettings[labelType]"
                 fixed-width="xxl">
        {foreach $label_types as $id => $name}
          <option {if $id == $data.configuration.labelType}selected="selected"{/if}
                  value="{$id|escape:'htmlall':'utf-8'}">
            {$name|escape:'htmlall':'utf-8'}
          </option>
        {/foreach}
      </ps-select>
      <ps-switch label="{l s='Out of Europe (E.U) exportation ' mod='delivengo'}"
                 name="exportationMode"
                 yes="{l s='Yes' mod='delivengo'}"
                 no="{l s='No' mod='delivengo'}"
                 active="{$data.configuration.exportationMode|escape:'htmlall':'utf-8'}">
      </ps-switch>
      <input type="hidden" name="action" value="SaveConfigForm"/>
      <div class="delivengo-customs-documents">
        <h2 class="col-lg-offset-3"></h2>
        {include file="../_partials/customs_documents.tpl"}
        {include file="./_partials/customs-documents-other-fields.tpl"}
      </div>
      <h2 class="col-lg-offset-3">{l s='Webhook configuration' mod='delivengo'}</h2>
      <div class="alert alert-info">
        <p>
          {l s='To enable order status auto-updates, please configure the following URL in your Delivengo account, as explained here:' mod='delivengo'}
          <a href="https://www.assistance-mydelivengo.fr/documentation/webhook/" target="_blank">
            https://www.assistance-mydelivengo.fr/documentation/webhook/
          </a>
        </p>
      </div>

      <h4 class="col-lg-offset-3"><b>{l s='Informations tab' mod='delivengo'}</b></h4>
      <div class="form-group form-group-html">
        <label class="control-label col-lg-3">
          URL
        </label>
        <div class="col-lg-9">
          <p>{$webhook_url|escape:'htmlall':'utf-8'}</p>
        </div>
      </div>
      <div class="form-group form-group-html">
        <label class="control-label col-lg-3">
          {l s='Trigger event' mod='delivengo'}
        </label>
        <div class="col-lg-9">
          <p>Mise à jour du suivi des numéros</p>
        </div>
      </div>

      <h4 class="col-lg-offset-3"><b>{l s='HTTP Header tab' mod='delivengo'}</b></h4>
      <div class="form-group form-group-html">
        <label class="control-label col-lg-3">
          {l s='Name' mod='delivengo'}
        </label>
        <div class="col-lg-9">
          <p>PrestaShop-Hash</p>
        </div>
      </div>
      <div class="form-group form-group-html">
        <label class="control-label col-lg-3">
          {l s='Value' mod='delivengo'}
        </label>
        <div class="col-lg-9">
          <p>{$webhook_hash|escape:'htmlall':'utf-8'}</p>
        </div>
      </div>
    </div>
    <ps-panel-footer>
      <ps-panel-footer-submit title="{l s='Save' mod='delivengo'}"
                              icon="process-icon-save"
                              direction="right"
                              name="submitDelivengoConfigForm">
      </ps-panel-footer-submit>
    </ps-panel-footer>
  </ps-panel>
</form>