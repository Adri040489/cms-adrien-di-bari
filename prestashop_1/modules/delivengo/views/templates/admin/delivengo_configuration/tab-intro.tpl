{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<div class="row delivengo-background">
  <div class="col-lg-6 col-md-6">
    <h1>{l s='Delivengo easy' mod='delivengo'}</h1>
    <p>
      {l s='Simplify your preparation of shipments and benefit from attractive rates for your shipments of less than 2kg to' mod='delivengo'}
      <strong>{l s='the international with Delivengo Easy' mod='delivengo'}</strong>
    </p>
    <a target="_blank"
       href="https://easy.mydelivengo.laposte.fr/clients/creation-de-compte"
       class="btn btn-xl btn-primary btn-delivengo-left">
      {l s='I create my account Delivengo Easy' mod='delivengo'}
    </a>
    <div class="delivengo-steps">
      <img src="{$data.path.img|escape:'html':'utf-8'}intro/delivengo-1.png"/>
      <p><strong>{l s='To install and configure my module, I create my delivengo easy account' mod='delivengo'}</strong>
      </p>
      <ul>
        <li>{l s='I get the API key available in the section' mod='delivengo'}&nbsp;
          <a target="_blank"
             href="https://easy.mydelivengo.laposte.fr/mon-compte">{l s='My account' mod='delivengo'}</a>
        </li>
        <li>{l s='I create' mod='delivengo'}&nbsp;<strong>{l s='a sepa direct debit mandate' mod='delivengo'}</strong>&nbsp;{l s='in the my account payment method section to make my postage purchases' mod='delivengo'}
        </li>
      </ul>
      <p class="delivengo-step-content">{l s='' mod='delivengo'}</p>
    </div>
    <div class="delivengo-steps">
      <img src="{$data.path.img|escape:'html':'utf-8'}intro/delivengo-2.png"/>
      <p>
        <strong>{l s='I simplify my shipment preparations thanks to the Delivengo easy functionalities' mod='delivengo'}</strong>
      </p>
      <ul>
        <li>{l s='Access your address book to facilitate your preparations' mod='delivengo'}</li>
        <li>{l s='automatically prepare your customs formalities for your shipments outside the European Union' mod='delivengo'}</li>
        <li>{l s='Track your shipments in real time from the Prestashop back office' mod='delivengo'} </li>
      </ul>
      <p class="delivengo-step-content">{l s='' mod='delivengo'}</p>
    </div>
    <div class="delivengo-steps">
      <img src="{$data.path.img|escape:'html':'utf-8'}intro/delivengo-3.png"/>
      <p><strong>{l s='Drop off your shipments at the nearest postal contact point.' mod='delivengo'}</strong></p>
      <p class="delivengo-step-content">{l s='Be careful, packages can not be dropped off in street letter boxes or communal or local post office.' mod='delivengo'}</p>
    </div>
  </div>
  <div class="col-lg-6 col-md-6">
    <div class="delivengo-video-wrapper">
      <iframe width="350"
              height="250"
              src="https://www.youtube.com/embed/3-71U2U12Vs"
              frameborder="0"
              ballowfullscreen></iframe>
    </div>
  </div>
</div>
