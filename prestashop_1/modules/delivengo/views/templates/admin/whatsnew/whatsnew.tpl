{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<div id="delivengo-whatsnew">
  <h1>{l s='Delivengo v2.0.4' mod='delivengo'}</h1>
  <div>
    <p>
        {l s='From January 1st, 2021, to send parcels to the U.K. you should provide a valide EORI UK Number to generate a label. To do so, please complete your account in ' mod='delivengo'}
        <a target='_blank' href='https://mydelivengo.laposte.fr/'> MyDelivengo.fr </a> {l s='(My Account > Customs Documents).'  mod='delivengo'}&nbsp;{l s='The info will be synchronize to your PrestaShop back office on your first attempt to edit a label for the U.K.' mod='delivengo'}
    </p>
    <p>{l s='You can find this new parameter into the  back office module configuration part, tab "backoffice parameters".' mod='delivengo'}</p>
  </div>
</div>
