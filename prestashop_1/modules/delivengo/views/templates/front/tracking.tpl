{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

{extends file='page.tpl'}

{block name='page_title'}
  {l s='Delivengo Tracking' mod='delivengo'}
{/block}

{block name='page_content_container'}
  <div class="tracking-response page-content">
    <div class="alert alert-info">
      {if $errors}
        <p>{l s='Your shipment is in preparation, the tracking link is not available yet.' mod='delivengo'}</p>
        <p>{l s='You will be informed about the shipping progress in you order history as soon as your shipment will be handled.' mod='delivengo'}</p>
        <p>{l s='Please reload or check this page later.' mod='delivengo'}</p>
      {else}
        <p>{l s='An unexpected error occurred. Please try again.' mod='delivengo'}</p>
      {/if}
    </div>
  </div>
{/block}
