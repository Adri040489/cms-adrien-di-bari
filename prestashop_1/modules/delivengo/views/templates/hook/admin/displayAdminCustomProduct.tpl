{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<div class="row">
  <div class="col-md-12">
    <p class="subtitle">{l s='Please fill in the customs information of your product' mod='delivengo'}</p>
    <div class="row">
      <div class="form-group col-md-4">
        <label class="form-control-label">{l s='Hs code' mod='delivengo'}</label>
        <input name="hs_code"
               type="text"
               class="form-control"
               value="{$product_details->hs_code|escape:'html':'utf-8'}"/>
      </div>
    </div>
    <div class="row">
      <div class="form-group col-md-4">
        <label class="form-control-label">{l s='Country origin' mod='delivengo'}</label>
        <select class="form-control" name="country_origin">
          <option value="0">{l s='-- Please select a country --' mod='delivengo'}</option>
          {foreach $countries as $country}
            <option value="{$country['id_country']|intval}"
                    {if $product_details->id_country_origin == $country['id_country']}selected{/if}>
              {$country['name']|escape:'html':'utf-8'}
            </option>
          {/foreach}
        </select>
      </div>
    </div>
    <div class="row">
      <div class="form-group col-md-4">
        <label class="form-control-label">{l s='Short description' mod='delivengo'}</label>
        <input name="short_desc"
               class="form-control"
               maxLenght="64"
               value="{$product_details->short_desc|escape:'html':'utf-8'}"/>
      </div>
    </div>
  </div>
</div>
