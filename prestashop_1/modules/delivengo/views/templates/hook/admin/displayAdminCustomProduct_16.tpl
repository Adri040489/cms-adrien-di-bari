{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<div class="panel product-tab">
  {include file='./displayAdminCustomProduct.tpl'}
  <div class="panel-footer">
    <a href="{$link->getAdminLink('AdminProducts')|escape:'html':'utf-8'}"
       class="btn btn-default"><i class="process-icon-cancel"></i> {l s='Cancel' mod='delivengo'}</a>
    <button type="submit" name="submitAddproduct" class="btn btn-default pull-right">
      <i class="process-icon-save"></i> {l s='Save' mod='delivengo'}</button>
    <button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right">
      <i class="process-icon-save"></i> {l s='Save and stay' mod='delivengo'}</button>
  </div>
</div>
