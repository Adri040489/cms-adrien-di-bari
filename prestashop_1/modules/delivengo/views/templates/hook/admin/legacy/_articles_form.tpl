{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<div class="alert alert-info mt-3 delivengo-alert-articles" role="alert">
  <div class="alert-text">
    <p>{l s='You can select up to' mod='delivengo'}
      <strong>{l s='4 articles' mod='delivengo'}</strong>{l s=', within the limit of 2 KG.' mod='delivengo'}
    </p>
    <p>{l s='Packages should not be larger than 90 x 90 x 90 cm' mod='delivengo'}</p>
    <p>{l s='Select the lines you want to send. NB: you can edit rows in order to group products' mod='delivengo'}</p>
  </div>
</div>
<table class="table delivengo-products-table">
  <thead>
  <tr>
    <th></th>
    <th><span class="title_box">{l s='Product description' mod='delivengo'}</span></th>
    <th><span class="title_box">{l s='Quantity' mod='delivengo'}</span></th>
    <th>
      <span class="title_box">{l s='Net weight' mod='delivengo'}</span>
      <small class="text-muted">{l s='(grams)' mod='delivengo'}</small>
    </th>
    <th>
      <span class="title_box">{l s='Total price' mod='delivengo'}</span>
      <small class="text-muted">{l s='(tax excl.)' mod='delivengo'}</small>
    </th>
    <th><span class="title_box">{l s='Country of origin' mod='delivengo'}</span></th>
    <th><span class="title_box">{l s='HS Code' mod='delivengo'}</span></th>
  </tr>
  </thead>
  <tbody>
  {foreach $articles as $i => $article}
    <tr>
      <td class="delivengo-td-selection">
        <div class="md-checkbox md-checkbox-inline">
          <label>
            <input type="checkbox"
                   id="delivengo_order_detail_{$i|intval}"
                   name="delivengo_label_form[orderDetail][{$i|intval}][selected]"
                   value="{$i|intval}"
                   {if $i < $params.max_articles}checked="checked"{/if}
            >
            <i class="md-checkbox-control"></i>
          </label>
        </div>
      </td>
      <td class="delivengo-td-description">
        <input type="text"
               id="delivengo_product_name"
               name="delivengo_label_form[orderDetail][{$i|intval}][description]"
               value="{$article.description|escape:'html':'utf-8'}"
               class="form-control">
      </td>
      <td class="delivengo-td-quantity">
        <input type="text"
               id="delivengo_product_quantity"
               name="delivengo_label_form[orderDetail][{$i|intval}][quantity]"
               value="{$article.quantity|intval}"
               class="form-control">
      </td>
      <td class="delivengo-td-weight">
        <input type="text"
               id="delivengo_product_weight"
               name="delivengo_label_form[orderDetail][{$i|intval}][weight]"
               value="{$article.weight|floatval}"
               class="form-control">
      </td>
      <td class="delivengo-td-value">
        <input type="text"
               id="delivengo_product_price"
               name="delivengo_label_form[orderDetail][{$i|intval}][price]"
               value="{$article.price|floatval}"
               class="form-control">
      </td>
      <td class="delivengo-td-country">
        <select id="delivengo_product_country"
                name="delivengo_label_form[orderDetail][{$i|intval}][id_country]"
                class="custom-select">
          {foreach $countries as $country}
            <option {if $country.id_country == $article.originCountry}selected="selected"{/if}
                    value="{$country.id_country|intval}">{$country.name|escape:'html':'utf-8'}</option>
          {/foreach}
        </select>
      </td>
      <td class="delivengo-td-hscode">
        <input type="text"
               id="delivengo_product_hscode"
               name="delivengo_label_form[orderDetail][{$i|intval}][hscode]"
              value="{$article.hsCode|escape:'html':'utf-8'}"
               class="form-control">
      </td>
    </tr>
  {/foreach}
  </tbody>
</table>
