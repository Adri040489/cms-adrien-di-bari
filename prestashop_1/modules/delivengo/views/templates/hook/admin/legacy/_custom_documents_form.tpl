{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<ps-radios label="{l s='Shipment nature' mod='delivengo'}">
  <ps-radio name="delivengo_label_form[shipmentNature]"
            value="1"
            {if $settings.configuration.customsDocuments.shipmentNature == "gift"}checked="true"{/if}>{l s='Gift' mod='delivengo'}</ps-radio>
  <ps-radio name="delivengo_label_form[shipmentNature]"
            value="3"
            {if $settings.configuration.customsDocuments.shipmentNature == "commercial_sample"}checked="true"{/if}> {l s='Samples' mod='delivengo'} </ps-radio>
  <ps-radio name="delivengo_label_form[shipmentNature]"
            value="4"
            {if $settings.configuration.customsDocuments.shipmentNature == "merchandise_return"}checked="true"{/if}> {l s='Merchandise return' mod='delivengo'} </ps-radio>
  <ps-radio name="delivengo_label_form[shipmentNature]"
            value="6"
            {if $settings.configuration.customsDocuments.shipmentNature == "merchandise_sale"}checked="true"{/if}> {l s='Merchandise sale' mod='delivengo'} </ps-radio>
  <ps-radio name="delivengo_label_form[shipmentNature]"
            value="5"
            {if $settings.configuration.customsDocuments.shipmentNature == "other"}checked="true"{/if}> {l s='Other' mod='delivengo'} </ps-radio>
</ps-radios>
<ps-input-text name="delivengo_label_form[observation]"
               label="{l s='Observation' mod='delivengo'}"
               size="10"
               help="{l s='In case of selling goods with a value < 300DTS : please provide details if the package is subject to quarantine or other restrictions.' mod='delivengo'}"
               value="{if isset($settings.configuration.customsDocuments.observation)}{$settings.configuration.customsDocuments.observation|escape:'htmlall':'utf-8'}{/if}"
               fixed-width="xxl">
</ps-input-text>
<ps-input-text name="delivengo_label_form[licenseNumber]"
               label="{l s='License number' mod='delivengo'}"
               size="10"
               idcounter="license-number"
               help="{l s='In case of selling goods with a value < 300DTS : When your shipment is accompanied by a license or certificate, please check the appropriate box and indicate its number. You must attach an invoice (in duplicate) to all commercial shipments.' mod='delivengo'}"
               value="{if isset($settings.configuration.customsDocuments.license_number)}{$settings.configuration.customsDocuments.license_number|escape:'htmlall':'utf-8'}{/if}"
               fixed-width="xxl">
</ps-input-text>
<ps-input-text name="delivengo_label_form[certificateNumber]"
               label="{l s='Certificate number' mod='delivengo'}"
               size="10"
               idcounter="certificate-number"
               help="{l s='In case of selling goods with a value < 300DTS : When your shipment is accompanied by a license or certificate, please check the appropriate box and indicate its number. You must attach an invoice (in duplicate) to all commercial shipments.' mod='delivengo'}"
               value="{if isset($settings.configuration.customsDocuments.certificate_number)}{$settings.configuration.customsDocuments.certificate_number|escape:'htmlall':'utf-8'}{/if}"
               fixed-width="xxl">
</ps-input-text>
