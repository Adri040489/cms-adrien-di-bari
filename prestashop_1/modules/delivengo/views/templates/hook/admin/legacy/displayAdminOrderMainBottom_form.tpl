{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<div class="row delivengo-admin-order-legacy">
  <div class="col-lg-12">
    <div class="panel">
      <div class="panel-heading">
        <i class="icon-truck"></i>
        {l s='Delivengo Shipping' mod='delivengo'}
      </div>
      <div class="row">
        <div class="js-delivengo-no-label info-block text-center">
          <p class="strong">{l s='You have not created a Delivengo Easy shipment yet' mod='delivengo'}</p>
          <p>
            <button class="btn btn-primary js-delivengo-show-label-form">{l s='Create a label' mod='delivengo'}</button>
          </p>
        </div>

        <div class="js-delivengo-label-form" style="display: none;">
          <form name="delivengo_label"
                class="defaultForm form-horizontal"
                action="#"
                method="post"
                enctype="multipart/form-data">
            <input type="hidden"
                   id="id_order"
                   name="delivengo_label_form[idOrder]"
                   class="form-control"
                   value="{$params.id_order|intval}">
            {include "./_address_form.tpl"}
            {if $params.need_customs_documents}
              <div class="info-block">
                <h2>{l s='Out of EU options' mod='delivengo'}</h2>
                <div class="delivengo-customs-documents">
                  <input type="hidden" name="delivengo_label_form[customs]" value="1"/>
                  {include "./_custom_documents_form.tpl"}
                </div>
              </div>
            {/if}
            <input type="hidden" name="id_order" value="{$params.id_order|intval}"/>
            <div class="delivengo-articles">
              {include "./_articles_form.tpl"}
            </div>
            <div class="delivengo-quotation">
              <button type="submit" class="btn btn-primary js-delivengo-estimation-submit">
                {l s='Request an estimation' mod='delivengo'}
              </button>
              <div class="js-delivengo-estimation delivengo-estimation"></div>
            </div>
            <div class="alert alert-danger mt-4 js-delivengo-error" style="display: none;"></div>
            <div class="js-delivengo-loading delivengo-loading" style="display:none;">
              <p>{l s='Loading...' mod='delivengo'}</p>
              <img src="{$settings.path.img|escape:'html':'utf-8'}icons/icon_carrier.png"/>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
