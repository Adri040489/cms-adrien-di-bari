{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<div class="form-group row type-choice">
  <label for="delivengo_shipping_address" class="form-control-label">
    {l s='Shipping address' mod='delivengo'}
  </label>
  <div class="col-sm">
    <select id="delivengo_shipping_address"
            name="delivengo_label_form[shipping_address_id]"
            class="custom-select">
      <optgroup label="{l s='Default address' mod='delivengo'}">
        {foreach $settings.shippingAddresses as $shippingAddress}
          {if $settings.configuration.defaultAddress == $shippingAddress.id}
            <option value="{$shippingAddress.id|intval}">
              {$shippingAddress.social_reason|escape:'html':'utf-8'|upper}
              - {$shippingAddress.address1|escape:'html':'utf-8'|upper}
              ({$shippingAddress.city|escape:'html':'utf-8'|upper})
            </option>
          {/if}
        {/foreach}
      </optgroup>
      <optgroup label="{l s='Other addresses' mod='delivengo'}">
        {foreach $settings.shippingAddresses as $shippingAddress}
          {if $settings.configuration.defaultAddress != $shippingAddress.id}
            <option value="{$shippingAddress.id|intval}">
              {$shippingAddress.social_reason|escape:'html':'utf-8'|upper}
              - {$shippingAddress.address1|escape:'html':'utf-8'|upper}
              ({$shippingAddress.city|escape:'html':'utf-8'|upper})
            </option>
          {/if}
        {/foreach}
      </optgroup>
    </select>
    <p class="mt-2">
      <strong>
        <a class="external-link"
           href="{$link->getAdminLink('AdminDelivengoConfiguration', true, [], ['delivengo_tab' => 'address-book'])}"
           target="_blank">
          {l s='Manage shipping addresses' mod='delivengo'}
        </a>
      </strong>
    </p>
  </div>
</div>
