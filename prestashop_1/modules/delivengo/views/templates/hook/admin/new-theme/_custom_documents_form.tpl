{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<div class="info-block">
  <h2>{l s='Out of EU shipment options' mod='delivengo'}</h2>
  <input type="hidden" name="delivengo_label_form[customs]" value="1"/>
  <div class="form-group row type-choice ">
    <label for="delivengo_shipment_nature" class="form-control-label ">
      {l s='Shipment nature' mod='delivengo'}
    </label>
    <div class="col-sm">
      <div id="delivengo_shipment_nature" class="">
        <div class="radio">
          <label class="">
            <input type="radio"
                   id="delivengo_shipment_nature_1"
                   name="delivengo_label_form[shipmentNature]"
                   value="1"
                   checked="checked">
            {l s='Gift' mod='delivengo'}
          </label>
        </div>
        <div class="radio">
          <label class="">
            <input type="radio"
                   id="delivengo_shipment_nature_2"
                   name="delivengo_label_form[shipmentNature]"
                   value="2"
                   checked="checked">
            {l s='Documents' mod='delivengo'}
          </label>
        </div>
        <div class="radio">
          <label class="">
            <input type="radio"
                   id="delivengo_shipment_nature_3"
                   name="delivengo_label_form[shipmentNature]"
                   value="3"
                   checked="checked">
            {l s='Samples' mod='delivengo'}
          </label>
        </div>
        <div class="radio">
          <label class="">
            <input type="radio"
                   id="delivengo_shipment_nature_4"
                   name="delivengo_label_form[shipmentNature]"
                   value="4"
                   checked="checked">
            {l s='Merchandise return' mod='delivengo'}
          </label>
        </div>
        <div class="radio">
          <label class="">
            <input type="radio"
                   id="delivengo_shipment_nature_5"
                   name="delivengo_label_form[shipmentNature]"
                   value="5"
                   checked="checked">
            {l s='Other' mod='delivengo'}
          </label>
        </div>
        <div class="radio">
          <label class="">
            <input type="radio"
                   id="delivengo_shipment_nature_6"
                   name="delivengo_label_form[shipmentNature]"
                   value="6"
                   checked="checked">
            {l s='Merchandise sale' mod='delivengo'}
          </label>
        </div>
      </div>
    </div>
  </div>


  <div class="form-group row type-text ">
    <label for="delivengo_observation" class="form-control-label ">
      {l s='Observation' mod='delivengo'}
    </label>
    <div class="col-sm">
      <input type="text"
             id="delivengo_observation"
             name="delivengo_label_form[observation]"
             class="form-control"
             value="">
      <small class="form-text">
        {l s='In case of selling goods with a value < 300DTS : please provide details if the package is subject to quarantine or other restrictions.' mod='delivengo'}
      </small>
    </div>
  </div>
  <div class="form-group row type-text ">
    <label for="delivengo_license_number" class="form-control-label ">
      {l s='License number' mod='delivengo'}
    </label>
    <div class="col-sm">
      <input type="text"
             id="delivengo_license_number"
             name="delivengo_label_form[licenseNumber]"
             class="form-control"
             value="">
      <small class="form-text">
        {l s='In case of selling goods with a value < 300DTS : When your shipment is accompanied by a license or certificate, please check the appropriate box and indicate its number. You must attach an invoice (in duplicate) to all commercial shipments.' mod='delivengo'}
      </small>
    </div>
  </div>
  <div class="form-group row type-text ">
    <label for="delivengo_certificate_number" class="form-control-label ">
      {l s='Certificate number' mod='delivengo'}
    </label>
    <div class="col-sm">
      <input type="text"
             id="delivengo_certificate_number"
             name="delivengo_label_form[certificateNumber]"
             class="form-control"
             value="">
      <small class="form-text">
        {l s='In case of selling goods with a value < 300DTS : When your shipment is accompanied by a license or certificate, please check the appropriate box and indicate its number. You must attach an invoice (in duplicate) to all commercial shipments.' mod='delivengo'}
      </small>
    </div>
  </div>
</div>
