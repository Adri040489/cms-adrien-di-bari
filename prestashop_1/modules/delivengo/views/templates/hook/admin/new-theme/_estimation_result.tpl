{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<div class="card-body">
  <h2 class="mb-3">{l s='Estimation' mod='delivengo'}</h2>
  <p class="m-0">
    <span>{l s='Product:' mod='delivengo'}</span>
    <span class="font-weight-bold">{$produit.libelle|escape}</span>
  </p>
  <p class="m-0">
    <span>{l s='Price (tax incl.):' mod='delivengo'}</span>
    <span class="font-weight-bold">{$produit.prix_ttc|floatval|string_format:'%.2f'} EUR</span>
  </p>
  <p class="mt-3">
    <button type="submit"
            class="btn btn-primary js-delivengo-label-submit">
      {l s='Pay & Create a label' mod='delivengo'}
    </button>
  </p>
</div>
