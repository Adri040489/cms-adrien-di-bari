{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<div class="delivengo-admin-order-new-theme">
  <div class="card">
    <h3 class="card-header">{l s='Delivengo Shipping' mod='delivengo'}</h3>
    <div class="card-body">
      <div class="js-delivengo-no-label info-block">
        <div class="row">
          <div class="col-md text-center">
            <p class="font-weight-bold">{l s='You have not created a Delivengo easy shipment yet' mod='delivengo'}</p>
            <p class="m-2">
              <button class="btn btn-primary js-delivengo-show-label-form">{l s='Create a label' mod='delivengo'}</button>
            </p>
          </div>
        </div>
      </div>
      <div class="js-delivengo-label-form" style="display: none;">
        <form name="delivengo_label" method="post" action="" class="form-horizontal">
          <input type="hidden"
                 id="id_order"
                 name="delivengo_label_form[idOrder]"
                 class="form-control"
                 value="{$params.id_order|intval}">
          <div class="card-block row">
            <div class="col-md-12">
              {include file="./_address_form.tpl"}
              {if $params.need_customs_documents}
                {include file="./_custom_documents_form.tpl"}
              {/if}
              {include file="./_articles_form.tpl"}
              <div class="row">
                <div class="col-6 offset-3 text-center">
                  <button type="submit"
                          class="btn btn-primary js-delivengo-estimation-submit">{l s='Request an estimation' mod='delivengo'}</button>
                  <div class="card mt-4 text-left js-delivengo-estimation" style="display: none"></div>
                </div>
              </div>
              <div class="alert alert-danger mt-4 js-delivengo-error" style="display: none;"></div>
              <div class="js-delivengo-loading delivengo-loading" style="display: none;">
                <p>{l s='Loading...' mod='delivengo'}</p>
                <img src="{$settings.path.img|escape:'html':'utf-8'}icons/icon_carrier.png"/>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
