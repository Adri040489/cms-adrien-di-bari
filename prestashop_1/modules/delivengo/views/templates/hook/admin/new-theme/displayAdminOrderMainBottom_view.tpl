{*
* 2020 Delivengo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0).
* It is also available through the world-wide-web at this URL: https://opensource.org/licenses/AFL-3.0
*
* @author    Delivengo
* @copyright 2020 Delivengo
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<div id="delivengo-module" class="delivengo-admin-order-new-theme">
  <div class="card">
    <h3 class="card-header">{l s='Delivengo Shipping' mod='delivengo'}</h3>
    <div class="card-body">
      <div class="delivengo-admin-logo">
        <img src="{$settings.path.img|escape:'html':'utf-8'}logo-easy-2x.png">
      </div>
      {if $pli.numero}
        <div class="card">
          <div class="card-body">
            <div class="row text-center label-info">
              <div class="col">
                {if $pli.tracking.url}
                  <b class="tracking-url">
                    <a href="{$pli.tracking.url|escape:'html':'utf-8'}" target="_blank">
                      {$pli.numero|escape:'html':'utf-8'}
                    </a>
                  </b>
                {else}
                  <b class="tracking-url">
                    {$pli.numero|escape:'html':'utf-8'}
                  </b>
                  <br/>
                  <i>{l s='Tracking link will be available soon. Please refresh again later.' mod='delivengo'}</i>
                {/if}
              </div>
              <div class="col">
                <b>{l s='Status' mod='delivengo'}</b><br/>
                <span>{$pli.statut.libelle|escape:'html':'utf-8'}</span>
              </div>
              <div class="col">
                <b>{l s='Date' mod='delivengo'}</b><br/>
                <span>{$pli.statut.date|date_format:'d-m-Y (H:i:s)'}</span>
              </div>
            </div>
          </div>
        </div>
      {else}
        <div class="alert alert-info">
          <p>{l s='In order to...' mod='delivengo'}</p>
          <p>{l s='Please download the label' mod='delivengo'}</p>
        </div>
      {/if}

      <div class="label-details">
        <div class="mt-2 info-block text-center">
          <div class="row">
            <div class="col-md">
              <strong>{l s='Label & Customs documents' mod='delivengo'}</strong>
              {if $settings.configuration.labelType == 'PDF_A4'}
                <p class="text-muted mt-2">
                  <i>{l s='(choose where to place the label on the A4 paper)' mod='delivengo'}</i></p>
                <div class="m-2 label-position">
                  <div>
                    <a target="_blank"
                       href="{$link->getAdminLink('AdminDelivengoAjax', true, [], ['action' => 'downloadLabel', 'id_envoi' => $envoi.id|intval, 'label_position' => 1])}"
                       class="btn btn-primary">1
                    </a>
                    <a target="_blank"
                       href="{$link->getAdminLink('AdminDelivengoAjax', true, [], ['action' => 'downloadLabel', 'id_envoi' => $envoi.id|intval, 'label_position' => 2])}"
                       class="btn btn-primary">2
                    </a>
                  </div>
                  <div>
                    <a target="_blank"
                       href="{$link->getAdminLink('AdminDelivengoAjax', true, [], ['action' => 'downloadLabel', 'id_envoi' => $envoi.id|intval, 'label_position' => 3])}"
                       class="btn btn-primary">3
                    </a>
                    <a target="_blank"
                       href="{$link->getAdminLink('AdminDelivengoAjax', true, [], ['action' => 'downloadLabel', 'id_envoi' => $envoi.id|intval, 'label_position' => 4])}"
                       class="btn btn-primary">4
                    </a>
                  </div>
                </div>
              {else}
                <p class="mt-4 download-link">
                  <a target="_blank"
                     href="{$link->getAdminLink('AdminDelivengoAjax', true, [], ['action' => 'downloadLabel', 'id_envoi' => $envoi.id|intval])}"
                     class="btn btn-primary">
                    {l s='Download label' mod='delivengo'}
                  </a>
                </p>
              {/if}
              <div class="js-delivengo-actions delivengo-actions">
                <strong>{l s='Label deletion' mod='delivengo'}</strong>
                <p class="text-muted"><i>{l s='(be careful, label has already been invoiced)' mod='delivengo'}</i></p>
                <a data-id-delivengo-label="{$id_delivengo_label|intval}"
                   title="{l s='Delete label' mod='delivengo'}"
                   class="btn btn-danger js-delivengo-delete-label"
                   href="#">
                  <i class="icon-trash"></i>
                  {l s='Delete label' mod='delivengo'}
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="mt-2 shipment-details">
          <div class="row">
            <div class="col-md">
              <p><strong>{l s='Shipment details' mod='delivengo'}</strong></p>
              <p class="m-2">{l s='Destination' mod='delivengo'} : {$pli.destinataire.pays|escape:'html':'utf-8'}</p>
              <p class="m-2">{l s='Procut' mod='delivengo'} : {$pli.produit.libelle}</p>
              <p class="m-2">{l s='Price' mod='delivengo'} : {$pli.produit.prix_ttc|floatval|string_format:'%.2f'} EUR</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
