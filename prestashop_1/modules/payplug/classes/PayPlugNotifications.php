<?php
/**
 * 2013 - 2021 PayPlug SAS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0).
 * It is available through the world-wide-web at this URL:
 * https://opensource.org/licenses/osl-3.0.php
 * If you are unable to obtain it through the world-wide-web, please send an email
 * to contact@payplug.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PayPlug module to newer
 * versions in the future.
 *
 * @author    PayPlug SAS
 * @copyright 2013 - 2021 PayPlug SAS
 * @license   https://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *  International Registered Trademark & Property of PayPlug SAS
 */

namespace PayPlug\classes;

use Address;
use Db;
use Cart;
use Configuration;
use Context;
use Country;
use Currency;
use Customer;
use Exception;
use Language;
use Message;
use Order;
use OrderHistory;
use Shop;
use Tools;
use Validate;

/**
 * Class PayPlugNotifications
 * Use for treat notification from Payplug API
 */
class PayPlugNotifications
{
    private $amountCurrencyClass;
    public $api_key;
    public $cart = null;
    public $except;
    public $flag;
    public $is_oney = false;
    public $is_installment = false;
    public $is_deferred = false;
    public $key;
    public $lock_key = null;
    public $logger;
    public $order = null;
    private $orderClass;
    public $order_states = [];
    public $payment = null;
    public $payplug;
    private $plugin;
    public $resource;
    public $resp;
    public $sandbox;
    public $type;

    public function __construct()
    {
        $this->setConfig();
    }

    /**
     * @descrition Check if the resource allow to save the payment card
     * @return bool
     */
    private function canSaveCard()
    {
        $this->logger->addLog('Notification: canSaveCard');
        $can_save_card = $this->is_installment ? false : true;

        return $can_save_card && (
            $this->payment->save_card
                || (
                    $this->payment->card->id
                    && $this->payment->hosted_payment
                )
        );
    }

    /**
     * @descrition Check if the payment resource can be treated
     */
    private function checkIsValidPaymentResource()
    {
        if (!$this->payment->is_paid && !$this->is_deferred) {
            $this->logger->addLog('The transaction is not paid yet.');
            $this->logger->addLog('No action will be done.');
            $this->exitProcess('The transaction is not paid.');
        }
    }

    /**
     * @descrition Dispatch the payment to create or update the relative order
     * @throws \PrestaShopDatabaseException
     * @throws \PrestaShopException
     */
    private function dispatchPayment()
    {
        $this->logger->addLog('Notification: dispatchPayment');
        $id_order = Order::getOrderByCartId($this->cart->id);
        if ($id_order) {
            $this->order = new Order((int)$id_order);
            if (!Validate::isLoadedObject($this->order)) {
                $this->exitProcess('Order cannot be loaded.', 500);
            }
            $this->processUpdateOrder();
        } else {
            $this->processCreateOrder();
        }
    }

    /**
     * @description Entry point to treat the notification
     * @param string $str
     * @param int $http_code
     */
    private function exitProcess($str = '', $http_code = 200)
    {
        $this->logger->addLog('Notification: exitProcess');
        if ($str) {
            $this->logger->addLog($str);
        }
        if ($this->lock_key) {
            if (!PayplugLock::deleteLockG2($this->lock_key)) {
                $this->logger->addLog('Lock cannot be deleted.', 'error');
            } else {
                $this->logger->addLog('Lock deleted.', 'debug');
            }
        }

        header($_SERVER['SERVER_PROTOCOL'] . ' ' . $http_code . ' ' . $str, true, $http_code);
        die;
    }

    /**
     * @descrition Get the new order state we should attribute to
     * @return array
     */
    private function getNewOrderState()
    {
        $this->logger->addLog('Notification: getNewOrderState');
        // Check if order is refused by oney
        if ($this->is_oney && $this->payment->failure) {
            $this->logger->addLog('NewOrderState: cancelled');
            return [
                'valid' => false,
                'status' => 'cancelled'
            ];
        }

        // CHeck if payment capture is expired
        if ($this->is_deferred && ($this->payment->authorization->expires_at - time()) <= 0) {
            $this->logger->addLog('NewOrderState: expired');
            return [
                'valid' => false,
                'status' => 'expired'
            ];
        }

        // Check if payment has failure
        if ($this->payment->failure) {
            $this->logger->addLog('NewOrderState: error');
            return [
                'valid' => false,
                'status' => 'error'
            ];
        }

        $this->logger->addLog('NewOrderState: paid');
        return [
            'valid' => true,
            'status' => 'paid'
        ];
    }

    /**
     * @description Get the resource from the notification
     */
    private function getResource()
    {
        $this->logger->addLog('Notification: getResource');
        $body = Tools::file_get_contents('php://input');

        try {
            $resource = json_decode($body);
            $this->api_key = (bool)$resource->is_live ?
                Configuration::get('PAYPLUG_LIVE_API_KEY') :
                Configuration::get('PAYPLUG_TEST_API_KEY');
            ApiClass::setSecretKey($this->api_key);
            $this->resource = \Payplug\Notification::treat($body);
            $this->logger->addLog('Resource ID: ' . $this->resource->id);
        } catch (\Payplug\Exception\UnknownAPIResourceException $exception) {
            $this->exitProcess($exception->getMessage(), 500);
        }
    }

    /**
     * @throws \PrestaShopDatabaseException
     * @throws \PrestaShopException
     */
    private function processCreateOrder()
    {
        $this->logger->addLog('Notification: processCreateOrder');

        if (isset($this->resource->failure) && $this->resource->failure !== null) {
            $this->logger->addLog('The payment has failed.');
            $this->exitProcess('No treatment because payment has failed.');
        }

        $is_paid = $this->resource->is_paid;

        if ($this->is_installment) {
            $installment = new PPPaymentInstallment($this->resource->installment_plan_id);
            $first_payment = $installment->getFirstPayment();
            if ($first_payment->isDeferred()) {
                $order_state = $this->order_states['auth'];
            } else {
                $order_state = $this->order_states['paid'];
            }

            $amount = 0;
            $installment = InstallmentClass::retrieveInstallment($this->resource->installment_plan_id);
            foreach ($installment->schedule as $schedule) {
                $amount += (int)$schedule->amount;
            }

            $transaction_id = $this->resource->installment_plan_id;
        } else {
            //We can't treat Oney pending IPN anymore because it's sent with no reason
            if ($this->is_oney && !$is_paid) {
                $order_state = $this->order_states['oney'];
            } elseif ($this->is_deferred && !$is_paid) {
                $order_state = $this->order_states['auth'];
            } else {
                $order_state = $this->order_states['paid'];
            }

            $amount = $this->payment->amount;
            $transaction_id = $this->payment->id;
        }

        $extra_vars = [
            'transaction_id' => $transaction_id
        ];

        $amount = $this->amountCurrencyClass->convertAmount($amount, true);

        $currency = (int)$this->cart->id_currency;
        try {
            $customer = new Customer((int)$this->cart->id_customer);
        } catch (Exception $exception) {
            $this->logger->addLog(
                'Customer cannot be loaded: ' . $exception->getMessage(),
                'error'
            );
            $this->exitProcess($exception->getMessage(), 500);
        }
        if (!Validate::isLoadedObject($customer)) {
            $this->logger->addLog('Customer cannot be loaded.', 'error');
            $this->exitProcess('Customer cannot be loaded.', 500);
        }

        /*
             * For some reasons, secure key form cart can differ from secure key from customer
             * Maybe due to migration or Prestashop's Update
             */
        $secure_key = false;
        if (isset($customer->secure_key) && !empty($customer->secure_key)) {
            if (isset($this->cart->secure_key)
                && !empty($this->cart->secure_key)
                && $this->cart->secure_key !== $customer->secure_key
            ) {
                $secure_key = $this->cart->secure_key;
                $this->logger->addLog('Secure keys do not match.', 'error');
                $this->logger->addLog(
                    'Customer Secure Key: ' . $customer->secure_key,
                    'error'
                );
                $this->logger->addLog('Cart Secure Key: ' . $this->cart->secure_key, 'error');
            } else {
                $secure_key = $customer->secure_key;
            }
        }

        $module_name = $this->payplug->displayName;
        if ($this->is_oney) {
            switch ($this->payment->payment_method['type']) {
                case 'oney_x3_with_fees':
                    $module_name = $this->payplug->l('Oney 3x');
                    break;
                case 'oney_x4_with_fees':
                    $module_name = $this->payplug->l('Oney 4x');
                    break;
                case 'oney_x3_without_fees':
                    $module_name = $this->payplug->l('notification.createOrder.oneyX3WithoutFees');
                    break;
                case 'oney_x4_without_fees':
                    $module_name = $this->payplug->l('notification.createOrder.oneyX4WithoutFees');
                    break;
                default:
                    break;
            }
        }

        // Create Order
        try {
            $cart_amount = (float)$this->cart->getOrderTotal(true, Cart::BOTH);

            if ($amount != $cart_amount) {
                $this->logger->addLog('Cart amount is different and may occurred an error');
                $this->logger->addLog('Cart amount:' . $cart_amount);
            }

            $this->logger->addLog('Order create with amount:' . $amount);
            $is_order_validated = $this->payplug->validateOrder(
                $this->cart->id,
                $order_state,
                $amount,
                $module_name,
                null,
                $extra_vars,
                $currency,
                false,
                $secure_key
            );
        } catch (Exception $exception) {
            $this->logger->addLog('Order cannot be validated: ' . $exception->getMessage(), 'error');
            $this->exitProcess($exception->getMessage(), 500);
        }
        if (!$is_order_validated) {
            $this->logger->addLog('Order cannot be validated.', 'error');
            $this->exitProcess('Order cannot be validated.', 500);
        }
        $this->logger->addLog('Order validated.');

        // Then load it
        $this->order = new Order($this->payplug->currentOrder);
        if (!Validate::isLoadedObject($this->order)) {
            $this->logger->addLog('Order cannot be loaded.', 'error');
            $this->exitProcess('Order cannot be loaded.', 500);
        }
        $this->logger->addLog('Order loaded.', 'debug');

        // Add payplug OrderPayment && Installment
        if ($this->is_installment) {
            InstallmentClass::addPayplugInstallment($this->resource->installment_plan_id, $this->order);
        } else {
            $data = [];
            $data['metadata'] = $this->payment->metadata;
            $data['metadata']['Order'] = $this->order->id;
            try {
                $this->logger->addLog('Payment patched.', 'debug');
                $this->payplug->patchPayment($this->payment->id, $data);
            } catch (Exception $exception) {
                $this->logger->addLog(
                    'Payment cannot be patched: ' . $exception->getMessage(),
                    'error'
                );
                $this->exitProcess($exception->getMessage(), 500);
            }

            if (!$this->orderClass->addPayplugOrderPayment($this->order->id, $this->payment->id)) {
                $this->logger->addLog(
                    'IPN Failed: unable to create order payment.',
                    'error'
                );
                $this->exitProcess('IPN Failed: unable to create order payment.', 500);
            } else {
                $this->logger->addLog('Order payment created.');
            }
        }

        // Add prestashop OrderPayment
        $order_payments = $this->order->getOrderPayments();
        if (!$order_payments) {
            $this->logger->addLog('Add new orderPayment for deferred - ' . count($order_payments), 'debug');
            $this->order->addOrderPayment($amount, null, $transaction_id);
        }

        // Check number of order using this cart
        $this->logger->addLog('Checking number of order passed with this id_cart');
        $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'orders` WHERE `id_cart` = ' . $this->cart->id;
        $res_nb_orders = Db::getInstance()->executeS($sql);
        if (!$res_nb_orders) {
            $this->logger->addLog(
                'No order can be found using id_cart ' . (int)$this->cart->id,
                'error'
            );
            $this->exitProcess('No order can be found using id_cart: ' . (int)$this->cart->id, 500);
        } elseif (count($res_nb_orders) > 1) {
            $this->logger->addLog(
                'There is more than one order using id_cart ' . (int)$this->cart->id,
                'error'
            );
            foreach ($res_nb_orders as $o) {
                $this->logger->addLog('Order ID : ' . $o['id_order'], 'debug');
            }
            $this->exitProcess('There is more than one order using id_cart: ' . (int)$this->cart->id, 500);
        } else {
            $this->logger->addLog('OK');
            $id_order = (int)$res_nb_orders[0]['id_order'];
        }

        // Check number of orderPayment using this cart
        $this->logger->addLog('Checking number of transaction validated for this order');
        $payments = $this->order->getOrderPayments();
        if (!$payments) {
            $this->logger->addLog(
                'No transaction can be found using id_order ' . (int)$id_order,
                'error'
            );
            $this->exitProcess('No transaction can be found using id_order: ' . (int)$id_order, 500);
        } elseif (count($payments) > 1) {
            $this->logger->addLog(
                'There is more than one transaction using id_order ' . (int)$id_order,
                'error'
            );
            $this->exitProcess('There is more than one transaction using id_order: ' . (int)$id_order, 500);
        } else {
            $this->logger->addLog('OK');
        }

        //
        $this->logger->addLog('Order created.');
        $this->exitProcess('Order created.');
    }

    /**
     * @description Treat the notification has an installment
     */
    private function processInstallment()
    {
        $this->logger->addLog('Notification: processInstallment');
        $this->logger->addLog('Installment ID: ' . $this->resource->id);
        $this->logger->addLog('Active : ' . (int)$this->resource->is_active);
        $this->exitProcess('Installment notification');
    }

    /**
     * @description Treat the notification has a payment
     */
    private function processPayment()
    {
        $this->logger->addLog('Notification: processPayment');

        // Set the payment
        $this->setPayment();
        if (!$this->payment) {
            $this->logger->addLog('Can\'t retrieve payment with the TEST and LIVE API Key');
            $this->exitProcess('Can\'t retrieve payment with the TEST and LIVE API Key', 500);
        }

        // Set the order state
        $this->setOrderStates();

        // Set the order state
        $this->setResourceProps(); // hydrate $resource_props

        // Check the payment ressource
        $this->checkIsValidPaymentResource();

        // Save card
        $this->processSaveCard();

        // Set cart from resource
        $this->setCartFromResource();

        // Set Context
        $this->setContext();

        // Set Lock
        $this->setLock();

        // Dipatch to the create|update process
        $this->dispatchPayment();
    }

    /**
     * @description Treat the notification has a refund
     */
    private function processRefund()
    {
        $this->logger->addLog('Notification: processRefund');
        $this->logger->addLog('Refund ID : ' . $this->resource->id);

        try {
            $this->payment = $this->payplug->retrievePayment($this->resource->payment_id);
            $this->setOrderStates();
        } catch (Exception $exception) {
            $this->logger->addLog('Payment cannot be retrieved: ' . $exception->getMessage(), 'error');
            $this->exitProcess($exception->getMessage(), 500);
        }

        if ($this->payment->installment_plan_id) {
            $installment = InstallmentClass::retrieveInstallment($this->payment->installment_plan_id);
            $meta = $installment->metadata;
        } else {
            $meta = $this->payment->metadata;
        }

        $is_totaly_refunded = $this->payment->is_refunded;
        if ($is_totaly_refunded) {
            $this->logger->addLog('TOTAL REFUND MODE');
            $cart_id = '';

            if (isset($meta['Cart'])) {
                $cart_id = (int)$meta['Cart'];
                $this->logger->addLog('Cart ID : ' . $cart_id);
            } elseif (isset($meta['ID Cart'])) {
                $cart_id = (int)$meta['ID Cart'];
                $this->logger->addLog('Cart ID : ' . $cart_id);
            } else {
                $this->logger->addLog(
                    'Can\'t be refunded, because there is an error during retrieving Cart ID.',
                    'error'
                );
                $this->exitProcess('Can\'t be refunded, because there is an error during retrieving Cart ID.', 500);
            }

            $this->cart = new Cart($cart_id);

            if (!Validate::isLoadedObject($this->cart)) {
                $this->logger->addLog('Cart cannot be loaded.', 'error');
                $this->logger->addLog('$cart_id : ' . $cart_id, 'debug');
                $this->exitProcess('Cart cannot be loaded.', 500);
            }

            $id_order = (int)Order::getOrderByCartId($cart_id);
            $this->order = new Order($id_order);
            $this->logger->addLog('Order ID : ' . $this->order->id);
            if (!Validate::isLoadedObject($this->order)) {
                $this->logger->addLog('Order cannot be loaded.', 'error');
                $this->exitProcess('Order cannot be loaded.', 500);
            }

            // Set lock Lock the process with id_cart from order object
            do {
                $cart_lock = PayplugLock::createLockG2($this->cart->id, 'ipn');
                if (!$cart_lock) {
                    $checkReturn = PayplugLock::check($this->cart->id);
                    if ($checkReturn == "stop ipn") {
                        $this->exitProcess('Lock cannot be created.', 500);
                    }
                } else {
                    $this->logger->addLog('Lock created');
                    $this->lock_key = $this->cart->id;
                }
            } while (!$cart_lock);

            $new_order_state = $this->order_states['refund'];
            $current_state = $this->orderClass->getCurrentOrderState($this->order->id);
            $this->logger->addLog('Current state: ' . $current_state);

            if ($current_state != $new_order_state) {
                $this->logger->addLog('Changing status to \'refunded\': ' . $new_order_state);
                $order_history = new OrderHistory();
                $order_history->id_order = $id_order;
                try {
                    $order_history->changeIdOrderState((int)$new_order_state, $this->order->id);
                    $order_history->save();
                    $this->exitProcess('Order state has been updated.');
                } catch (Exception $exception) {
                    $this->logger->addLog(
                        'Order history cannot be saved: ' . $exception->getMessage(),
                        'error'
                    );
                    $this->logger->addLog(
                        'Please check if order state ' . (int)$new_order_state . ' exists.',
                        'error'
                    );
                    $this->exitProcess($exception->getMessage(), 500);
                }
            } else {
                $this->logger->addLog('Order status is already \'refunded\'');
                $this->exitProcess('Order status is already \'refunded\'');
            }
        } else {
            $this->logger->addLog('PARTIAL REFUND');
            $this->exitProcess('PARTIAL REFUND');
        }
    }

    /**
     * @description Process the card saving for one click use
     */
    private function processSaveCard()
    {
        $this->logger->addLog('Notification: processSaveCard');
        if ($this->canSaveCard()) {
            $this->logger->addLog('[Save Card] Saving card...');
            $res_payplug_card = $this->plugin->getCard()->saveCard($this->payment);

            if (!$res_payplug_card) {
                $this->logger->addLog('[Save Card] Card cannot be saved.', 'error');

                if (!isset($this->payment->save_card)) {
                    $this->logger->addLog('[Save Card] $payment->save_card is not set', 'debug');
                }

                if (isset($this->payment->save_card) && $this->payment->save_card !== 1) {
                    $this->logger->addLog('[Save Card] $this->payment->save_card is set but not equal to 1', 'debug');
                }

                if (!isset($this->payment->card->id)) {
                    $this->logger->addLog('[Save Card] $this->payment->card->id is not set', 'debug');
                }

                if (isset($this->payment->card->id) && $this->payment->card->id == '') {
                    $this->logger->addLog('[Save Card] $this->payment->card->id is set but empty', 'debug');
                }

                if (!isset($this->payment->hosted_payment)) {
                    $this->logger->addLog('[Save Card] $this->payment->hosted_payment is not set', 'debug');
                }

                if ((isset($this->payment->hosted_payment)) && $this->payment->hosted_payment == '') {
                    $this->logger->addLog('[Save Card] $this->payment->hosted_payment is set but empty', 'debug');
                }
            } else {
                $this->logger->addLog('[Save Card] Card saved', 'debug');
            }
        }
    }

    /**
     * @description Process Payment Ressource when related Order is with a status Cancelled
     */
    private function processTypeCancelled()
    {
        $this->logger->addLog('Notification: processTypeCancelled');
        $this->exitProcess('Order is already set as cancelled.');
    }

    /**
     * @description Process Payment Ressource when related Order is with a status Error
     */
    private function processTypeError()
    {
        $this->logger->addLog('Notification: processTypeError');
        $this->exitProcess('Order is set as error.');
    }

    /**
     * @description Process Payment Ressource when related Order is with a status Expired
     */
    private function processTypeExpired()
    {
        $this->logger->addLog('Notification: processTypeExpired');
        $this->exitProcess('Order is already set as expired.');
    }

    /**
     * @description Process Payment Ressource when related Order is with a status Nothing
     */
    private function processTypeNothing()
    {
        $this->logger->addLog('Notification: processTypeNothing');
        $this->exitProcess('Order is configure to not be treat');
    }

    /**
     * @description Process Payment Ressource when related Order is with a status Paid
     */
    private function processTypePaid()
    {
        $this->logger->addLog('Notification: processTypePaid');
        $this->exitProcess('Order is already paid.');
    }

    /**
     * @description Process Payment Ressource when related Order is with a status Pending
     */
    private function processTypePending()
    {
        $this->logger->addLog('Notification: processTypePending');

        // Get the new order state
        $new_order_state = $this->getNewOrderState();
        $new_order_state_id = $this->order_states[$new_order_state['status']];
        if (!$new_order_state['valid']) {
            $this->updateOrderState($new_order_state_id);
        }

        // Check if payment is paid
        if (!$this->payment->is_paid) {
            $this->exitProcess('The payment is not paid yet.');
        }

        // Check if order amount is valid
        $is_valid_amount = (bool)$this->payment->is_paid;
        if ($this->is_installment) {
            $is_valid_amount = (bool)AmountCurrencyClass::checkAmountPaidIsCorrect(
                $this->payment->amount / 100,
                $this->order
            );
        }
        $this->logger->addLog('Is valid amount: ' . ($is_valid_amount ? 'ok' : 'ko'));

        if (!$is_valid_amount) {
            $message = new Message();
            $message->message = $this->payplug->l('The amount collected by PayPlug is not the same');
            $message->message .= $this->payplug->l(' as the total value of the order');
            $message->id_order = $this->order->id;
            $message->id_cart = $this->order->id_cart;
            $message->private = true;
            try {
                $message_saved = $message->save();
                $this->logger->addLog('Message saved: ' . ($message_saved ? 'ok' : 'ko'));
            } catch (Exception $exception) {
                $this->logger->addLog('The message cannot be saved: ' . $exception->getMessage(), 'error');
                $this->exitProcess($exception->getMessage(), 500);
            }

            $new_order_state_id = $this->order_states['error'];
            $this->updateOrderState($new_order_state_id);
        }

        // Get associated transaction
        $payplug_order_payments = $this->orderClass->getPayplugOrderPayments((int)$this->order->id);
        $order_payments = $this->order->getOrderPayments();

        // Check if the payment is related to the order with payplug order payment
        $related = false;
        if ($payplug_order_payments) {
            foreach ($payplug_order_payments as $payment) {
                if ($payment['id_payment'] == $this->payment->id) {
                    $related = true;
                }
            }
        } elseif ($order_payments) {
            foreach ($order_payments as $payment) {
                if ($payment->transaction_id == $this->payment->id) {
                    $related = true;
                }
            }
        }
        $this->logger->addLog('Is related: ' . ($related ? 'ok' : 'ko'));
        if (!$related) {
            $this->exitProcess('The payment is not related to this order.');
        }

        // Add prestashop OrderPayment if need
        $this->logger->addLog('Has order payments ' . ($order_payments ? 'ok' : 'ko'));
        if (!$order_payments) {
            $this->order->addOrderPayment($this->payment->amount / 100, null, $this->payment->id);
        }

        // If payment is paid, set the invoice
        if ($new_order_state_id == $this->order_states['paid']) {
            $this->order->setInvoice(true);
        }

        // Then update the order state
        $this->updateOrderState($new_order_state_id);
    }

    /**
     * @description Process Payment Ressource when related Order is with a status Refund
     */
    private function processTypeRefund()
    {
        $this->logger->addLog('Notification: processTypeRefund');
        $this->exitProcess('Order is already set as refunded.');
    }

    /**
     * @description Process Payment Ressource when related Order is with a status Nothing
     */
    private function processTypeUndefined()
    {
        $this->logger->addLog('Notification: processTypeUndefined');
        $id_order_state = $this->order->current_state;

        // check current order state type to create if it's empty
        $type = $this->plugin->getOrderState()->getType((int)$id_order_state);
        if ($type != $this->type) {
            $this->order_state->setType((int)$id_order_state, $this->type);
        }

        $this->exitProcess('The order state is not defined');
    }

    /**
     * @description Update order from the notification
     *
     * @param $id_order Identificer of the order to update
     */
    private function processUpdateOrder()
    {
        $this->logger->addLog('Notification: processUpdateOrder');

        $type = $this->plugin->getOrderState()->getType((int)$this->order->current_state);

        $this->logger->addLog('Current order state: ' . $this->order->current_state);
        $this->logger->addLog('Type: ' . $type);

        $this->type = $type ?: 'undefined';
        $method = 'processType' . Tools::ucfirst($this->type);
        $this->$method();
    }

    /**
     * @description Set $this->>cart in the DB from the resource id
     */
    private function setCartFromResource()
    {
        $this->logger->addLog('Notification: setCartFromResource');
        if ($this->is_installment) {
            $sql = 'SELECT `id_cart` 
                    FROM `' . _DB_PREFIX_ . 'payplug_payment` 
                    WHERE `id_payment` = "' . $this->payment->installment_plan_id . '"';
            $id_cart = Db::getInstance()->getValue($sql);

            if (!$id_cart) {
                $error_msg = 'The cart cannot be found with payment ID: ' . $this->payment->installment_plan_id;
                $this->exitProcess($error_msg, 500);
            }
        } else {
            $sql = 'SELECT `id_cart` 
                    FROM `' . _DB_PREFIX_ . 'payplug_payment` 
                    WHERE `id_payment` = "' . $this->resource->id . '"';
            $id_cart = Db::getInstance()->getValue($sql);

            if (!$id_cart) {
                $error_msg = 'The cart cannot be found with payment ID: ' . $this->resource->id;
                $this->exitProcess($error_msg, ($this->is_oney ? 242 : 500));
            }
        }

        $this->cart = new Cart($id_cart);
        if (!Validate::isLoadedObject($this->cart)) {
            $this->logger->addLog('The cart cannot be loaded with id ' . $id_cart, 'error');
            $this->exitProcess('The cart cannot be loaded.', 500);
        }
    }

    /**
     * @description Set the notification's global configuration
     *
     * @throws Exception
     */
    private function setConfig()
    {
        $this->key = microtime(true) * 10000;
        $this->flag = false;
        $this->except = null;
        $this->resp = [];
        $this->orderClass = new OrderClass();
        $this->payplug = new PayPlugClass();
        $this->plugin = $this->payplug->getPlugin();
        $this->amountCurrencyClass = $this->plugin->getAmountCurrencyClass();
        $this->sandbox = Configuration::get('PAYPLUG_SANDBOX_MODE');

        $this->setLogger();
        $this->getResource();
    }

    /**
     * @description Set the context of the order
     * @param $id_cart
     */
    private function setContext()
    {
        $this->logger->addLog('Notification: setContext');
        if (!isset($this->context)) {
            $this->context = Context::getContext();
        }

        $this->context->cart = $this->cart;
        $address = new Address((int)$this->cart->id_address_invoice);
        $this->context->country = new Country((int)$address->id_country);
        $this->context->customer = new Customer((int)$this->cart->id_customer);
        $this->context->language = new Language((int)$this->cart->id_lang);
        $this->context->currency = new Currency((int)$this->cart->id_currency);
        if (isset($this->cart->id_shop)) {
            $this->context->shop = new Shop($this->cart->id_shop);
        }

        $this->logger->addLog('Context setted');
    }

    /**
     *
     */
    private function setLock()
    {
        $this->logger->addLog('Notification: setLock');
        do {
            $cart_lock = PayplugLock::createLockG2($this->cart->id, 'ipn');
            if (!$cart_lock) {
                $checkReturn = PayplugLock::check($this->cart->id);
                if ($checkReturn == "stop ipn") {
                    $this->exitProcess('Lock cannot be created.', 500);
                }
            } else {
                $this->lock_key = $this->cart->id;
            }
        } while (!$cart_lock);
        $this->logger->addLog('Lock created');
    }

    /**
     * @description Set the logger
     */
    public function setLogger()
    {
        $this->logger = $this->plugin->getLogger();
        $this->logger->addLog('Notification: setLogger');
        $this->logger = $this->plugin->getLogger();
        $this->logger->setParams(['process' => 'notification']);
    }

    /**
     * @description Set the order state from configuration
     */
    private function setOrderStates()
    {
        $this->logger->addLog('Notification: setOrderStates');
        $state_addons = ($this->payment->is_live ? '' : '_TEST');
        $this->order_states = [
            'pending' => Configuration::get('PAYPLUG_ORDER_STATE_PENDING' . $state_addons),
            'paid' => Configuration::get('PAYPLUG_ORDER_STATE_PAID' . $state_addons),
            'error' => Configuration::get('PAYPLUG_ORDER_STATE_ERROR' . $state_addons),
            'auth' => Configuration::get('PAYPLUG_ORDER_STATE_AUTH' . $state_addons),
            'expired' => Configuration::get('PAYPLUG_ORDER_STATE_EXP' . $state_addons),
            'oney' => Configuration::get('PAYPLUG_ORDER_STATE_ONEY_PG' . $state_addons),
            'cancelled' => Configuration::get('PAYPLUG_ORDER_STATE_CANCELED'),
            'refund' => Configuration::get('PAYPLUG_ORDER_STATE_REFUND' . $state_addons)
        ];
    }

    /**
     *
     */
    private function setPayment()
    {
        $this->logger->addLog('Notification: setPayment');
        if (!$this->payment = $this->payplug->retrievePayment($this->resource->id)) {
            if ($this->sandbox) {
                $this->payplug->apiClass->initializeApi(false);
                if (!$this->payment = $this->payplug->retrievePayment($this->resource->id)) {
                    $this->logger->addLog('Can\'t retrieve payment with LIVE API Key.', 'debug');
                    $this->payplug->apiClass->initializeApi(true);
                    $this->payment = null;
                }
            } else {
                $this->payplug->apiClass->initializeApi(true);
                if (!$this->payment = $this->payplug->retrievePayment($this->resource->id)) {
                    $this->logger->addLog('Can\'t retrieve payment with the TEST API Key.', 'debug');
                    $this->payplug->apiClass->initializeApi(false);
                    $this->payment = null;
                }
            }
        }
    }

    /**
     *
     */
    private function setResourceProps()
    {
        $this->logger->addLog('Notification: setResourceProps');
        // Define if payment is oney resource
        $oney_payment_methods = [
            'oney_x3_with_fees',
            'oney_x4_with_fees',
            'oney_x3_without_fees',
            'oney_x4_without_fees'
        ];
        if (isset($this->payment->payment_method) && isset($this->payment->payment_method['type'])) {
            $this->is_oney = in_array($this->payment->payment_method['type'], $oney_payment_methods);
        }
        $this->logger->addLog('Notification: is_oney: ' . ($this->is_oney ? 'ok':'nok'));

        // Define if payment is deferred resource
        if (isset($this->payment->authorization) && !$this->is_oney) {
            $this->is_deferred = isset($this->payment->authorization->authorized_at)
                && $this->payment->authorization->authorized_at;
        }
        $this->logger->addLog('Notification: is_deferred: ' . ($this->is_deferred ? 'ok':'nok'));

        // Define if payment is from installment
        $this->is_installment = $this->payment->installment_plan_id ?: false;
        $this->logger->addLog('Notification: is_installment: ' . ($this->is_installment ? 'ok':'nok'));
    }

    /**
     * @description Entry point to treat the notification
     */
    public function treat()
    {
        //Notification identification
        $this->logger->addLog('Notification treatment and authenticity verification:');

        $this->logger->addLog('OK');

        if ($this->resource instanceof \Payplug\Resource\Payment) {
            $this->processPayment();
        } elseif ($this->resource instanceof \Payplug\Resource\Refund) {
            $this->processRefund();
        } elseif ($this->resource instanceof \Payplug\Resource\InstallmentPlan) {
            $this->processInstallment();
        }
    }

    /**
     * @param int $new_order_state
     * @throws \PrestaShopDatabaseException
     * @throws \PrestaShopException
     */
    private function updateOrderState($new_order_state = false)
    {
        if (!is_int($new_order_state) && !$new_order_state) {
            $this->exitProcess('Try to update order without valid order state id', 500);
        } elseif ($new_order_state == $this->order->current_state) {
            $this->exitProcess('The order is already with the status id: ' . $new_order_state, 200);
        }

        try {
            $order_history = new OrderHistory();
            $order_history->id_order = (int)$this->order->id;
            $order_history->changeIdOrderState((int)$new_order_state, $this->order->id);
            $order_history->save();
        } catch (Exception $exception) {
            $this->logger->addLog(
                'Order history cannot be saved: ' . $exception->getMessage(),
                'error'
            );
            $this->logger->addLog(
                'Please check if order state ' . (int)$new_order_state . ' exists.',
                'error'
            );
            $this->exitProcess($exception->getMessage(), 500);
        }

        $this->order->current_state = $order_history->id_order_state;
        try {
            $this->order->update();
        } catch (Exception $exception) {
            $this->logger->addLog('Order cannot be updated: ' . $exception->getMessage(), 'error');
            $this->exitProcess($exception->getMessage(), 500);
        }
        $this->exitProcess('Order updated with state id: ' . $new_order_state);
    }
}
