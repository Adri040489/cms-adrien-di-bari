<?php
/**
 * 2013 - 2021 PayPlug SAS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0).
 * It is available through the world-wide-web at this URL:
 * https://opensource.org/licenses/osl-3.0.php
 * If you are unable to obtain it through the world-wide-web, please send an email
 * to contact@payplug.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PayPlug module to newer
 * versions in the future.
 *
 * @author    PayPlug SAS
 * @copyright 2013 - 2021 PayPlug SAS
 * @license   https://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *  International Registered Trademark & Property of PayPlug SAS
 */

namespace PayPlug\src\repositories;

use DateTime;
use Exception;
use Payplug;
use Payplug\InstallmentPlan;
use Payplug\Payment;

class PaymentRepository extends Repository
{
    protected $payplug;
    private $apiPayment;
    private $cartSpecific;
    private $confSpecific;
    private $logger;
    private $paymentEntity;
    private $query;
    private $constant;

    public function __construct(
        $payplug,
        $cartSpecific,
        $confSpecific,
        $logger,
        $paymentEntity,
        $query,
        $constant
    ) {
        $this->payplug = $payplug;
        $this->cartSpecific = $cartSpecific;
        $this->confSpecific = $confSpecific;
        $this->logger = $logger;
        $this->paymentEntity = $paymentEntity;
        $this->query = $query;
        $this->constant = $constant;

        $this->logger->setParams(['process' => 'payment']);
    }

    /**
     * @description Return hashed cart with needed (and formatted) elements, ready to be hashed.
     * @param $paymentDetails
     * @return string | array
     */
    public function getHashedCart($paymentDetails)
    {
        if (!$paymentDetails
            || !is_array($paymentDetails)
            || !isset($paymentDetails['cart'])
            || !$paymentDetails['cart']
            || !is_object($paymentDetails['cart'])
        ) {
            return $this->returnPaymentError(
                ['name' => 'paymentDetails', 'value' => $paymentDetails],
                '[getHashedCart] $paymentDetails or cart is null, or $paymentDetails is not an array'
            );
        }
        $cartToHash = [];
        $products = $paymentDetails['cart']->getProducts();

        if (!$products) {
            return $this->returnPaymentError(
                ['name' => '$paymentDetails[\'cart\']', 'value' => $paymentDetails['cart']],
                '[getHashedCart] no product found'
            );
        }

        foreach ($products as $product) {
            $product['specific_prices'] = $product['features'] = $product['date_add'] = $product['date_upd'] = null;
            $cartToHash[] = array_map('strval', $product);
        }
        // For optimised / non optimised Oney + 3x / 4x to have a good hash ;-)
        if (isset($paymentDetails['oneyDetails'])) {
            $paymentDetails['paymentMethod'] .= $paymentDetails['oneyDetails'];
        }

        // Adding cart informationObjectModel
        $cartToHash[] = 'Cart $id_address_delivery: ' . $paymentDetails['cart']->id_address_delivery;
        $cartToHash[] = 'Cart $id_address_invoice: ' . $paymentDetails['cart']->id_address_invoice;
        $cartToHash[] = 'Cart $id_currency: ' . $paymentDetails['cart']->id_currency;
        $cartToHash[] = 'Cart $id_customer: ' . $paymentDetails['cart']->id_customer;
        $cartToHash[] = 'Cart $delivery_option: ' . $paymentDetails['cart']->delivery_option;

        // Adding cart amount to hash
        $cartToHash[] = 'Cart amount: ' . (float)$paymentDetails['cart']->getOrderTotal(true);

        return hash('sha256', $paymentDetails['paymentMethod'] . json_encode($cartToHash));
    }

    /**
     * @description Compare hash (sha256 on payment method + cart)
     * Create an other payment request if cart or payment method changed
     * and update payment table in consequence
     * @param array $paymentDetails
     * @return array
     */
    public function checkHash($paymentDetails)
    {
        if (!$paymentDetails
            || !is_array($paymentDetails)
            || !isset($paymentDetails['cartId'])
            || !$paymentDetails['cartId']
        ) {
            return $this->returnPaymentError(
                ['name' => 'paymentDetails', 'value' => $paymentDetails],
                '[checkHash] $paymentDetails or cartId is null, or $paymentDetails is not an array'
            );
        }

        $paymentStored = $this->checkPaymentTable($paymentDetails['cartId']);

        $cartHash = $this->getHashedCart($paymentDetails);

        if ($paymentStored['cart_hash'] === $cartHash
            &&
            ($paymentStored['payment_method'] == $paymentDetails['paymentMethod'])) {
            return [
                'result' => true,
                'paymentDetails' => $paymentDetails,
                'response' => 'OK. Comparaison result: Same hash and same payment method.'
            ];
        } else {
            // Create payment or installment
            try {
                $createPayment = $this->createPayment($paymentDetails);
            } catch (Payplug\Exception\ConfigurationNotSetException $e) {
                return $this->returnPaymentError(
                    ['name' => 'paymentDetails', 'value' => $paymentDetails],
                    '[checkHash -> createPayment] Error: ' . $e->getMessage()
                );
            }

            if ($createPayment['result'] && $createPayment['paymentDetails']) {
                $paymentDetails = $createPayment['paymentDetails'];
            } elseif (!$createPayment['result']) {
                return $this->returnPaymentError(
                    ['name' => 'paymentDetails', 'value' => $paymentDetails],
                    $createPayment['response']
                );
            }

            // Update payment table
            $updatePaymentTable = $this->updatePaymentTable($paymentDetails);
            if ($updatePaymentTable['result'] && $updatePaymentTable['paymentDetails']) {
                $paymentDetails = $updatePaymentTable['paymentDetails'];
            } elseif (!$updatePaymentTable['result']) {
                return $this->returnPaymentError(
                    ['name' => 'updatePaymentTable', 'value' => $updatePaymentTable],
                    $updatePaymentTable['response']
                );
            }

            return [
                'result' => true,
                'paymentDetails' => $paymentDetails,
                'response' => 'Payment created and updated successfully'
            ];
        }
    }

    /**
     * @description Return an error with some details in logger
     * @param array $element
     * @param null $errorMessage
     * @param string $level
     * @return array
     */
    public function returnPaymentError($element = [], $errorMessage = null, $level = 'error')
    {
        if (!$errorMessage || !is_string($errorMessage)) {
            $errorMessage = $this->l('[PaymentRepository] Error during payment creation process.');
        }

        $this->payplug->setPaymentErrorsCookie([
            $this->l('The transaction was not completed and your card was not charged.')
        ]);

        $this->logger->setParams(['process' => 'paymentRepository']);
        $this->logger->addLog($errorMessage, $level);

        if (!is_array($element) || empty($element)) {
            return [
                'result' => false,
                'response' => $errorMessage,
            ];
        }

        $element['value'] = json_encode($element['value']);
        $this->logger->addLog($element['name'] . ': ' . $element['value'], 'debug');

        return [
            'result' => false,
            $element['name'] => $element['value'],
            'response' => $errorMessage,
        ];
    }

    /**
     * @description Check if existing payment / installment in payment table
     * @param integer $idCart
     * @return bool|array
     */
    public function checkPaymentTable($idCart)
    {
        if (!$idCart || !is_int($idCart)) {
            return $this->returnPaymentError(
                ['name' => 'cart id', 'value' => $idCart],
                '[checkPaymentTable] Problem with $idCart parameter'
            );
        }

        $reqCheck = $this->query
            ->select()
            ->fields('*')
            ->from($this->constant->get('_DB_PREFIX_') . 'payplug_payment')
            ->where('id_cart = ' . (int)$idCart);

        $resCheck = $reqCheck->build();

        if (!$resCheck) {
            return false;
        } else {
            return end($resCheck);
        }
    }

    /**
     * @description Create payment / installment
     * @param array $paymentDetails
     * @return array
     */
    public function createPayment($paymentDetails)
    {
        if (!$paymentDetails
            || !$paymentDetails['paymentTab']
            || !$paymentDetails['paymentMethod']) {
            return $this->returnPaymentError(
                ['name' => 'paymentDetails', 'value' => $paymentDetails],
                '[createPayment] $paymentDetails or paymentTab or paymentMethod is null'
            );
        }

        if ($paymentDetails['paymentMethod'] == 'standard' && !$this->confSpecific->get('PAYPLUG_STANDARD')) {
            return $this->returnPaymentError(
                ['name' => 'Configuration::get', 'value' => $this->confSpecific->get('PAYPLUG_STANDARD')],
                '[createPayment] Try to create standard  payment with PAYPLUG_STANDARD disabled'
            );
        }

        if ($paymentDetails['paymentMethod'] !== 'installment') {
            try {
                if ($apiPayment = Payment::create($paymentDetails['paymentTab'])) {
                    $this->paymentEntity->setApiPayment($apiPayment);
                }
            } catch (Exception $e) {
                return $this->returnPaymentError(
                    ['name' => 'paymentDetails', 'value' => $paymentDetails],
                    '[createPayment] Exception. Unable to create payment. Error: ' . $e->getMessage()
                );
            }
        } else {
            try {
                $apiPayment = InstallmentPlan::create($paymentDetails['paymentTab']);
                $this->paymentEntity->setApiPayment($apiPayment);
            } catch (Exception $e) {
                return $this->returnPaymentError(
                    ['name' => 'paymentDetails', 'value' => $paymentDetails],
                    '[createPayment] Exception. Unable to installment plan. Error: ' . $e->getMessage()
                );
            }
        }

        $this->apiPayment = $this->paymentEntity->getApiPayment();

        if ($this->apiPayment->failure == true && !empty($this->apiPayment->failure->message)) {
            return $this->returnPaymentError(
                ['name' => 'paymentDetails', 'value' => $this->apiPayment],
                (string)$this->apiPayment->failure->message
            );
        }

        // We can now hydrate our params
        if (isset($this->apiPayment->id)) {
            $paymentDetails['paymentId'] = $this->apiPayment->id;
        }

        if (!$paymentDetails['paymentId']) {
            return $this->returnPaymentError(
                ['name' => 'paymentDetails', 'value' => $paymentDetails],
                '[createPayment ' . (string)$paymentDetails['paymentMethod'] . '] The payment id is null.'
            );
        }

        if (isset($this->apiPayment->hosted_payment->return_url)) {
            $paymentDetails['paymentReturnUrl'] = $this->apiPayment->hosted_payment->return_url;
        }

        if (!$paymentDetails['paymentReturnUrl']) {
            return $this->returnPaymentError(
                ['name' => 'paymentDetails', 'value' => $paymentDetails],
                '[createPayment] payment return URL is null.'
            );
        }

        if (($paymentDetails['paymentMethod'] !== 'installment') && ($this->apiPayment->authorization !== null)) {
            if ($this->apiPayment->authorization->authorized_at !== null) {
                $paymentDetails['authorizedAt'] = $this->apiPayment->authorization->authorized_at;
            }
        }

        if (isset($this->apiPayment->is_paid)) {
            $paymentDetails['isPaid'] = $this->apiPayment->is_paid;
        }

        if (isset($this->apiPayment->hosted_payment->payment_url)) {
            $paymentDetails['paymentUrl'] = $this->apiPayment->hosted_payment->payment_url;
        }

        return [
            'result' => true,
            'paymentDetails' => $paymentDetails,
            'response' => '[createPayment] Payment successfully created'
        ];
    }

    /**
     * @description Update hash and payment id in Payplug Payment Cart table
     * @param array $paymentDetails
     * @return array
     */
    public function updatePaymentTable($paymentDetails)
    {
        if (!$paymentDetails
            || !is_array($paymentDetails)
            || !isset($paymentDetails['cart'])
            || !$paymentDetails['cart']
        ) {
            return $this->returnPaymentError(
                ['name' => 'paymentDetails', 'value' => $paymentDetails],
                '[updatePaymentTable] $paymentDetails or cart is null, or $paymentDetails is not an array'
            );
        }

        $paymentDate = date('Y-m-d H:i:s');

        $cartHash = $this->getHashedCart($paymentDetails);

        if (isset($cartHash['result']) && !$cartHash['result']) {
            return $this->returnPaymentError(
                ['name' => 'paymentDetails', 'value' => $paymentDetails],
                '[updatePaymentTable] Problem with the getHashedCart method.'
            );
        }

        if (!$cartHash || !is_string($cartHash)) {
            return $this->returnPaymentError(
                ['name' => 'cartHash', 'value' => $cartHash],
                '[updatePaymentTable] $cartHash is null or not a string'
            );
        }

        $table = $this->constant->get('_DB_PREFIX_') . 'payplug_payment';

        $this->query
            ->update()
            ->table($table)
            ->set('id_payment =         \'' . $paymentDetails['paymentId'] . '\'')
            ->set('payment_method =     \'' . $paymentDetails['paymentMethod'] . '\'')
            ->set('payment_url =        \'' . $paymentDetails['paymentUrl'] . '\'')
            ->set('payment_return_url = \'' . $paymentDetails['paymentReturnUrl'] . '\'')
            ->set('cart_hash =          \'' . $cartHash . '\'')
            ->set('authorized_at =      \'' . $paymentDetails['authorizedAt'] . '\'')
            ->set('is_paid =            \'' . $paymentDetails['isPaid'] . '\'')
            ->set('date_upd =           \'' . $paymentDate . '\'')
            ->where('id_cart =          ' . (int)$paymentDetails['cartId']);

        try {
            if (!$this->query->build()) {
                return $this->returnPaymentError(
                    ['name' => 'paymentDetails', 'value' => $this->query],
                    '[updatePaymentTable] Unable to fetch the query on DB but no throw'
                );
            }
        } catch (Exception $e) {
            return $this->returnPaymentError(
                ['name' => 'paymentDetails', 'value' => $paymentDetails],
                '[updatePaymentTable] Unable to fetch the query on DB. Error: ' . $e->getMessage()
            );
        }

        return [
            'result' => true,
            'paymentDetails' => $paymentDetails,
            'response' => 'Update DB with new payment creation successfully'
        ];
    }

    /**
     * @description Check if payment created < 3 min in DB
     * @param $idCart
     * @return array|bool
     * @throws Exception
     */
    public function checkTimeoutPayment($idCart)
    {
        if (!$idCart || !is_int($idCart)) {
            return $this->returnPaymentError(
                ['name' => 'id cart', 'value' => $idCart],
                '[checkTimeoutPayment] Problem with $idCart parameter'
            );
        }

        $dateStored = $this->checkPaymentTable($idCart)['date_upd'];

        $date = new DateTime($dateStored);
        $date2 = new DateTime('now');

        if ($date->diff($date2)->y !== 0 ||
            $date->diff($date2)->d !== 0 ||
            $date->diff($date2)->h !== 0 ||
            $date->diff($date2)->i > 3) {
            // Plus de 3 minutes
            return false;
        } else {
            // Moins de 3 minutes
            return true;
        }
    }

    /**
     * @description Generate and return correct payment return url
     * @param array $paymentDetails
     * @return array
     */
    public function getPaymentReturnUrl($paymentDetails)
    {
        if (!$paymentDetails) {
            return $this->returnPaymentError(
                ['name' => 'paymentDetails', 'value' => $paymentDetails],
                '[getPaymentReturnUrl] $paymentDetails is null'
            );
        }

        $paymentStored = $this->checkPaymentTable($paymentDetails['cartId']);

        if (!$paymentStored) {
            return $this->returnPaymentError(
                ['name' => 'paymentStored', 'value' => false],
                '[getPaymentReturnUrl] $paymentStored is null or invalid'
            );
        }

        if (!$paymentDetails['paymentUrl']) {
            $paymentDetails['paymentUrl'] = $paymentStored['payment_url'];
        }

        if (!$paymentDetails['paymentReturnUrl']) {
            $paymentDetails['paymentReturnUrl'] = $paymentStored['payment_return_url'];
        }

        if (!$paymentDetails['authorizedAt']) {
            $paymentDetails['authorizedAt'] = $paymentStored['authorized_at'];
        }

        if (!$paymentDetails['isPaid']) {
            $paymentDetails['isPaid'] = $paymentStored['is_paid'];
        }

        if (!$paymentDetails['paymentUrl'] && !$paymentDetails['paymentReturnUrl']) {
            $err = '[getPaymentReturnUrl] $paymentDetails[\'paymentUrl\'] ';
            $err .= '&& $paymentDetails[\'paymentReturnUrl\'] are null';
            return $this->returnPaymentError(
                ['name' => 'paymentUrl', 'value' => false],
                $err
            );
        }

        switch ($paymentDetails['paymentMethod']) {
            case 'oneclick':
                $redirect = $paymentDetails['isPaid'];
                if (!$redirect && $paymentDetails['isDeferred']) {
                    $redirect = (bool)$paymentDetails['authorizedAt'];
                }

                $paymentReturnUrl = [
                    'result' => true,
                    'embedded' => true,
                    'redirect' => $redirect, // force `true` we are in 3DS 1
                    'return_url' => $redirect ?
                        $paymentDetails['paymentReturnUrl'] : $paymentDetails['paymentUrl'],
                ];
                break;
            case 'oney':
                $paymentReturnUrl = [
                    'result' => 'new_card',
                    'embedded' => false,
                    'redirect' => true,
                    'return_url' => $paymentDetails['paymentUrl'],
                ];
                break;
            case 'standard':
            case 'installment':
                $returnUrl = $paymentDetails['paymentUrl'] ? $paymentDetails['paymentUrl'] :
                    $paymentDetails['paymentReturnUrl'];
                $paymentReturnUrl = [
                    'result' => 'new_card',
                    'embedded' => $paymentDetails['isEmbedded'] && !$paymentDetails['isMobileDevice'],
                    'redirect' => $paymentDetails['isMobileDevice'],
                    'return_url' => $returnUrl,
                ];
                break;
            default:
                return $this->returnPaymentError(
                    ['name' => 'paymentStored', 'value' => false],
                    '[getPaymentReturnUrl] Invalid payment method given'
                );
        }

        return [
            'result' => true,
            'url' => $paymentReturnUrl,
            'response' => 'Return URL successfully generated'
        ];
    }

    /**
     * @description Insert payment with all details in table
     * @param array $paymentDetails
     * @return array|bool
     */
    public function insertPaymentTable($paymentDetails)
    {
        if (!$paymentDetails
            || !is_array($paymentDetails)
            || !isset($paymentDetails['paymentId'])
            || !$paymentDetails['paymentId']
        ) {
            return $this->returnPaymentError(
                ['name' => 'paymentDetails', 'value' => $paymentDetails],
                '[insertPaymentTable] $paymentDetail or paymentId is null, or $paymentDetail is not an array'
            );
        }

        $paymentDate = date('Y-m-d H:i:s');
        $cartHash = $this->getHashedCart($paymentDetails);

        if (isset($cartHash['result']) && !$cartHash['result']) {
            return $this->returnPaymentError(
                ['name' => 'paymentDetails', 'value' => $paymentDetails],
                '[insertPaymentTable] Problem with the getHashedCart method.'
            );
        }

        $this->query
            ->insert()
            ->into($this->constant->get('_DB_PREFIX_') . 'payplug_payment')
            ->fields('id_payment')->values($paymentDetails['paymentId'])
            ->fields('payment_method')->values($paymentDetails['paymentMethod'])
            ->fields('payment_url')->values($paymentDetails['paymentUrl'])
            ->fields('payment_return_url')->values($paymentDetails['paymentReturnUrl'])
            ->fields('id_cart')->values($paymentDetails['cartId'])
            ->fields('cart_hash')->values($cartHash)
            ->fields('authorized_at')->values((int)$paymentDetails['authorizedAt'])
            ->fields('is_paid')->values((int)$paymentDetails['isPaid'])
            ->fields('date_upd')->values($paymentDate);

        try {
            if (!$this->query->build()) {
                return $this->returnPaymentError(
                    ['name' => 'DB Query', 'value' => $this->query],
                    '[insertPaymentCart] Unable to flush DB (build method)'
                );
            }
        } catch (Exception $e) {
            return $this->returnPaymentError(
                ['name' => 'paymentDetails', 'value' => $paymentDetails],
                '[insertPaymentTable] Error: ' . $e->getMessage()
            );
        }

        return [
            'result' => true,
            'paymentDetails' => $paymentDetails,
            'response' => 'Insert data in DB successfully'
        ];
    }

    /**
     * @description Check if the payment is valid in API,
     * bc if cancelled, to recreate another one
     * @param array $paymentDetails
     * @return array
     * @throws Payplug\Exception\ConfigurationNotSetException
     */
    public function isValidApiPayment($paymentDetails)
    {
        if (!$paymentDetails
            || !is_array($paymentDetails)
            || !isset($paymentDetails['cartId'])
            || !$paymentDetails['cartId']
            || !is_int($paymentDetails['cartId'])
        ) {
            return $this->returnPaymentError(
                ['name' => 'paymentDetails', 'value' => $paymentDetails],
                '[isValidApiPayment] $paymentDetail or cartId is null, or $paymentDetail is not an array'
            );
        }

        try {
            $storedPayment = $this->checkPaymentTable($paymentDetails['cartId']);
        } catch (Exception $e) {
            return $this->returnPaymentError(
                ['name' => 'paymentDetails', 'value' => $paymentDetails],
                '[isValidApiPayment] Error: ' . $e->getMessage()
            );
        }

        if (!$storedPayment || !$storedPayment['payment_method'] || !$storedPayment['id_payment']) {
            return $this->returnPaymentError(
                ['name' => 'paymentDetails', 'value' => $paymentDetails],
                '[isValidApiPayment] $storedPayment or payment_method or id_payment is null'
            );
        }

        if ($storedPayment['payment_method'] == 'installment') {
            $retrievedInstallment = InstallmentPlan::retrieve($storedPayment['id_payment']);
            $firstSchedule = $retrievedInstallment->schedule[0]->payment_ids;
            /*
             * Try to see if the first schedule was cancelled
             */
            $storedPayment['id_payment'] = end($firstSchedule);
        }

        if (!$storedPayment['id_payment']) {
            return $this->returnPaymentError(
                ['name' => 'storedPayment', 'value' => $storedPayment],
                '[isValidApiPayment] $storedPayment[\'id_payment\'] is null'
            );
        }

        $retrievedPayment = Payment::retrieve($storedPayment['id_payment']);

        if ($retrievedPayment->failure) {
            $payment = $this->createPayment($paymentDetails);
            return $this->updatePaymentTable($payment['paymentDetails']);
        }

        return [
            'result' => true,
            'paymentDetails' => $paymentDetails,
            'response' => 'Valid API payment/installment'
        ];
    }
}
