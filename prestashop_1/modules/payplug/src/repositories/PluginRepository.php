<?php
/**
 * 2013 - 2021 PayPlug SAS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0).
 * It is available through the world-wide-web at this URL:
 * https://opensource.org/licenses/osl-3.0.php
 * If you are unable to obtain it through the world-wide-web, please send an email
 * to contact@payplug.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PayPlug module to newer
 * versions in the future.
 *
 * @author    PayPlug SAS
 * @copyright 2013 - 2021 PayPlug SAS
 * @license   https://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *  International Registered Trademark & Property of PayPlug SAS
 */

namespace PayPlug\src\repositories;

use PayPlug\classes\AmountCurrencyClass;
use PayPlug\classes\MyLogPHP;

use PayPlug\src\entities\CacheEntity;
use PayPlug\src\entities\OneyEntity;
use PayPlug\src\entities\PaymentEntity;
use PayPlug\src\entities\PluginEntity;
use PayPlug\src\entities\OrderStateEntity;

use PayPlug\src\specific\AddressSpecific;
use PayPlug\src\specific\AssignSpecific;
use PayPlug\src\specific\CarrierSpecific;
use PayPlug\src\specific\CartSpecific;
use PayPlug\src\specific\ConfigurationSpecific;
use PayPlug\src\specific\ConstantSpecific;
use PayPlug\src\specific\ContextSpecific;
use PayPlug\src\specific\CountrySpecific;
use PayPlug\src\specific\CurrencySpecific;
use PayPlug\src\specific\LanguageSpecific;
use PayPlug\src\specific\OrderStateSpecific;
use PayPlug\src\specific\ProductSpecific;
use PayPlug\src\specific\ShopSpecific;
use PayPlug\src\specific\ToolsSpecific;
use PayPlug\src\specific\ValidateSpecific;

class PluginRepository extends Repository
{
    protected $payplug;

    // Entities
    private $cacheEntity;
    private $oneyEntity;
    private $paymentEntity;
    private $plugin;
    private $order_state_entity;

    // Repositories & necessary classes
    private $amountCurrencyClass;
    private $apiClass;
    private $cache;
    private $card;
    private $hook;
    private $install;
//    private $installment;
    private $logger;
    private $myLogPhp;
    private $oney;
    private $order_state;
    private $orderClass;
    private $payment;
    private $query;
    private $sql;
    private $translate;

    // Specific classes
    private $address;
    private $assign;
    private $carrier;
    private $cart;
    private $configuration;
    private $constant;
    private $context;
    private $country;
    private $currency;
    private $language;
    private $order_state_specific;
    private $product;
    private $shop;
    private $tools;
    private $validate;
    private $constantSpecific;

    public function __construct($payplug = null)
    {
        $this->payplug = $payplug;
        $this->myLogPhp = new MyLogPHP();

        $this->setEntities();
        $this->setSpecific();
        $this->setRepositories();

        $this->plugin
            ->setApiClass($this->apiClass)
            ->setApiVersion('2019-08-06')
            ->setAddress($this->address)
            ->setAmountCurrencyClass($this->amountCurrencyClass)
            ->setCache($this->cache)
            ->setCard($this->card)
            ->setCarrier($this->carrier)
            ->setCart($this->cart)
            ->setConfiguration($this->configuration)
            ->setContext($this->context)
            ->setCountry($this->country)
            ->setCurrency($this->currency)
            ->setHook($this->hook)
            ->setInstall($this->install)
//            ->setInstallment($this->installment)
            ->setLogger($this->logger)
            ->setPayment($this->payment)
            ->setProduct($this->product)
            ->setOney($this->oney)
            ->setOrderClass($this->orderClass)
            ->setQuery($this->query)
            ->setSql($this->sql)
            ->setTools($this->tools)
            ->setTranslate($this->translate)
            ->setValidate($this->validate)
            ->setOrderState($this->order_state);
        $this->setEntity($this->plugin);
    }

    private function setEntities()
    {
        $this->cacheEntity = new CacheEntity();
        $this->oneyEntity = new OneyEntity();
        $this->paymentEntity = new PaymentEntity();
        $this->plugin = new PluginEntity();
        $this->order_state_entity = new OrderStateEntity();
    }

    private function setRepositories()
    {
        $this->amountCurrencyClass = new AmountCurrencyClass(
            $this->tools
        );
        $this->logger = new LoggerRepository();
        $this->query = new QueryRepository();
//        $this->installment = new InstallmentRepository();

        $this->card = new CardRepository(
            $this->configuration,
            $this->constant,
            $this->logger,
            $this->payplug,
            $this->query,
            $this->tools
        );
        $this->translate = new TranslationsRepository(
            $this->payplug,
            $this->tools
        );

        $this->sql = new SQLtableRepository(
            $this->query
        );

        $this->hook = new HookRepository(
            $this->payplug,
            $this->constant,
            $this->context,
            $this->tools
        );

        $this->cache    = new CacheRepository(
            $this->cacheEntity,
            $this->query,
            $this->configuration,
            $this->logger,
            $this->constant
        );

        $this->oney = new OneyRepository(
            $this->cache,
            $this->logger,
            $this->address,
            $this->cart,
            $this->carrier,
            $this->configuration,
            $this->context,
            $this->country,
            $this->currency,
            $this->tools,
            $this->validate,
            $this->oneyEntity,
            $this->myLogPhp,
            $this->payplug,
            $this->assign,
            $this->amountCurrencyClass
        );

        $this->order_state = new OrderStateRepository(
            $this->configuration,
            $this->constant,
            $this->language,
            $this->order_state_specific,
            $this->query,
            $this->tools,
            $this->validate,
            $this->myLogPhp
        );

        $this->payment = new PaymentRepository(
            $this->payplug,
            $this->cart,
            $this->configuration,
            $this->logger,
            $this->paymentEntity,
            $this->query,
            $this->constant
        );

        $this->install = new InstallRepository(
            $this->configuration,
            $this->constant,
            $this->context,
            $this->order_state,
            $this->order_state_entity,
            $this->order_state_specific,
            $this->shop,
            $this->sql,
            $this->tools,
            $this->validate,
            $this->payplug,
            $this->myLogPhp
        );
    }

    private function setSpecific()
    {
        $this->address = new AddressSpecific();
        $this->assign = new AssignSpecific();
        $this->carrier = new CarrierSpecific();
        $this->cart = new CartSpecific();
        $this->configuration = new ConfigurationSpecific();
        $this->constant = new ConstantSpecific();
        $this->context = new ContextSpecific();
        $this->country = new CountrySpecific();
        $this->currency  = new CurrencySpecific();
        $this->language = new LanguageSpecific();
        $this->order_state_specific = new OrderStateSpecific();
        $this->product = new ProductSpecific();
        $this->shop = new ShopSpecific();
        $this->tools = new ToolsSpecific();
        $this->validate = new ValidateSpecific();
    }
}
