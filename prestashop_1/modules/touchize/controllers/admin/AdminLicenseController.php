<?php
/**
 * 2018 Touchize Sweden AB.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to prestashop@touchize.com so we can send you a copy immediately.
 *
 *  @author    Touchize Sweden AB <prestashop@touchize.com>
 *  @copyright 2018 Touchize Sweden AB
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of Touchize Sweden AB
 */

/**
 * License controller.
 */

class AdminLicenseController extends BaseTouchizeController
{
    const INFO_TEMPLATE = 'info/license.tpl';

    /**
     * ~ constructor.
     */
    public function __construct()
    {
        $this->bootstrap = true;
        parent::__construct();
        $this->context->smarty->assign(array(
            'iso_code' => $this->context->language->iso_code
        ));
        $helper = new TouchizeAdminHelper();
        $this->licenseHelper = new TouchizeLicenseHelper();
        $helper->assignMenuVars();
        $this->fields_options = array(
            'enable' => array(
                'title' => $this->l('Enable Swipe-2-Buy'),
                'icon' => 'icon-power-off',
                'fields' => array(
                    'TOUCHIZE_ENABLED' => array(
                        'hint' => $this->l('Choose which devices Swipe-2-Buy should be enabled on.'),
                        'title' => $this->l('Enable on'),
                        'validation' => 'isGenericName',
                        'type' => 'radio',
                        'choices' => array(
                            3 => $this->l('Both mobile and tablet.'),
                            2 => $this->l('Only on tablet.'),
                            1 => $this->l('Only on mobile.'),
                            0 => $this->l('None (Disable and go to sandbox mode).'),
                        ),
                        'no_multishop_checkbox' => true
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'name' => 'SubmitEnabling'
                )
            ),
        );
    }
        /**
     * @return bool|ObjectModel
     */
    public function postProcess()
    {
        $ok = 3;
        if (Tools::isSubmit('SubmitEnabling')) {
            $option = (int)Tools::getValue('TOUCHIZE_ENABLED');
            $silent = false;
        } else {
            $option = Configuration::get('TOUCHIZE_ENABLED');
            $silent = true;
        }

        if (Tools::getIsset('TOUCHIZE_ENABLED')) {
            $_POST['TOUCHIZE_ENABLED'] =  $ok ? $option : 0;
        }
        Configuration::updateValue(
            'TOUCHIZE_ENABLED',
            $ok ? $option : 0
        );
        return parent::postProcess();
    }

    /**
     * AdminController::setMedia() override
     *
     * @see AdminController::setMedia()
     */
    public function setMedia($isNewTheme = false)
    {
        parent::setMedia();

        $this->context
            ->controller
            ->addCSS(
                _MODULE_DIR_.'touchize/views/css/touchize-admin.css'
            );
    }
}
