<?php
/**
 * 2018 Touchize Sweden AB.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to prestashop@touchize.com so we can send you a copy immediately.
 *
 *  @author    Touchize Sweden AB <prestashop@touchize.com>
 *  @copyright 2018 Touchize Sweden AB
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of Touchize Sweden AB
 */

/**
 * Product helper.
 */

class TouchizeFilterHelper extends TouchizeBaseHelper
{
    /**
     * [__construct description]
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getCategoryFilters($categoryId, $filter = null)
    {
        $products = null;
        $count = 0;

        Context::getContext()->controller->php_self = 'category';
        $_POST['id_category'] = $categoryId;
        $_POST['id_category_layered'] = $categoryId;
        if ($filter) {
            $_POST['selected_filters'] = $filter;
        }

        $blockLayered = Module::getInstanceByName('blocklayered');
        $selected_filters = $blockLayered->ajaxCall();

        //Extract the filters and map to our model
        $selected_filters = Tools::jsonDecode($selected_filters, true);
        $filters = $this->mapBlocklayeredData($selected_filters);

        $smartyProducts = $this->context->smarty->getVariable('products');
        $products = $smartyProducts->value;
        $smartyCount = $this->context->smarty->getVariable('nb_products');
        $count = $smartyCount->value;
        return array(
            'filters' => $filters,
            'products' => $products,
            'count' => $count
        );
    }

    private function mapBlocklayeredData($blData)
    {
        $filterOptions = array(
            'Type' => 'Filter',
            'Title' => $this->l('Apply Filters'),
            'ClearAllTitle' => $this->l('Clear all'),
            'Filters' => array()
        );
        if ($blData  && is_array($blData) && array_key_exists('filters', $blData) && is_array($blData['filters'])) {
            foreach ($blData['filters'] as $blEntry) {
                $entry = $this->mapBlocklayeredEntry($blEntry);
                if ($entry) {
                    array_push($filterOptions['Filters'], $entry);
                }
            }
        }
        return $filterOptions;
    }

    private function mapBlocklayeredEntry($blEntry)
    {
        $filter = array();
        if ($blEntry && is_array($blEntry) && array_key_exists('values', $blEntry) && is_array($blEntry['values'])) {
            if (array_key_exists('type_lite', $blEntry) && $blEntry['type_lite'] == 'category') {
                //Category filter does not work, not even in regular version if accessed as a reload of the url. Skip.
                return null;
            }
            $values = array();
            $filter['Title'] = $blEntry['name'];
            if (array_key_exists('slider', $blEntry) && $blEntry['slider']) {
                $filter['Type'] = 'min-max';
                if (array_key_exists('values', $blEntry) &&
                    is_array($blEntry['values']) &&
                    count($blEntry['values']) == 2) {
                    $filter['Min'] = $blEntry['min'];
                    $filter['Max'] = $blEntry['max'];
                    $filter['SetMin'] = $blEntry['values'][0];
                    $filter['SetMax'] = $blEntry['values'][1];
                    $filter['Url'] = '/' . $blEntry['type'] . '-';
                    $filter['Query'] = '/' . $blEntry['type'] . '-';
                    //There is an error in presta when ranges are in start url.
                    //PS uses "name" instead of "type" when creating the hash url.
                }
            } else {
                $filter['Type'] = 'checkbox';
                foreach ($blEntry['values'] as $blValue) {
                    if ($blValue && is_array($blValue)) {
                        if (array_key_exists('nbr', $blValue) && $blValue['nbr'] > 0) { //If there are items
                            $relativeUrl = TouchizeControllerHelper::getRelativeURL(
                                $blValue['link']
                            );
                            if (($tmp = strstr($relativeUrl, '#')) !== false) {
                                $query = '/' . Tools::substr($tmp, 1);
                            }
                            $values[] = array(
                                'Name' => $blValue['name'],
                                'Selected' =>  array_key_exists('checked', $blValue) && $blValue['checked'],
                                'Nbr' => $blValue['nbr'],
                                'Url' => $relativeUrl,
                                'Query' => $query
                            );
                        }
                    }
                }
            }
        }
        if (!empty($values)) {
            $filter['List'] = $values;
        }
        return !empty($filter) ? $filter : null;
    }
}
