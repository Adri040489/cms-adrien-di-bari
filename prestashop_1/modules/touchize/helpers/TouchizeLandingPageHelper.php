<?php
/**
 * 2019 Touchize Sweden AB.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to prestashop@touchize.com so we can send you a copy immediately.
 *
 *  @author    Touchize Sweden AB <prestashop@touchize.com>
 *  @copyright 2018 Touchize Sweden AB
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of Touchize Sweden AB
 */

/**
 * Landing page helper.
 */

class TouchizeLandingPageHelper extends TouchizeBaseHelper
{
    protected static $instance = null;
    protected static $categories = null;
    protected static $allowed_controllers = null;
    protected static $default_option = '[{"id":"best-sales","value":"Best sellers"}]';

    public static function run($isSetup = false)
    {
        self::instance()->context->controller->addJS(
            array(
                _MODULE_DIR_ . 'touchize/views/js/tagify.js'
            )
        );
        self::instance()->context->controller->addCSS(
            _MODULE_DIR_ . 'touchize/views/css/tagify.css'
        );
        Media::addJsDef(array('tagifyConfig' => array(
            'enforceWhitelist' => true,
            'whitelist' => $isSetup ? self::getActiveCategories() : self::getAllCategories(),
            'placehold$default_optioner' => self::instance()->l("Add categories"),
            'dropdown' => array(
                'maxItems' => 0,
                'enabled' => 0,
                'classname' => 'landingPageWhitelist',
                'highlightFirst' => true
            )
        )));
    }

    public static function getLandingPageIds()
    {
        $ids = array();
        foreach (self::getOption(true) as $category) {
            $ids[] = $category->id;
        }

        return implode(',', $ids);
    }

    public static function getOption($asArray = false)
    {
        $option = Configuration::get('TOUCHIZE_START_CATEGORY_ID');
        $categories = json_decode($option);
        if (!is_array($categories)) {
            $categories = array();
            foreach (explode(',', $option) as $id) {
                if (empty($id)) {
                    continue;
                }
                $categories[] = (object)array('id' => $id, 'value' => self::getCategoryName($id));
            }
            if (count($categories) !== 0) {
                $option = json_encode($categories);
            } else {
                $option = self::getDefaultOption();
            }
        }
        return $asArray ? $categories : $option;
    }

    public static function getDefaultOption()
    {
        return self::$default_option;
    }

    protected static function getCategoryName($id)
    {
        foreach (self::getActiveCategories() as $category) {
            if ($category->id == $id) {
                return $category->value;
            }
        }
        return $id;
    }

    protected static function getActiveCategories()
    {
        if (is_null(self::$categories)) {
            self::$categories = self::getAllowedControllers();
            $helperTreeCategories = new HelperTreeCategories('categories-treeview');
            $categories = $helperTreeCategories
                ->setChildrenOnly(true)
                ->getData();
            foreach ($categories as $c) {
                self::$categories[] = (object)array('id' => $c['id_category'], 'value' => $c['name']);
            }
        }

        return self::$categories;
    }

    protected static function getAllCategories()
    {
        if (is_null(self::$categories)) {
            self::$categories = self::getAllowedControllers();
            $categories = Category::getNestedCategories();
            foreach ($categories as $c) {
                self::processNode($c);
            }
        }

        return self::$categories;
    }

    protected static function processNode($category)
    {
        self::$categories[] = (object)array('id' => $category['id_category'], 'value' => $category['name']);
        if (isset($category['children'])) {
            foreach ($category['children'] as $child) {
                self::processNode($child);
            }
        }
    }

    protected static function getAllowedControllers()
    {
        if (is_null(self::$allowed_controllers)) {
            self::$allowed_controllers = array(
                (object)array('id' => 'prices-drop', 'value' => self::instance()->l('Specials')),
                (object)array('id' => 'best-sales', 'value' => self::instance()->l('Best sellers')),
                (object)array('id' => 'new-products', 'value' => self::instance()->l('New arrivals'))
            );
        }

        return self::$allowed_controllers;
    }

    public static function instance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}
