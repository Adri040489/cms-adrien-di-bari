<?php
/**
 * 2018 Touchize Sweden AB.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to prestashop@touchize.com so we can send you a copy immediately.
 *
 *  @author    Touchize Sweden AB <prestashop@touchize.com>
 *  @copyright 2018 Touchize Sweden AB
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of Touchize Sweden AB
 */

use PrestaShop\PrestaShop\Core\Filter\CollectionFilter;
use PrestaShop\PrestaShop\Core\Filter\HashMapWhitelistFilter;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
use PrestaShop\PrestaShop\Core\Product\Search\Pagination;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchContext;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchResult;
use PrestaShop\PrestaShop\Core\Product\Search\Facet;
use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchProviderInterface;
use PrestaShop\PrestaShop\Core\Product\Search\FacetsRendererInterface;
use PrestaShop\PrestaShop\Adapter\Category\CategoryProductSearchProvider;

class Touchize17ProductListingProcessor extends TouchizeBaseHelper
//class Touchize17ProductListingProcessor extends ProductListingFrontControllerCore
{
    public function getProducts($categoryId, $pageSize, $page)
    {
        $this->category = new Category(
            $categoryId,
            $this->context->language->id
        );
        //Since Ps_Facetedsearch reads the category id from the SERVER VARS!
        $_GET['id_category'] = $categoryId;

        // Code from ProductListingFrontControllerCore::getProductSearchVariables
        $context = $this->getProductSearchContext();
        $query = $this->getProductSearchQuery();
        $query
            ->setResultsPerPage($pageSize)
            ->setPage($page)
        ;

        $provider = $this->getProductSearchProviderFromModules($query);
        if (null === $provider) {
            $provider = $this->getDefaultProductSearchProvider();
        }

        $result = $provider->runQuery(
            $context,
            $query
        );
        return $result->getProducts();
    }

    public function getTotalItems($categoryId)
    {
        $this->category = new Category(
            $categoryId,
            $this->context->language->id
        );
        //Since Ps_Facetedsearch reads the category id from the SERVER VARS!
        $_GET['id_category'] = $categoryId;

        // Code from ProductListingFrontControllerCore::getProductSearchVariables
        $context = $this->getProductSearchContext();
        $query = $this->getProductSearchQuery();
        $provider = $this->getProductSearchProviderFromModules($query);
        if (null === $provider) {
            $provider = $this->getDefaultProductSearchProvider();
        }
        $result = $provider->runQuery(
            $context,
            $query
        );
        $filters = $this->getCategoryFilters($result);
        return array(
            'filters' => $filters,
            'count' => $result->getTotalProductsCount(),
            'products' => $result->getProducts()
        );
//        return $result->getTotalProductsCount();
    }

    protected function getCategoryFilters($result)
    {
        $filters = array();
        $facetCollection = $result->getFacetCollection();
        // not all search providers generate menus
        if (empty($facetCollection)) {
            return null;
        }

        $filterOptions = array(
            'Type' => 'Filter',
            'Title' => $this->l('Apply Filters'),
            'ClearAllTitle' => $this->l('Clear all'),
            'Filters' => array()
        );

        $facets = $facetCollection->getFacets();
        foreach ($facets as $facet) {
            $entry = $this->mapFacetData($facet);
            if ($entry) {
                array_push($filterOptions['Filters'], $entry);
            }
        }
        return $filterOptions;
    }

    protected function mapFacetData(Facet $facet)
    {
        $filter = array();
        $facetsArray = $facet->toArray();
        $filter['Title'] = $facet->getLabel();
        $values = array();
        foreach ($facetsArray['filters'] as $entry) {
            $url = TouchizeControllerHelper::getRelativeURL(
                $this->context->link->getCategoryLink(
                    $this->category
                )
            );
            $hasQuery = parse_url($url, PHP_URL_QUERY);
            if ($hasQuery) {
                $url .= '&q=' . $entry['nextEncodedFacets'];
            } else {
                $url .= '?q=' . $entry['nextEncodedFacets'];
            }

            $values[] = array(
                'Name' => $entry['label'],
                'Selected' => $entry['active'] ? $entry['active'] : false,
                'Nbr' => $entry['magnitude'],
                'Url' => $url,
                'Query' => $entry['nextEncodedFacets']
            );
        }
        if (!empty($values)) {
            $filter['List'] = $values;
        }
        return !empty($filter) ? $filter : null;
    }

    protected function getProductSearchContext()
    {
        return (new ProductSearchContext())
            ->setIdShop($this->context->shop->id)
            ->setIdLang($this->context->language->id)
            ->setIdCurrency($this->context->currency->id)
            ->setIdCustomer(
                $this->context->customer ?
                    $this->context->customer->id :
                    null
            )
        ;
    }
    private function getProductSearchProviderFromModules($query)
    {
        $providers = Hook::exec(
            'productSearchProvider',
            array('query' => $query),
            null,
            true
        );

        if (!is_array($providers)) {
            $providers = array();
        }

        foreach ($providers as $provider) {
            if ($provider instanceof ProductSearchProviderInterface) {
                return $provider;
            }
        }
    }

    protected function getProductSearchQuery()
    {
        $query = new ProductSearchQuery();
        $query
            ->setIdCategory($this->category->id)
            ->setSortOrder(new SortOrder('product', Tools::getProductsOrder('by'), Tools::getProductsOrder('way')))
            // ->setResultsPerPage(30)
            // ->setPage(1)
            // ->setSortOrder(new SortOrder('product', 'date_add', 'desc'))
        ;
        $encodedFacets = Tools::getValue('q');
        $query->setEncodedFacets($encodedFacets);

        return $query;
    }

    protected function getDefaultProductSearchProvider()
    {
        return new CategoryProductSearchProvider(
            $this->getTranslator(),
            $this->category
        );
    }

    public function getListingLabel()
    {
        return $this->trans(
            'New products',
            array(),
            'Shop.Theme.Catalog'
        );
    }
}
