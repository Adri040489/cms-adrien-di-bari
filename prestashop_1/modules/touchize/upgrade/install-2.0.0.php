<?php
/**
 * 2018 Touchize Sweden AB.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to prestashop@touchize.com so we can send you a copy immediately.
 *
 *  @author    Touchize Sweden AB <prestashop@touchize.com>
 *  @copyright 2018 Touchize Sweden AB
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of Touchize Sweden AB
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_2_0_0($object)
{
    Configuration::updateValue(
        'TOUCHIZE_START_CATEGORY_ID',
        TouchizeLandingPageHelper::getOption()
    );

    $banners = require _PS_MODULE_DIR_ . $object->name . '/install/banner_install.php';

    $pos = TouchizeTouchmap::getHigherPosition(); //Get highest position for all touchmaps

    foreach ($banners as $banner) {
        $name = $banner['name'];
        $image = $banner['value'];
        $width = $banner['width'];
        $height = $banner['height'];
        $pos++;

        $currentDate = new DateTime();
        $result = Db::getInstance()->insert(
            'touchize_touchmap',
            array(
                'id_shop' => 0,
                'name' => pSQL($name),
                'active' => 0,
                'mobile' => 1,
                'tablet' => 1,
                'runonce' => 1,
                'new_products' => 0,
                'best_sellers' => 0,
                'prices_drop' => 0,
                'home_page' => 1,
                'inslider' => 0,
                'position' => pSQL($pos),
                'width' => (int)pSQL($width),
                'height' => (int)pSQL($height),
                'date_add' => pSQL($currentDate->format('Y-m-d H:i:s')),
                'date_upd' => pSQL($currentDate->format('Y-m-d H:i:s')),
            )
        );
    
        if ($result) {
            $bannerId = Db::getInstance()->Insert_ID();
            $imageFolder = Touchize::getImgFolder();
            $imagePath = _PS_IMG_DIR_ . $imageFolder . $bannerId . '.jpg';
            $defBanner = _PS_MODULE_DIR_ . $object->name . '/views/img/defbanners/' . $image;
            copy($defBanner, $imagePath);
        } else {
            return false;
        }
    }
    return true;
}
