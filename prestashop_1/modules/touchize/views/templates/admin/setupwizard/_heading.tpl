{*
 * 2018 Touchize Sweden AB.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to prestashop@touchize.com so we can send you a copy immediately.
 *
 *  @author    Touchize Sweden AB <prestashop@touchize.com>
 *  @copyright 2018 Touchize Sweden AB
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of Touchize Sweden AB
 *}


<!-- Start of Intercom script -->
<script type="text/javascript">
  /*
* Integrate Intercom in a single page app
* https://www.intercom.com/help/en/articles/170-integrate-intercom-in-a-single-page-app
*/

  const APP_ID = 'wvyq8j85';

  // Adds the script to the head element
  (function() {
    var w = window;
    var ic = w.Intercom;
    if(typeof ic === 'function') {
      ic('reattach_activator');
      ic('update', w.intercomSettings);
    } else {
      var d = document;
      var i = function() {
        i.c(arguments);
      };
      i.q = [];
      i.c = function(args) {
        i.q.push(args);
      };
      w.Intercom = i;
      var l = function() {
        var s = d.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = 'https://widget.intercom.io/widget/' + APP_ID;
        var x = d.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
      };
      if(w.attachEvent) {
        w.attachEvent('onload', l);
      } else {
        w.addEventListener('load', l, false);
      }
    }
  })();
  // Initial data
  let user_data = {
    app_id: APP_ID, // This is Touchize App ID, don't change this
    user_id: "{$intercom_user_data['user_id']|escape:'htmlall':'UTF-8'}",
    name: "{$intercom_user_data['name']|escape:'htmlall':'UTF-8'}", // Full name
    email: "{$intercom_user_data['email']|escape:'htmlall':'UTF-8'}", // Email address
    created_at: String(Math.round((new Date()).getTime() / 1000)), // Signup date as a Unix timestamp
    Installed: true,
    Uninstalled: false,
    'Prestashop': true,
    'Prestashop URL': "{$intercom_user_data['Prestashop URL']|escape:'htmlall':'UTF-8'}", // Shop URL
    user_hash: "{$intercom_user_data['user_hash']|escape:'htmlall':'UTF-8'}" // User hash
  };

  window.Intercom('boot', user_data); // Sends the initial user_data to Intercom
</script>
<!-- End of Intercom script -->
<div class="panel-heading">
  <i class="icon-picture"></i>
  {l s='Setup wizard' mod='touchize'}
</div>
