<?php

 /* 23.05.18 - Webbax - Tuto 53 */

require(dirname(__FILE__).'/../../config/config.inc.php');
$Context = Context::getContext();

if(!empty($Context->cookie->id_customer)){

    $sql = 'UPDATE '._DB_PREFIX_.'customer 
            SET 
            `firstname`="anonymous",
            `lastname`="anonymous",
            `email`="anonymous@anonymous.com",
            `passwd`="",
            `birthday`="0000-00-00",
            `website`="",
            `active`="0",
            `deleted`="1"
            WHERE `id_customer`="'.pSQL($Context->cookie->id_customer).'"';
    Db::getInstance()->execute($sql);

    $sql = 'UPDATE '._DB_PREFIX_.'address 
            SET 
            `alias`="anonymous",
            `company`="",
            `firstname`="anonymous",
            `lastname`="anonymous",
            `address1`="anonymous",
            `address2`="anonymous",
            `postcode`="0000",
            `city`="anonymous",
            `other`="",
            `phone`="000000000",
            `phone_mobile`="000000000",
            `vat_number`="",
            `dni`="-",
            `active`="0",
            `deleted`="1"
            WHERE `id_customer`="'.pSQL($Context->cookie->id_customer).'"';
    Db::getInstance()->execute($sql);

    // déconnexion de la session
    $Customer = new Customer($Context->cookie->id_customer);
    $Customer->logout();

    // message de confirmation
    $msg = 'Votre compte a été supprimé : <a href="'.Tools::getHttpHost(true).__PS_BASE_URI__.'">Retour au site</a>';
    if($Context->cookie->id_lang=='xx'){
        $msg = 'xx';
    }

    echo $msg;

}

?>
    