<?php
/* Smarty version 3.1.39, created on 2021-12-06 11:14:44
  from 'module:pscustomeraccountlinkspsc' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61ade2947cfe97_22626015',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '42f9461127ce7396a601c2484841253ea5ba658f' => 
    array (
      0 => 'module:pscustomeraccountlinkspsc',
      1 => 1638294546,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_61ade2947cfe97_22626015 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'renderLogo' => 
  array (
    'compiled_filepath' => '/Applications/MAMP/htdocs/prestashop_1/var/cache/prod/smarty/compile/classiclayouts_layout_full_width_tpl/be/27/14/be271491bbe824fc25fd2cfc7e18eef233bae395_2.file.helpers.tpl.php',
    'uid' => 'be271491bbe824fc25fd2cfc7e18eef233bae395',
    'call_name' => 'smarty_template_function_renderLogo_128167705061add4a3edf2f7_65811579',
  ),
));
?>
<div id="block_myaccount_infos" class="col-md-3 links wrapper">
  <p class="h3 myaccount-title hidden-sm-down">
    <a class="text-uppercase" href="http://localhost:8888/prestashop_1/nl/mijn-account" rel="nofollow">
      Uw account
    </a>
  </p>
  <div class="title clearfix hidden-md-up" data-target="#footer_account_list" data-toggle="collapse">
    <span class="h3">Uw account</span>
    <span class="float-xs-right">
      <span class="navbar-toggler collapse-icons">
        <i class="material-icons add">&#xE313;</i>
        <i class="material-icons remove">&#xE316;</i>
      </span>
    </span>
  </div>
  <ul class="account-list collapse" id="footer_account_list">
            <li>
          <a href="http://localhost:8888/prestashop_1/nl/identiteit" title="Persoonlijke Info" rel="nofollow">
            Persoonlijke Info
          </a>
        </li>
            <li>
          <a href="http://localhost:8888/prestashop_1/nl/besteloverzicht" title="Bestellingen" rel="nofollow">
            Bestellingen
          </a>
        </li>
            <li>
          <a href="http://localhost:8888/prestashop_1/nl/bestel-bon" title="Creditnota&#039;s" rel="nofollow">
            Creditnota&#039;s
          </a>
        </li>
            <li>
          <a href="http://localhost:8888/prestashop_1/nl/adressen" title="Adressen" rel="nofollow">
            Adressen
          </a>
        </li>
            <li>
          <a href="http://localhost:8888/prestashop_1/nl/korting" title="Waardebonnen" rel="nofollow">
            Waardebonnen
          </a>
        </li>
          <li>
    <a href="http://localhost:8888/prestashop_1/nl/module/blockwishlist/lists" title="My wishlists" rel="nofollow">
      Verlanglijst
    <a>
  </li>

	</ul>
</div>
<?php }
}
