<?php
/* Smarty version 3.1.39, created on 2021-12-06 11:27:27
  from 'module:pscustomeraccountlinkspsc' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61ade58f74f894_86841724',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '42f9461127ce7396a601c2484841253ea5ba658f' => 
    array (
      0 => 'module:pscustomeraccountlinkspsc',
      1 => 1638294546,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_61ade58f74f894_86841724 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'renderLogo' => 
  array (
    'compiled_filepath' => '/Applications/MAMP/htdocs/prestashop_1/var/cache/prod/smarty/compile/classiclayouts_layout_full_width_tpl/be/27/14/be271491bbe824fc25fd2cfc7e18eef233bae395_2.file.helpers.tpl.php',
    'uid' => 'be271491bbe824fc25fd2cfc7e18eef233bae395',
    'call_name' => 'smarty_template_function_renderLogo_128167705061add4a3edf2f7_65811579',
  ),
));
?>
<div id="block_myaccount_infos" class="col-md-3 links wrapper">
  <p class="h3 myaccount-title hidden-sm-down">
    <a class="text-uppercase" href="http://localhost:8888/prestashop_1/gb/my-account" rel="nofollow">
      Your account
    </a>
  </p>
  <div class="title clearfix hidden-md-up" data-target="#footer_account_list" data-toggle="collapse">
    <span class="h3">Your account</span>
    <span class="float-xs-right">
      <span class="navbar-toggler collapse-icons">
        <i class="material-icons add">&#xE313;</i>
        <i class="material-icons remove">&#xE316;</i>
      </span>
    </span>
  </div>
  <ul class="account-list collapse" id="footer_account_list">
            <li>
          <a href="http://localhost:8888/prestashop_1/gb/identity" title="Personal info" rel="nofollow">
            Personal info
          </a>
        </li>
            <li>
          <a href="http://localhost:8888/prestashop_1/gb/order-history" title="Orders" rel="nofollow">
            Orders
          </a>
        </li>
            <li>
          <a href="http://localhost:8888/prestashop_1/gb/credit-slip" title="Credit notes" rel="nofollow">
            Credit notes
          </a>
        </li>
            <li>
          <a href="http://localhost:8888/prestashop_1/gb/addresses" title="Addresses" rel="nofollow">
            Addresses
          </a>
        </li>
            <li>
          <a href="http://localhost:8888/prestashop_1/gb/discount" title="Vouchers" rel="nofollow">
            Vouchers
          </a>
        </li>
          <li>
    <a href="http://localhost:8888/prestashop_1/gb/module/blockwishlist/lists" title="Mes listes" rel="nofollow">
      Wishlist
    <a>
  </li>

	</ul>
</div>
<?php }
}
