<?php
/* Smarty version 3.1.39, created on 2021-12-06 11:27:27
  from 'module:pslinklistviewstemplatesh' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61ade58f745279_06823591',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '906548e89c8c6025457ddaeffb1980a0c743b872' => 
    array (
      0 => 'module:pslinklistviewstemplatesh',
      1 => 1638294546,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_61ade58f745279_06823591 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'renderLogo' => 
  array (
    'compiled_filepath' => '/Applications/MAMP/htdocs/prestashop_1/var/cache/prod/smarty/compile/classiclayouts_layout_full_width_tpl/be/27/14/be271491bbe824fc25fd2cfc7e18eef233bae395_2.file.helpers.tpl.php',
    'uid' => 'be271491bbe824fc25fd2cfc7e18eef233bae395',
    'call_name' => 'smarty_template_function_renderLogo_128167705061add4a3edf2f7_65811579',
  ),
));
?><div class="col-md-6 links">
  <div class="row">
      <div class="col-md-6 wrapper">
      <p class="h3 hidden-sm-down">Products</p>
      <div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_1" data-toggle="collapse">
        <span class="h3">Products</span>
        <span class="float-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
      </div>
      <ul id="footer_sub_menu_1" class="collapse">
                  <li>
            <a
                id="link-product-page-prices-drop-1"
                class="cms-page-link"
                href="http://localhost:8888/prestashop_1/gb/prices-drop"
                title="Our special products"
                            >
              Prices drop
            </a>
          </li>
                  <li>
            <a
                id="link-product-page-new-products-1"
                class="cms-page-link"
                href="http://localhost:8888/prestashop_1/gb/new-products"
                title="Our new products"
                            >
              New products
            </a>
          </li>
                  <li>
            <a
                id="link-product-page-best-sales-1"
                class="cms-page-link"
                href="http://localhost:8888/prestashop_1/gb/best-sales"
                title="Our best sales"
                            >
              Best sales
            </a>
          </li>
              </ul>
    </div>
      <div class="col-md-6 wrapper">
      <p class="h3 hidden-sm-down">Our company</p>
      <div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_2" data-toggle="collapse">
        <span class="h3">Our company</span>
        <span class="float-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
      </div>
      <ul id="footer_sub_menu_2" class="collapse">
                  <li>
            <a
                id="link-cms-page-1-2"
                class="cms-page-link"
                href="http://localhost:8888/prestashop_1/gb/content/1-livraison"
                title="Nos conditions de livraison"
                            >
              Livraison
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-2-2"
                class="cms-page-link"
                href="http://localhost:8888/prestashop_1/gb/content/2-mentions-legales"
                title="Mentions légales"
                            >
              Mentions légales
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-3-2"
                class="cms-page-link"
                href="http://localhost:8888/prestashop_1/gb/content/3-conditions-utilisation"
                title="Nos conditions d&#039;utilisation"
                            >
              Conditions d&#039;utilisation
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-4-2"
                class="cms-page-link"
                href="http://localhost:8888/prestashop_1/gb/content/4-a-propos"
                title="En savoir plus sur notre entreprise"
                            >
              A propos
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-5-2"
                class="cms-page-link"
                href="http://localhost:8888/prestashop_1/gb/content/5-paiement-securise"
                title="Notre méthode de paiement sécurisé"
                            >
              Paiement sécurisé
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-contact-2"
                class="cms-page-link"
                href="http://localhost:8888/prestashop_1/gb/contact-us"
                title="Use our form to contact us"
                            >
              Contact us
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-sitemap-2"
                class="cms-page-link"
                href="http://localhost:8888/prestashop_1/gb/sitemap"
                title="Lost ? Find what your are looking for"
                            >
              Sitemap
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-stores-2"
                class="cms-page-link"
                href="http://localhost:8888/prestashop_1/gb/stores"
                title=""
                            >
              Stores
            </a>
          </li>
              </ul>
    </div>
    </div>
</div>
<?php }
}
