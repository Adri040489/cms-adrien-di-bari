<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:55:44
  from '/Applications/MAMP/htdocs/prestashop_1/pdf/invoice.shipping-tab.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61adde20c6dbb7_65339287',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '78887dd46a5e60466da41ad7a885bc385e535723' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/pdf/invoice.shipping-tab.tpl',
      1 => 1638294536,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61adde20c6dbb7_65339287 (Smarty_Internal_Template $_smarty_tpl) {
?><table id="shipping-tab" width="100%">
	<tr>
		<td class="shipping center small grey bold" width="44%"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Carrier','d'=>'Shop.Pdf','pdf'=>'true'),$_smarty_tpl ) );?>
</td>
		<td class="shipping center small white" width="56%"><?php echo $_smarty_tpl->tpl_vars['carrier']->value->name;?>
</td>
	</tr>
</table>
<?php }
}
