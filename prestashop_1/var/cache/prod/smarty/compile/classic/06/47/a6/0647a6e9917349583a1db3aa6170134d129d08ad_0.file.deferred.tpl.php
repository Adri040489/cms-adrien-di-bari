<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:34:32
  from '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/settings/deferred.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add928ca7416_35887370',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0647a6e9917349583a1db3aa6170134d129d08ad' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/settings/deferred.tpl',
      1 => 1638738537,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./switch.tpl' => 1,
  ),
),false)) {
function content_61add928ca7416_35887370 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="panel-row separate_margin_block">
    <div class="payplugPanel">
        <div class="payplugPanel_label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Defer the payment capture','mod'=>'payplug'),$_smarty_tpl ) );?>
</div>
        <div class="payplugPanel_content"><?php $_smarty_tpl->_subTemplateRender('file:./switch.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('switch'=>$_smarty_tpl->tpl_vars['payplug_switch']->value['deferred']), 0, false);
?></div>
    </div>
    <div class="payplugPanel">
        <div class="payplugPanel_content">
            <p>
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.panel.settings.deferred.7days','mod'=>'payplug'),$_smarty_tpl ) );?>

                <a class="payplugLink" href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['faq_links']->value['deferred'],'htmlall','UTF-8' ));?>
" data-e2e-link="faq" target="_blank"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.panel.settings.deffered.more','mod'=>'payplug'),$_smarty_tpl ) );?>
</a>
                <br/>
                <span class="payplugDeferred -example"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.panel.settings.deferred.example','mod'=>'payplug'),$_smarty_tpl ) );?>
</span>
            </p>
            <div class="payplugTips -<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['payplug_switch']->value['deferred']['name'],'htmlall','UTF-8' ));?>
">
                <div class="payplugTips_item -left<?php if (!$_smarty_tpl->tpl_vars['payplug_switch']->value['deferred']['checked']) {?> -hide<?php }?>">
                    <div class="payplugDeferred">
                        <label for="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['payplug_switch']->value['deferred_auto']['name'],'htmlall','UTF-8' ));?>
">
                            <input type="checkbox" name="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['payplug_switch']->value['deferred_auto']['name'],'htmlall','UTF-8' ));?>
" value="1" id="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['payplug_switch']->value['deferred_auto']['name'],'htmlall','UTF-8' ));?>
" <?php if ($_smarty_tpl->tpl_vars['payplug_switch']->value['deferred_auto']['checked']) {?>checked="checked"<?php }?>>
                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.panel.settings.deferred.trigger','mod'=>'payplug'),$_smarty_tpl ) );?>


                        </label>
                        <select name="payplug_deferred_state" data-id_state="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['PAYPLUG_DEFERRED_STATE']->value,'htmlall','UTF-8' ));?>
" id="payplug_deferred_state"<?php if (!$_smarty_tpl->tpl_vars['payplug_switch']->value['deferred_auto']['checked']) {?> disabled="disabled"<?php }?>>
                            <option value="0"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.panel.settings.deferred.choose','mod'=>'payplug'),$_smarty_tpl ) );?>
</option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['order_states']->value, 'order_state');
$_smarty_tpl->tpl_vars['order_state']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['order_state']->value) {
$_smarty_tpl->tpl_vars['order_state']->do_else = false;
?>
                                <option value="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['order_state']->value['id_order_state'],'htmlall','UTF-8' ));?>
"<?php if ($_smarty_tpl->tpl_vars['PAYPLUG_DEFERRED_STATE']->value == $_smarty_tpl->tpl_vars['order_state']->value['id_order_state']) {?> selected="selected"<?php }?>><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['order_state']->value['name'],'htmlall','UTF-8' ));?>
</option>
                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </select>
                            <span style="display: none;" data-e2e-error="deferred_state" class="payplugDeferred_error"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.panel.settings.deferred.mustchoose','mod'=>'payplug'),$_smarty_tpl ) );?>
</span>
                    <div style="display: none;" data-e2e-error="change_state" class="payplugDeferred_warning"><p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.panel.settings.deferred.warning','mod'=>'payplug'),$_smarty_tpl ) );?>
</p></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }
}
