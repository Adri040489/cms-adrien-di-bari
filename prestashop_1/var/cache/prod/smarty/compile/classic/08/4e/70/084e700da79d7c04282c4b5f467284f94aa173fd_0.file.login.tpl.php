<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:34:32
  from '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/login.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add928c09841_93630738',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '084e700da79d7c04282c4b5f467284f94aa173fd' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/login.tpl',
      1 => 1638738537,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./user/connected.tpl' => 1,
    'file:./user/disconnected.tpl' => 1,
  ),
),false)) {
function content_61add928c09841_93630738 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="panel payplugLogin<?php if ($_smarty_tpl->tpl_vars['connected']->value) {?> -logged<?php }?>">
    <div class="panel-heading"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'CONNECT','mod'=>'payplug'),$_smarty_tpl ) );?>
</div>
    <div class="panel-row">
        <?php if ($_smarty_tpl->tpl_vars['connected']->value) {?>
            <?php $_smarty_tpl->_subTemplateRender('file:./user/connected.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php } else { ?>
            <?php $_smarty_tpl->_subTemplateRender('file:./user/disconnected.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php }?>
        <img class="payplugLoader" src="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['module_dir']->value,'htmlall','UTF-8' ));?>
views/img/admin/spinner.gif" />
    </div>
</div>
<?php }
}
