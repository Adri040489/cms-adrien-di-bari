<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:15:16
  from '/Applications/MAMP/htdocs/prestashop_1/modules/ps_checkout/views/templates/hook/displayPaymentByBinaries.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add4a4220706_20548016',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0da5eeb48a86a8c539bbd2d975268c891f5283be' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/ps_checkout/views/templates/hook/displayPaymentByBinaries.tpl',
      1 => 1638738859,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61add4a4220706_20548016 (Smarty_Internal_Template $_smarty_tpl) {
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['paymentOptions']->value, 'fundingSource');
$_smarty_tpl->tpl_vars['fundingSource']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['fundingSource']->value) {
$_smarty_tpl->tpl_vars['fundingSource']->do_else = false;
?>
  <section class="js-payment-binary js-payment-ps_checkout js-payment-ps_checkout-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['fundingSource']->value, ENT_QUOTES, 'UTF-8');?>
 disabled">
    <p class="alert alert-warning accept-cgv"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You must accept the terms and conditions to be able to process your order.','mod'=>'ps_checkout'),$_smarty_tpl ) );?>
</p>
    <div id="ps_checkout-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['fundingSource']->value, ENT_QUOTES, 'UTF-8');?>
-buttons-container">
      <div class="ps_checkout-button" data-funding-source="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['fundingSource']->value, ENT_QUOTES, 'UTF-8');?>
"></div>
    </div>
  </section>
<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}
}
