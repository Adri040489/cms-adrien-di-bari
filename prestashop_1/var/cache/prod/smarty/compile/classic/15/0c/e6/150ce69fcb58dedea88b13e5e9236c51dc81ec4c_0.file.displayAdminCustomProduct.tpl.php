<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:52:33
  from '/Applications/MAMP/htdocs/prestashop_1/modules/colissimo/views/templates/hook/admin/displayAdminCustomProduct.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61addd61c12bc5_58803039',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '150ce69fcb58dedea88b13e5e9236c51dc81ec4c' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/colissimo/views/templates/hook/admin/displayAdminCustomProduct.tpl',
      1 => 1638383286,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61addd61c12bc5_58803039 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="row">
  <div class="col-md-12">
    <p class="subtitle"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Please fill in the customs information of your product','mod'=>'colissimo'),$_smarty_tpl ) );?>
</p>
    <div class="row">
      <div class="form-group col-md-4">
        <label class="form-control-label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Hs code','mod'=>'colissimo'),$_smarty_tpl ) );?>
</label>
        <input name="colissimo_hs_code"
               type="text"
               class="form-control"
               value="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_details']->value->hs_code,'html','UTF-8' ));?>
"/>
      </div>
    </div>
    <div class="row">
      <div class="form-group col-md-4">
        <label class="form-control-label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Country origin','mod'=>'colissimo'),$_smarty_tpl ) );?>
</label>
        <select class="form-control" name="colissimo_country_origin">
          <option value="0"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'-- Please select a country --','mod'=>'colissimo'),$_smarty_tpl ) );?>
</option>
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['countries']->value, 'country');
$_smarty_tpl->tpl_vars['country']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['country']->value) {
$_smarty_tpl->tpl_vars['country']->do_else = false;
?>
            <option value="<?php echo intval($_smarty_tpl->tpl_vars['country']->value['id_country']);?>
"
                    <?php if ($_smarty_tpl->tpl_vars['product_details']->value->id_country_origin == $_smarty_tpl->tpl_vars['country']->value['id_country']) {?>selected<?php }?>>
              <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['country']->value['name'],'html','UTF-8' ));?>

            </option>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </select>
      </div>
    </div>
    <div class="row">
      <div class="form-group col-md-4">
        <label class="form-control-label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Short description','mod'=>'colissimo'),$_smarty_tpl ) );?>
</label>
        <input name="colissimo_short_desc"
               class="form-control"
               maxLenght="64"
               value="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_details']->value->short_desc,'html','UTF-8' ));?>
"/>
      </div>
    </div>
  </div>
</div>
<?php }
}
