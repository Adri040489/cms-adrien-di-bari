<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:34:32
  from '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/settings/standard.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add928c45426_88352087',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '17acec8a493951d75892da6f7b510110e7cd4318' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/settings/standard.tpl',
      1 => 1638738537,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./switch.tpl' => 1,
    'file:./one_click.tpl' => 1,
  ),
),false)) {
function content_61add928c45426_88352087 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="panel-row separate_margin_block">
    <div class="payplugPanel">
        <div class="payplugPanel_label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.panel.setting.standard.label','mod'=>'payplug'),$_smarty_tpl ) );?>
</div>
        <div class="payplugPanel_content"><?php $_smarty_tpl->_subTemplateRender('file:./switch.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('switch'=>$_smarty_tpl->tpl_vars['payplug_switch']->value['standard']), 0, false);
?></div>
    </div>
    <div class="payplugPanel">
        <div class="payplugPanel_content">
            <p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.panel.setting.standard.content','mod'=>'payplug'),$_smarty_tpl ) );?>
</p>
            <?php $_smarty_tpl->_subTemplateRender('file:./one_click.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        </div>
    </div>
</div>
<?php }
}
