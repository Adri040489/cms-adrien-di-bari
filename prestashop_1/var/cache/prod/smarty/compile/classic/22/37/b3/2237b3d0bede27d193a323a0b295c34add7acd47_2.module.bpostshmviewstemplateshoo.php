<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:15:16
  from 'module:bpostshmviewstemplateshoo' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add4a418f947_89822791',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2237b3d0bede27d193a323a0b295c34add7acd47' => 
    array (
      0 => 'module:bpostshmviewstemplateshoo',
      1 => 1638383107,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61add4a418f947_89822791 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript">
// <![CDATA[
var version 		= <?php echo htmlspecialchars(floatval($_smarty_tpl->tpl_vars['version']->value), ENT_QUOTES, 'UTF-8');?>
,
	id_carrier 		= <?php echo htmlspecialchars((($tmp = @intval($_smarty_tpl->tpl_vars['id_carrier']->value))===null||$tmp==='' ? 0 : $tmp), ENT_QUOTES, 'UTF-8');?>
,
	url_handler 	= '<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['url_carriers_js']->value,"javascript" ));?>
',
	no_address 		= <?php if ((isset($_smarty_tpl->tpl_vars['no_address']->value))) {?>`<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['no_address']->value,"none" ));?>
`<?php } else { ?>''<?php }?>,
	debug_src		= <?php if ((isset($_smarty_tpl->tpl_vars['debug_mode']->value))) {?>`<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['debug_mode']->value,'none' ));?>
`<?php } else { ?>''<?php }?>,
	ShippingInfo 	= {
		carriers_shm: 	<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'json_encode' ][ 0 ], array( $_smarty_tpl->tpl_vars['carriers_shm']->value ));?>
,
		url_cbox: 		'<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['url_carrierbox']->value,"javascript" ));?>
',
		l_messages:	 	<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'json_encode' ][ 0 ], array( $_smarty_tpl->tpl_vars['l_messages']->value ));?>
,
		opc: 			<?php echo htmlspecialchars((($tmp = @intval($_smarty_tpl->tpl_vars['opc']->value))===null||$tmp==='' ? false : $tmp), ENT_QUOTES, 'UTF-8');?>
,
	};
// ]]>
<?php echo '</script'; ?>
>
<?php if ((isset($_smarty_tpl->tpl_vars['inc_src']->value))) {
echo '<script'; ?>
 type="text/javascript" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['inc_src']->value,"javascript" )), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>
<?php }
}
}
