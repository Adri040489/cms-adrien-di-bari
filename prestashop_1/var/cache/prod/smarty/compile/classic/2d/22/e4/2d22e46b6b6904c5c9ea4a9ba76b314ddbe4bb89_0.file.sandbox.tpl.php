<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:34:32
  from '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/settings/sandbox.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add928c2e438_25114320',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2d22e46b6b6904c5c9ea4a9ba76b314ddbe4bb89' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/settings/sandbox.tpl',
      1 => 1638738537,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./switch.tpl' => 1,
  ),
),false)) {
function content_61add928c2e438_25114320 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="panel-row separate_margin_block">
    <div class="payplugPanel">
        <div class="payplugPanel_label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Mode','mod'=>'payplug'),$_smarty_tpl ) );?>
</div>
        <div class="payplugPanel_content"><?php $_smarty_tpl->_subTemplateRender('file:./switch.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('switch'=>$_smarty_tpl->tpl_vars['payplug_switch']->value['sandbox']), 0, false);
?></div>
    </div>
    <div class="payplugPanel">
        <div class="payplugPanel_content">
            <div class="payplugTips -<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['payplug_switch']->value['sandbox']['name'],'htmlall','UTF-8' ));?>
">
                <div class="payplugTips_item -left <?php if (!$_smarty_tpl->tpl_vars['payplug_switch']->value['sandbox']['checked']) {?>-hide<?php }?>">
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'In TEST mode, all payments will be simulations and will not generate real transactions.','mod'=>'payplug'),$_smarty_tpl ) );?>

                    <a class="payplugLink" href="http://support.payplug.com/customer/portal/articles/1701656" data-e2e-link="faq" target="_blank"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Learn more.','mod'=>'payplug'),$_smarty_tpl ) );?>
</a>
                </div>
                <div class="payplugTips_item -right <?php if ($_smarty_tpl->tpl_vars['payplug_switch']->value['sandbox']['checked']) {?>-hide<?php }?>">
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'In LIVE mode, the payments will generate real transactions.','mod'=>'payplug'),$_smarty_tpl ) );?>

                </div>
            </div>
        </div>
    </div>
</div>

<?php }
}
