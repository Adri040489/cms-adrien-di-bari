<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:34:32
  from '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/settings/embedded.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add928c3d4e0_17880671',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3540045b22a5fead2a970653a72f82b5b44df55e' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/settings/embedded.tpl',
      1 => 1638738537,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./switch.tpl' => 1,
  ),
),false)) {
function content_61add928c3d4e0_17880671 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="panel-row separate_margin_block">
    <div class="payplugPanel">
        <div class="payplugPanel_label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Payment page','mod'=>'payplug'),$_smarty_tpl ) );?>
</div>
        <div class="payplugPanel_content">
            <?php $_smarty_tpl->_subTemplateRender('file:./switch.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('switch'=>$_smarty_tpl->tpl_vars['payplug_switch']->value['embedded']), 0, false);
?>
        </div>
    </div>
    <div class="payplugPanel">
        <div class="payplugPanel_content">
            <div class="payplugTips -<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['payplug_switch']->value['embedded']['name'],'htmlall','UTF-8' ));?>
">
                <div class="payplugTips_item -left <?php if (!$_smarty_tpl->tpl_vars['payplug_switch']->value['embedded']['checked']) {?>-hide<?php }?>">
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Payments are performed in an embeddable payment form.','mod'=>'payplug'),$_smarty_tpl ) );?>
<br><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'The customers will pay without being redirected.','mod'=>'payplug'),$_smarty_tpl ) );?>

                    <a class="payplugLink" href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['faq_links']->value['payment_page'],'htmlall','UTF-8' ));?>
" data-e2e-link="faq" target="_blank"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Customize my payment page.','mod'=>'payplug'),$_smarty_tpl ) );?>
</a>
                </div>
                <div class="payplugTips_item -right <?php if ($_smarty_tpl->tpl_vars['payplug_switch']->value['embedded']['checked']) {?>-hide<?php }?>">
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'The customers will be redirected to a PayPlug payment page to finalize the transaction.','mod'=>'payplug'),$_smarty_tpl ) );?>

                    <a class="payplugLink" href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['faq_links']->value['payment_page'],'htmlall','UTF-8' ));?>
" data-e2e-link="faq" target="_blank"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Customize my payment page.','mod'=>'payplug'),$_smarty_tpl ) );?>
</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }
}
