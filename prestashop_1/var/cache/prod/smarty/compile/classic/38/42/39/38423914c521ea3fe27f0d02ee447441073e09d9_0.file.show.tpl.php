<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:34:32
  from '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/show.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add928bed0f4_46726251',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '38423914c521ea3fe27f0d02ee447441073e09d9' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/show.tpl',
      1 => 1638738537,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./settings/switch.tpl' => 1,
  ),
),false)) {
function content_61add928bed0f4_46726251 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="panel payplugShow">
    <div class="panel-heading"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Display to customers','mod'=>'payplug'),$_smarty_tpl ) );?>
</div>
    <?php if (!$_smarty_tpl->tpl_vars['connected']->value) {?>
        <p class="payplugAlert -warning"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Before being able to display PayPlug to your customers you need to connect your PayPlug account below.','mod'=>'payplug'),$_smarty_tpl ) );?>
</p>
    <?php }?>
    <div class="panel-row">
        <div class="payplugPanel">
            <div class="payplugPanel_label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Show Payplug to my customers','mod'=>'payplug'),$_smarty_tpl ) );?>
</div>
            <div class="payplugPanel_content"><?php $_smarty_tpl->_subTemplateRender('file:./settings/switch.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('switch'=>$_smarty_tpl->tpl_vars['payplug_switch']->value['show']), 0, false);
?></div>
        </div>
    </div>
</div>
<?php }
}
