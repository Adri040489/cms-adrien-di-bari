<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:31:13
  from '/Applications/MAMP/htdocs/prestashop_1/modules/ps_checkout/views/templates/admin/configuration.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add86148de26_05626057',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3c197b581087e6ba3f66b2dbce969987848076f3' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/ps_checkout/views/templates/admin/configuration.tpl',
      1 => 1638738859,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61add86148de26_05626057 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="app"></div>

<style>
  /** Hide native multistore module activation panel, because of visual regressions on non-bootstrap content */
  #content.nobootstrap div.bootstrap.panel {
    display: none;
  }
</style>
<?php }
}
