<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:15:15
  from '/Applications/MAMP/htdocs/prestashop_1/modules/ps_checkout/views/templates/hook/header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add4a3e8c034_26204720',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4523cc07c649766e9ffd90d0a39253f516bdbbcf' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/ps_checkout/views/templates/hook/header.tpl',
      1 => 1638738859,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61add4a3e8c034_26204720 (Smarty_Internal_Template $_smarty_tpl) {
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['contentToPrefetch']->value, 'content');
$_smarty_tpl->tpl_vars['content']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['content']->value) {
$_smarty_tpl->tpl_vars['content']->do_else = false;
?>
  <link rel="prefetch" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['link'], ENT_QUOTES, 'UTF-8');?>
" as="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['type'], ENT_QUOTES, 'UTF-8');?>
">
<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}
}
