<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:15:15
  from '/Applications/MAMP/htdocs/prestashop_1/modules/smartsupp/views/templates/front/chat_widget.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add4a3ea2c08_69094113',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '649e97ec8eeca71bcee22671e8b23a9b47b57541' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/smartsupp/views/templates/front/chat_widget.tpl',
      1 => 1638782045,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61add4a3ea2c08_69094113 (Smarty_Internal_Template $_smarty_tpl) {
echo '<?php
';?>
/**
 * NOTICE OF LICENSE
 *
 * Smartsupp live chat - official plugin. Smartsupp is free live chat with visitor recording. 
 * The plugin enables you to create a free account or sign in with existing one. Pre-integrated 
 * customer info with WooCommerce (you will see names and emails of signed in webshop visitors).
 * Optional API for advanced chat box modifications.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Smartsupp
 *  @copyright 2021 Smartsupp.com
 *  @license   GPL-2.0+
**/ 
<?php echo '?>';?>


<?php echo $_smarty_tpl->tpl_vars['smartsupp_js']->value;
}
}
