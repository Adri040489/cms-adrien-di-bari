<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:52:02
  from '/Applications/MAMP/htdocs/prestashop_1/modules/skrill/views/templates/hook/displayStatusOrder.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61addd42331071_04181208',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '64d275c6de1f142d18ec62b2e6440aa6c92deff8' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/skrill/views/templates/hook/displayStatusOrder.tpl',
      1 => 1638738073,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61addd42331071_04181208 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['module']->value == "skrill") {?>
    <?php echo '<script'; ?>
>
        $(document).ready(function() {
            $('#desc-order-standard_refund').css("display","none");
            var refundButton = <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['refundButton']->value,'htmlall','UTF-8' ));?>
;
            if (refundButton) {
                var refund = '<a id="skrillRefund" class="btn btn-default"><i class="icon-exchange"></i> Refund</a>'
                $('#desc-order-partial_refund').after(refund);
                $('#skrillRefund').click(function(event){
                    event.preventDefault();
                    $("#skrillRefundOrder").click();
                });
            }
        });
    <?php echo '</script'; ?>
>

    <?php if (!empty($_smarty_tpl->tpl_vars['warningMessage']->value)) {?>
        <div class="alert alert-warning">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <?php if ($_smarty_tpl->tpl_vars['warningMessage']->value == "refund") {?>
                <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'SUCCESS_GENERAL_REFUND_PAYMENT_PENDING','mod'=>'skrill'),$_smarty_tpl ) );
$_prefixVariable1 = ob_get_clean();
if ($_prefixVariable1 == "SUCCESS_GENERAL_REFUND_PAYMENT_PENDING") {?>Your attempt to refund the payment is pending.<?php } else {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'SUCCESS_GENERAL_REFUND_PAYMENT_PENDING','mod'=>'skrill'),$_smarty_tpl ) );
}?>
            <?php }?>
        </div>
    <?php }?>
    <?php if (!empty($_smarty_tpl->tpl_vars['successMessage']->value)) {?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <?php if ($_smarty_tpl->tpl_vars['successMessage']->value == "refund") {?>
                <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'SUCCESS_GENERAL_REFUND_PAYMENT','mod'=>'skrill'),$_smarty_tpl ) );
$_prefixVariable2 = ob_get_clean();
if ($_prefixVariable2 == "SUCCESS_GENERAL_REFUND_PAYMENT") {?>Your attempt to refund the payment success.<?php } else {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'SUCCESS_GENERAL_REFUND_PAYMENT','mod'=>'skrill'),$_smarty_tpl ) );
}?>
            <?php } elseif ($_smarty_tpl->tpl_vars['successMessage']->value == "updateOrder") {?>
                <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'SUCCESS_GENERAL_UPDATE_PAYMENT','mod'=>'skrill'),$_smarty_tpl ) );
$_prefixVariable3 = ob_get_clean();
if ($_prefixVariable3 == "SUCCESS_GENERAL_UPDATE_PAYMENT") {?>The payment status has been successfully updated.<?php } else {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'SUCCESS_GENERAL_UPDATE_PAYMENT','mod'=>'skrill'),$_smarty_tpl ) );
}?>
            <?php }?>
        </div>
    <?php }?>
    <?php if (!empty($_smarty_tpl->tpl_vars['errorMessage']->value)) {?>
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <?php if ($_smarty_tpl->tpl_vars['errorMessage']->value == "refund") {?>
                <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'ERROR_GENERAL_REFUND_PAYMENT','mod'=>'skrill'),$_smarty_tpl ) );
$_prefixVariable4 = ob_get_clean();
if ($_prefixVariable4 == "ERROR_GENERAL_REFUND_PAYMENT") {?>Unfortunately, your attempt to refund the payment failed.<?php } else {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'ERROR_GENERAL_REFUND_PAYMENT','mod'=>'skrill'),$_smarty_tpl ) );
}?>
            <?php } elseif ($_smarty_tpl->tpl_vars['errorMessage']->value == "updateOrder") {?>
                <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'ERROR_UPDATE_BACKEND','mod'=>'skrill'),$_smarty_tpl ) );
$_prefixVariable5 = ob_get_clean();
if ($_prefixVariable5 == "ERROR_UPDATE_BACKEND") {?>Order status can not be updated. <?php } else {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'ERROR_UPDATE_BACKEND','mod'=>'skrill'),$_smarty_tpl ) );
}?>
            <?php }?>
        </div>
    <?php }
}
}
}
