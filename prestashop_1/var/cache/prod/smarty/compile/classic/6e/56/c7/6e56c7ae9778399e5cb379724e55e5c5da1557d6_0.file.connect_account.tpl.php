<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:14:26
  from '/Applications/MAMP/htdocs/prestashop_1/modules/smartsupp/views/templates/admin/connect_account.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add47269e284_71294018',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6e56c7ae9778399e5cb379724e55e5c5da1557d6' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/smartsupp/views/templates/admin/connect_account.tpl',
      1 => 1638782045,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./includes/features.tpl' => 1,
    'file:./includes/clients.tpl' => 1,
  ),
),false)) {
function content_61add47269e284_71294018 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="smartsupp_connect_account" class="panel">
	<header class="header">
		<img src="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['module_dir']->value,'html','UTF-8' ));?>
views/img/smartsupp_logo.png" alt="Smartsupp" />
		<nav>
			<div class="header-user">
				<span class="header-user__email">
					<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Not a Smartsupp user yet?','mod'=>'smartsupp'),$_smarty_tpl ) );?>

				</span>
				<button id="create_account_btn2" class="btn btn--sm">
					<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Create a free account','mod'=>'smartsupp'),$_smarty_tpl ) );?>

				</button>
			</div>
		</nav>
	</header>

	<main class="main" role="main">
		<div class="main__left">
			<div class="main-form">
				<h1 class="main-form__h1">
					<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Log in','mod'=>'smartsupp'),$_smarty_tpl ) );?>

				</h1>
				<div class="form-horizontal">
					<input id="SMARTSUPP_EMAIL" type="email" value="" name="SMARTSUPP_EMAIL" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'E-mail:','mod'=>'smartsupp'),$_smarty_tpl ) );?>
" class="input" required>
					<input id="SMARTSUPP_PASSWORD" type="password" value="" name="SMARTSUPP_PASSWORD" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Password:','mod'=>'smartsupp'),$_smarty_tpl ) );?>
" class="input" required>
					<button id="connect_existing_account_do" class="btn btn--primary btn--arrow btn--all-width">
						<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Log in','mod'=>'smartsupp'),$_smarty_tpl ) );?>

					</button>
                    <p class="main-form__bottom-text">
						<span>
							<a href="https://app.smartsupp.com/app/sign/reset" target="_blank" class="link">
								<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'I forgot my password','mod'=>'smartsupp'),$_smarty_tpl ) );?>

							</a>
						</span>
					</p>
				</div>
			</div>
		</div>

		<div class="main__right">
			<img src="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['module_dir']->value,'html','UTF-8' ));?>
views/img/tablet-screen.png">
		</div>
	</main>

	<?php $_smarty_tpl->_subTemplateRender('file:./includes/features.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
	<?php $_smarty_tpl->_subTemplateRender('file:./includes/clients.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</div><?php }
}
