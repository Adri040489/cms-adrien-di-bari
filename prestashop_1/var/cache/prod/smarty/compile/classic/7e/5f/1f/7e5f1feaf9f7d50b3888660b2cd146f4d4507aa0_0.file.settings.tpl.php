<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:34:32
  from '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/settings.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add928c24666_65181037',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7e5f1feaf9f7d50b3888660b2cd146f4d4507aa0' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/settings.tpl',
      1 => 1638738537,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./settings/sandbox.tpl' => 1,
    'file:./settings/embedded.tpl' => 1,
    'file:./settings/standard.tpl' => 1,
    'file:./settings/oney.tpl' => 1,
    'file:./settings/installment.tpl' => 1,
    'file:./settings/deferred.tpl' => 1,
  ),
),false)) {
function content_61add928c24666_65181037 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="panel payplugSettings">
    <div class="panel-heading"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'SETTINGS','mod'=>'payplug'),$_smarty_tpl ) );?>
</div>

    <?php if ($_smarty_tpl->tpl_vars['connected']->value && !$_smarty_tpl->tpl_vars['verified']->value) {?>
        <div class="panel-row">
            <p class="payplugAlert -warning">
                <span>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You are able to perform only TEST transactions.','mod'=>'payplug'),$_smarty_tpl ) );?>
 <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Please activate your account to perform LIVE transactions.','mod'=>'payplug'),$_smarty_tpl ) );?>

                    <a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['faq_links']->value['activation'],'htmlall','UTF-8' ));?>
" target="_blank"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'More information','mod'=>'payplug'),$_smarty_tpl ) );?>
</a>
                </span>
            </p>
        </div>
    <?php }?>

    <?php $_smarty_tpl->_subTemplateRender('file:./settings/sandbox.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php $_smarty_tpl->_subTemplateRender('file:./settings/embedded.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <div class="payplugSettings_separator">
        <p><strong><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Advanced settings','mod'=>'payplug'),$_smarty_tpl ) );?>
</strong></p>
    </div>

    <div class="payplugSettings_advanced">
        <?php $_smarty_tpl->_subTemplateRender('file:./settings/standard.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php $_smarty_tpl->_subTemplateRender('file:./settings/oney.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php $_smarty_tpl->_subTemplateRender('file:./settings/installment.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php $_smarty_tpl->_subTemplateRender('file:./settings/deferred.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    </div>

    <div class="panel-footer">
        <button type="submit" name="submitSettings" class="payplugButton -green<?php if (!$_smarty_tpl->tpl_vars['connected']->value) {?> -disabled<?php }?>"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Update settings','mod'=>'payplug'),$_smarty_tpl ) );?>
</button>
    </div>
</div>
<?php }
}
