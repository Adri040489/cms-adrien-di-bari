<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:34:32
  from '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/settings/oney_fees.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add928c6d8e8_84391465',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '845622b6e91c9a74483bcaad41bf442ea51c12d5' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/settings/oney_fees.tpl',
      1 => 1638738537,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61add928c6d8e8_84391465 (Smarty_Internal_Template $_smarty_tpl) {
if ((isset($_smarty_tpl->tpl_vars['can_use_oney_fees']->value)) && $_smarty_tpl->tpl_vars['can_use_oney_fees']->value) {?>
    <div class="payplugOneyFees">
        <label class="payplugOneyFees_option<?php if ($_smarty_tpl->tpl_vars['payplug_switch']->value['oney_fees']['checked']) {?> -selected<?php }?>">
            <span class="payplugOneyFees_title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.panel.setting.oney.withFees','mod'=>'payplug'),$_smarty_tpl ) );?>
</span>
            <span class="payplugOneyFees_content"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.panel.setting.oney.withFeesText','mod'=>'payplug'),$_smarty_tpl ) );?>
</span>
            <span class="payplugOneyFees_state"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.panel.setting.oney.activate','mod'=>'payplug'),$_smarty_tpl ) );?>
</span>
            <span class="payplugOneyFees_checker">
                <input type="radio"
                        id="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['payplug_switch']->value['oney_fees']['name'],'htmlall','UTF-8' ));?>
_left"
                        name="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['payplug_switch']->value['oney_fees']['name'],'htmlall','UTF-8' ));?>
"
                        value="1"
                        <?php if ($_smarty_tpl->tpl_vars['payplug_switch']->value['oney_fees']['checked']) {?> checked="checked"<?php }?>/>
            </span>
        </label>
        <label class="payplugOneyFees_option<?php if (!$_smarty_tpl->tpl_vars['payplug_switch']->value['oney_fees']['checked']) {?> -selected<?php }?>">
            <span class="payplugOneyFees_title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.panel.setting.oney.withoutFees','mod'=>'payplug'),$_smarty_tpl ) );?>
</span>
            <span class="payplugOneyFees_content"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.panel.setting.oney.withoutFeesText','mod'=>'payplug'),$_smarty_tpl ) );?>
</span>
            <span class="payplugOneyFees_state"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.panel.setting.oney.activate','mod'=>'payplug'),$_smarty_tpl ) );?>
</span>
            <span class="payplugOneyFees_checker">
                <input type="radio"
                        id="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['payplug_switch']->value['oney_fees']['name'],'htmlall','UTF-8' ));?>
_right"
                        name="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['payplug_switch']->value['oney_fees']['name'],'htmlall','UTF-8' ));?>
"
                        value="0"
                        <?php if (!$_smarty_tpl->tpl_vars['payplug_switch']->value['oney_fees']['checked']) {?> checked="checked"<?php }?>/>
            </span>
        </label>
    </div>
<?php }
}
}
