<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:34:32
  from '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/settings/installment.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add928c8adc0_79879744',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9b7fed083a926b5c86d8752093b1eabd8094a211' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/settings/installment.tpl',
      1 => 1638738537,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./switch.tpl' => 1,
  ),
),false)) {
function content_61add928c8adc0_79879744 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="payplugInstallment panel-row separate_margin_block">
    <div class="payplugPanel">
        <div class="payplugPanel_label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.panel.setting.installment.label','mod'=>'payplug'),$_smarty_tpl ) );?>
</div>
        <div class="payplugPanel_content"><?php $_smarty_tpl->_subTemplateRender('file:./switch.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('switch'=>$_smarty_tpl->tpl_vars['payplug_switch']->value['installment']), 0, false);
?></div>
    </div>
    <div class="payplugPanel">
        <div class="payplugPanel_content">
            <p>
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Allow customers to spread out payments over 2, 3 or 4 installments.','mod'=>'payplug'),$_smarty_tpl ) );?>

                <a class="payplugLink" href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['faq_links']->value['installments'],'htmlall','UTF-8' ));?>
" data-e2e-link="faq" target="_blank"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Learn more.','mod'=>'payplug'),$_smarty_tpl ) );?>
</a>
            </p>

            <div class="payplugTips -<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['payplug_switch']->value['installment']['name'],'htmlall','UTF-8' ));?>
">
                <div class="payplugTips_item -left <?php if (!$_smarty_tpl->tpl_vars['payplug_switch']->value['installment']['checked'] || !$_smarty_tpl->tpl_vars['payplug_switch']->value['installment']['active']) {?> -hide<?php }?>">
                    <p class="payplugAlert -warning"><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Payments by installment are not guaranteed. A default of payment may occur for the upcoming installments.','mod'=>'payplug'),$_smarty_tpl ) );?>
</span></p>
                    <p>
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You can consult all your past and pending installment payments in','mod'=>'payplug'),$_smarty_tpl ) );?>

                        <a class="payplugLink" href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['installments_panel_url']->value,'htmlall','UTF-8' ));?>
" data-e2e-link="installment"> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'a dedicated menu','mod'=>'payplug'),$_smarty_tpl ) );?>
</a>
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'made accessible from the navigation bar, and in the details of each order within the','mod'=>'payplug'),$_smarty_tpl ) );?>

                        <i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Payment with PayPlug','mod'=>'payplug'),$_smarty_tpl ) );?>
</i>
                    </p>
                    <p>
                        <a class="payplugLink" href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['faq_links']->value['installments'],'htmlall','UTF-8' ));?>
" data-e2e-link="faq" target="_blank"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Learn more.','mod'=>'payplug'),$_smarty_tpl ) );?>
</a>
                    </p>

                    <div class="payplugInstallment_fieldset payplugPanel">
                        <div class="payplugPanel_label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Enable payments:','mod'=>'payplug'),$_smarty_tpl ) );?>
</div>
                        <div class="payplugPanel_content">
                            <label for="payplug_installment_mode_2">
                                <input id="payplug_installment_mode_2" type="radio" name="PAYPLUG_INST_MODE" value="2" <?php if ($_smarty_tpl->tpl_vars['PAYPLUG_INST_MODE']->value == 2) {?>checked="checked"<?php }?>>
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'in 2 installments','mod'=>'payplug'),$_smarty_tpl ) );?>

                            </label>
                            <label for="payplug_installment_mode_3">
                                <input id="payplug_installment_mode_3" type="radio" name="PAYPLUG_INST_MODE" value="3" <?php if ($_smarty_tpl->tpl_vars['PAYPLUG_INST_MODE']->value == 3) {?>checked="checked"<?php }?>>
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'in 3 installments','mod'=>'payplug'),$_smarty_tpl ) );?>

                            </label>
                            <label for="payplug_installment_mode_4">
                                <input id="payplug_installment_mode_4" type="radio" name="PAYPLUG_INST_MODE" value="4" <?php if ($_smarty_tpl->tpl_vars['PAYPLUG_INST_MODE']->value == 4) {?>checked="checked"<?php }?>>
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'in 4 installments','mod'=>'payplug'),$_smarty_tpl ) );?>

                            </label>
                        </div>
                    </div>

                    <div class="payplugInstallment_fieldset payplugPanel">
                        <div class="payplugPanel_label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Enable this option from:','mod'=>'payplug'),$_smarty_tpl ) );?>
</div>
                        <div class="payplugPanel_content">
                            <div class="payplugInstallment_amount">
                                <input type="text" name="PAYPLUG_INST_MIN_AMOUNT" value="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['PAYPLUG_INST_MIN_AMOUNT']->value,'htmlall','UTF-8' ));?>
"> €
                                <span style="display: none;" data-e2e-error="installment_amount"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Amount must be greater than 4€ and lower than 20000€.','mod'=>'payplug'),$_smarty_tpl ) );?>
</span>
                            </div>
                        </div>
                    </div>

                    <div class="payplugInstallment_fieldset payplugPanel">
                        <div class="payplugPanel_label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Receive:','mod'=>'payplug'),$_smarty_tpl ) );?>
</div>
                        <div class="payplugPanel_content">
                            <p class="payplugInstallment_schedule -x2<?php if ($_smarty_tpl->tpl_vars['PAYPLUG_INST_MODE']->value == 2) {?> -select<?php }?>">
                                50% <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'of order amount on the first day','mod'=>'payplug'),$_smarty_tpl ) );?>
,<br>
                                50% <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'of order amount after 30 days','mod'=>'payplug'),$_smarty_tpl ) );?>
.
                            </p>
                            <p class="payplugInstallment_schedule -x3<?php if ($_smarty_tpl->tpl_vars['PAYPLUG_INST_MODE']->value == 3) {?> -select<?php }?>">
                                34% <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'of order amount on the first day','mod'=>'payplug'),$_smarty_tpl ) );?>
,<br>
                                33% <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'of order amount after 30 days','mod'=>'payplug'),$_smarty_tpl ) );?>
,<br>
                                33% <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'of order amount after 60 days','mod'=>'payplug'),$_smarty_tpl ) );?>
.
                            </p>
                            <p class="payplugInstallment_schedule -x4<?php if ($_smarty_tpl->tpl_vars['PAYPLUG_INST_MODE']->value == 4) {?> -select<?php }?>">
                                25% <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'of order amount on the first day','mod'=>'payplug'),$_smarty_tpl ) );?>
,<br>
                                25% <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'of order amount after 30 days','mod'=>'payplug'),$_smarty_tpl ) );?>
,<br>
                                25% <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'of order amount after 60 days','mod'=>'payplug'),$_smarty_tpl ) );?>
,<br>
                                25% <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'of order amount after 90 days','mod'=>'payplug'),$_smarty_tpl ) );?>
.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }
}
