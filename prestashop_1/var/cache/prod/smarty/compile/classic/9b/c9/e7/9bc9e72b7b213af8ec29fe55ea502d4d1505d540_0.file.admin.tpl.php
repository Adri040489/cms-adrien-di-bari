<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:34:32
  from '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/admin.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add928bc4cd7_93302372',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9bc9e72b7b213af8ec29fe55ea502d4d1505d540' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/admin.tpl',
      1 => 1638738537,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./panel/fieldset.tpl' => 1,
    'file:./panel/show.tpl' => 1,
    'file:./panel/login.tpl' => 1,
    'file:./panel/settings.tpl' => 1,
  ),
),false)) {
function content_61add928bc4cd7_93302372 (Smarty_Internal_Template $_smarty_tpl) {
?>



<form class="payplug" action="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['form_action']->value,'htmlall','UTF-8' ));?>
" method="post">
    <?php if ((isset($_smarty_tpl->tpl_vars['updated_deferred_state']->value)) && $_smarty_tpl->tpl_vars['updated_deferred_state']->value) {?>
        <p class="alert alert-warning" style="width: 100%;">
            <span>
                <?php $_smarty_tpl->_assignInScope('link_to_order_state', "<a href ='".((string)$_smarty_tpl->tpl_vars['admin_orders_link']->value)."'>");?>
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.admin.toaccesscontrols','sprintf'=>array($_smarty_tpl->tpl_vars['updated_deferred_state_name']->value),'tags'=>array($_smarty_tpl->tpl_vars['link_to_order_state']->value,'<strong>'),'mod'=>'payplug'),$_smarty_tpl ) );?>

            </span>
        </p>
    <?php }?>
    <div class="panel panel-show">
        <div class="panel-heading"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'PRESENTATION','mod'=>'payplug'),$_smarty_tpl ) );?>
</div>
        <div class="panel-row">
            <img src="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['url_logo']->value,'htmlall','UTF-8' ));?>
" />
            <p class="block-title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'The payment solution that increases your sales','mod'=>'payplug'),$_smarty_tpl ) );?>
</p>
            <p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'PayPlug provides merchants all the benefits of a full online payment solution.','mod'=>'payplug'),$_smarty_tpl ) );?>
</p>
            <ul>
                <li><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Accept all Visa and MasterCard credit and debit cards','mod'=>'payplug'),$_smarty_tpl ) );?>
</li>
                <li><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Display the payment form directly on your website, without redirection','mod'=>'payplug'),$_smarty_tpl ) );?>
</li>
                <li><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Customise your payment page with your own colours and design','mod'=>'payplug'),$_smarty_tpl ) );?>
</li>
                <li><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Avoid fraud by using Verified by Visa and MasterCard Secure Code','mod'=>'payplug'),$_smarty_tpl ) );?>
</li>
                <li><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Automatic order update and email confirmation','mod'=>'payplug'),$_smarty_tpl ) );?>
</li>
                <li><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Web interface to manage and export transaction history','mod'=>'payplug'),$_smarty_tpl ) );?>
</li>
                <li><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Funds available on your bank account within 2 to 5 business days','mod'=>'payplug'),$_smarty_tpl ) );?>
</li>
            </ul>
        </div>
    </div>

    <?php $_smarty_tpl->_subTemplateRender('file:./panel/fieldset.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <p class="payplugInterpanel payplugAlert -success">
        <span>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'For more information about installing and configuring the plugin, please consult','mod'=>'payplug'),$_smarty_tpl ) );?>

            <a class="payplugLink" href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['faq_links']->value['guide'],'htmlall','UTF-8' ));?>
" data-e2e-link="faq" target="_blank"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'this support article','mod'=>'payplug'),$_smarty_tpl ) );?>
</a>.
        </span>
    </p>
    <?php $_smarty_tpl->_subTemplateRender('file:./panel/show.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php $_smarty_tpl->_subTemplateRender('file:./panel/login.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php $_smarty_tpl->_subTemplateRender('file:./panel/settings.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</form>
<?php }
}
