<?php
/* Smarty version 3.1.39, created on 2021-12-06 11:43:36
  from '/Applications/MAMP/htdocs/prestashop_1/modules/bpostshm/views/templates/admin/settings/tabs/tab_account.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61ade958d375a0_12053691',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9e8adba67ffaa4e4b098c9d8b70703ad01dd2dc3' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/bpostshm/views/templates/admin/settings/tabs/tab_account.tpl',
      1 => 1638383107,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../blurb/tab_acc-".((string)$_smarty_tpl->tpl_vars[\'iso_lang\']->value).".tpl' => 1,
  ),
),false)) {
function content_61ade958d375a0_12053691 (Smarty_Internal_Template $_smarty_tpl) {
?>
<form class="form-horizontal<?php if ($_smarty_tpl->tpl_vars['version']->value < 1.5) {?> v1-4<?php } elseif ($_smarty_tpl->tpl_vars['version']->value < 1.6) {?> v1-5<?php }?>" action="#" method="POST" autocomplete="off">
	<fieldset class="panel" id="fs-account">

<?php $_smarty_tpl->_subTemplateRender("file:../blurb/tab_acc-".((string)$_smarty_tpl->tpl_vars['iso_lang']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

		<div class="form-group">
			<label class="control-label<?php if ($_smarty_tpl->tpl_vars['version']->value < 1.6) {?>-bw<?php }?> col-lg-3" for="account_id_account"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Account ID','mod'=>'bpostshm'),$_smarty_tpl ) );?>
</label>
			<div class="margin-form col-lg-9">
				<input type="text" name="account_id_account" id="account_id_account" value="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['account_id_account']->value ));?>
" size="50" />
			</div>
			<div class="margin-form col-lg-9 col-lg-offset-3">
				<p class="preference_description help-block">
					<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your 6 digits bpost account ID used for the Shipping Manager','mod'=>'bpostshm'),$_smarty_tpl ) );?>

				</p>
			</div>
		</div>
		<div class="clear"></div>
		<div class="form-group">
			<label class="control-label<?php if ($_smarty_tpl->tpl_vars['version']->value < 1.6) {?>-bw<?php }?> col-lg-3" for="account_passphrase"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Passphrase','mod'=>'bpostshm'),$_smarty_tpl ) );?>
</label>
			<div class="margin-form col-lg-9">
				<input type="text" name="account_passphrase" id="account_passphrase" value="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['account_passphrase']->value ));?>
" size="50" />
			</div>
			<div class="margin-form col-lg-9 col-lg-offset-3">
				<p class="preference_description help-block">
					<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'The passphrase you entered in bpost Shipping Manager back-office application. This is not the password used to access bpost portal.','mod'=>'bpostshm'),$_smarty_tpl ) );?>

				</p>
			</div>
		</div>
		<div class="clear"></div>
								<div class="form-group">
			<label class="control-label<?php if ($_smarty_tpl->tpl_vars['version']->value < 1.6) {?>-bw<?php }?> col-lg-3" for="gmaps_api_key"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Gmaps API key','mod'=>'bpostshm'),$_smarty_tpl ) );?>
</label>
			<div class="margin-form col-lg-9">
								<input type="text" name="gmaps_api_key" id="gmaps_api_key" value="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['gmaps_api_key']->value ));?>
" size="50" />
			</div>
			<div class="margin-form col-lg-9 col-lg-offset-3">
				<p class="preference_description help-block">
					<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your personal Google maps API key','mod'=>'bpostshm'),$_smarty_tpl ) );?>
.
				</p>
			</div>
		</div>
		<div class="clear"></div>
		<input type="hidden" name="store_details" value="">
	<?php if ((isset($_smarty_tpl->tpl_vars['store_details_info']->value))) {?>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['store_details_info']->value, 'details', false, 'key');
$_smarty_tpl->tpl_vars['details']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['details']->value) {
$_smarty_tpl->tpl_vars['details']->do_else = false;
?>
		<?php $_smarty_tpl->_assignInScope('sd_id', "sd-".((string)$_smarty_tpl->tpl_vars['key']->value));?>	
			<div class="form-group">
				<label class="control-label<?php if ($_smarty_tpl->tpl_vars['version']->value < 1.6) {?>-bw<?php }?> col-lg-3" for="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['sd_id']->value ));?>
"><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['details']->value['title'] ));?>
</label>
				<div class="margin-form col-lg-9">
				<?php if (empty($_smarty_tpl->tpl_vars['details']->value['max'])) {?>
					<select name="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['sd_id']->value ));?>
" id="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['sd_id']->value ));?>
">
						<option value="BE" selected><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['details']->value['value'] ));?>
</option>
					</select>
				<?php } else { ?>
				<?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, "input_msg", null, null);?>
					<?php if ((isset($_smarty_tpl->tpl_vars['details']->value['invalid']))) {?>
						"<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['details']->value['invalid'] ));?>
"
					<?php } else { ?>
						"<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'This field cannot be empty','mod'=>'bpostshm'),$_smarty_tpl ) );?>
"
					<?php }?>
				<?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>
															<input type="text" name="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['sd_id']->value ));?>
" id="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['sd_id']->value ));?>
" value="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['details']->value['value'] ));?>
" data-sdkey="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['key']->value ));?>
" size="50" maxlength="<?php echo intval($_smarty_tpl->tpl_vars['details']->value['max']);?>
" <?php if ($_smarty_tpl->tpl_vars['details']->value['required']) {?>required="required" placeholder=<?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'input_msg');?>
 data-invalid=<?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'input_msg');?>
 <?php if ((isset($_smarty_tpl->tpl_vars['details']->value['pattern']))) {?>data-pattern="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['details']->value['pattern'],'javascript' ));?>
"<?php }
}?> />
					<?php if ($_smarty_tpl->tpl_vars['details']->value['required']) {?><sup>*</sup><?php }?>
				<?php }?>
				</div>
				<?php if ((isset($_smarty_tpl->tpl_vars['details']->value['description']))) {?>
				<div class="margin-form col-lg-9 col-lg-offset-3">
					<p class="preference_description help-block">
						<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['details']->value['description'] ));?>

					</p>
				</div>
				<?php }?>
			</div>
			<div class="clear"></div>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	<?php }?>
		<div class="margin-form panel-footer">
			<button class="button btn btn-default pull-right" type="submit" name="submitAccountSettings">
				<i class="process-icon-save"></i>
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Save settings','mod'=>'bpostshm'),$_smarty_tpl ) );?>

			</button>
		</div>
	</fieldset>
</form>
<?php }
}
