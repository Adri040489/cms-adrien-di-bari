<?php
/* Smarty version 3.1.39, created on 2021-12-06 11:43:36
  from '/Applications/MAMP/htdocs/prestashop_1/modules/bpostshm/views/templates/admin/settings/blurb/tab_acc-fr.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61ade958d42604_76905839',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b9b0f83622c4469622e094f0eb5d5425cbfc780e' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/bpostshm/views/templates/admin/settings/blurb/tab_acc-fr.tpl',
      1 => 1638383107,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61ade958d42604_76905839 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel-body">
	<p>bpost Shipping Manager est un service offert par bpost et qui autorise votre clientèle à choisir sa méthode de livraison préférée lors d'une commande sur votre boutique.</p>
	<p>Les méthodes de livraison suivantes sont supportées :</p>
	<ul><li>Livraison à domicile ou au bureau</li><li>Livraison en point de retrait ou bureau de poste</li><li>Livraison en distributeur de paquets</li></ul>
	<p>
		Une fois activé et correctement configuré, ce module permet une intégration complète de l'outil d'administration bpost dans votre boutique et l'ajout automatique de vos commandes avec le portail bpost. Il est de plus possible de générer vos étiquettes et codes de suivi directement depuis l'administration PrestaShop.
		<br />
		Zéro tracas, transparence totale !
	</p>
	<p><span class="label label-danger red">Attention</span>:  Si vous n'utilisez PAS PrestaShop pour gérer les étiquettes ET si vous laissez votre client choisir une date de livraison (le samedi ou choix libre en semaine), le jour de dépôt (drop date) dans le réseau bpost ne serra PAS affiché dans le Shipping Manager.
	</p>
	<p>
		<a href="http://bpost.freshdesk.com/support/solutions/folders/208531" title="Documentation" target="_blank">
			<img src="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['module_dir']->value ));?>
views/img/icons/information.png" alt="Documentation" />Documentation
		</a>
	</p>
</div>
<br>
<div class="form-group">
	<?php if ($_smarty_tpl->tpl_vars['version']->value < 1.6) {?>
	<div class="control-label<?php if ($_smarty_tpl->tpl_vars['version']->value < 1.6) {?>-bw<?php }?> col-lg-3">
		<span class="label label-danger red">Important</span>
	</div>
	<?php }?>
	<div class="margin-form col-lg-9<?php if ($_smarty_tpl->tpl_vars['version']->value >= 1.6) {?> col-lg-offset-3<?php }?>">
		<?php if ($_smarty_tpl->tpl_vars['version']->value >= 1.6) {?><p><span class="label label-danger red">Important</span></p><?php }?>
		<p>
			Un compte bpost est requis pour utiliser ce module. Appelez le 02/201 11 11.
			<br />
			<a href="https://www.bpost.be/portal/goLogin?cookieAdded=yes&oss_language=<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['iso_code']->value ));?>
" title="Cliquer ici" target="_blank">Cliquer ici</a>
			pour vous connecter à votre compte bpost.
		</p>
	</div>
</div>
<div class="clear"></div>
<?php }
}
