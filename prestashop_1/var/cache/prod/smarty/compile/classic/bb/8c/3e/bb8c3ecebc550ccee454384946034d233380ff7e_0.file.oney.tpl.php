<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:34:32
  from '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/settings/oney.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add928c5bc77_48907176',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bb8c3ecebc550ccee454384946034d233380ff7e' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/settings/oney.tpl',
      1 => 1638738537,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./switch.tpl' => 2,
    'file:./oney_fees.tpl' => 1,
  ),
),false)) {
function content_61add928c5bc77_48907176 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="panel-row separate_margin_block">
    <div class="payplugPanel">
        <div class="payplugPanel_label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.panel.setting.oney.label','mod'=>'payplug'),$_smarty_tpl ) );?>
</div>
        <div class="payplugPanel_content"><?php $_smarty_tpl->_subTemplateRender('file:./switch.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('switch'=>$_smarty_tpl->tpl_vars['payplug_switch']->value['oney']), 0, false);
?></div>
    </div>
    <div class="payplugPanel">
        <div class="payplugPanel_content">
            <p>
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Allow customers to spread out payments over 3 or 4 installments from %d € to %d €.','mod'=>'payplug','sprintf'=>array($_smarty_tpl->tpl_vars['oney_min_amounts']->value,$_smarty_tpl->tpl_vars['oney_max_amounts']->value)),$_smarty_tpl ) );?>

                <a class="payplugLink" href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['faq_links']->value['oney'],'htmlall','UTF-8' ));?>
" data-e2e-link="faq" target="_blank"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Learn more.','mod'=>'payplug'),$_smarty_tpl ) );?>
</a>
            </p>
            <div class="payplugTips -<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['payplug_switch']->value['oney']['name'],'htmlall','UTF-8' ));?>
">
                <div class="payplugTips_item -left <?php if (!$_smarty_tpl->tpl_vars['payplug_switch']->value['oney']['checked'] || !$_smarty_tpl->tpl_vars['payplug_switch']->value['oney']['active']) {?> -hide<?php }?>">
                    <div class="payplugOney">
                        <?php $_smarty_tpl->_subTemplateRender('file:./oney_fees.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                        <div class="payplugOneyOptimized">
                            <?php $_smarty_tpl->_subTemplateRender('file:./switch.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('switch'=>$_smarty_tpl->tpl_vars['payplug_switch']->value['oney_optimized']), 0, true);
?>
                            <p>
                                <strong><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Go further','mod'=>'payplug'),$_smarty_tpl ) );?>
</strong>
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Maximise your customers\' experience with dynamic calculations of the 3 and 4 instalments by switching on to the advanced configuration.','mod'=>'payplug'),$_smarty_tpl ) );?>

                                <a class="payplugLink" href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['faq_links']->value['oney'],'htmlall','UTF-8' ));?>
#h_2595dd3d-a281-43ab-a51a-4986fecde5ee" data-e2e-link="faq" target="_blank"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Learn more.','mod'=>'payplug'),$_smarty_tpl ) );?>
</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }
}
