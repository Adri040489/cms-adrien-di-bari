<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:34:32
  from '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/fieldset.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add928be4219_22981490',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c5e17622ed161c298049e13f0e6fa1d92585dcf1' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/fieldset.tpl',
      1 => 1638738537,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61add928be4219_22981490 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="panel payplugConfig">
    <div class="panel-heading"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'STATUS','mod'=>'payplug'),$_smarty_tpl ) );?>
</div>
    <div class="panel-row">
        <?php if ((isset($_smarty_tpl->tpl_vars['check_configuration']->value['warning'])) && !empty($_smarty_tpl->tpl_vars['check_configuration']->value['warning']) && sizeof($_smarty_tpl->tpl_vars['check_configuration']->value['warning'])) {?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['check_configuration']->value['warning'], 'warning');
$_smarty_tpl->tpl_vars['warning']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['warning']->value) {
$_smarty_tpl->tpl_vars['warning']->do_else = false;
?>
                <p class="payplugAlert -warning"><span><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['warning']->value,'htmlall','UTF-8' ));?>
</span></p>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        <?php }?>
        <p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Version of PayPlug module:','mod'=>'payplug'),$_smarty_tpl ) );?>
 <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pp_version']->value,'htmlall','UTF-8' ));?>
</p>
        <?php if ((isset($_smarty_tpl->tpl_vars['check_configuration']->value['success'])) && !empty($_smarty_tpl->tpl_vars['check_configuration']->value['success']) && sizeof($_smarty_tpl->tpl_vars['check_configuration']->value['success'])) {?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['check_configuration']->value['success'], 'success');
$_smarty_tpl->tpl_vars['success']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['success']->value) {
$_smarty_tpl->tpl_vars['success']->do_else = false;
?>
                <p class="payplugConfig_item -success"><span><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['success']->value,'htmlall','UTF-8' ));?>
</span></p>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        <?php }?>
        <?php if ((isset($_smarty_tpl->tpl_vars['check_configuration']->value['error'])) && !empty($_smarty_tpl->tpl_vars['check_configuration']->value['error']) && sizeof($_smarty_tpl->tpl_vars['check_configuration']->value['error'])) {?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['check_configuration']->value['error'], 'error');
$_smarty_tpl->tpl_vars['error']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['error']->value) {
$_smarty_tpl->tpl_vars['error']->do_else = false;
?>
                <p class="payplugConfig_item -error"><span><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['error']->value,'htmlall','UTF-8' ));?>
</span></p>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        <?php }?>
        <?php if ((isset($_smarty_tpl->tpl_vars['check_configuration']->value['other'])) && !empty($_smarty_tpl->tpl_vars['check_configuration']->value['other']) && sizeof($_smarty_tpl->tpl_vars['check_configuration']->value['other'])) {?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['check_configuration']->value['other'], 'other');
$_smarty_tpl->tpl_vars['other']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['other']->value) {
$_smarty_tpl->tpl_vars['other']->do_else = false;
?>
                <p class="payplugConfig_item -<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['other']->value['type'],'quotes','UTF-8' ));?>
"><span><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['other']->value['text'],'quotes','UTF-8' ));?>
</span></p>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        <?php }?>

        <img class="payplugLoader" src="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['module_dir']->value,'htmlall','UTF-8' ));?>
views/img/admin/spinner.gif" />
    </div>
    <div class="panel-footer">
        <button type="button" class="payplugButton payplugConfig_check" data-e2e-type="button" data-e2e-action="check"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Check','mod'=>'payplug'),$_smarty_tpl ) );?>
</button>
    </div>
</div>
<?php }
}
