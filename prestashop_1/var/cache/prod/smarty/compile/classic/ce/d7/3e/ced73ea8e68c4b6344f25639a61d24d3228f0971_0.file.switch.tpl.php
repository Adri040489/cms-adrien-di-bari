<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:34:32
  from '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/settings/switch.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add928c02552_60369013',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ced73ea8e68c4b6344f25639a61d24d3228f0971' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/settings/switch.tpl',
      1 => 1638738537,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61add928c02552_60369013 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="payplugSwitch<?php if (!$_smarty_tpl->tpl_vars['switch']->value['checked'] || !$_smarty_tpl->tpl_vars['switch']->value['active']) {?> -right<?php }
if ((isset($_smarty_tpl->tpl_vars['switch']->value['small'])) && $_smarty_tpl->tpl_vars['switch']->value['small']) {?> -small<?php }
if (!$_smarty_tpl->tpl_vars['switch']->value['active']) {?> -disabled<?php }?>">
    <input type="radio" name="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['switch']->value['name'],'htmlall','UTF-8' ));?>
" value="1" id="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['switch']->value['name'],'htmlall','UTF-8' ));?>
_left" <?php if ($_smarty_tpl->tpl_vars['switch']->value['checked']) {?>checked="checked"<?php }?>>
    <input type="radio" name="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['switch']->value['name'],'htmlall','UTF-8' ));?>
" value="0" id="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['switch']->value['name'],'htmlall','UTF-8' ));?>
_right" <?php if (!$_smarty_tpl->tpl_vars['switch']->value['checked']) {?>checked="checked"<?php }?>>

    <?php if ((isset($_smarty_tpl->tpl_vars['switch']->value['label_left'])) && $_smarty_tpl->tpl_vars['switch']->value['label_left']) {?><label for="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['switch']->value['name'],'htmlall','UTF-8' ));?>
_left" class="payplugSwitch_label -left"><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['switch']->value['label_left'],'htmlall','UTF-8' ));?>
</label><?php }?>
    <?php if ((isset($_smarty_tpl->tpl_vars['switch']->value['label_right'])) && $_smarty_tpl->tpl_vars['switch']->value['label_right']) {?><label for="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['switch']->value['name'],'htmlall','UTF-8' ));?>
_right" class="payplugSwitch_label -right"><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['switch']->value['label_right'],'htmlall','UTF-8' ));?>
</label><?php }?>
</div>
<?php }
}
