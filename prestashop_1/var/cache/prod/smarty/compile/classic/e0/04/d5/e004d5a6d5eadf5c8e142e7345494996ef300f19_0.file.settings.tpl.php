<?php
/* Smarty version 3.1.39, created on 2021-12-06 11:43:36
  from '/Applications/MAMP/htdocs/prestashop_1/modules/bpostshm/views/templates/admin/settings/settings.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61ade958d0df88_66209044',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e004d5a6d5eadf5c8e142e7345494996ef300f19' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/bpostshm/views/templates/admin/settings/settings.tpl',
      1 => 1638383107,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./tabs/tab_account.tpl' => 1,
    'file:./tabs/tab_delivery_opts.tpl' => 1,
    'file:./tabs/tab_delivery.tpl' => 1,
    'file:./tabs/tab_label.tpl' => 1,
    'file:./tabs/tab_intl.tpl' => 1,
  ),
),false)) {
function content_61ade958d0df88_66209044 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="bpost-settings">
	<h2><img src="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['module_dir']->value,'htmlall','UTF-8' ));?>
views/img/logo-carrier.jpg" alt="bpost" /> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'bpost Shipping manager','mod'=>'bpostshm'),$_smarty_tpl ) );?>
</h2>
	<br />
	<?php if (!empty($_smarty_tpl->tpl_vars['errors']->value)) {?>
		<?php if ($_smarty_tpl->tpl_vars['version']->value >= 1.6) {?>
			<?php if ((!(isset($_smarty_tpl->tpl_vars['disableDefaultErrorOutPut']->value)) || $_smarty_tpl->tpl_vars['disableDefaultErrorOutPut']->value == false)) {?>
				<div class="bootstrap">
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?php if (count($_smarty_tpl->tpl_vars['errors']->value) > 1) {?>
							<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'%d errors','sprintf'=>count($_smarty_tpl->tpl_vars['errors']->value),'mod'=>'bpostshm'),$_smarty_tpl ) );?>

							<br/>
						<?php }?>
						<ol>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['errors']->value, 'error');
$_smarty_tpl->tpl_vars['error']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['error']->value) {
$_smarty_tpl->tpl_vars['error']->do_else = false;
?>
								<li><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['error']->value ));?>
</li>
							<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
						</ol>
					</div>
				</div>
			<?php }?>
		<?php } else { ?>
			<div class="error">
				<ul><?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['errors']->value, 'error');
$_smarty_tpl->tpl_vars['error']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['error']->value) {
$_smarty_tpl->tpl_vars['error']->do_else = false;
?><li><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['error']->value ));?>
</li><?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?></ul>
			</div>
		<?php }?>
		<br />
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['version']->value < 1.6) {?><legend class="tab-wrapper"><?php }?>
	<ul class="bpost-tabs">
				<li>
			<a href="#fs-account">
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Account settings','mod'=>'bpostshm'),$_smarty_tpl ) );?>

			</a>
		</li>
		<?php if (((isset($_smarty_tpl->tpl_vars['valid_account']->value)) && $_smarty_tpl->tpl_vars['valid_account']->value)) {?>
		<li>
			<a href="#fs-delopts">
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Delivery options','mod'=>'bpostshm'),$_smarty_tpl ) );?>

			</a>
		</li>
		<li>
			<a href="#fs-delivery-set">
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Delivery settings','mod'=>'bpostshm'),$_smarty_tpl ) );?>

			</a>
		</li>
		<li>
			<a href="#fs-intl-set">
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'International settings','mod'=>'bpostshm'),$_smarty_tpl ) );?>

			</a>
		</li>
		<li>
			<a href="#fs-label-set">
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Label settings','mod'=>'bpostshm'),$_smarty_tpl ) );?>

			</a>
		</li>
		<?php }?>
	</ul>
	<?php if ($_smarty_tpl->tpl_vars['version']->value < 1.6) {?></legend><?php }?>
	
		<?php $_smarty_tpl->_subTemplateRender("file:./tabs/tab_account.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
	
	<?php if (((isset($_smarty_tpl->tpl_vars['valid_account']->value)) && $_smarty_tpl->tpl_vars['valid_account']->value)) {?>
				<?php $_smarty_tpl->_subTemplateRender("file:./tabs/tab_delivery_opts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

				<?php if ((isset($_smarty_tpl->tpl_vars['display_delivery_date']->value))) {?>
			<?php $_smarty_tpl->_subTemplateRender("file:./tabs/tab_delivery.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
		<?php }?>
		
				<?php if ((isset($_smarty_tpl->tpl_vars['label_use_ps_labels']->value))) {?>
			<?php $_smarty_tpl->_subTemplateRender("file:./tabs/tab_label.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
		<?php }?>
		
				<?php if (empty($_smarty_tpl->tpl_vars['errors']->value)) {?>
			<?php $_smarty_tpl->_subTemplateRender("file:./tabs/tab_intl.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
		<?php }?>
	<?php }?>
</div>
<?php echo '<script'; ?>
 type="text/javascript">
// <![CDATA[
var sas = {
		lastTab: <?php echo intval($_smarty_tpl->tpl_vars['last_set_tab']->value);?>
,
		contentText: {
			tipFrom: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Minimum purchase total required in order to trigger the option excluding taxes & shipping costs','mod'=>'bpostshm','js'=>1),$_smarty_tpl ) );?>
",
			tipCost: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'added shipping costs','mod'=>'bpostshm','js'=>1),$_smarty_tpl ) );?>
",
			errors: {
				retrieveList: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Unable to retrieve the list. Please try again later.','mod'=>'bpostshm','js'=>1),$_smarty_tpl ) );?>
",
				emptyField: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'This field cannot be empty','mod'=>'bpostshm','js'=>1),$_smarty_tpl ) );?>
",
				gmapsBadKey: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Invalid Gmaps key','mod'=>'bpostshm','js'=>1),$_smarty_tpl ) );?>
",
			}
		},
		urls: {
			enabledCountries: "<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['url_get_enabled_countries']->value,'javascript' ));?>
",
			testGmaps: "<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['url_test_gmaps']->value,'javascript' ));?>
",
		},
	<?php if (((isset($_smarty_tpl->tpl_vars['valid_account']->value)) && $_smarty_tpl->tpl_vars['valid_account']->value)) {?>		
		home24hBusiness: <?php echo intval($_smarty_tpl->tpl_vars['home_24h_business']->value);?>
,
		intlEnabled: <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'json_encode' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_countries']->value ));?>
,
		def_cutoff: '<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['cutoff_time']->value ));?>
',
		delopt_args: {
			tf: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'as from','mod'=>'bpostshm','js'=>1),$_smarty_tpl ) );?>
",
			tc: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'with additional cost of','mod'=>'bpostshm','js'=>1),$_smarty_tpl ) );?>
",
			disables: { "1-350": ["1-300"] },
			inputStyle: 'width: <?php if ($_smarty_tpl->tpl_vars['version']->value > 1.5) {?>75<?php } else { ?>65<?php }?>px; padding-left: 4px; margin-left: 4px;',
		},
		defOrderStates: <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'json_encode' ][ 0 ], array( $_smarty_tpl->tpl_vars['display_order_states']->value ));?>
,
		daysDisplayedLimits: {
			min: 2,
			max: 999,
			def: <?php echo intval($_smarty_tpl->tpl_vars['order_display_days']->value);?>
,
		},
	<?php }?>		
};
// ]]>
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['settings_src']->value,'javascript' ));?>
"><?php echo '</script'; ?>
>
<?php }
}
