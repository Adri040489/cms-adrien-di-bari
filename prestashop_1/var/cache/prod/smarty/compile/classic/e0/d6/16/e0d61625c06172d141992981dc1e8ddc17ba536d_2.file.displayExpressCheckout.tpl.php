<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:28:10
  from '/Applications/MAMP/htdocs/prestashop_1/modules/ps_checkout/views/templates/hook/displayExpressCheckout.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add7aac93a34_81985720',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e0d61625c06172d141992981dc1e8ddc17ba536d' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/ps_checkout/views/templates/hook/displayExpressCheckout.tpl',
      1 => 1638738859,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61add7aac93a34_81985720 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="js-ps_checkout-express-button-container">
</div>

<div class="ps_checkout payment-method-logo-block">
  <div class="ps_checkout payment-method-logo-block-title">
    <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['modulePath']->value, ENT_QUOTES, 'UTF-8');?>
views/img/lock_checkout.svg" alt="">
    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'100% secure payments','mod'=>'ps_checkout'),$_smarty_tpl ) );?>

  </div>
  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['paymentOptions']->value, 'paymentOption');
$_smarty_tpl->tpl_vars['paymentOption']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['paymentOption']->value) {
$_smarty_tpl->tpl_vars['paymentOption']->do_else = false;
?>
    <?php if ($_smarty_tpl->tpl_vars['paymentOption']->value == 'card') {?>
      <div class="ps_checkout payment-method-logo w-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['width']->value, ENT_QUOTES, 'UTF-8');?>
">
        <div class="wrapper"><img class="" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['modulePath']->value, ENT_QUOTES, 'UTF-8');?>
views/img/visa.svg" alt=""></div>
      </div>
      <div class="ps_checkout payment-method-logo w-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['width']->value, ENT_QUOTES, 'UTF-8');?>
">
        <div class="wrapper"><img class="" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['modulePath']->value, ENT_QUOTES, 'UTF-8');?>
views/img/mastercard.svg" alt=""></div>
      </div>
      <div class="ps_checkout payment-method-logo w-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['width']->value, ENT_QUOTES, 'UTF-8');?>
">
        <div class="wrapper"><img class="" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['modulePath']->value, ENT_QUOTES, 'UTF-8');?>
views/img/amex.svg" alt=""></div>
      </div>
    <?php } else { ?>
      <div class="ps_checkout payment-method-logo w-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['width']->value, ENT_QUOTES, 'UTF-8');?>
">
          <div class="wrapper"><img class="" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['modulePath']->value, ENT_QUOTES, 'UTF-8');?>
views/img/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['paymentOption']->value, ENT_QUOTES, 'UTF-8');?>
.svg" alt=""></div>
      </div>
    <?php }?>
  <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</div>

<?php if ((isset($_smarty_tpl->tpl_vars['cart']->value)) && $_smarty_tpl->tpl_vars['payIn4XisOrderPageEnabled']->value == true) {?>
  <hr />
  <div
    data-pp-message
    data-pp-placement="cart"
    data-pp-style-layout="text"
    data-pp-style-logo-type="inline"
    data-pp-style-text-color="black"
    data-pp-amount="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['totals']['total']['amount'], ENT_QUOTES, 'UTF-8');?>
"></div>
  <?php echo '<script'; ?>
>
    window.ps_checkoutPayPalSdkInstance
      && window.ps_checkoutPayPalSdkInstance.Messages
      && window.ps_checkoutPayPalSdkInstance.Messages().render('[data-pp-message]');
  <?php echo '</script'; ?>
>
<?php }
}
}
