<?php
/* Smarty version 3.1.39, created on 2021-12-06 11:05:03
  from '/Applications/MAMP/htdocs/prestashop_1/modules/packlink/views/templates/hook/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61ade04f1caf56_00516194',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e3ca780cc7942947dad8d59fd409095205da21ed' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/packlink/views/templates/hook/index.tpl',
      1 => 1638781461,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61ade04f1caf56_00516194 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="pl-page">
  <header id="pl-main-header">
    <div class="pl-main-logo">
      <img src="https://cdn.packlink.com/apps/giger/logos/packlink-pro.svg" alt="logo">
    </div>
    <div class="pl-header-holder" id="pl-header-section"></div>
  </header>

  <main id="pl-main-page-holder"></main>

  <div class="pl-spinner pl-hidden" id="pl-spinner">
    <div></div>
  </div>

  <template id="pl-alert">
    <div class="pl-alert-wrapper">
      <div class="pl-alert">
        <span class="pl-alert-text"></span>
        <i class="material-icons">close</i>
      </div>
    </div>
  </template>

  <template id="pl-modal">
    <div id="pl-modal-mask" class="pl-modal-mask pl-hidden">
      <div class="pl-modal">
        <div class="pl-modal-close-button">
          <i class="material-icons">close</i>
        </div>
        <div class="pl-modal-title">

        </div>
        <div class="pl-modal-body">

        </div>
        <div class="pl-modal-footer">
        </div>
      </div>
    </div>
  </template>

  <template id="pl-error-template">
    <div class="pl-error-message" data-pl-element="error">
    </div>
  </template>
</div>

<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['gridResizerScript']->value;?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
    document.addEventListener('DOMContentLoaded', function () {
        Packlink.translations = {
            default: <?php echo htmlspecialchars_decode(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['lang']->value['default'],'htmlall','UTF-8' )),3);?>
,
            current: <?php echo htmlspecialchars_decode(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['lang']->value['current'],'htmlall','UTF-8' )),3);?>

        };

        let pageConfiguration = <?php echo htmlspecialchars_decode(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( json_encode($_smarty_tpl->tpl_vars['urls']->value),'htmlall','UTF-8' )),3);?>
;

        Packlink.state = new Packlink.StateController(
            {
                baseResourcesUrl: "<?php echo htmlspecialchars_decode(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['baseResourcesUrl']->value,'htmlall','UTF-8' )),3);?>
",
                stateUrl: "<?php echo htmlspecialchars_decode(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['stateUrl']->value,'htmlall','UTF-8' )),3);?>
",
                pageConfiguration: pageConfiguration,
                templates: <?php echo htmlspecialchars_decode(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( json_encode($_smarty_tpl->tpl_vars['templates']->value),'htmlall','UTF-8' )),3);?>

            }
        );

        Packlink.state.display();

        hidePrestaSpinner();
        calculateContentHeight(5);
    });
<?php echo '</script'; ?>
>
<?php }
}
