<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:34:32
  from '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/user/disconnected.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add928c173e6_02931382',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e66537bc870a60d4948721c2a44036564bce738a' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/user/disconnected.tpl',
      1 => 1638738537,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61add928c173e6_02931382 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="payplugPanel">
    <div class="payplugPanel_label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Email','mod'=>'payplug'),$_smarty_tpl ) );?>
</div>
    <div class="payplugPanel_content">
        <input type="text" name="PAYPLUG_EMAIL" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'E-mail address','mod'=>'payplug'),$_smarty_tpl ) );?>
" value="<?php if ((isset($_smarty_tpl->tpl_vars['PAYPLUG_EMAIL']->value))) {
echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['PAYPLUG_EMAIL']->value,'htmlall','UTF-8' ));
}?>"/>
        <span class="input-error">
            <span class="error-email-input"><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['p_error']->value,'htmlall','UTF-8' ));?>
</span>
            <span id="error-email-regexp" class="hide"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'E-mail address is not valid.','mod'=>'payplug'),$_smarty_tpl ) );?>
</span>
        </span>
    </div>
</div>
<div class="payplugPanel">
    <div class="payplugPanel_label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Password','mod'=>'payplug'),$_smarty_tpl ) );?>
</div>
    <div class="payplugPanel_content">
        <input type="password" name="PAYPLUG_PASSWORD" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Password','mod'=>'payplug'),$_smarty_tpl ) );?>
" value=""/>
        <span class="input-error">
            <span class="error-password-input"><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['p_error']->value,'htmlall','UTF-8' ));?>
</span>
            <span id="error-password-regexp" class="hide"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Password must be a least 8 caracters long.','mod'=>'payplug'),$_smarty_tpl ) );?>
</span>
        </span>
    </div>
</div>
<div class="payplugPanel">
    <div class="payplugPanel_content">
        <a class="payplugLink" href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['site_url']->value,'htmlall','UTF-8' ));?>
/portal/forgot_password" target="_blank" data-e2e-link="forgot_password"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Forgot your password?','mod'=>'payplug'),$_smarty_tpl ) );?>
</a>
    </div>
</div>
<div class="payplugPanel">
    <div class="payplugPanel_content">
        <button type="button" class="payplugButton -green payplugLogin_login" data-e2e-type="button" data-e2e-action="login"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Connect account','mod'=>'payplug'),$_smarty_tpl ) );?>
</button>
    </div>
</div>
<div class="payplugPanel">
    <div class="payplugPanel_content">
        <p>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Don\'t have an account?','mod'=>'payplug'),$_smarty_tpl ) );?>
<br>
            <a class="payplugLink" href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['site_url']->value,'htmlall','UTF-8' ));?>
/portal/signup?origin=PrestashopV2Config" data-e2e-link="create_account" target="_blank"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sign up','mod'=>'payplug'),$_smarty_tpl ) );?>
</a>
        </p>
    </div>
</div>

<?php }
}
