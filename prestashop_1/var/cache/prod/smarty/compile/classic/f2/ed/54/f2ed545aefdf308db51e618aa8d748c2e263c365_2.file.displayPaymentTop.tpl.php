<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:15:16
  from '/Applications/MAMP/htdocs/prestashop_1/modules/ps_checkout/views/templates/hook/displayPaymentTop.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add4a4217d63_28701004',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f2ed545aefdf308db51e618aa8d748c2e263c365' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/ps_checkout/views/templates/hook/displayPaymentTop.tpl',
      1 => 1638738859,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61add4a4217d63_28701004 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="ps_checkout-notification-container">
  <div id="ps_checkout-canceled" class="alert alert-warning-custom" style="display:none;">
    <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['shoppingCartWarningPath']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['warningTranslatedText']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
    <strong><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Processing payment canceled, please choose another payment method or try again.','mod'=>'ps_checkout'),$_smarty_tpl ) );?>
</strong>
  </div>
  <div id="ps_checkout-error" class="alert alert-danger-custom" style="display:none;">
    <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['shoppingCartWarningPath']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['warningTranslatedText']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
    <strong><span id="ps_checkout-error-text"></span></strong>
  </div>
</div>

<?php if ($_smarty_tpl->tpl_vars['is17']->value && $_smarty_tpl->tpl_vars['isExpressCheckout']->value) {?>
<div class="express-checkout-block mb-2">
  <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['paypalLogoPath']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="express-checkout-img" alt="PayPal">
  <p class="express-checkout-label">
    <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['translatedText']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

  </p>
</div>
<?php } elseif ($_smarty_tpl->tpl_vars['is17']->value) {?>
  <div id="ps_checkout-loader" class="express-checkout-block mb-2">
    <div class="express-checkout-block-wrapper">
      <p class="express-checkout-spinner-text">
        <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['loaderTranslatedText']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

      </p>
      <div class="express-checkout-spinner">
        <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spinnerPath']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['loaderTranslatedText']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
      </div>
    </div>
  </div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['is17']->value) {
echo '<script'; ?>
>
  document.addEventListener('DOMContentLoaded', function() {
    const paymentOptions = document.querySelectorAll('input[name="payment-option"]');

    if (null !== paymentOptions) {
      paymentOptions.forEach(function(paymentOption) {
        const paymentOptionContainer = document.getElementById(paymentOption.id + '-container');
        const paymentOptionName = paymentOption.getAttribute('data-module-name');

        if (-1 !== paymentOptionName.search('ps_checkout')) {
          paymentOptionContainer.style.display = 'none';
        }
      });
    }
  });
<?php echo '</script'; ?>
>
<?php }
}
}
