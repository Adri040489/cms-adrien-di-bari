<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:15:16
  from '/Applications/MAMP/htdocs/prestashop_1/modules/packlink/views/templates/hook/shipping_methods_17.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add4a419b638_79315786',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'faa2ce840d106031e0da3ada930675bad7e46a8b' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/packlink/views/templates/hook/shipping_methods_17.tpl',
      1 => 1638781461,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61add4a419b638_79315786 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="pl-template">
  <div class="row pl-dropof" id="pl-dropoff">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12">
          <p>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'This shipping service supports delivery to pre-defined drop-off locations. Please choose location that suits you the most by clicking on the "Select drop-off location" button.','mod'=>'packlink'),$_smarty_tpl ) );?>

          </p>
        </div>
      </div>
      <div class="row pl-sm-margin-bottom">
        <div class="col-xs-6">
          <span id="pl-message" class="pl-message"></span>
        </div>
        <div class="col-xs-6">
          <button
                  type="button"
                  class="btn btn-primary button button-primary float-xs-right pl-17-button"
                  id="pl-dropoff-button"
          >
          </button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="pl-input-mask hidden" id="pl-map-modal">
  <div class="pl-map-modal" id="pl-modal-content">
    <div class="pl-modal-spinner-wrapper disabled" id="pl-modal-spinner">
      <div class="pl-modal-spinner"></div>
    </div>
    <div class="pl-close-modal" id="pl-close-modal-btn"><i class="material-icons">close</i></div>

    <location-picker>
      <div class="lp-content" data-lp-id="content">
        <div class="lp-locations">
          <div class="lp-input-wrapper">
            <div class="input">
              <input type="text" title="" required data-lp-id="search-box">
              <span class="label" data-lp-id="search-box-label"></span>
            </div>
          </div>

          <div data-lp-id="locations"></div>
        </div>
      </div>
    </location-picker>

  </div>
</div>


<location-picker-template>
  <div class="lp-template" id="template-container">
    <div data-lp-id="working-hours-template" class="lp-hour-wrapper">
      <div class="day" data-lp-id="day">
      </div>
      <div class="hours" data-lp-id="hours">
      </div>
    </div>

    <div class="lp-location-wrapper" data-lp-id="location-template">
      <div class="composite lp-expand">
        <div class="street-name uppercase" data-lp-id="composite-address"></div>
        <div class="lp-working-hours-btn excluded" data-lp-composite data-lp-id="show-composite-working-hours-btn"></div>
        <div data-lp-id="composite-working-hours" class="lp-working-hours">

        </div>
        <div class="lp-select-column">
          <div class="lp-select-button excluded" data-lp-id="composite-select-btn"></div>
          <a class="excluded" href="#" data-lp-id="composite-show-on-map" target="_blank"></a>
        </div>
      </div>
      <div class="name uppercase lp-collapse" data-lp-id="location-name"></div>
      <div class="street lp-collapse">
        <div class="street-name uppercase" data-lp-id="location-street"></div>
        <div class="lp-working-hours-btn excluded" data-lp-id="show-working-hours-btn"></div>
        <div data-lp-id="working-hours" class="lp-working-hours">

        </div>
      </div>
      <div class="city uppercase lp-collapse" data-lp-id="location-city">
      </div>
      <div class="lp-select-column lp-collapse">
        <div class="lp-select-button excluded" data-lp-id="select-btn"></div>
      </div>
      <a class="excluded lp-collapse" href="#" data-lp-id="show-on-map" target="_blank">
        <div class="lp-show-on-map-btn excluded"></div>
      </a>
    </div>
  </div>
</location-picker-template>

<?php echo '<script'; ?>
>
  Packlink.trans = {
    select: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Select drop-off location','mod'=>'packlink'),$_smarty_tpl ) );?>
",
    change: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Change drop-off location','mod'=>'packlink'),$_smarty_tpl ) );?>
",
    address: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Package will be delivered to:','mod'=>'packlink'),$_smarty_tpl ) );?>
",
    wrongAddress: "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'There are no delivery locations available for your delivery address. Please change your address.','mod'=>'packlink'),$_smarty_tpl ) );?>
"
  };

  Packlink.checkOut = new Packlink.CheckOutController(
      JSON.parse(
          '<?php echo htmlspecialchars(htmlspecialchars_decode(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['configuration']->value,'htmlall','UTF-8' )),3), ENT_QUOTES, 'UTF-8');?>
'
              .replace(/&quot;/g, '"')
              .replace(/&amp;/g, '&')
      )
  );

  Packlink.checkOut.init();
<?php echo '</script'; ?>
>
<?php }
}
