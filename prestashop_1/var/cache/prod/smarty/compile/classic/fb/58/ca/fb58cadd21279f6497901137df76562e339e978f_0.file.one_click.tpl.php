<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:34:32
  from '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/settings/one_click.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add928c4d296_80589629',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fb58cadd21279f6497901137df76562e339e978f' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/modules/payplug/views/templates/admin/panel/settings/one_click.tpl',
      1 => 1638738537,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./switch.tpl' => 1,
  ),
),false)) {
function content_61add928c4d296_80589629 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="panel-row separate_margin_block payplugTips -payplug_standard">
    <div class="payplugTips_item -left <?php if (!$_smarty_tpl->tpl_vars['payplug_switch']->value['standard']['checked']) {?> -hide<?php }?>">
        <div class="payplugPanel">
            <div class="payplugPanel_label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.panel.setting.oneclick.label','mod'=>'payplug'),$_smarty_tpl ) );?>
</div>
            <div class="payplugPanel_content"><?php $_smarty_tpl->_subTemplateRender('file:./switch.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('switch'=>$_smarty_tpl->tpl_vars['payplug_switch']->value['one_click']), 0, false);
?></div>
        </div>
        <div class="payplugPanel">
            <div class="payplugPanel_content">
                <p>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.panel.setting.oneclick.content','mod'=>'payplug'),$_smarty_tpl ) );?>

                    <a class="payplugLink" href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['faq_links']->value['one_click'],'htmlall','UTF-8' ));?>
" data-e2e-link="faq" target="_blank">
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'admin.panel.setting.oneclick.link','mod'=>'payplug'),$_smarty_tpl ) );?>

                    </a>
                </p>
            </div>
        </div>
    </div>
</div>
<?php }
}
