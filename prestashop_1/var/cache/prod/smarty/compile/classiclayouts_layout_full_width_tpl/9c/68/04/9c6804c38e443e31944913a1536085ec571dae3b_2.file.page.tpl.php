<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:18:19
  from '/Applications/MAMP/htdocs/prestashop_1/themes/classic/templates/page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add55b7d78f7_86543611',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9c6804c38e443e31944913a1536085ec571dae3b' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/themes/classic/templates/page.tpl',
      1 => 1638294544,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61add55b7d78f7_86543611 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_195565784661add55b7d0ee1_27794436', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_title'} */
class Block_42484188661add55b7d1ed2_21825408 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <header class="page-header">
          <h1><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</h1>
        </header>
      <?php
}
}
/* {/block 'page_title'} */
/* {block 'page_header_container'} */
class Block_165755984661add55b7d15c9_90924799 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_42484188661add55b7d1ed2_21825408', 'page_title', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content_top'} */
class Block_214392486861add55b7d4c05_70392910 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_150645452261add55b7d54e6_45237236 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_8560054361add55b7d4566_49680479 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <div id="content" class="page-content card card-block">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_214392486861add55b7d4c05_70392910', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_150645452261add55b7d54e6_45237236', 'page_content', $this->tplIndex);
?>

      </div>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_148981227261add55b7d68f9_71259233 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Footer content -->
        <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_152770912961add55b7d62a9_49738561 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <footer class="page-footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_148981227261add55b7d68f9_71259233', 'page_footer', $this->tplIndex);
?>

      </footer>
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_195565784661add55b7d0ee1_27794436 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_195565784661add55b7d0ee1_27794436',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_165755984661add55b7d15c9_90924799',
  ),
  'page_title' => 
  array (
    0 => 'Block_42484188661add55b7d1ed2_21825408',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_8560054361add55b7d4566_49680479',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_214392486861add55b7d4c05_70392910',
  ),
  'page_content' => 
  array (
    0 => 'Block_150645452261add55b7d54e6_45237236',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_152770912961add55b7d62a9_49738561',
  ),
  'page_footer' => 
  array (
    0 => 'Block_148981227261add55b7d68f9_71259233',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_165755984661add55b7d15c9_90924799', 'page_header_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8560054361add55b7d4566_49680479', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_152770912961add55b7d62a9_49738561', 'page_footer_container', $this->tplIndex);
?>


  </section>

<?php
}
}
/* {/block 'content'} */
}
