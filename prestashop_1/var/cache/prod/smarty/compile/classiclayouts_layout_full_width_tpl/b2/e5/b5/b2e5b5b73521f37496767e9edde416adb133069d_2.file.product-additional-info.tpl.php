<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:50:52
  from '/Applications/MAMP/htdocs/prestashop_1/themes/classic/templates/catalog/_partials/product-additional-info.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61addcfc1261e4_56771088',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b2e5b5b73521f37496767e9edde416adb133069d' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/themes/classic/templates/catalog/_partials/product-additional-info.tpl',
      1 => 1638294544,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61addcfc1261e4_56771088 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="product-additional-info js-product-additional-info">
  <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayProductAdditionalInfo','product'=>$_smarty_tpl->tpl_vars['product']->value),$_smarty_tpl ) );?>

</div>
<?php }
}
