<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:15:16
  from '/Applications/MAMP/htdocs/prestashop_1/themes/classic/templates/checkout/_partials/cart-summary-top.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add4a422f069_36739298',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bc372635979e46006a328e40cc912a97037845ae' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/themes/classic/templates/checkout/_partials/cart-summary-top.tpl',
      1 => 1638294544,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61add4a422f069_36739298 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="cart-summary-top js-cart-summary-top">
  <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayCheckoutSummaryTop'),$_smarty_tpl ) );?>

</div>
<?php }
}
