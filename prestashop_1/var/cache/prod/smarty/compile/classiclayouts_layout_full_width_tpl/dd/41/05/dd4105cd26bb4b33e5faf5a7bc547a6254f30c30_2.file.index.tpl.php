<?php
/* Smarty version 3.1.39, created on 2021-12-06 10:18:19
  from '/Applications/MAMP/htdocs/prestashop_1/themes/classic/templates/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61add55b7cc8a8_46918145',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dd4105cd26bb4b33e5faf5a7bc547a6254f30c30' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop_1/themes/classic/templates/index.tpl',
      1 => 1638294544,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61add55b7cc8a8_46918145 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_162831395661add55b7c98b1_10306695', 'page_content_container');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_content_top'} */
class Block_87443840661add55b7ca011_11079050 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'hook_home'} */
class Block_52720714961add55b7cb119_04880494 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo $_smarty_tpl->tpl_vars['HOOK_HOME']->value;?>

          <?php
}
}
/* {/block 'hook_home'} */
/* {block 'page_content'} */
class Block_20629631361add55b7caaa7_32446997 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_52720714961add55b7cb119_04880494', 'hook_home', $this->tplIndex);
?>

        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_162831395661add55b7c98b1_10306695 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_162831395661add55b7c98b1_10306695',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_87443840661add55b7ca011_11079050',
  ),
  'page_content' => 
  array (
    0 => 'Block_20629631361add55b7caaa7_32446997',
  ),
  'hook_home' => 
  array (
    0 => 'Block_52720714961add55b7cb119_04880494',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-home">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_87443840661add55b7ca011_11079050', 'page_content_top', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20629631361add55b7caaa7_32446997', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
}
