<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__1f0a3324c6fab2d502e498e0024436ad1773a520ecbc6058383eea7f5ed06bf4 */
class __TwigTemplate_468f19dbdece4b1bd14940f2be3a6308106a5049aa05513aa7a8255de504042d extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'extra_stylesheets' => [$this, 'block_extra_stylesheets'],
            'content_header' => [$this, 'block_content_header'],
            'content' => [$this, 'block_content'],
            'content_footer' => [$this, 'block_content_footer'],
            'sidebar_right' => [$this, 'block_sidebar_right'],
            'javascripts' => [$this, 'block_javascripts'],
            'extra_javascripts' => [$this, 'block_extra_javascripts'],
            'translate_javascripts' => [$this, 'block_translate_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"fr\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/prestashop_1/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/prestashop_1/img/app_icon.png\" />

<title>Commandes • HypeLuxury</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminOrders';
    var iso_user = 'fr';
    var lang_is_rtl = '0';
    var full_language_code = 'fr';
    var full_cldr_language_code = 'fr-FR';
    var country_iso_code = 'BE';
    var _PS_VERSION_ = '1.7.8.1';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'Une nouvelle commande a été passée sur votre boutique.';
    var order_number_msg = 'Numéro de commande : ';
    var total_msg = 'Total : ';
    var from_msg = 'Du ';
    var see_order_msg = 'Afficher cette commande';
    var new_customer_msg = 'Un nouveau client s\\'est inscrit sur votre boutique';
    var customer_name_msg = 'Nom du client : ';
    var new_msg = 'Un nouveau message a été posté sur votre boutique.';
    var see_msg = 'Lire le message';
    var token = '5dc5ac9193ec2fc5b8cbce8ad67901f4';
    var token_admin_orders = tokenAdminOrders = '5dc5ac9193ec2fc5b8cbce8ad67901f4';
    var token_admin_customers = '6bbd9dd098ff1b86233ac4d5ead43a88';
    var token_admin_customer_threads = tokenAdminCustomerThreads = '813ef1cfe7894aae7bd2bf328c22c6e2';
    var currentIndex = 'index.php?controller=AdminOrders';
    var employee_token = '8d629afd70221281dcc1df76a8dc7a18';
    var choose_language_translate = 'Choisissez la langue :';
    var default_language = '1';
    var admin_modules_link = '/prestashop_1/admin951g8xvjh/index.php/improve/modules/catalog/recommended?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo';
    var admin_notification_get_link = '/prestashop_1/admin951g8xvjh/index.php/common/notifications?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo';
    var admin_notification_push_link = adminNotificationPushLink = '/prestashop_1/admin951g8xvjh/index.php/common/notifications/ack?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo';
    var tab_modules_list = '';
    var update_success_msg = 'Mise à jour réussie';
    var errorLogin = 'PrestaShop n\\'a pas pu se connecter à Addons. Veuillez vérifier vos identifiants et votre connexion Internet.';
    var search_product_msg = 'Rechercher un produit';
  </script>

      <link href=\"/prestashop_1/admin951g8xvjh/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/js/jquery/plugins/fancybox/jquery.fancybox.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/modules/blockwishlist/public/backoffice.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/admin951g8xvjh/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/modules/gamification/views/css/gamification.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/modules/psgdpr/views/css/overrideAddress.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/modules/ps_mbo/views/css/recommended-modules.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/modules/colissimo/views/css/admin.back.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/modules/colissimo/views/css/admin.order.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/modules/colissimo/views/css/header.back.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/modules/colissimo/views/css/admin.modal.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/modules/colissimo/views/css/bootstrap.colissimo.min.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/modules/colissimo/views/css/colissimo.widget.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/modules/colissimo/views/css/mapbox.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/modules/payplug/views/css/admin_order-v3.4.0.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/modules/packlink/views/css/packlink-order-overview.css?v=3.2.2\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/modules/packlink/views/css/packlink.css?v=3.2.2\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/modules/ps_facebook/views/css/admin/menu.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/prestashop_1\\/admin951g8xvjh\\/\";
var baseDir = \"\\/prestashop_1\\/\";
var changeFormLanguageUrl = \"\\/prestashop_1\\/admin951g8xvjh\\/index.php\\/configure\\/advanced\\/employees\\/change-form-language?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\";
var currency = {\"iso_code\":\"EUR\",\"sign\":\"\\u20ac\",\"name\":\"Euro\",\"format\":null};
var currency_specifications = {\"symbol\":[\",\",\"\\u202f\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"currencyCode\":\"EUR\",\"currencySymbol\":\"\\u20ac\",\"numberSymbols\":[\",\",\"\\u202f\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"positivePattern\":\"#,##0.00\\u00a0\\u00a4\",\"negativePattern\":\"-#,##0.00\\u00a0\\u00a4\",\"maxFractionDigits\":2,\"minFractionDigits\":2,\"groupingUsed\":true,\"primaryGroupSize\":3,\"secondaryGroupSize\":3};
var host_mode = false;
var number_specifications = {\"symbol\":[\",\",\"\\u202f\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"numberSymbols\":[\",\",\"\\u202f\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"positivePattern\":\"#,##0.###\",\"negativePattern\":\"-#,##0.###\",\"maxFractionDigits\":3,\"minFractionDigits\":0,\"groupingUsed\":true,\"primaryGroupSize\":3,\"secondaryGroupSize\":3};
var prestashop = {\"debug\":false};
var psgdprNoAddresses = \"Donn\\u00e9es client supprim\\u00e9es par le module RGPD officiel.\";
var show_new_customers = \"1\";
var show_new_messages = \"1\";
var show_new_orders = \"1\";
</script>
<script type=\"text/javascript\" src=\"/prestashop_1/admin951g8xvjh/themes/new-theme/public/main.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/js/jquery/plugins/fancybox/jquery.fancybox.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/js/admin.js?v=1.7.8.1\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/admin951g8xvjh/themes/new-theme/public/cldr.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/js/tools.js?v=1.7.8.1\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/admin951g8xvjh/themes/default/js/vendor/nv.d3.min.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/gamification/views/js/gamification_bt.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/psgdpr/views/js/overrideAddress.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/ps_mbo/views/js/recommended-modules.js?v=2.0.1\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/admin951g8xvjh/themes/default/js/bundle/module/module_card.js?v=1.7.8.1\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/colissimo/views/js/admin.back.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/colissimo/views/js/jquery.plugin.colissimo.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/colissimo/views/js/print.min.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/payplug/views/js/admin_order-v3.4.0.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/payplug/views/js/utilities-v3.4.0.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/ps_checkout/views/js/adminOrderView.js?version=2.15.5\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/packlink/views/js/OrderOverviewDraft.js?v=3.2.2\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/packlink/views/js/core/UtilityService.js?v=3.2.2\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/packlink/views/js/core/ResponseService.js?v=3.2.2\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/packlink/views/js/core/StateUUIDService.js?v=3.2.2\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/packlink/views/js/core/AjaxService.js?v=3.2.2\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/packlink/views/js/PrestaAjaxService.js?v=3.2.2\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/packlink/views/js/PrestaPrintShipmentLabels.js?v=3.2.2\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/packlink/views/js/PrestaCreateOrderDraft.js?v=3.2.2\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/ps_faviconnotificationbo/views/js/favico.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/ps_faviconnotificationbo/views/js/ps_faviconnotificationbo.js\"></script>

  <script>
  if (undefined !== ps_faviconnotificationbo) {
    ps_faviconnotificationbo.initialize({
      backgroundColor: '#DF0067',
      textColor: '#FFFFFF',
      notificationGetUrl: '/prestashop_1/admin951g8xvjh/index.php/common/notifications?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo',
      CHECKBOX_ORDER: 1,
      CHECKBOX_CUSTOMER: 1,
      CHECKBOX_MESSAGE: 1,
      timer: 120000, // Refresh every 2 minutes
    });
  }
</script>
<script>
            var admin_gamification_ajax_url = \"http:\\/\\/localhost:8888\\/prestashop_1\\/admin951g8xvjh\\/index.php?controller=AdminGamification&token=e47dcaedc729b04a6c3332f15e88e1e5\";
            var current_id_tab = 4;
        </script><link rel=\"stylesheet\" href=\"/prestashop_1/modules/bpostshm/views/css/admin-bpost.css\" type=\"text/css\" />
<link rel=\"stylesheet\" href=\"/prestashop_1/modules/bpostshm/views/css/jquery.qtip.min.css\">
<script src=\"/prestashop_1/modules/bpostshm/views/js/eon.jquery.base.min.js\"></script>
<script src=\"/prestashop_1/modules/bpostshm/views/js/eon.jquery.inputs.min.js\"></script>
<script src=\"/prestashop_1/modules/bpostshm/views/js/jquery.qtip.min.js\"></script>
<script src=\"/prestashop_1/modules/bpostshm/views/js/jquery.idTabs.min.js\"></script>

";
        // line 136
        $this->displayBlock('stylesheets', $context, $blocks);
        $this->displayBlock('extra_stylesheets', $context, $blocks);
        echo "</head>

<body
  class=\"lang-fr adminorders\"
  data-base-url=\"/prestashop_1/admin951g8xvjh/index.php\"  data-token=\"r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\">

  <header id=\"header\" class=\"d-print-none\">

    <nav id=\"header_infos\" class=\"main-header\">
      <button class=\"btn btn-primary-reverse onclick btn-lg unbind ajax-spinner\"></button>

            <i class=\"material-icons js-mobile-menu\">menu</i>
      <a id=\"header_logo\" class=\"logo float-left\" href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminDashboard&amp;token=583c442dbcfafb9930d04e220a657743\"></a>
      <span id=\"shop_version\">1.7.8.1</span>

      <div class=\"component\" id=\"quick-access-container\">
        <div class=\"dropdown quick-accesses\">
  <button class=\"btn btn-link btn-sm dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" id=\"quick_select\">
    Accès rapide
  </button>
  <div class=\"dropdown-menu\">
          <a class=\"dropdown-item quick-row-link\"
         href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminOrders&amp;token=5dc5ac9193ec2fc5b8cbce8ad67901f4\"
                 data-item=\"Commandes\"
      >Commandes</a>
          <a class=\"dropdown-item quick-row-link\"
         href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminStats&amp;module=statscheckup&amp;token=da357960ed6adb0623c30a8b07c9da3a\"
                 data-item=\"Évaluation du catalogue\"
      >Évaluation du catalogue</a>
          <a class=\"dropdown-item quick-row-link\"
         href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php/improve/modules/manage?token=5a9614830f092abc70c8e9e611db0061\"
                 data-item=\"Modules installés\"
      >Modules installés</a>
          <a class=\"dropdown-item quick-row-link\"
         href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=2829aa0ec86d7588ad88dcc209f3d57b\"
                 data-item=\"Nouveau bon de réduction\"
      >Nouveau bon de réduction</a>
          <a class=\"dropdown-item quick-row-link\"
         href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php/sell/catalog/products/new?token=5a9614830f092abc70c8e9e611db0061\"
                 data-item=\"Nouveau produit\"
      >Nouveau produit</a>
          <a class=\"dropdown-item quick-row-link\"
         href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php/sell/catalog/categories/new?token=5a9614830f092abc70c8e9e611db0061\"
                 data-item=\"Nouvelle catégorie\"
      >Nouvelle catégorie</a>
        <div class=\"dropdown-divider\"></div>
          <a id=\"quick-add-link\"
        class=\"dropdown-item js-quick-link\"
        href=\"#\"
        data-rand=\"92\"
        data-icon=\"icon-AdminParentOrders\"
        data-method=\"add\"
        data-url=\"index.php/sell/orders/?-7VnR_8h0s0WbTwL1Ivo\"
        data-post-link=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminQuickAccesses&token=bd643ed599550cb85aff3d7f0d0ba084\"
        data-prompt-text=\"Veuillez nommer ce raccourci :\"
        data-link=\"Commandes - Liste\"
      >
        <i class=\"material-icons\">add_circle</i>
        Ajouter la page actuelle à l'accès rapide
      </a>
        <a id=\"quick-manage-link\" class=\"dropdown-item\" href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminQuickAccesses&token=bd643ed599550cb85aff3d7f0d0ba084\">
      <i class=\"material-icons\">settings</i>
      Gérez vos accès rapides
    </a>
  </div>
</div>
      </div>
      <div class=\"component\" id=\"header-search-container\">
        <form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form collapsed\"
      method=\"post\"
      action=\"/prestashop_1/admin951g8xvjh/index.php?controller=AdminSearch&amp;token=98166be49cfeb75dd1c70ac9d453c82b\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input type=\"text\" class=\"form-control js-form-search\" id=\"bo_query\" name=\"bo_query\" value=\"\" placeholder=\"Rechercher (ex. : référence produit, nom du client, etc.)\" aria-label=\"Barre de recherche\">
    <div class=\"input-group-append\">
      <button type=\"button\" class=\"btn btn-outline-secondary dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
        Partout
      </button>
      <div class=\"dropdown-menu js-items-list\">
        <a class=\"dropdown-item\" data-item=\"Partout\" href=\"#\" data-value=\"0\" data-placeholder=\"Que souhaitez-vous trouver ?\" data-icon=\"icon-search\"><i class=\"material-icons\">search</i> Partout</a>
        <div class=\"dropdown-divider\"></div>
        <a class=\"dropdown-item\" data-item=\"Catalogue\" href=\"#\" data-value=\"1\" data-placeholder=\"Nom du produit, référence, etc.\" data-icon=\"icon-book\"><i class=\"material-icons\">store_mall_directory</i> Catalogue</a>
        <a class=\"dropdown-item\" data-item=\"Clients par nom\" href=\"#\" data-value=\"2\" data-placeholder=\"Nom\" data-icon=\"icon-group\"><i class=\"material-icons\">group</i> Clients par nom</a>
        <a class=\"dropdown-item\" data-item=\"Clients par adresse IP\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\"><i class=\"material-icons\">desktop_mac</i> Clients par adresse IP</a>
        <a class=\"dropdown-item\" data-item=\"Commandes\" href=\"#\" data-value=\"3\" data-placeholder=\"ID commande\" data-icon=\"icon-credit-card\"><i class=\"material-icons\">shopping_basket</i> Commandes</a>
        <a class=\"dropdown-item\" data-item=\"Factures\" href=\"#\" data-value=\"4\" data-placeholder=\"Numéro de facture\" data-icon=\"icon-book\"><i class=\"material-icons\">book</i> Factures</a>
        <a class=\"dropdown-item\" data-item=\"Paniers\" href=\"#\" data-value=\"5\" data-placeholder=\"ID panier\" data-icon=\"icon-shopping-cart\"><i class=\"material-icons\">shopping_cart</i> Paniers</a>
        <a class=\"dropdown-item\" data-item=\"Modules\" href=\"#\" data-value=\"7\" data-placeholder=\"Nom du module\" data-icon=\"icon-puzzle-piece\"><i class=\"material-icons\">extension</i> Modules</a>
      </div>
      <button class=\"btn btn-primary\" type=\"submit\"><span class=\"d-none\">RECHERCHE</span><i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
    \$('#bo_query').one('click', function() {
    \$(this).closest('form').removeClass('collapsed');
  });
});
</script>
      </div>

      
      
              <div class=\"component\" id=\"header-shop-list-container\">
            <div class=\"shop-list\">
    <a class=\"link\" id=\"header_shopname\" href=\"http://localhost:8888/prestashop_1/\" target= \"_blank\">
      <i class=\"material-icons\">visibility</i>
      <span>Voir ma boutique</span>
    </a>
  </div>
        </div>
                    <div class=\"component header-right-component\" id=\"header-notifications-container\">
          <div id=\"notif\" class=\"notification-center dropdown dropdown-clickable\">
  <button class=\"btn notification js-notification dropdown-toggle\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count hide\">0</span>
  </button>
  <div class=\"dropdown-menu dropdown-menu-right js-notifs_dropdown\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Commandes<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
              Clients<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Messages<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Pas de nouvelle commande pour le moment :(<br>
              Avez-vous consulté vos <strong><a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminCarts&action=filterOnlyAbandonedCarts&token=3aea9b7403d072e19283415c063f3867\">paniers abandonnés</a></strong> ?<br> Votre prochaine commande s'y trouve peut-être !
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Aucun nouveau client pour l'instant :(<br>
              Êtes-vous actifs sur les réseaux sociaux en ce moment ?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Pas de nouveau message pour l'instant.<br>
              On dirait que vos clients sont satisfaits :)
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
    <a class=\"notif\" href='order_url'>
      #_id_order_ -
      de <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"float-sm-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href='customer_url'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - enregistré le <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href='message_url'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
        </div>
      
      <div class=\"component\" id=\"header-employee-container\">
        <div class=\"dropdown employee-dropdown\">
  <div class=\"rounded-circle person\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">account_circle</i>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"employee-wrapper-avatar\">

      <span class=\"employee-avatar\"><img class=\"avatar rounded-circle\" src=\"http://localhost:8888/prestashop_1/img/pr/default.jpg\" /></span>
      <span class=\"employee_profile\">Ravi de vous revoir Adrien</span>
      <a class=\"dropdown-item employee-link profile-link\" href=\"/prestashop_1/admin951g8xvjh/index.php/configure/advanced/employees/1/edit?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\">
      <i class=\"material-icons\">edit</i>
      <span>Votre profil</span>
    </a>
    </div>

    <p class=\"divider\"></p>
    <a class=\"dropdown-item\" href=\"https://www.prestashop.com/fr/ressources/documentation?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=resources-fr&amp;utm_content=download17\" target=\"_blank\" rel=\"noreferrer\"><i class=\"material-icons\">book</i> Documentation</a>
    <a class=\"dropdown-item\" href=\"https://www.prestashop.com/fr/formation?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=resources-fr&amp;utm_content=download17\" target=\"_blank\" rel=\"noreferrer\"><i class=\"material-icons\">school</i> Formation</a>
    <a class=\"dropdown-item\" href=\"https://www.prestashop.com/fr/experts?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=resources-fr&amp;utm_content=download17\" target=\"_blank\" rel=\"noreferrer\"><i class=\"material-icons\">person_pin_circle</i> Trouver un expert</a>
    <a class=\"dropdown-item\" href=\"https://addons.prestashop.com/fr/?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=addons-fr&amp;utm_content=download17\" target=\"_blank\" rel=\"noreferrer\"><i class=\"material-icons\">extension</i> Place de marché de PrestaShop</a>
    <a class=\"dropdown-item\" href=\"https://www.prestashop.com/fr/contact?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=resources-fr&amp;utm_content=download17\" target=\"_blank\" rel=\"noreferrer\"><i class=\"material-icons\">help</i> Centre d'assistance</a>
    <p class=\"divider\"></p>
    <a class=\"dropdown-item employee-link text-center\" id=\"header_logout\" href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminLogin&amp;logout=1&amp;token=3fa28c08de38d6377c1a8c1b63f78d2d\">
      <i class=\"material-icons d-lg-none\">power_settings_new</i>
      <span>Déconnexion</span>
    </a>
  </div>
</div>
      </div>
          </nav>
  </header>

  <nav class=\"nav-bar d-none d-print-none d-md-block\">
  <span class=\"menu-collapse\" data-toggle-url=\"/prestashop_1/admin951g8xvjh/index.php/configure/advanced/employees/toggle-navigation?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\">
    <i class=\"material-icons\">chevron_left</i>
    <i class=\"material-icons\">chevron_left</i>
  </span>

  <div class=\"nav-bar-overflow\">
      <ul class=\"main-menu\">
              
                    
                    
          
            <li class=\"link-levelone\" data-submenu=\"1\" id=\"tab-AdminDashboard\">
              <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminDashboard&amp;token=583c442dbcfafb9930d04e220a657743\" class=\"link\" >
                <i class=\"material-icons\">trending_up</i> <span>Tableau de bord</span>
              </a>
            </li>

          
                      
                                          
                    
          
            <li class=\"category-title link-active\" data-submenu=\"2\" id=\"tab-SELL\">
                <span class=\"title\">Vendre</span>
            </li>

                              
                  
                                                      
                                                          
                  <li class=\"link-levelone has_submenu link-active open ul-open\" data-submenu=\"3\" id=\"subtab-AdminParentOrders\">
                    <a href=\"/prestashop_1/admin951g8xvjh/index.php/sell/orders/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\">
                      <i class=\"material-icons mi-shopping_basket\">shopping_basket</i>
                      <span>
                      Commandes
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_up
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-3\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo link-active\" data-submenu=\"4\" id=\"subtab-AdminOrders\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/sell/orders/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Commandes
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"5\" id=\"subtab-AdminInvoices\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/sell/orders/invoices/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Factures
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"6\" id=\"subtab-AdminSlip\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/sell/orders/credit-slips/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Avoirs
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"7\" id=\"subtab-AdminDeliverySlip\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/sell/orders/delivery-slips/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Bons de livraison
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"8\" id=\"subtab-AdminCarts\">
                                <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminCarts&amp;token=3aea9b7403d072e19283415c063f3867\" class=\"link\"> Paniers
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"9\" id=\"subtab-AdminCatalog\">
                    <a href=\"/prestashop_1/admin951g8xvjh/index.php/sell/catalog/products?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\">
                      <i class=\"material-icons mi-store\">store</i>
                      <span>
                      Catalogue
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-9\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"10\" id=\"subtab-AdminProducts\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/sell/catalog/products?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Produits
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"11\" id=\"subtab-AdminCategories\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/sell/catalog/categories?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Catégories
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"12\" id=\"subtab-AdminTracking\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/sell/catalog/monitoring/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Suivi
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"13\" id=\"subtab-AdminParentAttributesGroups\">
                                <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminAttributesGroups&amp;token=0fc132d7c52b6a3c1ebc59877c3d8c38\" class=\"link\"> Attributs &amp; caractéristiques
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"16\" id=\"subtab-AdminParentManufacturers\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/sell/catalog/brands/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Marques et fournisseurs
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"19\" id=\"subtab-AdminAttachments\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/sell/attachments/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Fichiers
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"20\" id=\"subtab-AdminParentCartRules\">
                                <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminCartRules&amp;token=2829aa0ec86d7588ad88dcc209f3d57b\" class=\"link\"> Réductions
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"23\" id=\"subtab-AdminStockManagement\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/sell/stocks/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Stock
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"24\" id=\"subtab-AdminParentCustomer\">
                    <a href=\"/prestashop_1/admin951g8xvjh/index.php/sell/customers/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\">
                      <i class=\"material-icons mi-account_circle\">account_circle</i>
                      <span>
                      Clients
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-24\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"25\" id=\"subtab-AdminCustomers\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/sell/customers/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Clients
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"26\" id=\"subtab-AdminAddresses\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/sell/addresses/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Adresses
                                </a>
                              </li>

                                                                                                                                    </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"28\" id=\"subtab-AdminParentCustomerThreads\">
                    <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminCustomerThreads&amp;token=813ef1cfe7894aae7bd2bf328c22c6e2\" class=\"link\">
                      <i class=\"material-icons mi-chat\">chat</i>
                      <span>
                      SAV
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-28\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"29\" id=\"subtab-AdminCustomerThreads\">
                                <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminCustomerThreads&amp;token=813ef1cfe7894aae7bd2bf328c22c6e2\" class=\"link\"> SAV
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"30\" id=\"subtab-AdminOrderMessage\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/sell/customer-service/order-messages/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Messages prédéfinis
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"31\" id=\"subtab-AdminReturn\">
                                <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminReturn&amp;token=0ab7dead8e996ee36c609ae068be9ec7\" class=\"link\"> Retours produits
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"32\" id=\"subtab-AdminStats\">
                    <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminMetricsStats&amp;token=fd939b57c55d283466f8f3d8a143b51d\" class=\"link\">
                      <i class=\"material-icons mi-assessment\">assessment</i>
                      <span>
                      Statistiques
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-32\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"146\" id=\"subtab-AdminMetricsStats\">
                                <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminMetricsStats&amp;token=fd939b57c55d283466f8f3d8a143b51d\" class=\"link\"> PrestaShop Metrics
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"147\" id=\"subtab-AdminLegacyStatsMetrics\">
                                <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminLegacyStatsMetrics&amp;token=9f99f942412918cec29b86f90f74d7f7\" class=\"link\"> Statistiques
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                                            
          
                      
                                          
                    
          
            <li class=\"category-title\" data-submenu=\"42\" id=\"tab-IMPROVE\">
                <span class=\"title\">Personnaliser</span>
            </li>

                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"43\" id=\"subtab-AdminParentModulesSf\">
                    <a href=\"/prestashop_1/admin951g8xvjh/index.php/improve/modules/manage?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\">
                      <i class=\"material-icons mi-extension\">extension</i>
                      <span>
                      Modules
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-43\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"44\" id=\"subtab-AdminModulesSf\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/improve/modules/manage?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Gestionnaire de modules 
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"48\" id=\"subtab-AdminParentModulesCatalog\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/modules/addons/modules/catalog?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Catalogue de modules
                                </a>
                              </li>

                                                                                                                                                                                          </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"52\" id=\"subtab-AdminParentThemes\">
                    <a href=\"/prestashop_1/admin951g8xvjh/index.php/improve/design/themes/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\">
                      <i class=\"material-icons mi-desktop_mac\">desktop_mac</i>
                      <span>
                      Apparence
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-52\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"130\" id=\"subtab-AdminThemesParent\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/improve/design/themes/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Thème et logo
                                </a>
                              </li>

                                                                                                                                        
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"140\" id=\"subtab-AdminPsMboTheme\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/modules/addons/themes/catalog?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Catalogue de thèmes
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"55\" id=\"subtab-AdminParentMailTheme\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/improve/design/mail_theme/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Thème d&#039;email
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"57\" id=\"subtab-AdminCmsContent\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/improve/design/cms-pages/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Pages
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"58\" id=\"subtab-AdminModulesPositions\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/improve/design/modules/positions/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Positions
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"59\" id=\"subtab-AdminImages\">
                                <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminImages&amp;token=013525151ba8d32bbe6dcafb37753660\" class=\"link\"> Images
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"129\" id=\"subtab-AdminLinkWidget\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/modules/link-widget/list?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Liste de liens
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"60\" id=\"subtab-AdminParentShipping\">
                    <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminCarriers&amp;token=bd71e2c2757d6d6952be1e3e712987d4\" class=\"link\">
                      <i class=\"material-icons mi-local_shipping\">local_shipping</i>
                      <span>
                      Livraison
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-60\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"61\" id=\"subtab-AdminCarriers\">
                                <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminCarriers&amp;token=bd71e2c2757d6d6952be1e3e712987d4\" class=\"link\"> Transporteurs
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"62\" id=\"subtab-AdminShipping\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/improve/shipping/preferences/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Préférences
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"158\" id=\"subtab-AdminColissimoDashboard\">
                                <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminColissimoDashboard&amp;token=d43d902de369cd859a71d397bee4922a\" class=\"link\"> Colissimo - Tableau de bord
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"159\" id=\"subtab-AdminColissimoAffranchissement\">
                                <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminColissimoAffranchissement&amp;token=a5af6d489b42ddff50544db442041eda\" class=\"link\"> Colissimo - Affranchissement
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"160\" id=\"subtab-AdminColissimoDepositSlip\">
                                <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminColissimoDepositSlip&amp;token=3db44876156aa7219354130cedf057a0\" class=\"link\"> Colissimo - Bordereaux
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"161\" id=\"subtab-AdminColissimoColiship\">
                                <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminColissimoColiship&amp;token=b436f69b4be362b9a0ca6f44126f4f51\" class=\"link\"> Colissimo - Coliship
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"162\" id=\"subtab-AdminColissimoCustomsDocuments\">
                                <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminColissimoCustomsDocuments&amp;token=8fd426211faec6005c16b9678dedee13\" class=\"link\"> Colissimo - Documents
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"191\" id=\"subtab-AdminSendcloud\">
                                <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminSendcloud&amp;token=874dffb6b670d21a144a4542dbe774b1\" class=\"link\"> Sendcloud
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"192\" id=\"subtab-Packlink\">
                                <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=Packlink&amp;token=feea9d46785d49aacd33528f1460c339\" class=\"link\"> Packlink PRO
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"63\" id=\"subtab-AdminParentPayment\">
                    <a href=\"/prestashop_1/admin951g8xvjh/index.php/improve/payment/payment_methods?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\">
                      <i class=\"material-icons mi-payment\">payment</i>
                      <span>
                      Paiement
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-63\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"64\" id=\"subtab-AdminPayment\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/improve/payment/payment_methods?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Modes de paiement
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"65\" id=\"subtab-AdminPaymentPreferences\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/improve/payment/preferences?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Préférences
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"66\" id=\"subtab-AdminInternational\">
                    <a href=\"/prestashop_1/admin951g8xvjh/index.php/improve/international/localization/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\">
                      <i class=\"material-icons mi-language\">language</i>
                      <span>
                      International
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-66\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"67\" id=\"subtab-AdminParentLocalization\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/improve/international/localization/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Localisation
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"72\" id=\"subtab-AdminParentCountries\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/improve/international/zones/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Zones géographiques
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"76\" id=\"subtab-AdminParentTaxes\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/improve/international/taxes/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Taxes
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"79\" id=\"subtab-AdminTranslations\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/improve/international/translations/settings?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Traductions
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"150\" id=\"subtab-Marketing\">
                    <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminPsfacebookModule&amp;token=0c76863392b8b3fb80061cdebf201769\" class=\"link\">
                      <i class=\"material-icons mi-campaign\">campaign</i>
                      <span>
                      Marketing
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-150\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"151\" id=\"subtab-AdminPsfacebookModule\">
                                <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminPsfacebookModule&amp;token=0c76863392b8b3fb80061cdebf201769\" class=\"link\"> Facebook
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                              
          
                      
                                          
                    
          
            <li class=\"category-title\" data-submenu=\"80\" id=\"tab-CONFIGURE\">
                <span class=\"title\">Configurer</span>
            </li>

                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"81\" id=\"subtab-ShopParameters\">
                    <a href=\"/prestashop_1/admin951g8xvjh/index.php/configure/shop/preferences/preferences?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\">
                      <i class=\"material-icons mi-settings\">settings</i>
                      <span>
                      Paramètres de la boutique
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-81\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"82\" id=\"subtab-AdminParentPreferences\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/configure/shop/preferences/preferences?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Paramètres généraux
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"85\" id=\"subtab-AdminParentOrderPreferences\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/configure/shop/order-preferences/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Commandes
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"88\" id=\"subtab-AdminPPreferences\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/configure/shop/product-preferences/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Produits
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"89\" id=\"subtab-AdminParentCustomerPreferences\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/configure/shop/customer-preferences/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Clients
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"93\" id=\"subtab-AdminParentStores\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/configure/shop/contacts/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Contact
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"96\" id=\"subtab-AdminParentMeta\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/configure/shop/seo-urls/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Trafic et SEO
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"100\" id=\"subtab-AdminParentSearchConf\">
                                <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminSearchConf&amp;token=73957f7a24beb54f9da9ae02f0668aa3\" class=\"link\"> Rechercher
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"134\" id=\"subtab-AdminGamification\">
                                <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminGamification&amp;token=e47dcaedc729b04a6c3332f15e88e1e5\" class=\"link\"> Merchant Expertise
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"103\" id=\"subtab-AdminAdvancedParameters\">
                    <a href=\"/prestashop_1/admin951g8xvjh/index.php/configure/advanced/system-information/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\">
                      <i class=\"material-icons mi-settings_applications\">settings_applications</i>
                      <span>
                      Paramètres avancés
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-103\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"104\" id=\"subtab-AdminInformation\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/configure/advanced/system-information/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Informations
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"105\" id=\"subtab-AdminPerformance\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/configure/advanced/performance/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Performances
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"106\" id=\"subtab-AdminAdminPreferences\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/configure/advanced/administration/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Administration
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"107\" id=\"subtab-AdminEmails\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/configure/advanced/emails/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Email
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"108\" id=\"subtab-AdminImport\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/configure/advanced/import/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Importer
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"109\" id=\"subtab-AdminParentEmployees\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/configure/advanced/employees/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Équipe
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"113\" id=\"subtab-AdminParentRequestSql\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/configure/advanced/sql-requests/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Base de données
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"116\" id=\"subtab-AdminLogs\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/configure/advanced/logs/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Logs
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"117\" id=\"subtab-AdminWebservice\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/configure/advanced/webservice-keys/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Webservice
                                </a>
                              </li>

                                                                                                                                                                                              
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"120\" id=\"subtab-AdminFeatureFlag\">
                                <a href=\"/prestashop_1/admin951g8xvjh/index.php/configure/advanced/feature-flags/?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\" class=\"link\"> Fonctionnalités Expérimentales
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                              
          
                      
                                          
                    
          
            <li class=\"category-title\" data-submenu=\"172\" id=\"tab-AdminPayPlug\">
                <span class=\"title\">PayPlug</span>
            </li>

                              
                  
                                                      
                  
                  <li class=\"link-levelone\" data-submenu=\"173\" id=\"subtab-AdminPayPlugInstallment\">
                    <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminPayPlugInstallment&amp;token=d365062a309f82977ba2ead52c4947b9\" class=\"link\">
                      <i class=\"material-icons mi-extension\">extension</i>
                      <span>
                      Paiements en plusieurs fois
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                        </li>
                              
          
                  </ul>
  </div>
  
</nav>

<div id=\"main-div\">
          
<div class=\"header-toolbar d-print-none\">
    
  <div class=\"container-fluid\">

    
      <nav aria-label=\"Breadcrumb\">
        <ol class=\"breadcrumb\">
                      <li class=\"breadcrumb-item\">Commandes</li>
          
                  </ol>
      </nav>
    

    <div class=\"title-row\">
      
  
      <h1 class=\"title\">
      Commandes    </h1>
  

      
        <div class=\"toolbar-icons\">
          <div class=\"wrapper\">
            
                                                          <a
                  class=\"btn btn-primary pointer\"                  id=\"page-header-desc-configuration-add\"
                  href=\"/prestashop_1/admin951g8xvjh/index.php/sell/orders/new?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\"                  title=\"Ajouter une commande\"                >
                  <i class=\"material-icons\">add_circle_outline</i>                  Ajouter une commande
                </a>
                                      
            
                              <a class=\"btn btn-outline-secondary btn-help btn-sidebar\" href=\"#\"
                   title=\"Aide\"
                   data-toggle=\"sidebar\"
                   data-target=\"#right-sidebar\"
                   data-url=\"/prestashop_1/admin951g8xvjh/index.php/common/sidebar/https%253A%252F%252Fhelp.prestashop.com%252Ffr%252Fdoc%252FAdminOrders%253Fversion%253D1.7.8.1%2526country%253Dfr/Aide?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\"
                   id=\"product_form_open_help\"
                >
                  Aide
                </a>
                                    </div>
        </div>

      
    </div>
  </div>

  
  
  <div class=\"btn-floating\">
    <button class=\"btn btn-primary collapsed\" data-toggle=\"collapse\" data-target=\".btn-floating-container\" aria-expanded=\"false\">
      <i class=\"material-icons\">add</i>
    </button>
    <div class=\"btn-floating-container collapse\">
      <div class=\"btn-floating-menu\">
        
                              <a
              class=\"btn btn-floating-item  pointer\"              id=\"page-header-desc-floating-configuration-add\"
              href=\"/prestashop_1/admin951g8xvjh/index.php/sell/orders/new?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\"              title=\"Ajouter une commande\"            >
              Ajouter une commande
              <i class=\"material-icons\">add_circle_outline</i>            </a>
                  
                              <a class=\"btn btn-floating-item btn-help btn-sidebar\" href=\"#\"
               title=\"Aide\"
               data-toggle=\"sidebar\"
               data-target=\"#right-sidebar\"
               data-url=\"/prestashop_1/admin951g8xvjh/index.php/common/sidebar/https%253A%252F%252Fhelp.prestashop.com%252Ffr%252Fdoc%252FAdminOrders%253Fversion%253D1.7.8.1%2526country%253Dfr/Aide?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\"
            >
              Aide
            </a>
                        </div>
    </div>
  </div>
  <script>
  if (undefined !== mbo) {
    mbo.initialize({
      translations: {
        'Recommended Modules and Services': 'Modules et services recommandés',
        'Close': 'Fermer',
      },
      recommendedModulesUrl: '/prestashop_1/admin951g8xvjh/index.php/modules/addons/modules/recommended?tabClassName=AdminOrders&_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo',
      shouldAttachRecommendedModulesAfterContent: 0,
      shouldAttachRecommendedModulesButton: 1,
      shouldUseLegacyTheme: 0,
    });
  }
</script>

</div>
      
      <div class=\"content-div  \">

        

                                                        
        <div class=\"row \">
          <div class=\"col-sm-12\">
            <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>


  ";
        // line 1289
        $this->displayBlock('content_header', $context, $blocks);
        // line 1290
        echo "                 ";
        $this->displayBlock('content', $context, $blocks);
        // line 1291
        echo "                 ";
        $this->displayBlock('content_footer', $context, $blocks);
        // line 1292
        echo "                 ";
        $this->displayBlock('sidebar_right', $context, $blocks);
        // line 1293
        echo "
            
          </div>
        </div>

      </div>
    </div>

  <div id=\"non-responsive\" class=\"js-non-responsive\">
  <h1>Oh non !</h1>
  <p class=\"mt-3\">
    La version mobile de cette page n'est pas encore disponible.
  </p>
  <p class=\"mt-2\">
    En attendant que cette page soit adaptée au mobile, vous êtes invité à la consulter sur ordinateur.
  </p>
  <p class=\"mt-2\">
    Merci.
  </p>
  <a href=\"http://localhost:8888/prestashop_1/admin951g8xvjh/index.php?controller=AdminDashboard&amp;token=583c442dbcfafb9930d04e220a657743\" class=\"btn btn-primary py-1 mt-3\">
    <i class=\"material-icons\">arrow_back</i>
    Précédent
  </a>
</div>
  <div class=\"mobile-layer\"></div>

      <div id=\"footer\" class=\"bootstrap\">
    
</div>
  

      <div class=\"bootstrap\">
      <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"https://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-FR&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/fr/login?email=dibari.adrien%40gmail.com&amp;firstname=Adrien&amp;lastname=Di+Bari&amp;website=http%3A%2F%2Flocalhost%3A8888%2Fprestashop_1%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-FR&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/prestashop_1/admin951g8xvjh/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Connectez-vous à la place de marché de PrestaShop afin d'importer automatiquement tous vos achats.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Vous n'avez pas de compte ?</h4>
\t\t\t\t\t\t<p class='text-justify'>Les clés pour réussir votre boutique sont sur PrestaShop Addons ! Découvrez sur la place de marché officielle de PrestaShop plus de 3 500 modules et thèmes pour augmenter votre trafic, optimiser vos conversions, fidéliser vos clients et vous faciliter l’e-commerce.</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Connectez-vous à PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link float-right _blank\" href=\"//addons.prestashop.com/fr/forgot-your-password\">Mot de passe oublié</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/fr/login?email=dibari.adrien%40gmail.com&amp;firstname=Adrien&amp;lastname=Di+Bari&amp;website=http%3A%2F%2Flocalhost%3A8888%2Fprestashop_1%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-FR&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tCréer un compte
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Connexion
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>

    </div>
  
";
        // line 1400
        $this->displayBlock('javascripts', $context, $blocks);
        $this->displayBlock('extra_javascripts', $context, $blocks);
        $this->displayBlock('translate_javascripts', $context, $blocks);
        echo "</body>
</html>";
    }

    // line 136
    public function block_stylesheets($context, array $blocks = [])
    {
    }

    public function block_extra_stylesheets($context, array $blocks = [])
    {
    }

    // line 1289
    public function block_content_header($context, array $blocks = [])
    {
    }

    // line 1290
    public function block_content($context, array $blocks = [])
    {
    }

    // line 1291
    public function block_content_footer($context, array $blocks = [])
    {
    }

    // line 1292
    public function block_sidebar_right($context, array $blocks = [])
    {
    }

    // line 1400
    public function block_javascripts($context, array $blocks = [])
    {
    }

    public function block_extra_javascripts($context, array $blocks = [])
    {
    }

    public function block_translate_javascripts($context, array $blocks = [])
    {
    }

    public function getTemplateName()
    {
        return "__string_template__1f0a3324c6fab2d502e498e0024436ad1773a520ecbc6058383eea7f5ed06bf4";
    }

    public function getDebugInfo()
    {
        return array (  1490 => 1400,  1485 => 1292,  1480 => 1291,  1475 => 1290,  1470 => 1289,  1461 => 136,  1453 => 1400,  1344 => 1293,  1341 => 1292,  1338 => 1291,  1335 => 1290,  1333 => 1289,  176 => 136,  39 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__1f0a3324c6fab2d502e498e0024436ad1773a520ecbc6058383eea7f5ed06bf4", "");
    }
}
