<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @PrestaShop/Admin/Common/Grid/Columns/Content/order_packlink_draft_action.html.twig */
class __TwigTemplate_f3785a937bd3c544d232bf4001eac6ac25a9d430840675815516427a3573bd76 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<span class=\"btn-group-action pl-grid-draft-container\">
    <span class=\"btn-group\">
        ";
        // line 3
        if (($this->getAttribute(($context["record"] ?? null), "draftStatus", []) == "completed")) {
            // line 4
            echo "            <a class=\"btn btn-default pl-draft-button
          ";
            // line 5
            if ($this->getAttribute(($context["record"] ?? null), "draftDeleted", [])) {
                // line 6
                echo "          pl-draft-button-disabled\"
        ";
            } else {
                // line 7
                echo "\"
            href=\"";
                // line 8
                echo twig_escape_filter($this->env, $this->getAttribute(($context["record"] ?? null), "draftLink", []), "html", null, true);
                echo "\"
            target=\"_blank\"
        ";
            }
            // line 11
            echo "          >
          <img
                class=\"pl-image\"
                src=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["grid"] ?? null), "data", []), "packlinkLogo", []), "html", null, true);
            echo "\"
                alt=\"";
            // line 15
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Packlink order draft", [], "Modules.Packlink.Admin"), "html", null, true);
            echo "\"
        >
            <span>";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("View on Packlink", [], "Modules.Packlink.Admin"), "html", null, true);
            echo "</span>
            </a>
        ";
        } elseif (($this->getAttribute(        // line 19
($context["record"] ?? null), "draftStatus", []) == "queued")) {
            // line 20
            echo "            <span
                    class=\"pl-draft-in-progress\"
                    data-order-id=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute(($context["record"] ?? null), "id_order", []), "html", null, true);
            echo "\"
                    data-draft-status-url=\"{html_entity_decode(\$draftStatusUrl|escape:'html':'UTF-8')}\"
            >
              ";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Draft is currently being created.", [], "Modules.Packlink.Admin"), "html", null, true);
            echo "
          </span>
        ";
        } else {
            // line 28
            echo "            <a
                    class=\"btn btn-default pl-create-draft-button\"
                    data-order-id=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute(($context["record"] ?? null), "id_order", []), "html", null, true);
            echo "\"
            >
              <img
                      class=\"pl-image\"
                      src=\"";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["grid"] ?? null), "data", []), "packlinkLogo", []), "html", null, true);
            echo "\"
                      alt=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Packlink order draft", [], "Modules.Packlink.Admin"), "html", null, true);
            echo "\"
              >
              <span>";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Send with Packlink", [], "Modules.Packlink.Admin"), "html", null, true);
            echo "</span>
          </a>
        ";
        }
        // line 40
        echo "    </span>
</span>";
    }

    public function getTemplateName()
    {
        return "@PrestaShop/Admin/Common/Grid/Columns/Content/order_packlink_draft_action.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 40,  111 => 37,  106 => 35,  102 => 34,  95 => 30,  91 => 28,  85 => 25,  79 => 22,  75 => 20,  73 => 19,  68 => 17,  63 => 15,  59 => 14,  54 => 11,  48 => 8,  45 => 7,  41 => 6,  39 => 5,  36 => 4,  34 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "@PrestaShop/Admin/Common/Grid/Columns/Content/order_packlink_draft_action.html.twig", "/Applications/MAMP/htdocs/prestashop_1/modules/packlink/views/PrestaShop/Admin/Common/Grid/Columns/Content/order_packlink_draft_action.html.twig");
    }
}
