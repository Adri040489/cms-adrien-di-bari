<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @PrestaShop/Admin/Common/Grid/Columns/Content/order_packlink_label_action.html.twig */
class __TwigTemplate_bef8a929aca110bea9656ff9e3385fc07aad281fb734fefdcfd03bc8b951ef6a extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if ($this->getAttribute(($context["record"] ?? null), "isLabelAvailable", [])) {
            // line 2
            echo "    <a class=\"btn btn-default pl-label-button\"
       href=\"\"
       data-order=\"";
            // line 4
            echo twig_escape_filter($this->env, $this->getAttribute(($context["record"] ?? null), "id_order", []), "html", null, true);
            echo "\"
       onclick=\"plPrintLabelOnOrdersPage(this)\"
       ";
            // line 6
            if ($this->getAttribute(($context["record"] ?? null), "isLabelPrinted", [])) {
                // line 7
                echo "       style=\"color: #c3c3c3\"
       ";
            }
            // line 9
            echo "    >
        <i class=\"material-icons\">label</i>
        <span>
        ";
            // line 12
            if ($this->getAttribute(($context["record"] ?? null), "isLabelPrinted", [])) {
                // line 13
                echo "            ";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Printed", [], "Modules.Packlink.Admin"), "html", null, true);
                echo "
        ";
            } else {
                // line 15
                echo "            ";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Print", [], "Modules.Packlink.Admin"), "html", null, true);
                echo "
        ";
            }
            // line 17
            echo "        </span>
    </a>
";
        } else {
            // line 20
            echo "    <div class=\"pl-no-label\">
        <span>-</span>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@PrestaShop/Admin/Common/Grid/Columns/Content/order_packlink_label_action.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 20,  66 => 17,  60 => 15,  54 => 13,  52 => 12,  47 => 9,  43 => 7,  41 => 6,  36 => 4,  32 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "@PrestaShop/Admin/Common/Grid/Columns/Content/order_packlink_label_action.html.twig", "/Applications/MAMP/htdocs/prestashop_1/modules/packlink/views/PrestaShop/Admin/Common/Grid/Columns/Content/order_packlink_label_action.html.twig");
    }
}
