<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Modules/packlink/views/templates/admin/packlink_shipping_content/shipping_content.html.twig */
class __TwigTemplate_015cf37826d8047a1dfe693f21d498288e16e5498646a5229de89f715f5328ff extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"tab-pane d-print-block fade\" id=\"packlinkShippingTabContent\" role=\"tabpanel\" aria-labelledby=\"packlinkShippingTab\">
    <p id=\"pl-label-printed\" hidden>";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Printed", [], "Modules.Packlink.Admin"), "html", null, true);
        echo "</p>
    <p id=\"pl-label-ready\" hidden>";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Ready", [], "Modules.Packlink.Admin"), "html", null, true);
        echo "</p>
    <p id=\"pl-print-labels-url\" hidden>
        ";
        // line 5
        echo twig_escape_filter($this->env, ($context["printLabelsUrl"] ?? null), "html", null, true);
        echo "
    </p>
    <div class=\"tab-pane\" id=\"packlink-shipping\">
        ";
        // line 8
        if (( !twig_test_empty(($context["shipping"] ?? null)) && ($this->getAttribute(($context["shipping"] ?? null), "reference", []) != ""))) {
            // line 9
            echo "            ";
            if (($context["isLabelAvailable"] ?? null)) {
                // line 10
                echo "                <h4>";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Shipment labels", [], "Modules.Packlink.Admin"), "html", null, true);
                echo "</h4>
                <div class=\"table-responsive\">
                    <table class=\"table\">
                        <thead>
                        <tr>
                            <th>
                                <span class=\"title_box \">";
                // line 16
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Date", [], "Modules.Packlink.Admin"), "html", null, true);
                echo "</span>
                            </th>
                            <th>
                                <span class=\"title_box \">";
                // line 19
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Number", [], "Modules.Packlink.Admin"), "html", null, true);
                echo "</span>
                            </th>
                            <th>
                                <span class=\"title_box \">";
                // line 22
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Status", [], "Modules.Packlink.Admin"), "html", null, true);
                echo "</span>
                            </th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>";
                // line 29
                echo twig_escape_filter($this->env, ($context["date"] ?? null), "html", null, true);
                echo "</td>
                            <td>
                                <a
                                        href=\"\"
                                        title=\"";
                // line 33
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Print", [], "Modules.Packlink.Admin"), "html", null, true);
                echo "\"
                                        data-order=\"";
                // line 34
                echo twig_escape_filter($this->env, ($context["orderId"] ?? null), "html", null, true);
                echo "\"
                                        data-label-printed=\"";
                // line 35
                echo twig_escape_filter($this->env, ($context["isLabelPrinted"] ?? null), "html", null, true);
                echo "\"
                                        onclick=\"plPrintLabelOnOrderDetailsPage(this)\">
                                    ";
                // line 37
                echo twig_escape_filter($this->env, ($context["number"] ?? null), "html", null, true);
                echo "
                                </a>
                            </td>
                            <td>";
                // line 40
                echo twig_escape_filter($this->env, ($context["status"] ?? null), "html", null, true);
                echo "</td>
                            <td class=\"text-right\">
                                <a class=\"btn btn-default\"
                                   href=\"\"
                                   title=\"";
                // line 44
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Print", [], "Modules.Packlink.Admin"), "html", null, true);
                echo "\"
                                   data-order=\"";
                // line 45
                echo twig_escape_filter($this->env, ($context["orderId"] ?? null), "html", null, true);
                echo "\"
                                   data-label-printed=\"";
                // line 46
                echo twig_escape_filter($this->env, ($context["isLabelPrinted"] ?? null), "html", null, true);
                echo "\"
                                   onclick=\"plPrintLabelOnOrderDetailsPage(this)\">
                                    <i class=\"material-icons\">print</i>
                                    ";
                // line 49
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Print label", [], "Modules.Packlink.Admin"), "html", null, true);
                echo "
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            ";
            }
            // line 57
            echo "            ";
            if (($this->getAttribute(($context["shipping"] ?? null), "name", []) != "")) {
                // line 58
                echo "                <h4 style=\"margin-top: 15px;\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Shipment details", [], "Modules.Packlink.Admin"), "html", null, true);
                echo "</h4>
                <table class=\"table\" id=\"shipping_table\">
                    <thead>
                    <tr>
                        <th>
                            <span class=\"title_box\">";
                // line 63
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Carrier logo", [], "Modules.Packlink.Admin"), "html", null, true);
                echo "</span>
                        </th>
                        <th>
                            <span class=\"title_box\">";
                // line 66
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Carrier", [], "Modules.Packlink.Admin"), "html", null, true);
                echo "</span>
                        </th>
                        <th>
                            <span class=\"title_box\">";
                // line 69
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Carrier tracking numbers", [], "Modules.Packlink.Admin"), "html", null, true);
                echo "</span>
                        </th>
                        <th>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            ";
                // line 78
                if (($this->getAttribute(($context["shipping"] ?? null), "icon", []) != "")) {
                    // line 79
                    echo "                                <img
                                        src=\"";
                    // line 80
                    echo twig_escape_filter($this->env, $this->getAttribute(($context["shipping"] ?? null), "icon", []), "html", null, true);
                    echo "\"
                                        alt=\"";
                    // line 81
                    echo twig_escape_filter($this->env, $this->getAttribute(($context["shipping"] ?? null), "name", []), "html", null, true);
                    echo "\"
                                        style=\"width: 85px;\"
                                />
                            ";
                }
                // line 85
                echo "                        </td>
                        <td>
                            ";
                // line 87
                echo twig_escape_filter($this->env, $this->getAttribute(($context["shipping"] ?? null), "name", []), "html", null, true);
                echo "
                        </td>
                        <td>
                            ";
                // line 90
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["shipping"] ?? null), "carrier_tracking_numbers", []));
                foreach ($context['_seq'] as $context["key"] => $context["tracking_number"]) {
                    // line 91
                    echo "                                ";
                    echo twig_escape_filter($this->env, $context["tracking_number"], "html", null, true);
                    echo "
                                ";
                    // line 92
                    if (($context["key"] != (twig_length_filter($this->env, $this->getAttribute(($context["shipping"] ?? null), "carrier_tracking_numbers", [])) - 1))) {
                        echo ", ";
                    }
                    // line 93
                    echo "                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['tracking_number'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 94
                echo "                        </td>
                        <td style=\"text-align: right;\">
                            ";
                // line 96
                if ( !twig_test_empty($this->getAttribute(($context["shipping"] ?? null), "carrier_tracking_numbers", []))) {
                    // line 97
                    echo "                                <a
                                        class=\"btn btn-default\"
                                        href=\"";
                    // line 99
                    echo twig_escape_filter($this->env, $this->getAttribute(($context["shipping"] ?? null), "carrier_tracking_url", []), "html", null, true);
                    echo "\"
                                        title=\"";
                    // line 100
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Track it!", [], "Modules.Packlink.Admin"), "html", null, true);
                    echo "\"
                                        target=\"_blank\"
                                >
                                    ";
                    // line 103
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Track it!", [], "Modules.Packlink.Admin"), "html", null, true);
                    echo "
                                </a>
                            ";
                }
                // line 106
                echo "                        </td>
                    </tr>
                    </tbody>
                </table>
            ";
            }
            // line 111
            echo "            <dl style=\"margin-top: 15px;\">
                ";
            // line 112
            if ( !twig_test_empty($this->getAttribute(($context["shipping"] ?? null), "status", []))) {
                // line 113
                echo "                    <dt>";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Status", [], "Modules.Packlink.Admin"), "html", null, true);
                echo "</dt>
                    <dd style=\"margin-bottom: 10px\">
                <span style=\"color: grey\">
                    <i class=\"material-icons\">calendar_today</i> ";
                // line 116
                echo twig_escape_filter($this->env, $this->getAttribute(($context["shipping"] ?? null), "time", []), "html", null, true);
                echo "
                </span> - <b>";
                // line 117
                echo twig_escape_filter($this->env, $this->getAttribute(($context["shipping"] ?? null), "status", []), "html", null, true);
                echo "</b>
                    </dd>
                ";
            }
            // line 120
            echo "                ";
            if ( !twig_test_empty($this->getAttribute(($context["shipping"] ?? null), "reference", []))) {
                // line 121
                echo "                    <dt>";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Packlink reference number", [], "Modules.Packlink.Admin"), "html", null, true);
                echo "</dt>
                    <dd style=\"margin-bottom: 10px\">";
                // line 122
                echo twig_escape_filter($this->env, $this->getAttribute(($context["shipping"] ?? null), "reference", []), "html", null, true);
                echo "</dd>
                ";
            }
            // line 124
            echo "                ";
            if ( !twig_test_empty($this->getAttribute(($context["shipping"] ?? null), "packlink_shipping_price", []))) {
                // line 125
                echo "                    <dt>";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Packlink shipping price", [], "Modules.Packlink.Admin"), "html", null, true);
                echo "</dt>
                    <dd style=\"margin-bottom: 10px\">";
                // line 126
                echo twig_escape_filter($this->env, $this->getAttribute(($context["shipping"] ?? null), "packlink_shipping_price", []), "html", null, true);
                echo "</dd>
                ";
            }
            // line 128
            echo "            </dl>
            ";
            // line 129
            if (( !twig_test_empty($this->getAttribute(($context["shipping"] ?? null), "link", [])) &&  !$this->getAttribute(($context["shipping"] ?? null), "deleted", []))) {
                // line 130
                echo "                <a
                        class=\"btn btn-default\"
                        href=\"";
                // line 132
                echo twig_escape_filter($this->env, $this->getAttribute(($context["shipping"] ?? null), "link", []), "html", null, true);
                echo "\"
                        title=\"";
                // line 133
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("View on Packlink PRO", [], "Modules.Packlink.Admin"), "html", null, true);
                echo "\"
                        target=\"_blank\"
                >
                    <i class=\"material-icons\">remove_red_eye</i>
                    ";
                // line 137
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("View on Packlink PRO", [], "Modules.Packlink.Admin"), "html", null, true);
                echo "
                </a>
            ";
            }
            // line 140
            echo "        ";
        } else {
            // line 141
            echo "            <div class=\"table-responsive\">
                <table class=\"table\">
                    <tbody>
                    <tr>
                        <td style=\"border:none; width: 180px;\">
                            <img alt=\"";
            // line 146
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Packlink PRO Shipping", [], "Modules.Packlink.Admin"), "html", null, true);
            echo "\"
                                 src=\"";
            // line 147
            echo twig_escape_filter($this->env, ($context["pluginBasePath"] ?? null), "html", null, true);
            echo "views/img/logo-pl.svg\"
                                 width=\"150px;\"
                            >
                        </td>
                        <td style=\"border:none;\">
              <span style=\"font-weight: normal;\">
                  ";
            // line 153
            echo twig_escape_filter($this->env, ($context["message"] ?? null), "html", null, true);
            echo "
              </span>
                        </td>
                        ";
            // line 156
            if (($context["displayDraftButton"] ?? null)) {
                // line 157
                echo "                            <td style=\"border:none;text-align:right;\">
                                <button
                                        type=\"button\"
                                        class=\"btn btn-default\"
                                        data-order=\"";
                // line 161
                echo twig_escape_filter($this->env, ($context["orderId"] ?? null), "html", null, true);
                echo "\"
                                        data-create-draft-url=\"";
                // line 162
                echo twig_escape_filter($this->env, ($context["createDraftUrl"] ?? null), "html", null, true);
                echo "\"
                                        onclick=\"plCreateOrderDraft(this)\"
                                >
                                    <i class=\"material-icons\">add_circle</i> ";
                // line 165
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Create Draft", [], "Modules.Packlink.Admin"), "html", null, true);
                echo "
                                </button>
                            </td>
                        ";
            }
            // line 169
            echo "                    </tr>
                    </tbody>
                </table>
            </div>
        ";
        }
        // line 174
        echo "    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "@Modules/packlink/views/templates/admin/packlink_shipping_content/shipping_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  392 => 174,  385 => 169,  378 => 165,  372 => 162,  368 => 161,  362 => 157,  360 => 156,  354 => 153,  345 => 147,  341 => 146,  334 => 141,  331 => 140,  325 => 137,  318 => 133,  314 => 132,  310 => 130,  308 => 129,  305 => 128,  300 => 126,  295 => 125,  292 => 124,  287 => 122,  282 => 121,  279 => 120,  273 => 117,  269 => 116,  262 => 113,  260 => 112,  257 => 111,  250 => 106,  244 => 103,  238 => 100,  234 => 99,  230 => 97,  228 => 96,  224 => 94,  218 => 93,  214 => 92,  209 => 91,  205 => 90,  199 => 87,  195 => 85,  188 => 81,  184 => 80,  181 => 79,  179 => 78,  167 => 69,  161 => 66,  155 => 63,  146 => 58,  143 => 57,  132 => 49,  126 => 46,  122 => 45,  118 => 44,  111 => 40,  105 => 37,  100 => 35,  96 => 34,  92 => 33,  85 => 29,  75 => 22,  69 => 19,  63 => 16,  53 => 10,  50 => 9,  48 => 8,  42 => 5,  37 => 3,  33 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "@Modules/packlink/views/templates/admin/packlink_shipping_content/shipping_content.html.twig", "/Applications/MAMP/htdocs/prestashop_1/modules/packlink/views/templates/admin/packlink_shipping_content/shipping_content.html.twig");
    }
}
