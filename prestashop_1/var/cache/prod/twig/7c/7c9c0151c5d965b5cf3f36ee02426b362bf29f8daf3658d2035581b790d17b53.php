<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @PrestaShop/Admin/Common/Grid/grid.html.twig */
class __TwigTemplate_623af2b65849590105ad72a49cb2c277a5e713dc46de0b5e2b831a5ae98b6162 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'grid_extra_content' => [$this, 'block_grid_extra_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "PrestaShopBundle:Admin/Common/Grid:grid.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("PrestaShopBundle:Admin/Common/Grid:grid.html.twig", "@PrestaShop/Admin/Common/Grid/grid.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_grid_extra_content($context, array $blocks = [])
    {
        // line 4
        echo "    ";
        if ($this->getAttribute($this->getAttribute(($context["grid"] ?? null), "data", [], "any", false, true), "createDraftUrl", [], "any", true, true)) {
            // line 5
            echo "        <input type=\"hidden\" class=\"pl-create-endpoint\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["grid"] ?? null), "data", []), "createDraftUrl", []), "html", null, true);
            echo "\"/>
        <input type=\"hidden\" class=\"pl-draft-status\" value=\"";
            // line 6
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["grid"] ?? null), "data", []), "draftStatusUrl", []), "html", null, true);
            echo "\"/>
        <p id=\"pl-label-printed\" hidden>";
            // line 7
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Printed", [], "Modules.Packlink.Admin"), "html", null, true);
            echo "</p>
        <p id=\"pl-label-ready\" hidden>";
            // line 8
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Ready", [], "Modules.Packlink.Admin"), "html", null, true);
            echo "</p>
        <p id=\"pl-disable-popup\" hidden>
            ";
            // line 10
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Please disable pop-up blocker on this page in order to bulk open shipment labels", [], "Modules.Packlink.Admin"), "html", null, true);
            echo "
        </p>
        <p id=\"pl-print-labels-url\" hidden>
            ";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["grid"] ?? null), "data", []), "printLabelsUrl", []), "html", null, true);
            echo "
        </p>
        <input type=\"hidden\" class=\"pl-draft-in-progress-message\"
               value=\"";
            // line 16
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Draft is currently being created.", [], "Modules.Packlink.Admin"), "html", null, true);
            echo "\"/>
        <input type=\"hidden\" class=\"pl-draft-failed-message\"
               value=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Previous attempt to create a draft failed.", [], "Modules.Packlink.Admin"), "html", null, true);
            echo "\"/>

        <a class=\"btn btn-default pl-create-draft-button pl-create-draft-template\">
            <img
                    class=\"pl-image\"
                    src=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["grid"] ?? null), "data", []), "packlinkLogo", []), "html", null, true);
            echo "\"
                    alt=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Packlink order draft", [], "Modules.Packlink.Admin"), "html", null, true);
            echo "\"
            >
            <span></span>
        </a>

        <a class=\"btn btn-default pl-draft-button pl-draft-button-template\" target=\"_blank\">
            <img
                    class=\"pl-image\"
                    src=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["grid"] ?? null), "data", []), "packlinkLogo", []), "html", null, true);
            echo "\"
                    alt=\"";
            // line 33
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Packlink order draft", [], "Modules.Packlink.Admin"), "html", null, true);
            echo "\"
            >
            <span>";
            // line 35
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("View on Packlink", [], "Modules.Packlink.Admin"), "html", null, true);
            echo "</span>
        </a>
    ";
        }
    }

    public function getTemplateName()
    {
        return "@PrestaShop/Admin/Common/Grid/grid.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 35,  107 => 33,  103 => 32,  92 => 24,  88 => 23,  80 => 18,  75 => 16,  69 => 13,  63 => 10,  58 => 8,  54 => 7,  50 => 6,  45 => 5,  42 => 4,  39 => 3,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "@PrestaShop/Admin/Common/Grid/grid.html.twig", "/Applications/MAMP/htdocs/prestashop_1/modules/packlink/views/PrestaShop/Admin/Common/Grid/grid.html.twig");
    }
}
