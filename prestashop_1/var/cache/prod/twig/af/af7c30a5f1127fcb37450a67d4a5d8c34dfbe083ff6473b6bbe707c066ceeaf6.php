<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Modules/packlink/views/templates/admin/packlink_shipping_tab/shipping_tab.html.twig */
class __TwigTemplate_1e4ce32f54d6bc745aa22777b4316b5ccbc0e357d1d6f72cb321275309098a36 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<li class=\"nav-item\">
    <a class=\"nav-link\" id=\"packlinkShippingTab\" data-toggle=\"tab\" href=\"#packlinkShippingTabContent\">
        <i class=\"material-icons\">local_shipping</i>
        ";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Packlink Shipping", [], "Modules.Packlink.Admin"), "html", null, true);
        echo "
    </a>
</li>";
    }

    public function getTemplateName()
    {
        return "@Modules/packlink/views/templates/admin/packlink_shipping_tab/shipping_tab.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 4,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "@Modules/packlink/views/templates/admin/packlink_shipping_tab/shipping_tab.html.twig", "/Applications/MAMP/htdocs/prestashop_1/modules/packlink/views/templates/admin/packlink_shipping_tab/shipping_tab.html.twig");
    }
}
