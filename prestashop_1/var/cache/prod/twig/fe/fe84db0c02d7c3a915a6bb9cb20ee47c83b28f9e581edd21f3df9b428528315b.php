<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__0ee435a3c775a7835c84827545f0c52ae5cf2542924def372249f95165e6e364 */
class __TwigTemplate_de59f765c3e63f2631bd71683b90298edef88e42f9e6ca19e20b8c03796c56d6 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'extra_stylesheets' => [$this, 'block_extra_stylesheets'],
            'content_header' => [$this, 'block_content_header'],
            'content' => [$this, 'block_content'],
            'content_footer' => [$this, 'block_content_footer'],
            'sidebar_right' => [$this, 'block_sidebar_right'],
            'javascripts' => [$this, 'block_javascripts'],
            'extra_javascripts' => [$this, 'block_extra_javascripts'],
            'translate_javascripts' => [$this, 'block_translate_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"fr\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/prestashop_1/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/prestashop_1/img/app_icon.png\" />

<title>Modifier • HypeLuxury</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminAddresses';
    var iso_user = 'fr';
    var lang_is_rtl = '0';
    var full_language_code = 'fr';
    var full_cldr_language_code = 'fr-FR';
    var country_iso_code = 'BE';
    var _PS_VERSION_ = '1.7.8.1';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'Une nouvelle commande a été passée sur votre boutique.';
    var order_number_msg = 'Numéro de commande : ';
    var total_msg = 'Total : ';
    var from_msg = 'Du ';
    var see_order_msg = 'Afficher cette commande';
    var new_customer_msg = 'Un nouveau client s\\'est inscrit sur votre boutique';
    var customer_name_msg = 'Nom du client : ';
    var new_msg = 'Un nouveau message a été posté sur votre boutique.';
    var see_msg = 'Lire le message';
    var token = 'a386685e29e007881362c3ff6b1620ae';
    var token_admin_orders = tokenAdminOrders = '5dc5ac9193ec2fc5b8cbce8ad67901f4';
    var token_admin_customers = '6bbd9dd098ff1b86233ac4d5ead43a88';
    var token_admin_customer_threads = tokenAdminCustomerThreads = '813ef1cfe7894aae7bd2bf328c22c6e2';
    var currentIndex = 'index.php?controller=AdminAddresses';
    var employee_token = '8d629afd70221281dcc1df76a8dc7a18';
    var choose_language_translate = 'Choisissez la langue :';
    var default_language = '1';
    var admin_modules_link = '/prestashop_1/admin951g8xvjh/index.php/improve/modules/catalog/recommended?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo';
    var admin_notification_get_link = '/prestashop_1/admin951g8xvjh/index.php/common/notifications?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo';
    var admin_notification_push_link = adminNotificationPushLink = '/prestashop_1/admin951g8xvjh/index.php/common/notifications/ack?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo';
    var tab_modules_list = '';
    var update_success_msg = 'Mise à jour réussie';
    var errorLogin = 'PrestaShop n\\'a pas pu se connecter à Addons. Veuillez vérifier vos identifiants et votre connexion Internet.';
    var search_product_msg = 'Rechercher un produit';
  </script>

      <link href=\"/prestashop_1/admin951g8xvjh/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/js/jquery/plugins/fancybox/jquery.fancybox.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/modules/blockwishlist/public/backoffice.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/admin951g8xvjh/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/modules/gamification/views/css/gamification.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/prestashop_1/modules/payplug/views/css/admin-v3.4.0.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/prestashop_1\\/admin951g8xvjh\\/\";
var baseDir = \"\\/prestashop_1\\/\";
var changeFormLanguageUrl = \"\\/prestashop_1\\/admin951g8xvjh\\/index.php\\/configure\\/advanced\\/employees\\/change-form-language?_token=r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\";
var currency = {\"iso_code\":\"EUR\",\"sign\":\"\\u20ac\",\"name\":\"Euro\",\"format\":null};
var currency_specifications = {\"symbol\":[\",\",\"\\u202f\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"currencyCode\":\"EUR\",\"currencySymbol\":\"\\u20ac\",\"numberSymbols\":[\",\",\"\\u202f\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"positivePattern\":\"#,##0.00\\u00a0\\u00a4\",\"negativePattern\":\"-#,##0.00\\u00a0\\u00a4\",\"maxFractionDigits\":2,\"minFractionDigits\":2,\"groupingUsed\":true,\"primaryGroupSize\":3,\"secondaryGroupSize\":3};
var host_mode = false;
var number_specifications = {\"symbol\":[\",\",\"\\u202f\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"numberSymbols\":[\",\",\"\\u202f\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"positivePattern\":\"#,##0.###\",\"negativePattern\":\"-#,##0.###\",\"maxFractionDigits\":3,\"minFractionDigits\":0,\"groupingUsed\":true,\"primaryGroupSize\":3,\"secondaryGroupSize\":3};
var prestashop = {\"debug\":false};
var show_new_customers = \"1\";
var show_new_messages = \"1\";
var show_new_orders = \"1\";
</script>
<script type=\"text/javascript\" src=\"/prestashop_1/admin951g8xvjh/themes/new-theme/public/main.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/js/jquery/plugins/fancybox/jquery.fancybox.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/js/admin.js?v=1.7.8.1\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/admin951g8xvjh/themes/new-theme/public/cldr.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/js/tools.js?v=1.7.8.1\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/admin951g8xvjh/themes/default/js/vendor/nv.d3.min.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/gamification/views/js/gamification_bt.js\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/ps_mbo/views/js/recommended-modules.js?v=2.0.1\"></script>
<script type=\"text/javascript\" src=\"/prestashop_1/modules/payplug/views/js/admin-v3.4.0.js\"></script>


";
        // line 84
        $this->displayBlock('stylesheets', $context, $blocks);
        $this->displayBlock('extra_stylesheets', $context, $blocks);
        echo "</head>

<body
  class=\"lang-fr adminaddresses\"
  data-base-url=\"/prestashop_1/admin951g8xvjh/index.php\"  data-token=\"r2POoQj___FkklYQB9paocl-7VnR_8h0s0WbTwL1Ivo\">



<div id=\"main-div\">
                
      <div class=\" -notoolbar \">

        

        
        <div class=\"row \">
          <div class=\"col-sm-12\">
            <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>


  ";
        // line 104
        $this->displayBlock('content_header', $context, $blocks);
        // line 105
        echo "                 ";
        $this->displayBlock('content', $context, $blocks);
        // line 106
        echo "                 ";
        $this->displayBlock('content_footer', $context, $blocks);
        // line 107
        echo "                 ";
        $this->displayBlock('sidebar_right', $context, $blocks);
        // line 108
        echo "
            
          </div>
        </div>

      </div>
    </div>




";
        // line 119
        $this->displayBlock('javascripts', $context, $blocks);
        $this->displayBlock('extra_javascripts', $context, $blocks);
        $this->displayBlock('translate_javascripts', $context, $blocks);
        echo "</body>
</html>";
    }

    // line 84
    public function block_stylesheets($context, array $blocks = [])
    {
    }

    public function block_extra_stylesheets($context, array $blocks = [])
    {
    }

    // line 104
    public function block_content_header($context, array $blocks = [])
    {
    }

    // line 105
    public function block_content($context, array $blocks = [])
    {
    }

    // line 106
    public function block_content_footer($context, array $blocks = [])
    {
    }

    // line 107
    public function block_sidebar_right($context, array $blocks = [])
    {
    }

    // line 119
    public function block_javascripts($context, array $blocks = [])
    {
    }

    public function block_extra_javascripts($context, array $blocks = [])
    {
    }

    public function block_translate_javascripts($context, array $blocks = [])
    {
    }

    public function getTemplateName()
    {
        return "__string_template__0ee435a3c775a7835c84827545f0c52ae5cf2542924def372249f95165e6e364";
    }

    public function getDebugInfo()
    {
        return array (  209 => 119,  204 => 107,  199 => 106,  194 => 105,  189 => 104,  180 => 84,  172 => 119,  159 => 108,  156 => 107,  153 => 106,  150 => 105,  148 => 104,  124 => 84,  39 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__0ee435a3c775a7835c84827545f0c52ae5cf2542924def372249f95165e6e364", "");
    }
}
